<?php
   
    if (!$_SESSION['autentic']) {
        header('Location:../../php_cerrar.php');
    } else {
        $IdUsuariologin=$_SESSION['id'];
        $useradministrador=$_SESSION['administrador']; // valor: true - si es superadministrador de desarrollo
        require_once("../conn_BD.php");
        require_once("../funciones.php");
        require_once("../usuarios/class/ClassUsuario.php");
        $InstanciaDB=new Conexion();
        $InstUsuarios=new Proceso_Usuario($InstanciaDB);
        $ListaRecursos=$InstUsuarios->Listarrecursos();
?>
<!-- /. NAV TOP  -->
    <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">
                <li>
                    <a class="active-menu" href="../principal/principal.php"><i class="fa fa-home fa-3x"></i> Inicio</a>
                </li>
                <li>
                    <a href=""><i class="fa fa-user fa-3x"></i> Aspectos Iniciales</a>
                    <ul class="nav nav-second-level">
                        <?php
                            // colocar_permiso($IdUsuario,$accion,$IdRecurso)
                            // $IdUsuario: $IdUsuariologin=$_SESSION['id'];
                            // $accion: 1-Consultar, 2-Editar,3-Insertar,4-Eliminar,5-Listar
                            // $IdRecurso: Cod del recurso - Columna: CodRecurso 

                            $Permisos_Listar=colocar_permiso($IdUsuariologin,5,2); 
                            if ($Permisos_Listar== 1 || $useradministrador) { ?>
                            <li>
                                <a href="../actividades/indexActividades.php"> Actividades</a>                         
                            </li>
                        <?php } ?>
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,3); //$Accion: 1-Consultar, 2-Editar,3-Insertar,4-Eliminar,5-Listar
                            if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                                <a href="../alumnos/indexAlumnos.php"> Alumnos</a>                          
                            </li>
                        <?php } ?>
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,4);
                            if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                                <a href="../area/indexArea.php"> Area</a>                         
                            </li>
                        <?php } ?>
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,12);
                            if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                                <a href="../centro_costos/indexCentroCostosP.php"> Centro de Costos Padre</a>
                            </li>
                        <?php } ?>
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,13);
                            if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                                <a href="../centro_costos/indexCentroCostosH.php"> Centro de Costos Hijo</a>
                            </li>
                        <?php } ?>                            
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,14);
                        if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                                <a href="../clasificacionc/indexClasificacionC.php"> Clasificacion Contactos</a>
                            </li>
                        <?php } ?>                                                       
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,16);
                        if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                                <a href="../conceptos_gastos/indexConceptoGastos.php"> Conceptos Gastos</a>
                            </li>
                        <?php } ?>                           
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,5);
                        if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                                <a href="../contacto/indexContacto.php"> Contactos</a>
                            </li>
                        <?php } ?>                            
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,17);
                        if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                                <a href="../departamentos/indexDepartamentos.php"> Departamentos</a>
                            </li>
                        <?php } ?>                           
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,19);
                        if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                                <a href="../dotaciones/indexDotaciones.php"> Elementos Dotacion</a>
                            </li>
                        <?php } ?>                           
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,20);
                        if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                                <a href="../empleados/indexEmpleados.php"> Empleados</a>
                            </li>
                        <?php } ?>                           
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,21);
                        if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                                <a href="../entidades/indexEntidades.php"> Entidades Vinculadas</a>
                            </li>
                        <?php } ?>                           
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,22);
                        if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                                <a href="../fuentes/indexFuentes.php"> Fuentes Abastecimientos</a>
                            </li>
                        <?php } ?>                          
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,23);
                        if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                                <a href="../institucion/indexInstitucion.php"> Instituciones</a>
                            </li>
                        <?php } ?>                           
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,24);
                        if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                                <a href="../materialesR/indexMaterialesR.php"> Materiales</a>
                            </li>
                        <?php } ?>                          
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,18);
                        if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                                <a href="../municipios/indexMunicipios.php"> Municipios</a>
                            </li>
                        <?php } ?>	                            
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,9);
                        if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                                <a href="../plan_cuentas/indexPlanCuentas.php"> Plan Cuentas</a>  
                            </li>
                        <?php } ?>                           
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,25);
                        if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                                <a href="../procesos/indexProcesos.php"> Procesos</a>
                            </li>
                        <?php } ?>	                                               
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,8);
                        if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                                <a href='../programas/indexProgramas.php'> Programas</a>
                            </li>
                        <?php } ?>                           
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,26);
                        if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                                <a href="../proyectos/indexProyecto.php"> Proyectos</a>
                            </li>
                        <?php } ?>                           
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,27);
                        if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                                <a href="../regiones/indexRegiones.php"> Regiones</a>
                            </li>
                        <?php } ?>                                                       
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,28);
                        if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                                <a href="../tipo_municipios/indexTipoMunicipios.php"> Tipo de Municipios</a>
                            </li>
                        <?php } ?>
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,29);
                        if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                                <a href="../tipo_encuestas/indexTipoEncuestasInf.php"> Tipo Material Encuesta Infraestructura</a>
                            </li>
                        <?php } ?>                           
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,30);
                        if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>                        
                                <a href="../tipo_gastos/indexTipodeGastos.php"> Tipo Gastos</a>
                            </li>
                        <?php } ?>                           
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,31);
                        if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                                <a href="../tipo_instituciones/indexTipoInstitucion.php"> Tipo Instituciones</a>
                            </li>
                        <?php } ?>	                           
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,32);
                        if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                                <a href="../tipo_materiales/indexTipoMaterial.php"> Tipo Materiales</a>
                            </li>
                        <?php } ?>                           
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,33);
                        if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                                <a href="../tipo_novedades/indexTipoNovedadesMaterial.php"> Tipo Novedades Materiales</a>
                            </li>
                        <?php } ?>                                                                        
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,34);
                        if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                                <a href="../tipo_novedades/indexTipoNovedadesPlan.php"> Tipo Novedades Planeacion</a>
                            </li>
                        <?php } ?>                           
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,35);
                        if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                                <a href="../tipo_poblacion/indexTipoPoblacion.php"> Tipo Poblacion</a>
                            </li>
                        <?php } ?>                           
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,36);
                        if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                                <a href="../tipo_talleres/indexTipoTalleres.php"> Tipo Talleres</a>
                            </li>
                        <?php } ?>	                                                                           
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,38);
                        if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                                <a href="../veredas/indexVeredas.php"> Veredas</a>
                            </li>
                        <?php } ?>		  																
                    </ul>
                </li>
                    <li>
                    <a href=""><i class="fa fa-edit fa-3x"></i> Procesos</a>
                    <ul class="nav nav-second-level">                        
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,50);
                          if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                                <a href="../solicitud_gastos/indexSolicitudGastos.php"> Solicitud Gastos de Viaje</a>
                            </li>
                        <?php } ?> 
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,44);
                          if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                                <a href="../aprobacionSolicGasto/indexaprobacionSolicGasto.php"> Aprobar Gastos</a>
                            </li>
                        <?php } ?>
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,39);
                          if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                            <a href="../anticipos/indexAnticipos.php"> Anticipos a Solicitudes aprobadas</a>
                            </li>
                        <?php } ?>                     
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,46);
                         if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                                <a href="../legalizaciongastos/indexLegalizacionGastos.php"> Legalizar Gastos</a>
                            </li>
                        <?php } ?>  
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,43);
                         if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                                <a href="../asistCapacInstalacion/indexAsistenciaCapacInstalacion.php"> Asistencia a Capacitaciones</a>
                            </li>
                        <?php } ?>	
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,45);
                         if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                                <a href="../informeInstalacion/indexInformeInstalacion.php"> Informe Instalacion</a>
                            </li>
                        <?php } ?>	                                             
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,49);
                         if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                                <a href="../requisicion_materiales/indexRequisicionMateriales.php"> Solicitud de Materiales</a>
                            </li>
                        <?php } ?>	   
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,47);
                         if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                                <a href="../requisicion_materiales/indexReciboMaterial.php"> Recibo Materiales</a>
                            </li>
                        <?php } ?>	                                
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,42);
                         if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                                <a href="../donantes/indexDonantes.php"> Asignacion de Donantes</a>
                            </li>
                        <?php } ?>	
                        </ul>
                </li>
                    <li>
                    <a href="modulos/pacientes/index.php"><i class="fa fa-qrcode fa-3x"></i> Consultas</a>
                    <ul class="nav nav-second-level">
                    <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,39);
                          if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                            <a href="../consulta1/indexSolicGastos.php">Solicitudes de gastos</a>
                            </li>
                        <?php } ?>
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,42);
                           if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                              <a href="../consulta1/indexInfInst.php">Informe instalación</a>
                            </li>
                        <?php } ?>
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,43);
                           if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                              <a href="../consulta1/indexAsistCapacit.php">Asistencia capacitaciones</a>
                            </li>
                        <?php } ?>
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,44);
                           if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                            <a href="../consulta1/indexReqMaterial.php">Requisiciones de materiales</a>
                            </li>
                        <?php } ?>
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,45);
                           if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                            <a href="../consulta1/indexReciboMat.php">Recibo de materiales</a>
                            </li>
                        <?php } ?>
                        <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,46);
                           if ($Permisos_Listar==1 || $useradministrador) { ?>
                            <li>
                            <a href="../consulta1/indexEncInfraest.php">Encuesta de infraestructura</a>
                            </li>
                        <?php } ?>                                    
                    </ul>
                </li>
                    <li>
                    <a href="modulos/pacientes/index.php"><i class="fa fa-bar-chart-o fa-3x"></i> Utilidades</a>
                    <ul class="nav nav-second-level">
                        
                    </ul>
                </li>           
                <li>
                    <a href="#"><i class="fa fa-cog fa-3x"></i> Administración<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,1);
                            if ($Permisos_Listar==1 || $useradministrador) { ?>
                                <li>
                                    <a href="../usuarios/indexUsuarios.php"> Usuarios</a>
                                </li>
                            <?php } ?> 
                            
                            <?php $Permisos_Listar=colocar_permiso($IdUsuariologin,5,11);
                            if ($Permisos_Listar==1 || $useradministrador) { ?>
                                <li>
                                    <a href="../usuarios/permisos.php"> Permisos de Usuario</a>
                                </li>
                            <?php } ?>

                        </ul>
                    </li>                   
            </ul>
        </div>
    </nav>  
    <!-- /. NAV SIDE  -->
<?php }?>