<?php
        
        switch ($TipoCalendario) {
            case 'Fecha':
               echo "
                    <script type='text/javascript'>
                        $('.datetime').datetimepicker({
                        language:'es',
                        format: 'yyyy-m-d',
                        showMeridian: false,
                        autoclose: true,
                        todayBtn: true,
                        minView: 2,
                        maxView: 3,
                        });
                    </script>
               ";
            break;
            case 'FechaHora':
                echo "
                    <script type='text/javascript'>
                        $('.datetime').datetimepicker({
                            language:'es',
                            format: 'yyyy-m-d hh:ii',
                            autoclose: true,
                            todayBtn: true,
                            minView: 0,
                            maxView: 3,
                            minuteStep:30,
                            showMeridian: true
                        });
                    </script>                
               ";
            break;
            default:
            echo "
                    <script type='text/javascript'>
                        $('.datetime').datetimepicker({
                        language:'es',
                        format: 'yyyy-m-d',
                        showMeridian: false,
                        autoclose: true,
                        todayBtn: true,
                        minView: 2,
                        maxView: 3,
                        });
                    </script>
                ";
                break;
        }
     
?>
