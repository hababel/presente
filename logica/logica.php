<?php 
    session_start();
    require_once("../modulos/conn_BD.php");
    require_once("../modulos/usuarios/class/ClassUsuario.php");
    require_once("../modulos/programas/class/ClassProgramas.php");
    require_once("../modulos/tipo_gastos/class/ClassTipodeGasto.php");
    require_once("../modulos/plan_cuentas/class/ClassPlanCuentas.php");
    require_once("../modulos/conceptos_gastos/class/classConceptoGastos.php");
    require_once("../modulos/empleados/class/classEmpleados.php");
    require_once("../modulos/solicitud_gastos/class/ClassSolicitudGastos.php");
    require_once("../modulos/actividades/class/ClassActividades.php");
    require_once("../modulos/departamentos/class/ClassDepartamentos.php");
    require_once("../modulos/tipo_materiales/class/ClassTipoMaterial.php");
    require_once("../modulos/tipo_novedades/class/classTipoNovedadesPlan.php");
    require_once("../modulos/centro_costos/class/classCentrosCostosP.php");
    require_once("../modulos/centro_costos/class/classCentroCostosH.php");
    require_once("../modulos/entidades/class/classEntidades.php");
    require_once('../modulos/proyectos/class/classProyecto.php');
    require_once('../modulos/dotaciones/class/classDotaciones.php');
    require_once('../modulos/fuentes/class/classFuentes.php');
    require_once('../modulos//clasificacionc/class/classClasificacionC.php');
    require_once('../modulos/contacto/class/classContacto.php');
    require_once('../modulos/institucion/class/classInstitucion.php');
    require_once("../modulos/tipo_municipios/class/ClassTipoMunicipios.php");
    require_once("../modulos/regiones/class/classRegiones.php");
    require_once("../modulos/tipo_instituciones/class/ClassTipoInstitucion.php");
    require_once("../modulos/tipo_encuestas/class/ClassTipoEncuestasInf.php");
    require_once("../modulos/procesos/class/ClassProcesos.php");
    require_once("../modulos/tipo_talleres/class/ClassTipoTalleres.php");
    require_once("../modulos/tipo_tarifas/class/ClassTipoTarifa.php");
    require_once("../modulos/veredas/class/classVeredas.php");
    require_once("../modulos/municipios/class/ClassMunicipios.php");
    require_once("../modulos/tipo_novedades/class/classTipoNovedadesMaterial.php");
    require_once("../modulos/legalizaciongastos/class/ClassLegalizacionGastos.php");
    require_once("../modulos/funciones.php");

    if ($_POST) {
        $accion=$_GET['accion'];
        $InstanciaDB=new Conexion();
        $InstUsuario=new Proceso_Usuario($InstanciaDB);
        $InstPrograma=new Proceso_Programa($InstanciaDB);
        $InstTipoGasto=new Proceso_TipoGastos($InstanciaDB);
        $InstPlanCuentas=new Proceso_PlanCuentas($InstanciaDB);
        $InstConceptoGasto=new Proceso_ConceptoGastos($InstanciaDB);
        $InstEmpleados=new Proceso_Empleados($InstanciaDB);
        $InstSolicitudGasto= new Proceso_SolicitudGastos($InstanciaDB);
        $InstActividades=new Proceso_Actividad($InstanciaDB);
        $InstDpto=new Proceso_Departamento($InstanciaDB);
        $InstTipoMaterial=new Proceso_TipoMaterial($InstanciaDB);
        $InstTipoNovedadesPlan=new Proceso_TipoNovedadesPla($InstanciaDB);
        $InstCentroCostosP=new Proceso_CentroCostosP($InstanciaDB);
        $InstCentroCostosH=new Proceso_CentroCostosH($InstanciaDB);
        $InstEntidad=new Proceso_Entidades($InstanciaDB);
        $InstProyecto=new Proceso_Proyecto($InstanciaDB);
        $InstDotaciones=new Proceso_Dotaciones($InstanciaDB);
        $InstFuentes=new Proceso_Fuentes($InstanciaDB);
        $InstClasificacionC=new Proceso_ClasificacionC($InstanciaDB);
        $InstContacto=new Proceso_Contacto($InstanciaDB);
        $InstInstitucion=new Proceso_Institucion($InstanciaDB);
        $InstTipoMcpio=new Proceso_TipoMunicipio($InstanciaDB);
        $InstRegion=new Proceso_Region($InstanciaDB);
        $InstTipoInstitu=new Proceso_TipoInstitucion($InstanciaDB);
        $InstTipoEncInf=new Proceso_TipoEncInfr($InstanciaDB);
        $InstProc=new Proceso_Procesos($InstanciaDB);
        $InstTipoTall=new Proceso_TipoTaller($InstanciaDB);
        $InstTipoTarifa=new Proceso_TipoTarifa($InstanciaDB);
        $InstVereda= new Proceso_Vereda($InstanciaDB);
        $InstMcpio= new Proceso_Municipios($InstanciaDB);
        $InstTipoNovedadesMater=new Proceso_TipoNovedadesMaterial($InstanciaDB);
        $InstLegalizacionDetalleSG=new  Proceso_LegalizacionGastos($InstanciaDB);

        switch ($accion) {
            case 'login':
                    $EmailForm=limpiar($_POST['email']);
                    $ClaveForm=limpiar($_POST['pass']);
                    $LoginUser=$InstUsuario->BuscarLogin($EmailForm);
                    
                    if ($LoginUser->num_rows > 0) {
                        $row=$LoginUser->fetch_array(MYSQLI_BOTH);
                        if (password_verify($ClaveForm,$row[3])) {
                            $_SESSION['autentic']=1;
                            $_SESSION['id']=$row[0];
                            $_SESSION['nombre']=$row[1];
                            $_SESSION['email']=$row[2];
                            $_SESSION['estado']=$row[4];
                            $_SESSION['idempleado']=$row[5];
                            // echo $_SESSION['autentic'];
                            if($row[8] == 1){
                                $_SESSION['administrador']=true;
                            }else{
                                $_SESSION['administrador']=false;
                            }
                            
                            echo 1;
                        } else {
                            echo 0;
                        }
                    } else {
                       echo 0;
                    }
            break;
            case 'InsertPrograma':
                $descPrograma=primera_mayuscula(limpiar($_POST['descPrograma']));
                $estado=limpiar($_POST['estado']);
                $codigoPrograma=texto_mayusculas(limpiar($_POST['codigoPrograma']));
                $existe=$InstPrograma->BuscarCodigoPrograma($codigoPrograma);
                if ($existe->num_rows>0) {
                    // Si result es 3, significa que la descripcion del Programa ya existe
                    echo 3;
                } else {
                    $InsertadoPrograma=$InstPrograma->InsertPrograma($descPrograma,$estado,$codigoPrograma);
                    if ($InsertadoPrograma>0 ) {
                        echo 1;
                    } else {
                        echo 0;
                    } 
                }
            break;
            case 'EditPrograma':
                $IdPrograma=limpiar($_POST['IdPrograma']);
                $CodigoProgramaEditarFM=texto_mayusculas(limpiar($_POST['CodigoProgramaEdit']));
                $DescProgramaEditarFM=primera_mayuscula(limpiar($_POST['DescProgramaEdit']));
                $EstadoProgramaEditarFM=limpiar($_POST['EstadoProgramaEdit']);
                $DatosProgramaDB=$InstPrograma->BuscarPrograma($IdPrograma);
                $rowDB=$DatosProgramaDB->fetch_array(MYSQLI_BOTH);
                $diferenciaCodigo=strcmp($CodigoProgramaEditarFM,$rowDB[1]);
                $diferenciaDescripcion=strcmp($DescProgramaEditarFM,$rowDB[2]);
                $diferenciaEstado=strcmp($EstadoProgramaEditarFM,$rowDB[3]);
                $cambio=true;
                if ($diferenciaCodigo==0 && $diferenciaDescripcion==0  && $diferenciaEstado==0 ) {
                    echo 0;
                    $cambio=false;
                } else {
                   switch (true) {
                        case ($diferenciaCodigo != 0):
                            $codigoencontrado=$InstPrograma->BuscarCodigoPrograma($CodigoProgramaEditarFM);
                            if ($codigoencontrado->num_rows>0) {
                                $cambio=false;
                            } else {
                                $cambio=true;
                            }
                        break;
                        case ($diferenciaEstado != 0 ):
                            $cambio=true;
                        break;
                        default:
                            break;
                   }
                   if ($cambio) {
                       $actualizarprograma=$InstPrograma->EditarPrograma($IdPrograma,$CodigoProgramaEditarFM,$DescProgramaEditarFM,$EstadoProgramaEditarFM);
                       echo 1;
                   }else{
                       echo 3;
                   }
                }
            break;

            case 'InsertTipoGasto':
                $CodTipoGasto=texto_mayusculas(limpiar($_POST['CodTipoGasto']));
                $DescTipoGasto=primera_mayuscula(limpiar($_POST['DescTipoGasto']));
                $EstadoTipoGasto=limpiar($_POST['EstadoTipoGasto']);
                $TparaVereda=limpiar($_POST['TparaVereda']);

                $buscaTipoGasto=$InstTipoGasto->buscartipogasto($CodTipoGasto);
                if ($buscaTipoGasto->num_rows>0) {
                    // Si result es 3, significa que el email del usuario ya existe.
                    echo 3;
                } else {
                    
                    if($TparaVereda){
                        $TparaVereda=1;
                    }else{
                        $TparaVereda=0;
                    }

                    $InsertadoTipoGasto=$InstTipoGasto->creartipogasto($CodTipoGasto,$DescTipoGasto,$EstadoTipoGasto,$TparaVereda);
                    if ($InsertadoTipoGasto>0 ) {
                        echo 1;
                    } else {
                        echo 0;
                    }
                }
            break;
            case 'EditTipoGasto':
                $IdTipoGastoFM=limpiar($_POST['IdTipoGastoFM']);
                $CodigoTipoGastoFM=texto_mayusculas(limpiar($_POST['CodigoTipoGastoFM']));
                $descTipoGastoFM=primera_mayuscula(limpiar($_POST['descTipoGastoFM']));
                $EstadoTipoGastoFM=limpiar($_POST['EstadoTipoGastoFM']);
                $TparaVeredaFM=$_POST['TparaVeredaFM'];
                
                if($TparaVeredaFM == "true"){
                    $TparaVeredaFM=1;
                }else{
                    $TparaVeredaFM=0;
                    
                }

                $DatosProgramaDB=$InstTipoGasto->buscartipogasto($IdTipoGastoFM);
                $rowDB=$DatosProgramaDB->fetch_array(MYSQLI_BOTH);
                $diferenciaCodigo=strcmp($CodigoTipoGastoFM,$rowDB[1]);
                $diferenciaDescripcion=strcmp($descTipoGastoFM,$rowDB[2]);
                $diferenciaEstado=strcmp($EstadoTipoGastoFM,$rowDB[3]);
                $diferenciaTparaVereda=strcmp($TparaVeredaFM,$rowDB[4]);

                $cambio=true;
                if ($diferenciaCodigo==0 && $diferenciaDescripcion==0  && $diferenciaEstado==0 && $diferenciaTparaVereda==0) {
                    echo 0;
                    $cambio=false;
                } else {
                    switch (true) {
                        case ($diferenciaCodigo != 0):
                            $codigoencontrado=$InstTipoGasto->BuscarTipodeGastoxCodigo($CodigoTipoGastoFM);
                            if ($codigoencontrado->num_rows>0) {
                                $cambio=false;
                            } else {
                                $cambio=true;
                            }
                        break; 
                        case($diferenciaDescripcion !=0):
                            $cambio=true;
                        break;
                        case ($diferenciaEstado != 0 ):
                            $cambio=true;
                        break;
                        case ($diferenciaTparaVereda != 0 ):
                            $cambio=true;
                        break;
                        default:
                            break;
                    }
                    if ($cambio) {
                        $actualizarprograma=$InstTipoGasto->actualizartipodegasto($IdTipoGastoFM,$CodigoTipoGastoFM,$descTipoGastoFM,$EstadoTipoGastoFM,$TparaVeredaFM);
                        echo 1;
                    }else{
                        echo 3;
                    }
                }
            break;
            case 'InsertPlanCuentas':
                $CodPlanCuentas=texto_mayusculas(limpiar($_POST['CodPlanCuentas']));
                $DescPlanCuentas=primera_mayuscula(limpiar($_POST['DescPlanCuentas']));
                $EstadoPlanCuentas=limpiar($_POST['EstadoPlanCuentas']);
                $buscaPlanCuentas=$InstPlanCuentas->BuscarPlanCuentasxcodigo($CodPlanCuentas);
                if ($buscaPlanCuentas->num_rows>0) {
                    // Si result es 3, significa que el email del usuario ya existe.
                    echo 3;
                } else {
                    $InsertadoPlanCuentas=$InstPlanCuentas->InsertarPlanCuentas($CodPlanCuentas,$DescPlanCuentas,$EstadoPlanCuentas);
                    if ($InsertadoPlanCuentas>0 ) {
                        echo 1;
                    } else {
                        echo 0;
                    }
                }
            break;
            case 'EditPlanCuentas':
                $IdPlanCuentasFM=limpiar($_POST['IdPlanCuentasFM']);
                $CodigoPlanCuentasFM=texto_mayusculas(limpiar($_POST['CodigoPlanCuentasFM']));
                $descPlanCuentasFM=primera_mayuscula(limpiar($_POST['descPlanCuentasFM']));
                $EstadoPlanCuentasFM=limpiar($_POST['EstadoPlanCuentasFM']);
                $DatosProgramaDB=$InstPlanCuentas->BuscarPlanCuentasxid($IdPlanCuentasFM);
                $rowDB=$DatosProgramaDB->fetch_array(MYSQLI_BOTH);
                $diferenciaCodigo=strcmp($CodigoPlanCuentasFM,$rowDB[1]);
                $diferenciaDescripcion=strcmp($descPlanCuentasFM,$rowDB[2]);
                $diferenciaEstado=strcmp($EstadoPlanCuentasFM,$rowDB[3]);
                $cambio=true;
                if ($diferenciaCodigo==0 && $diferenciaDescripcion==0  && $diferenciaEstado==0 ) {
                    echo 0;
                    $cambio=false;
                } else {
                    switch (true) {
                        case ($diferenciaCodigo != 0):
                            $codigoencontrado=$InstPlanCuentas->BuscarPlanCuentasxcodigo($CodigoPlanCuentasFM);
                            if ($codigoencontrado->num_rows>0) {
                                $cambio=false;
                            } else {
                                $cambio=true;
                            }
                        break;
                        case ($diferenciaEstado != 0 ):
                            $cambio=true;
                        break;
                        default:
                            break;
                    }
                    if ($cambio) {
                        $actualizarprograma=$InstPlanCuentas->EditarPlanCuentas($IdPlanCuentasFM,$CodigoPlanCuentasFM,$descPlanCuentasFM,$EstadoPlanCuentasFM);
                        echo 1;
                    }else{
                        echo 3;
                    }
                }
            break;
            case 'InsertConceptoGasto':
                $CodigoConceptoGasto=texto_mayusculas(limpiar($_POST['CodigoConceptoGasto']));
                $DesConceptoGasto=primera_mayuscula(limpiar($_POST['DesConceptoGasto']));
                $TarifaSNConceptoGasto=limpiar($_POST['TarifaSNConceptoGasto']);
                $TipoGastoConceptoGasto=limpiar($_POST['TipoGastoConceptoGasto']);
                $PlanCuentasConceptoGasto=limpiar($_POST['PlanCuentasConceptoGasto']);
                $EstadoConceptoGasto=limpiar($_POST['EstadoConceptoGasto']);
               
                if ($TarifaSNConceptoGasto) {
                    $TarifaSNConceptoGasto=1;
                } else {
                    $TarifaSNConceptoGasto=0;
                }
                
                $buscaCodigoConceptoGasto=$InstConceptoGasto->BuscarConceptoGastoxcod($CodigoConceptoGasto);
                if ($buscaCodigoConceptoGasto->num_rows>0) {
                    // Si result es 3, significa que el email del usuario ya existe.
                    echo 3;
                } else {
                    $InsertadoConceptoGasto=$InstConceptoGasto->InsertarConceptoGasto($CodigoConceptoGasto,$DesConceptoGasto,$TarifaSNConceptoGasto,$TipoGastoConceptoGasto,$PlanCuentasConceptoGasto,$EstadoConceptoGasto);
                    if ($InsertadoConceptoGasto>0 ) {
                        echo 1;
                    } else {
                        echo 0;
                    }
                }
            break;
            case 'EditConceptoGasto':
                $idConceptodeGastoFM=limpiar($_POST["idConceptodeGastoFM"]);
                $CodigoConceptoGastoFM=texto_mayusculas(limpiar($_POST["CodigoConceptoGastoFM"]));
                $DesConceptoGastoFM=primera_mayuscula(limpiar($_POST["DesConceptoGastoFM"]));
                $TarifaSNConceptoGastoFM=limpiar($_POST["TarifaSNConceptoGastoFM"]);
                $TipoGastoConceptoGastoFM=limpiar($_POST["TipoGastoConceptoGastoFM"]);
                $PlanCuentasConceptoGastoFM=limpiar($_POST["PlanCuentasConceptoGastoFM"]);
                $EstadoConceptoGastoFM=limpiar($_POST["EstadoConceptoGastoFM"]);
                
                $DatosProgramaDB=$InstConceptoGasto->BuscarConceptoGastoxid($idConceptodeGastoFM);
                $rowDB=$DatosProgramaDB->fetch_array(MYSQLI_BOTH);
               
                $diferenciaCodigo=strcmp($CodigoConceptoGastoFM,$rowDB[1]);
                $diferenciaDescripcion=strcmp($DesConceptoGastoFM,$rowDB[2]);
               
                if ($TarifaSNConceptoGastoFM=='true') {
                    $TarifaSNConceptoGastoFM=1;
                } else {
                    $TarifaSNConceptoGastoFM=0;
                }                
                $diferenciaTarifaSN=strcmp($TarifaSNConceptoGastoFM,$rowDB[3]);
                $diferenciaTipoGasto=strcmp($TipoGastoConceptoGastoFM,$rowDB[7]);
                $diferenciaPlanCuentas=strcmp($PlanCuentasConceptoGastoFM,$rowDB[8]);
                $diferenciaEstado=strcmp($EstadoConceptoGastoFM,$rowDB[6]);
                  
                $cambio=true;

                if ($diferenciaCodigo==0 && $diferenciaDescripcion==0 && $diferenciaTarifaSN==0 && $diferenciaTipoGasto==0 && $diferenciaPlanCuentas==0 && $diferenciaEstado==0) {
                    echo 0;
                    $cambio=false;
                } else {
                    switch (true) {
                        case ($diferenciaCodigo != 0):
                            $codigoencontrado=$InstConceptoGasto->BuscarConceptoGastoxcod($CodigoConceptoGastoFM);
                            if ($codigoencontrado->num_rows>0) {
                                $cambio=false;
                            } else {
                                $cambio=true;
                            }
                        break;
                        case($diferenciaDescripcion != 0 || $diferenciaTarifaSN != 0 || $diferenciaTipoGasto != 0 || $diferenciaPlanCuentas !=0 || $diferenciaEstado !=0):
                            $cambio=true;
                        break;
                        default:
                        break;
                    }
                    if ($cambio) {
                        $actualizarconceptogasto=$InstConceptoGasto->EditarConceptoGasto($idConceptodeGastoFM,$CodigoConceptoGastoFM,$DesConceptoGastoFM,$TarifaSNConceptoGastoFM,$TipoGastoConceptoGastoFM,$PlanCuentasConceptoGastoFM,$EstadoConceptoGastoFM);                                            
                        echo 1;
                    }else{
                        echo 3;
                    }
                }
            break;
            case 'BuscarEmpleadosAjax':
                $search = strip_tags(trim($_POST['valorBusqueda']));
                $caracteres_malos = array("<", ">", "\"", "'", "/", "<", ">", "'", "/");
                $caracteres_buenos = array("& lt;", "& gt;", "& quot;", "& #x27;", "& #x2F;", "& #060;", "& #062;", "& #039;", "& #047;");
                $search=str_replace($caracteres_malos, $caracteres_buenos, $search);
                $buscarempleado=$InstEmpleados->BuscarEmpleadoAjax($search);
                if ($buscarempleado->num_rows>0) {
                    $rowEM=$buscarempleado->fetch_array(MYSQLI_BOTH);
                    $Documento = $rowEM[1];
                    $Nombre = $rowEM[2];
                    $mensaje = '
                    <p>
                    <strong>Documento:</strong> ' . $Documento . '<br>
                    <strong>Nombre:</strong> ' . $Nombre . '<br>
                    </p>';
                }
                echo $mensaje;
            break;
                    
            case 'ListarDetalleTMP':
            $UsuarioCreacion=$_POST['UsuarioCreacion'];
            $listaConceptoGasto=$InstConceptoGasto->ListarConceptoGastos();
                 echo '
                 <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="table-responsive">
                        <table class="table table-striped  table-hover table-responsive">
                            <thead>
                                <tr>
                                    <th class="text-center">Item</th>
                                    <th>Vereda</th>
                                    <th>Concepto Gasto</th>
                                    <th class="text-center">Cantidad</th>
                                    <th class="text-right">Valor</th>
                                    <th class="text-right">Sub Total</th>
                                    <th class="text-center">(<a href="#" onclick="eliminar_Todos_TMP('.$UsuarioCreacion.');")>
                                    <span class="glyphicon glyphicon-trash" style="color: red;"></span>
                                </a>)</th>
                                </tr>
                            </thead>
                            <tbody class="items">';
                $listadetalleTMP=$InstSolicitudGasto->listardetalleTMP($UsuarioCreacion);
                $items=1;
                $suma=0;
                if ($listadetalleTMP->num_rows > 0) {
                    while($rowTMP=$listadetalleTMP->fetch_array()){
                        echo '
                        <tr>
                            <td class="text-center">'.$items.'</td>
                            <td>'.$rowTMP["NombreVereda"].'</td>
                            <td>'.$InstSolicitudGasto->getnombreconceptoGasto($rowTMP["TMP_IdConceptoSolicitudGastos"]).'</td>
                            <td class="text-center"> '.$rowTMP['TMP_NumDisSolicitudGastos'].'</td>
                            <td class="text-right"> $ '.number_format($rowTMP["TMP_ValorundSolicitudGastos"]).'</td>
                            <td class="text-right"> $ '.number_format((($rowTMP['TMP_NumDisSolicitudGastos'])*intval($rowTMP["TMP_ValorundSolicitudGastos"]))).'</td>
                            <td class="text-center">
                                <a href="#" onclick="eliminar_item('.$rowTMP["idTMP_DetalleSolicitudGastos"].');")>
                                    <span class="glyphicon glyphicon-trash" style="color: red;"></span>
                                </a>
                            </td>
                        </tr>';
                        $items++;
                        $suma+=(($rowTMP['TMP_NumDisSolicitudGastos'])*intval($rowTMP["TMP_ValorundSolicitudGastos"]));
                    }
                }
                echo '
                            </tbody>
                        </table>
                                <div class="form-group col-lg-12 text-right">
                                    <div style="width:70%; float:left;"><h4><label>TOTAL</label></h4></div>
                                    <div style="width:30%; float:right;"><h4><b>$</b><label id="TotalSolicitudGastoSG">'.number_format($suma).'</label></h4></div>
                                    <div id="mgEliminarDetalleTMP"></div>
                                </div>                        
                        </div>
                    </div>
                </div>';
            break;

            case 'InsertarDetalleTMP':
                                            
                $IdConceptoGasto=$_POST['ConceptoGastoSG'];
                $NumDias=$_POST['NumDiasSG'];
                $ValorSolicitud=str_replace(',','',$_POST['ValorConceptoSG']);
                $UsuarioCreacion=$_POST['UsuarioCreacion'];
                $CodVereda=$_POST['CodVereda'];
                $InsertarTPMDetalle=$InstSolicitudGasto->InsertarDetalleTMP($IdConceptoGasto,$NumDias,$ValorSolicitud,$UsuarioCreacion,$CodVereda);
            break;



            case 'eliminar_item':
                $id_detalleTMP_Eliminar=$_POST['id_detalle_TMP'];
                $delete=$InstSolicitudGasto->borrarlineadetalleTMP($id_detalleTMP_Eliminar);
                if ($delete>0) {
                    echo "<div class='alert alert-dismissible alert-danger'><strong>Eliminado:</strong> Registro eliminado !!</div>";

                }
            break;

            case 'eliminar_item_DetalleSG':
                $id_detalle_SG=$_POST['id_detalle_SG'];
                $IdSG=$_POST['IdSG'];

                $delete=$InstSolicitudGasto->borrarlineadetalleSG($id_detalle_SG);
                if ($delete>0) {
                    echo "<div class='alert alert-dismissible alert-danger'><strong>Eliminado:</strong> Registro eliminado !!</div>";
                }
                $actualizarvalorCABSG=$InstSolicitudGasto->actualizarValorSG($IdSG);
            break;

            case 'eliminar_Todos_TMP':
                require_once('../modulos/solicitud_gastos/class/ClassSolicitudGastos.php'); 
                $InstSG=new Proceso_SolicitudGastos($InstanciaDB);
                $UsuarioCreacion=$_POST['UsuarioCreacion'];
                $DeleteTodosTMP=$InstSG->vaciarTMPDetalle($UsuarioCreacion);
            break;



            case 'InsertTipoMaterial':
                $DescTipoMaterial=primera_mayuscula(limpiar($_POST['DescTipoMaterial']));
                $InsertadoTipoMaterial=$InstTipoMaterial->creartipoMaterial($DescTipoMaterial);
                    if ($InsertadoTipoMaterial>0 ) {
                        echo 1;
                    } else {
                        echo 0;
                    }
            break;
            case 'EditTipoMaterial':
                $IdTipoMaterialFM=limpiar($_POST['IdTipoMaterialFM']);
                $descTipoMaterialFM=primera_mayuscula(limpiar($_POST['descTipoMaterialFM']));
                $DatosProgramaDB=$InstTipoMaterial->buscartipoMaterial($IdTipoMaterialFM);
                $rowDB=$DatosProgramaDB->fetch_array(MYSQLI_BOTH);
                $diferenciaDescripcion=strcmp($descTipoMaterialFM,$rowDB[1]);                
                $cambio=true;
                if ($diferenciaDescripcion==0) {
                    echo 0;
                    $cambio=false;
                } else {
                    $actualizarprograma=$InstTipoMaterial->actualizartipomaterial($IdTipoMaterialFM,$descTipoMaterialFM);
                    echo 1;
                }
            break;

            case 'InsertTipoNovedadesPlan':
                
                $DescTipoNovedadesPlan=primera_mayuscula(limpiar($_POST['DescTipoNovedadesPlan']));
                $InsertadoTipoNovedadesPlan=$InstTipoNovedadesPlan->creartipoNovedadesPlan($DescTipoNovedadesPlan);
                    if ($InsertadoTipoNovedadesPlan>0 ) {
                        echo 1;
                    } else {
                        echo 0;
                    }
            break;

            case 'EditTipoNovedadesPlan':
                $IdTipoNovedadesPlanFM=limpiar($_POST['IdTipoNovedadesPlanFM']);
                $descTipoNovedadesPlanFM=primera_mayuscula(limpiar($_POST['descTipoNovedadesPlanFM']));
                $DatosProgramaDB=$InstTipoNovedadesPlan->buscartipoNovedadesPlan($IdTipoNovedadesPlanFM);
                $rowDB=$DatosProgramaDB->fetch_array(MYSQLI_BOTH);
                $diferenciaDescripcion=strcmp($descTipoNovedadesPlanFM,$rowDB[1]);                
                $cambio=true;
                if ($diferenciaDescripcion==0) {
                    echo 0;
                    $cambio=false;
                } else {
                    $actualizarprograma=$InstTipoNovedadesPlan->actualizartipoNovedadesPlan($IdTipoNovedadesPlanFM,$descTipoNovedadesPlanFM);
                    echo 1;
                }
            break;
            case 'InsertCentroCostosP':
                $CodCentroCostosP=texto_mayusculas(limpiar($_POST['CodCentroCostosP']));
                $DescCentroCostosP=primera_mayuscula(limpiar($_POST['DescCentroCostosP']));
                
                $buscaCentroCostosP=$InstCentroCostosP->BuscarCentroCostosPxcodigo($CodCentroCostosP);
                if ($buscaCentroCostosP->num_rows>0) {
                    echo 3;
                } else {
                    $InsertadoCentroCostosP=$InstCentroCostosP->InsertarCentroCostosP($CodCentroCostosP,$DescCentroCostosP);
                    if ($InsertadoCentroCostosP>0 ) {
                        echo 1;
                    } else {
                        echo 0;
                    }
                }
            break;

            case 'EditCentroCostosP':
                $IdCentroCostosPFM=limpiar($_POST['IdCentroCostosPFM']);
                $CodigoCentroCostosPFM=texto_mayusculas(limpiar($_POST['CodigoCentroCostosPFM']));
                $descCentroCostosPFM=primera_mayuscula(limpiar($_POST['descCentroCostosPFM']));
                $DatosProgramaDB=$InstCentroCostosP->BuscarCentroCostosPxid($IdCentroCostosPFM);
                $rowDB=$DatosProgramaDB->fetch_array(MYSQLI_BOTH);
                $diferenciaCodigo=strcmp($CodigoCentroCostosPFM,$rowDB[1]);
                $diferenciaDescripcion=strcmp($descCentroCostosPFM,$rowDB[2]);
                $cambio=true;
                if ($diferenciaCodigo==0 && $diferenciaDescripcion==0) {
                    echo 0;
                    $cambio=false;
                } else {
                    switch (true) {
                        case ($diferenciaCodigo != 0):
                            $codigoencontrado=$InstCentroCostosP->BuscarCentroCostosPxcodigo($CodigoCentroCostosPFM);
                            if ($codigoencontrado->num_rows>0) {
                                $cambio=false;
                            } else {
                                $cambio=true;
                            }
                        break;
                        case ($diferenciaDescripcion != 0 ):
                            $cambio=true;
                        break;
                        default:
                            break;
                        }
                    if ($cambio) {
                        $actualizarprograma=$InstCentroCostosP->EditarCentroCostosP($IdCentroCostosPFM,$CodigoCentroCostosPFM,$descCentroCostosPFM);
                        echo 1;
                    }else{
                        echo 3;
                    }
                }
            break;

            case 'InsertCentroCostosH':
                $CodCentroCostosH=texto_mayusculas(limpiar($_POST['CodCentroCostosH']));
                $DescCentroCostosH=primera_mayuscula(limpiar($_POST['DescCentroCostosH']));
                $CodCentroCostosP=limpiar($_POST['CodCentroCostosP']);
                $buscaCentroCostosH=$InstCentroCostosH->BuscarCentroCostosHxcodigo($CodCentroCostosH);
                if ($buscaCentroCostosH->num_rows>0) {
                    echo 3;
                } else {
                    $InsertadoCentroCostosH=$InstCentroCostosH->InsertarCentroCostosH($CodCentroCostosH,$DescCentroCostosH,$CodCentroCostosP);
                    if ($InsertadoCentroCostosH>0 ) {
                        echo 1;
                    } else {
                        echo 0;
                    }
                }
            break;

            case 'EditCentroCostosH':
                $IdCentroCostosHFM=limpiar($_POST['IdCentroCostosHFM']);
                $CodigoCentroCostosHFM=texto_mayusculas(limpiar($_POST['CodigoCentroCostosHFM']));
                $descCentroCostosHFM=primera_mayuscula(limpiar($_POST['descCentroCostosHFM']));
                $CodCentroCostosPFM=limpiar($_POST['CodCentroCostosPFM']);
                $DatosProgramaDB=$InstCentroCostosH->BuscarCentroCostosHxid($IdCentroCostosHFM);
                $rowDB=$DatosProgramaDB->fetch_array(MYSQLI_BOTH);
                $diferenciaCodigo=strcmp($CodigoCentroCostosHFM,$rowDB[1]);
                $diferenciaDescripcion=strcmp($descCentroCostosHFM,$rowDB[2]);
                $diferenciaCodCentroCostosPFM=strcmp($CodCentroCostosPFM,$rowDB[3]);
                $cambio=true;
                if ($diferenciaCodigo==0 && $diferenciaDescripcion==0 && $diferenciaCodCentroCostosPFM ==0) {
                    echo 0;
                    $cambio=false;
                } else {
                    switch (true) {
                        case ($diferenciaCodigo != 0):
                            $codigoencontrado=$InstCentroCostosH->BuscarCentroCostosHxcodigo($CodigoCentroCostosHFM);
                            if ($codigoencontrado->num_rows>0) {
                                $cambio=false;
                            } else {
                                $cambio=true;
                            }
                        break;
                        case ($diferenciaDescripcion != 0 ):
                            $cambio=true;
                        break;
                        case ($diferenciaCodCentroCostosPFM != 0):
                            $cambio=true;
                        break;
                        default:
                            break;
                        }
                    if ($cambio) {
                        $actualizarprograma=$InstCentroCostosH->EditarCentroCostosH($IdCentroCostosHFM,$CodigoCentroCostosHFM,$descCentroCostosHFM,$CodCentroCostosPFM);
                        echo 1;
                    }else{
                        echo 3;
                    }
                }
            break;

            
            case 'InsertEntidad':
                $NitEntidad=texto_mayusculas(limpiar($_POST['NitEntidad']));
                $Nombreentidad=primera_mayuscula(limpiar($_POST['Nombreentidad']));
                
                $buscaNitEntidad=$InstEntidad->BuscarEntidadesxnit($NitEntidad);
                if ($buscaNitEntidad->num_rows>0) {
                    echo 3;
                } else {
                    $InsertadoEntidad=$InstEntidad->InsertarEntidades($NitEntidad,$Nombreentidad);
                    if ($InsertadoEntidad>0 ) {
                        echo 1;
                    } else {
                        echo 0;
                    }
                }
            break;

            case 'EditarEntidades':
                $IdEntidadesFM=limpiar($_POST['IdEntidadesFM']);
                $NitEntidadesFM=texto_mayusculas(limpiar($_POST['NitEntidadesFM']));
                $NombreEntidadesFM=primera_mayuscula(limpiar($_POST['NombreEntidadesFM']));
                
                $DatosProgramaDB=$InstEntidad->BuscarEntidadesxid($IdEntidadesFM);
                $rowDB=$DatosProgramaDB->fetch_array(MYSQLI_BOTH);
                $diferenciaCodigo=strcmp($NitEntidadesFM,$rowDB[1]);
                $diferenciaDescripcion=strcmp($NombreEntidadesFM,$rowDB[2]);
                
                $cambio=true;
                if ($diferenciaCodigo==0 && $diferenciaDescripcion==0) {
                    echo 0;
                    $cambio=false;
                } else {
                    switch (true) {
                        case ($diferenciaCodigo != 0):
                            $codigoencontrado=$InstEntidad->BuscarEntidadesxnit($NitEntidadesFM);
                            if ($codigoencontrado->num_rows>0) {
                                $cambio=false;
                            } else {
                                $cambio=true;
                            }
                        break;
                        case ($diferenciaDescripcion != 0 ):
                            $cambio=true;
                        break;
                        
                        default:
                            break;
                        }
                    if ($cambio) {
                        $actualizarprograma=$InstEntidad->EditarEntidad($IdEntidadesFM,$NitEntidadesFM,$NombreEntidadesFM);
                        echo 1;
                    }else{
                        echo 3;
                    }
                }
            break;

            case 'InsertProyecto':
                $DescProyecto=primera_mayuscula(limpiar($_POST['DescProyecto']));
                $idCentrodeCostosHijo=limpiar($_POST['idCentrodeCostosHijo']);
                $idPrograma=limpiar($_POST['idPrograma']);
                $InsertadoProyecto=$InstProyecto->InsertarProyecto($DescProyecto,$idCentrodeCostosHijo,$idPrograma);
                if ($InsertadoProyecto>0 ) {
                    echo 1;
                } else {
                    echo 0;
                }
            break;

            case 'EditarProyecto':
            
                $IdProyectoFM=limpiar($_POST['IdProyectoFM']);
                $DescProyectoFM=primera_mayuscula(limpiar($_POST['DescProyectoFM']));
                $idCentrodeCostosHijoFM=limpiar($_POST['idCentrodeCostosHijoFM']);
                $idProgramaFM=limpiar($_POST['idProgramaFM']);
                $DatosProgramaDB=$InstProyecto->BuscarProyectoxid($IdProyectoFM);
                $rowDB=$DatosProgramaDB->fetch_array(MYSQLI_BOTH);
                $diferenciaDesc=strcmp($DescProyectoFM,$rowDB[1]);
                $diferenciaCCH=strcmp($idCentrodeCostosHijoFM,$rowDB[2]);
                $diferenciaidPrograma=strcmp($idProgramaFM,$rowDB[3]);
                $cambio=true;
                if ($diferenciaDesc==0 && $diferenciaCCH==0 && $diferenciaidPrograma==0) {
                    echo 0;
                    $cambio=false;
                } else {
                    switch (true) {
                        case ($diferenciaDesc != 0):
                            $cambio=true;
                        break;
                        case ($diferenciaCCH != 0 ):
                            $cambio=true;
                        break;
                        case($diferenciaidPrograma != 0):
                            $cambio=true;
                        break;
                        default:
                            $cambio=false;
                            break;
                        }
                    if ($cambio) {
                        $actualizarprograma=$InstProyecto->EditarProyecto($IdProyectoFM,$DescProyectoFM,$idCentrodeCostosHijoFM,$idProgramaFM);
                        echo 1;
                    }else{
                        echo 3;
                    }
                }
            break;
            case 'InsertDotaciones':
                $DescDotaciones=primera_mayuscula(limpiar($_POST['DescDotaciones']));
                $InsertadoDotaciones=$InstDotaciones->crearDotaciones($DescDotaciones);
                    if ($InsertadoDotaciones>0 ) {
                        echo 1;
                    } else {
                        echo 0;
                    }
            break;
            case 'EditDotaciones':
                $IdDotacionesFM=limpiar($_POST['IdDotacionesFM']);
                $descDotacionesFM=primera_mayuscula(limpiar($_POST['descDotacionesFM']));
                $DatosProgramaDB=$InstDotaciones->buscarDotaciones($IdDotacionesFM);
                $rowDB=$DatosProgramaDB->fetch_array(MYSQLI_BOTH);
                $diferenciaDescripcion=strcmp($descDotacionesFM,$rowDB[1]);                
                $cambio=true;
                if ($diferenciaDescripcion==0) {
                    echo 0;
                    $cambio=false;
                } else {
                    $actualizarprograma=$InstDotaciones->actualizarDotaciones($IdDotacionesFM,$descDotacionesFM);
                    echo 1;
                }
            break;
            
            case 'InsertFuentes':
                $DescFuentes=primera_mayuscula(limpiar($_POST['DescFuentes']));
                $InsertadoFuentes=$InstFuentes->crearFuentes($DescFuentes);
                if ($InsertadoFuentes>0 ) {
                    echo 1;
                } else {
                    echo 0;
                }
            break;
            case 'EditFuentes':
                $IdFuentesFM=limpiar($_POST['IdFuentesFM']);
                $descFuentesFM=primera_mayuscula(limpiar($_POST['descFuentesFM']));
                $DatosProgramaDB=$InstFuentes->buscarFuentes($IdFuentesFM);
                $rowDB=$DatosProgramaDB->fetch_array(MYSQLI_BOTH);
                $diferenciaDescripcion=strcmp($descFuentesFM,$rowDB[1]);                
                $cambio=true;
                if ($diferenciaDescripcion==0) {
                    echo 0;
                    $cambio=false;
                } else {
                    $actualizarprograma=$InstFuentes->actualizarFuentes($IdFuentesFM,$descFuentesFM);
                    echo 1;
                }
            break;

            case 'InsertClasificacionC':
                $DescClasificacionC=primera_mayuscula(limpiar($_POST['DescClasificacionC']));
                $InsertadoClasificacionC=$InstClasificacionC->crearClasificacionC($DescClasificacionC);
                if ($InsertadoClasificacionC>0 ) {
                    echo 1;
                } else {
                    echo 0;
                }
            break;
            case 'EditClasificacionC':
                $IdClasificacionCFM=limpiar($_POST['IdClasificacionCFM']);
                $descClasificacionCFM=primera_mayuscula(limpiar($_POST['descClasificacionCFM']));
                $DatosProgramaDB=$InstClasificacionC->buscarClasificacionC($IdClasificacionCFM);
                $rowDB=$DatosProgramaDB->fetch_array(MYSQLI_BOTH);
                $diferenciaDescripcion=strcmp($descClasificacionCFM,$rowDB[1]);                
                $cambio=true;
                if ($diferenciaDescripcion==0) {
                    echo 0;
                    $cambio=false;
                } else {
                    $actualizarprograma=$InstClasificacionC->actualizarClasificacionC($IdClasificacionCFM,$descClasificacionCFM);
                    echo 1;
                }
            break;




            //Contacto
            case 'InsertContacto':
            $Nombre=primera_mayuscula(limpiar($_POST['Nombre']));
            $idClasificacion=limpiar($_POST['idClasificacion']);
            $CargoContacto=primera_mayuscula(limpiar($_POST['CargoContacto']));
            $TelefonoContacto=limpiar($_POST['TelefonoContacto']);
            $CelularContacto=limpiar($_POST['CelularContacto']);
            $emailContacto=limpiar($_POST['emailContacto']);
            $idRegion=$_POST['idRegion'];
            $idDepartamento=$_POST['idDepartamento'];
            $idTipoMunicipio=$_POST['idTipoMunicipio'];
                
                $buscaEmailContacto=$InstContacto->buscarEmailContacto($emailContacto);
                if ($buscaEmailContacto->num_rows>0) {
                    echo 3;
                } else {
                    $InsertadoContacto=$InstContacto->insertarContacto($Nombre,$idClasificacion,$CargoContacto,$TelefonoContacto,$CelularContacto,$emailContacto,$idRegion,$idDepartamento,$idTipoMunicipio);
                    if ($InsertadoContacto>0 ) {
                        echo 1;
                    } else {
                        echo 0;
                    }
                }
            break;
            case'EditarContacto':
                $idContactoFM=limpiar($_POST['idContactoFM']);
                $NombreFM=primera_mayuscula(limpiar($_POST['NombreFM']));
                $idClasificacionFM=limpiar($_POST['idClasificacionFM']);
                $CargoContactoFM=primera_mayuscula(limpiar($_POST['CargoContactoFM']));
                $TelefonoContactoFM=limpiar($_POST['TelefonoContactoFM']);
                $CelularContactoFM=limpiar($_POST['CelularContactoFM']);
                $emailContactoFM=limpiar($_POST['emailContactoFM']);
                $idRegionFM=$_POST['idRegionFM'];
                $idDepartamentoFM=$_POST['idDepartamentoFM'];
                $idTipoMunicipioFM=$_POST['idTipoMunicipioFM'];

                $DatosProgramaDB=$InstContacto->buscarContactoxid($idContactoFM);
                $rowDB=$DatosProgramaDB->fetch_array(MYSQLI_BOTH);
                $diferenciaNombre=strcmp($NombreFM,$rowDB[2]);
                $diferenciaClasificacion=strcmp($idClasificacionFM,$rowDB[1]);
                $diferenciaCargo=strcmp($CargoContactoFM,$rowDB[3]);
                $diferenciaTelefono=strcmp($TelefonoContactoFM,$rowDB[4]);
                $diferenciaCelular=strcmp($CelularContactoFM,$rowDB[5]);
                $diferenciaEmail=strcmp($emailContactoFM,$rowDB[6]);
                $diferenciaRegion=strcmp($idRegionFM,$rowDB[7]);
                $diferenciaDepartamento=strcmp($idDepartamentoFM,$rowDB[8]);
                $diferenciaTipoMunicipio=strcmp($idTipoMunicipioFM,$rowDB[9]);

                $cambio=true;
                if ($diferenciaNombre==0 && $diferenciaClasificacion==0 && $diferenciaCargo==0 && $diferenciaTelefono==0 && 
                $diferenciaCelular==0 && $diferenciaEmail==0 && $diferenciaRegion==0 && $diferenciaDepartamento==0 
                && $diferenciaTipoMunicipio==0) {
                    echo 0;
                    $cambio=false;
                } else {
                    switch (true) {
                        case ($diferenciaNombre != 0):
                            $cambio=true;
                        break;
                        case ($diferenciaClasificacion != 0 ):
                            $cambio=true;
                        break;
                        case($diferenciaCargo != 0):
                            $cambio=true;
                        break;
                        case ($diferenciaTelefono != 0):
                            $cambio=true;
                        break;
                        case ($diferenciaCelular != 0 ):
                            $cambio=true;
                        break;
                        case($diferenciaEmail != 0):
                            $cambio=true;
                        break;
                        case ($diferenciaRegion != 0 ):
                            $cambio=true;
                        break;
                        case($diferenciaDepartamento != 0):
                            $cambio=true;
                        break;
                        case($diferenciaTipoMunicipio != 0):
                            $cambio=true;
                        break;
                        default:
                            $cambio=false;
                            break;
                        }
                    if ($cambio) {
                        $actualizarContacto=$InstContacto->actualizarContacto($idContactoFM,$NombreFM,$idClasificacionFM,$CargoContactoFM,$TelefonoContactoFM,$CelularContactoFM,$emailContactoFM,$idRegionFM,$idDepartamentoFM,$idTipoMunicipioFM);
                        echo 1;
                    }else{
                        echo 3;
                    }
                }
            break;
            case 'InsertInstitucion':
                $CodDaneInstitucion=texto_mayusculas(limpiar($_POST['CodDaneInstitucion']));
                $NombreInstitucion=primera_mayuscula(limpiar($_POST['NombreInstitucion']));
                $idTipoInstitucion=limpiar($_POST['idTipoInstitucion']);
                $idMcpio=limpiar($_POST['idMcpio']);
                $idVereda=limpiar($_POST['idVereda']);                            
                $BuscarcodDANE=$InstInstitucion->buscarInstitucionxDANE($CodDaneInstitucion);
                if ($BuscarcodDANE->num_rows>0) {
                    echo 3;
                } else {
                    $InsertarInstitucion=$InstInstitucion->InsertarInstitucion($CodDaneInstitucion,$NombreInstitucion,$idTipoInstitucion,$idMcpio,$idVereda);
                    if ($InsertarInstitucion>0 ) {
                        echo 1;
                    } else {
                        echo 0;
                    }
                }
            break;

            case 'EditInstitucion':
            $idInstitucionFM=limpiar($_POST['idInstitucionFM']);
            $CodDaneInstitucionFM=texto_mayusculas(limpiar($_POST['CodDaneInstitucionFM']));
            $NombreInstitucionFM=primera_mayuscula(limpiar($_POST['NombreInstitucionFM']));
            $idTipoInstitucionFM=limpiar($_POST['idTipoInstitucionFM']);
            $idMcpioFM=limpiar($_POST['idMcpioFM']); 
            $idVeredaFM=limpiar($_POST['idVeredaFM']);                
            $DatosProgramaDB=$InstInstitucion->buscarInstitucion($idInstitucionFM);
            $rowDB=$DatosProgramaDB->fetch_array(MYSQLI_BOTH);
            $diferenciaCodigoDANE=strcmp($CodDaneInstitucionFM,$rowDB[1]);
            $diferenciaNombre=strcmp($NombreInstitucionFM,$rowDB[2]);
            $diferenciaTipoInstitucion=strcmp($idTipoInstitucionFM,$rowDB[3]);
            $diferenciaVereda=strcmp($idVeredaFM,$rowDB[4]);
            $cambio=true;

            if ($diferenciaCodigoDANE==0 && $diferenciaNombre==0 && $diferenciaTipoInstitucion==0 && $diferenciaVereda==0) {
                echo 0;
                $cambio=false;
            } else {
                switch (true) {
                    case ($diferenciaCodigoDANE != 0):
                        $codigoencontrado=$InstInstitucion->buscarInstitucionxDANE($CodDaneInstitucionFM);
                        if ($codigoencontrado->num_rows>0) {
                            $cambio=false;
                        } else {
                            $cambio=true;
                        }
                    break;
                    case ($diferenciaNombre != 0):
                        $cambio=true;
                    break;
                    default:
                        $cambio=true;
                        break;
                    }
                if ($cambio) {
                    $actualizarprograma=$InstInstitucion->actualizarInstitucion($idInstitucionFM,$CodDaneInstitucionFM,
                    $NombreInstitucionFM,$idTipoInstitucionFM,$idMcpioFM,$idVeredaFM);
                    // echo "<b>Se cambio: 1</b> <br>";
                }else{
                    echo 3;
                }
            }


            // Modulos Javier Octubre 25


            case 'InsertarActividad':
            $descripcionactividad=primera_mayuscula(limpiar($_POST['descripcionactividad']));
            $unidadtiempoac=primera_mayuscula(limpiar($_POST['unidadtiempoac']));
            $actividadinsertada=$InstActividades->InsertActividad($descripcionactividad,$unidadtiempoac);
            if($actividadinsertada>0){
                echo 1;
            }else{
                echo 0;
            }
            break;

            case 'EditarActividad':
            $IdActividad=(limpiar($_POST['IdActividad']));
            $descActividad=primera_mayuscula(limpiar($_POST['descActividad']));
            $unTiemActividad=primera_mayuscula(limpiar($_POST['unTiemActividad']));
            $DatosProgramaDB=$InstActividades->BuscarActividad($IdActividad);
            $rowDB=$DatosProgramaDB->fetch_array(MYSQLI_BOTH);
            $diferenciaDescripcion=strcmp($descActividad,$rowDB[1]);
            $diferenciaUdTiemp=strcmp($unTiemActividad,$rowDB[2]);
            $cambio=true;
            if ($diferenciaDescripcion==0  && $diferenciaUdTiemp==0 ) {
                echo 0;
                $cambio=false;
            } else {
                switch (true) {
                    case ($diferenciaDescripcion != 0):
                        $descEncontrado=$InstActividades->BuscardescActividad($descActividad);
                        if ($descEncontrado->num_rows>0) {
                            $cambio=false;
                        } else {
                            $cambio=true;
                        }
                    break;
                    case ($diferenciaUdTiemp != 0 ):
                        $cambio=true;
                    break;
                    default:
                        break;
                }
                if ($cambio) {
                    $actualizarprograma=$InstActividades->EditarActividad($IdActividad,$descActividad,$unTiemActividad);
                    echo 1;
                }else{
                    echo 3;
                }
            }
            break;

            //Modulo departamento.
            case 'insertarDpto':
            $codDaneDpto=limpiar($_POST['codDaneDpto']);
            $DescDpto=primera_mayuscula(limpiar($_POST['descripDpto']));
            $Dptoinsertado=$InstDpto->InsertDepartamento($codDaneDpto,$DescDpto);
            if($Dptoinsertado>0){
                echo 1;
            }else{
                echo 0;
            }
            break;

            case 'EditarDpto':
            $IdDpto=limpiar($_POST['IdDpto']);
            $codDANEDpto=limpiar($_POST['codDANEDpto']);
            $descDpto=primera_mayuscula(limpiar($_POST['descDpto']));
            $DatosProgramaDB=$InstDpto->BuscaridDepartamento($IdDpto);
            $rowDB=$DatosProgramaDB->fetch_array(MYSQLI_BOTH);
            $diferenciaDANE=strcmp($codDANEDpto,$rowDB[1]); 
            $diferenciaDescDpto=strcmp($descDpto,$rowDB[2]);               
            $cambio=true;
            if ($diferenciaDANE ==0 && $diferenciaDescDpto==0) {
                echo 0;
                $cambio=false;
            } else {
                switch(true){
                        case($diferenciaDANE !=0):
                            $codigoDANEEncontrado=$InstDpto->BuscarCodDepartamento($codDANEDpto);
                            if($codigoDANEEncontrado->num_rows>0){
                                $cambio=false;
                            }else{
                                $cambio=true;
                            }
                        break;
                        case($diferenciaDescDpto !=0):
                            $cambio=true;
                        break;
                }
                if($cambio){
                    $actualizarprograma=$InstDpto->EditarDepartamento($IdDpto,$codDANEDpto,$descDpto);
                    echo 1;
                }else{
                    echo 3;
                }
            }
            break;

            // Modulo de tipo Municipio
            case 'insertarTipoMcpio':
            $codTipoMcpio=limpiar($_POST['codTipoMcpio']);
            $DescTipoMcpio=primera_mayuscula(limpiar($_POST['descTipoMcpio']));
            
            
            $buscarCodTipoMcpio=$InstTipoMcpio->BuscarxCodMunicipio($codTipoMcpio);
            if ($buscarCodTipoMcpio->num_rows>0) {
                echo 3;
            } else {
                $TipoMcpioinsertado=$InstTipoMcpio->InsertTipoMunicipio($codTipoMcpio,$DescTipoMcpio);
                if($TipoMcpioinsertado>0){
                    echo 1;
                    }else{
                    echo 0;
                    }
            }
                        
            break;

            case 'EditarTipoMcpio':
            $IdTipoMcpio=limpiar($_POST['IdTipoMcpio']);
            $descTipoMcpio=primera_mayuscula(limpiar($_POST['descTipoMcpio']));
            $codTipoMcpio=limpiar($_POST['codTipoMcpio']);
            $DatosProgramaDB=$InstTipoMcpio->BuscarTipoMunicipio($IdTipoMcpio);
            $rowDB=$DatosProgramaDB->fetch_array(MYSQLI_BOTH);
            $diferenciaDescTipoMcpio=strcmp($descTipoMcpio,$rowDB[2]); 
            $diferenciaCodTipoMcpio=strcmp($codTipoMcpio,$rowDB[1]);              
            $cambio=true;
            if ($diferenciaDescTipoMcpio ==0 && $diferenciaCodTipoMcpio==0) {
                echo 0;
                $cambio=false;
            } else {
                if($cambio){
                    $actualizarprograma=$InstTipoMcpio->EditarTipoMunicipio($IdTipoMcpio,$codTipoMcpio,$descTipoMcpio);
                    echo 1;
                }else{
                    echo 3;
                }
            }
            break;

            //Modulo de Region
            case 'InsertRegion':
            
            $DescRegion=primera_mayuscula(limpiar($_POST['descripcionRegion']));
            $Regioninsertado=$InstRegion->InsertRegion($DescRegion);
            if($Regioninsertado>0){
            echo 1;
            }else{
            echo 0;
            }
            break;

            case 'EditarRegion':
            $IdRegion=limpiar($_POST['idRegion']);
            $descRegion=primera_mayuscula(limpiar($_POST['descRegion']));
            $DatosProgramaDB=$InstRegion->BuscarIdRegion($IdRegion);
            $rowDB=$DatosProgramaDB->fetch_array(MYSQLI_BOTH);
            $diferenciaDescReg=strcmp($descRegion,$rowDB[1]);               
            $cambio=true;
            if ($diferenciaDescReg ==0) {
                echo 0;
                $cambio=false;
            } else {
                if($cambio){
                    $actualizarprograma=$InstRegion->EditarRegion($IdRegion,$descRegion);
                    echo 1;
                }else{
                    echo 3;
                }
            }
            break;

            //Modulo de Municipio
            case 'insertarMunicipio':
            $codDaneDpto=limpiar($_POST['codDaneDpto']);
            $DescDpto=primera_mayuscula(limpiar($_POST['descTipoInsti']));
            $Dptoinsertado=$InstMunicipio->InsertDepartamento($codDaneDpto,$DescDpto);
            if($Dptoinsertado>0){
            echo 1;
            }else{
                echo 0;
                }
            break;

            //Modulo de tipo institucion 
        case 'insertarTipoInsti':
            $descTipoInsti=primera_mayuscula(limpiar($_POST['descTipoInsti']));
            $TipoInstitinsertado=$InstTipoInstitu->InsertTipoInstitucion($descTipoInsti);
            if($TipoInstitinsertado>0){
            echo 1;
            }else{
                echo 0;
                }
        break;

        case 'EditarTipoInst':
            $idTipoInst=limpiar($_POST['idTipoInst']);
            $descTipoInst=primera_mayuscula(limpiar($_POST['descTipoInst']));
            $DatosProgramaDB=$InstRegion->BuscarIdRegion($idTipoInst);
            $rowDB=$DatosProgramaDB->fetch_array(MYSQLI_BOTH);
            $diferenciaDescTipoInst=strcmp($descTipoInst,$rowDB[1]);               
            $cambio=true;
            if ($diferenciaDescTipoInst ==0) {
                echo 0;
                $cambio=false;
            } else {
                if($cambio){
                    $actualizarprograma=$InstTipoInstitu->EditarTipoInstitucion($idTipoInst,$descTipoInst);
                    echo 1;
                }else{
                    echo 3;
                }
            }
        break;

            //Modulo de tipo Encuesta infraestructura
        case 'insertarTipoEncInf':
            $descTipoEncInf=primera_mayuscula(limpiar($_POST['descTipoEncInf']));
            $TipoEncInfinsertado=$InstTipoEncInf->InsertTipoEncInfraes($descTipoEncInf);
            if($TipoEncInfinsertado>0){
            echo 1;
            }else{
            echo 0;
            }
        break;

        case 'EditarTipoEncInf':
            $idTipoEncInf=limpiar($_POST['idTipoEncInf']);
            $descTipoEncInf=primera_mayuscula(limpiar($_POST['descTipoEncInf']));
            $DatosProgramaDB=$InstTipoEncInf->BuscarTipoEncInfraes($idTipoEncInf);
            $rowDB=$DatosProgramaDB->fetch_array(MYSQLI_BOTH);
            $diferenciadescTipoEncInf=strcmp($descTipoEncInf,$rowDB[1]);               
            $cambio=true;
            if ($diferenciadescTipoEncInf ==0) {
                echo 0;
                $cambio=false;
            } else {
                if($cambio){
                    $actualizarprograma=$InstTipoEncInf->EditarTipoEncInfraes($idTipoEncInf,$descTipoEncInf);
                    echo 1;
                }else{
                    echo 3;
                }
            }
        break;

            // modulo Procesos.
        case 'InsertProcesos':
            $descProceso=primera_mayuscula(limpiar($_POST['descProceso']));
            $procesoinsertada=$InstProc->InsertProcesos($descProceso);
            if($procesoinsertada>0){
            echo 1;
            }else{
            echo 0;
            }
        break;

        case 'EditarProceso':
            $idProceso=limpiar($_POST['idProceso']);
            $descProceso=primera_mayuscula(limpiar($_POST['descProceso']));
            $DatosProgramaDB=$InstProc->BuscarCodProcesos($idProceso);
            $rowDB=$DatosProgramaDB->fetch_array(MYSQLI_BOTH);
            $diferenciadescProceso=strcmp($descProceso,$rowDB[1]);               
            $cambio=true;
            if ($diferenciadescProceso ==0) {
            echo 0;
            $cambio=false;
            } else {
            if($cambio){
            $actualizarprograma=$InstProc->EditarProcesos($idProceso,$descProceso);
            echo 1;
            }else{
            echo 3;
            }
            }
        break;

        // modulo Tipo taller.
        case 'InsertTipoTaller':
            $descTipoTaller=primera_mayuscula(limpiar($_POST['descTipoTaller']));
            $estadoTipoTaller=primera_mayuscula(limpiar($_POST['estadoTipoTaller']));
            $tipoTallerinsertado=$InstTipoTall->InsertTipoTaller($descTipoTaller, $estadoTipoTaller);
            if($tipoTallerinsertado>0){
                echo 1;
            }else{
                echo 0;
            }
        break;

        case 'EditarTipoTaller':
            $IdTipoTaller=limpiar($_POST['IdTipoTaller']);
            $descTipoTaller=limpiar($_POST['descTipoTaller']);
            $estadoTipoTaller=primera_mayuscula(limpiar($_POST['estadoTipoTaller']));
            $DatosProgramaDB=$InstTipoTall->BuscaridTipoTaller($IdTipoTaller);
            $rowDB=$DatosProgramaDB->fetch_array(MYSQLI_BOTH);
            $diferenciadesc=strcmp($descTipoTaller,$rowDB[1]); 
            $diferenciaestadoTipoTaller=strcmp($estadoTipoTaller,$rowDB[2]);               
            $cambio=true;
            if ($diferenciadesc==0 && $diferenciaestadoTipoTaller==0) {
                echo 0;
                $cambio=false;
            } else {
                switch(true){
                        case($diferenciadesc!=0):
                            $descEncontrado=$InstTipoTall->BuscarTipoTaller($descTipoTaller);
                            if($descEncontrado->num_rows >0){
                                $cambio=false;
                            }else{
                                $cambio=true;
                            }
                        break;
                        case($diferenciaestadoTipoTaller!=0):
                            $cambio=true;
                        break;
                }
                if($cambio){
                    $actualizarprograma=$InstTipoTall->EditarTipoTaller($IdTipoTaller,$descTipoTaller,$estadoTipoTaller);
                    echo 1;
                }else{
                    echo 3;
                }
            }
        break;

        // modulo Colecciones.
        // case 'InsertColeccion':
        //     $descColeccion=primera_mayuscula(limpiar($_POST['descColeccion']));
        //     $idEntColeccion=primera_mayuscula(limpiar($_POST['idEntColeccion']));
        //     $coleccioninsertado=$InstColeccion->InsertColecciones($descColeccion, $idEntColeccion);
        //     if($coleccioninsertado>0){
        //         echo 1;
        //     }else{
        //         echo 0;
        //     }
        // break;
        
  // Modulo Javier Octubre 25

        // case 'EditarColeccion':
        //     $idColeccion=limpiar($_POST['idColeccion']);
        //     $DescColeccion=primera_mayuscula(limpiar($_POST['DescColeccion']));
        //     $DatosProgramaDB=$InstColeccion->BuscarIdColecciones($idColeccion);
        //     $rowDB=$DatosProgramaDB->fetch_array(MYSQLI_BOTH);
        //     $diferenciaDescColeccion=strcmp($DescColeccion,$rowDB[1]);               
        //     $cambio=true;
        //     if ($diferenciaDescColeccion ==0) {
        //         echo 0;
        //         $cambio=false;
        //     } else {
        //         if($cambio){
        //             $actualizarprograma=$InstColeccion->EditarColeccion($idColeccion,$DescColeccion);
        //             echo 1;
        //         }else{
        //             echo 3;
        //         }
        //     }
        // break;

   // modulo Tipo Tarifas.
        case 'InsertTipoTarifa':
                $descTipoTarifa=primera_mayuscula(limpiar($_POST['descTipoTarifa']));
                $TipoTarifainsertado=$InstTipoTarifa->Inserttipotarifa($descTipoTarifa);
                if($TipoTarifainsertado>0){
                    echo 1;
                }else{
                    echo 0;
                }
        break;
    
        case 'EditarTipoTarifa':
            $idTipoTarifa=limpiar($_POST['idTipoTarifa']);
            $descTipoTarifa=primera_mayuscula(limpiar($_POST['descTipoTarifa']));
            $DatosProgramaDB=$InstTipoTarifa->Buscartipotarifa($idTipoTarifa);
            $rowDB=$DatosProgramaDB->fetch_array(MYSQLI_BOTH);
            $diferenciadescTipoTarifa=strcmp($descTipoTarifa,$rowDB[1]);               
            $cambio=true;
            if ($diferenciadescTipoTarifa ==0) {
                echo 0;
            } else {
                $actualizarprograma=$InstTipoTarifa->Editartipotarifa($idTipoTarifa,$descTipoTarifa);
                if($actualizarprograma>0){
                    echo 1;
                }else{
                    echo 3;
                }
            }
        break;
        
    // modulo Veredas.
        case 'InsertVereda':
            $descVereda=primera_mayuscula(limpiar($_POST['descVereda']));
            $IdMcpio=primera_mayuscula(limpiar($_POST['IdMcpio']));
            $Veredainsertado=$InstVereda->Insertvereda($descVereda, $IdMcpio);
            if($Veredainsertado>0){
                echo 1;
            }else{
                echo 0;
            }
        break;

        case 'EditarVereda':
            $IdVereda=limpiar($_POST['idVereda']);
            $descVereda=primera_mayuscula(limpiar($_POST['descVereda']));
            $IdMcpio=limpiar($_POST['IdMcpio']);
            $ValorMovilidad=str_replace(',','',$_POST['ValorMovilidadVereda']);
            $DatosProgramaDB=$InstVereda->BuscaridVereda($IdVereda);
            $rowDB=$DatosProgramaDB->fetch_array(MYSQLI_NUM);
            $diferenciadesc=strcmp($descVereda,$rowDB[1]); 
            $diferenciaIdMcpio=strcmp($IdMcpio,$rowDB[2]); 
            $diferenciaValorMovilidad=strcmp($ValorMovilidad,$rowDB[3]);               
            $cambio=true;
            
            if ($diferenciadesc==0 && $diferenciaIdMcpio==0 && $diferenciaValorMovilidad==0) {
                echo 0;
                $cambio=false;
            } else {
                switch(true){
                        case($diferenciadesc!=0):
                            $descEncontrado=$InstVereda->BuscarVereda($descVereda);
                            if($descEncontrado->num_rows >0){
                                $cambio=false;
                            }else{
                                $cambio=true;
                            }
                        break;
                        case($diferenciaIdMcpio!=0):
                            $cambio=true;
                        break;
                        case($diferenciaValorMovilidad!=0):
                            $cambio=true;
                        break;
                }
                if($cambio){
                    $actualizarprograma=$InstVereda->Editarvereda($IdVereda,$descVereda,$IdMcpio,$ValorMovilidad);
                    echo 1;
                }else{
                    echo 3;
                }
            }
        break;

        case 'LlenaSelectVeredasSegunMunicipio':
            $CodMunicipioSelect=limpiar($_POST['CodMunicipioSelect']);
            $ListaVeredasxMunicipio=$InstVereda->ListaVeredasxMunicipio($CodMunicipioSelect);
            $texto='<option value="0"> -- Seleccione Vereda -- </option>';
                while ($rowVer=$ListaVeredasxMunicipio->fetch_array(MYSQLI_BOTH)) {
                    $texto.="<option value='".$rowVer[0]."'>".$rowVer[1]."</option>";
                }
                echo json_encode($texto);
        break;

        case 'GetTarifaVereda':
            require_once '../modulos/tarifas/class/classtarifagastosgnr.php';
            $InstTarifasVeredas=new Proceso_TarifasGNR($InstanciaDB);
            $idVereda=$_POST['CodVeredaSG'];

            $idConcepto=$_POST['idConcepto'];
            $VeredaValor=$InstTarifasVeredas->BuscarTarifasConcepto_Veredas($idVereda,$idConcepto);
            if($VeredaValor->num_rows >0){
                echo json_encode($VeredaValor->fetch_array(MYSQLI_NUM));
            }else{
                echo 0;
            }

            
        break;
           // modulo Municipios.
       case 'InsertMunicipio':
           $codMunicipio=limpiar($_POST['codMunicipio']);
           $descMunicipio=primera_mayuscula(limpiar($_POST['descMunicipio']));
           $idRegion=limpiar($_POST['idRegion']);
           $idTipoMcpio=limpiar($_POST['idTipoMcpio']);
           $idTipoTarifa=limpiar($_POST['idTipoTarifa']);
           $idDpto=limpiar($_POST['idDpto']);
           $Mcpioinsertado=$InstMcpio->InsertMunicipio($codMunicipio,$descMunicipio,$idRegion,$idTipoMcpio,$idTipoTarifa,$idDpto);
           if($Mcpioinsertado>0){
               echo 1;
           }else{
               echo 0;
           }
       break;

       case 'EditarMunicipio':
           $IdMcpio=limpiar($_POST['IdMcpio']);
           $codMunicipio=limpiar($_POST['codMunicipio']);
           $descMunicipio=primera_mayuscula(limpiar($_POST['descMunicipio']));
           $idRegion=limpiar($_POST['idRegion']);
           $idTipoMcpio=limpiar($_POST['idTipoMcpio']);
           $idTipoTarifa=limpiar($_POST['idTipoTarifa']);
           $idDpto=limpiar($_POST['idDpto']);
           $DatosProgramaDB=$InstMcpio->BuscarMunicipio($IdMcpio);
           $rowDB=$DatosProgramaDB->fetch_array(MYSQLI_BOTH);
           $diferenciaDANEMcpio=strcmp($codMunicipio,$rowDB[1]); 
           $diferenciadescMunicipio=strcmp($descMunicipio,$rowDB[2]);               
           $cambio=true;
           if ($diferenciaDANEMcpio==0 && $diferenciadescMunicipio==0) {
               echo 0;
               $cambio=false;
           } else {
               switch(true){
                       case($diferenciaDANEMcpio!=0):
                           $descEncontrado=$InstMcpio->BuscarCodMunicipio($codMunicipio);
                           if($descEncontrado->num_rows >0){
                               $cambio=false;
                           }else{
                               $cambio=true;
                           }
                       break;
                       case($diferenciadescMunicipio!=0):
                           $cambio=true;
                       break;
               }
               if($cambio){
                   $actualizarprograma=$InstMcpio->EditarMunicipio($IdMcpio,$codMunicipio,$descMunicipio,$idRegion,$idTipoMcpio,$idTipoTarifa,$idDpto);
                   echo 1;
               }else{
                   echo 3;
               }
           }
       break;

// Modulo Javier Octubre 25

// ******************** Modulo Solicitud de Gastos
        case 'InsertarSolicitudGasto':
        
           
            $UsuarioCreacion=$_POST['IdUsuario'];
            $fecha=$_POST['fecha'];
            $CodProyectoSG=$_POST['CodProyectoSG'];
            $CodProcesoSG=$_POST['CodProcesoSG'];
            $CodActividadSG=$_POST['CodActividadSG'];
            $CodMunicipioSG=$_POST['CodMunicipioSG'];
            $CodDepartamentoSG=$_POST['CodDepartamentoSG'];
            $CodEntidadSG=$_POST['CodEntidadSG'];
            $FechaHoraSalidaSG=$_POST['FechaHoraSalidaSG'];
            $FechaHoraRegresoSG=$_POST['FechaHoraRegresoSG'];
            $responsableSG=$_POST['responsableSG'];
            $ObservacionesSG=$_POST['ObservacionesSG'];
            $TotalSolicitudGastoSG=str_replace(',','',$_POST['TotalSolicitudGastoSG']);
            $InsertarSolicitudGastoCab=$InstSolicitudGasto->InsertarSolicitudGastos($fecha,$CodProyectoSG,
            $CodProcesoSG,$CodActividadSG,$CodMunicipioSG,$FechaHoraSalidaSG,$FechaHoraRegresoSG,
            $TotalSolicitudGastoSG,$ObservacionesSG,$CodDepartamentoSG,$UsuarioCreacion);

            if($InsertarSolicitudGastoCab>0){
                    for($i=0; $i<count($responsableSG); $i++){
                        $InsertarResponsbleSG=$InstSolicitudGasto->InsertarResponsablesSG($InsertarSolicitudGastoCab,$responsableSG[$i]);
                    }
                    for ($j=0; $j<count($CodEntidadSG); $j++) {
                        $InsertarEntidades=$InstSolicitudGasto->InsDetalleEntidadSG($InsertarSolicitudGastoCab,$CodEntidadSG[$j]);                       
                    }
                    $ListaDetalleTMP=$InstSolicitudGasto->listardetalleTMP($UsuarioCreacion);
                    while ($rowDSG=$ListaDetalleTMP->fetch_array(MYSQLI_BOTH)) { 
                        $InsertandoDetalleSG=$InstSolicitudGasto->InsertarTPMaDetalle($InsertarSolicitudGastoCab,$rowDSG[1], $rowDSG[2],$rowDSG[3],$rowDSG[5]);
                    }
                    $BorrarTMPDetalle=$InstSolicitudGasto->vaciarTMPDetalle($UsuarioCreacion);
                echo 1;              
            }else{
                echo 0;
            }        
        break;

    // editar modulo solicitud de gasto  /////
        case 'editarSolicitudGasto':
            $IdSolicitudGastoSG=$_POST['IdSolicitudGastoSG'];
            
            $CodProyectoSG=$_POST['CodProyectoSG'];
            $CodActividadSG=$_POST['CodActividadSG'];
            $CodMunicipioSG=$_POST['CodMunicipioSG'];
            $CodDepartamentoSG=$_POST['CodDepartamentoSG'];
            $CodEntidadSG=$_POST['CodEntidadSG'];
            $FechaHoraSalidaSG=$_POST['FechaHoraSalidaSG'];
            $FechaHoraRegresoSG=$_POST['FechaHoraRegresoSG'];
            $responsableSG=$_POST['responsableSG'];
            $ObservacionesSG=$_POST['ObservacionesSG'];
            $TotalSolicitudGastoSG=str_replace(',','',$_POST['TotalSolicitudGastoSG']);

            $editSolicitudGastoCab=$InstSolicitudGasto->EditarSolicitudGastos($IdSolicitudGastoSG,$CodProyectoSG,
            $CodActividadSG,$CodMunicipioSG,$FechaHoraSalidaSG,$FechaHoraRegresoSG,$TotalSolicitudGastoSG,
            $ObservacionesSG,$CodDepartamentoSG);

            if($editSolicitudGastoCab>0){
                $i=0;
                $j=0;
                $ResponsablesSG=$InstSolicitudGasto->ListaResponsablesxIDsolicitud($IdSolicitudGastoSG);
                $EntidadesSG=$InstSolicitudGasto->ListaEntidadesxIDsolicitud($IdSolicitudGastoSG);
            
                while($rowResp=$ResponsablesSG->fetch_array(MYSQLI_BOTH)) {
                $RespBD[$i]=$rowResp[2];
                $i++;
                }
                $difRes=array_diff($responsableSG,$RespBD);
                $difRes1=count($difRes);

                $difRes2=array_diff($responsableSG,$RespBD);
                $difRes2=count($difRes2);
                if ($difRes1>0 || $difRes2>0) {
                $eliminarDetResponsable=$InstSolicitudGasto->borrarResponsableSG($IdSolicitudGastoSG);  
                for($i=0; $i<count($responsableSG); $i++){
                    $editarResponsbleSG=$InstSolicitudGasto->InsertarResponsablesSG($IdSolicitudGastoSG,$responsableSG[$i]);
                }
                }
                
                while ($rowEnt=$EntidadesSG->fetch_array(MYSQLI_BOTH)) {
                $EntidBD[$j]=$rowEnt[2];
                $j++;
                }                
                $difEnt=array_diff($CodEntidadSG,$EntidBD);
                $difEnt1=count($difEnt);

                $difEnt2=array_diff($CodEntidadSG,$EntidBD);
                $difEnt2=count($difEnt2);
                if ($difEnt1>0 || $difEnt2>0) {
                $eliminarDetEntidad=$InstSolicitudGasto->borrarEntidadSG($IdSolicitudGastoSG);  
                for($i=0; $i<count($CodEntidadSG); $i++){
                    $editarEntidadesSG=$InstSolicitudGasto->InsDetalleEntidadSG($IdSolicitudGastoSG,$responsableSG[$i]);
                }
                }                     
                echo 1;              
            }else{
                echo 0;
            }        
        break;

    case 'LlenaSelectMunicipioSegunDepartamento2':
        $CodDepartamentoSG2=limpiar($_POST['CodDepartamentoSG2']);
        $ListaMunicipioSegunDepartamento2=$InstMcpio->ListarMunicipiosxDepartamento($CodDepartamentoSG2);
        $texto='<option value="0">---Seleccione Municipio ---</option>';
        while ($rowDpt=$ListaMunicipioSegunDepartamento2->fetch_array(MYSQLI_BOTH)) {
            $texto.="<option value='".$rowDpt[0]."'>".$rowDpt[2]."</option>";
        }
        echo json_encode($texto);
    break;


    case 'llenarDetalleSGasto':       
        $idSG=limpiar($_POST['idSolicGastoFM']);
        $idConcGasto=limpiar($_POST['ConceptoGastoSGFM']);
        $unidades=limpiar($_POST['NumDiasSG']);
        $vrUnidad=str_replace(',','',$_POST['ValorConceptoSGFM']);
        $IdVereda=$_POST['IdVereda']; 
        $InsertandoDetalleSG=$InstSolicitudGasto->InsertarTPMaDetalle($idSG,$idConcGasto,$unidades,$vrUnidad,$IdVereda);
        $UpdateValorCABSG=$InstSolicitudGasto->actualizarValorSG($idSG);

    break;

    case 'BuscarIDSG';
        $IdSG=$_POST['IdSG'];
        $VerIdSG=$InstSolicitudGasto->BuscarSolicitudGastosxid($IdSG);
        echo json_encode($VerIdSG->fetch_array(MYSQLI_NUM));
    break;

    case 'CargarEntidadesSelect2':
        $i=0;
            $IdSG=$_POST['IdSG'];
            $entidades=array();
            $ListaEntidades=$InstSolicitudGasto->ListaEntidadesxIDsolicitud($IdSG);
            while($rowEMSG=$ListaEntidades->fetch_array()){
                $entidades[$i]=intval($rowEMSG[2]);
                $i++;
            }
            echo json_encode($entidades);              
    break;
    
// fin editar solicitud de gasto

    case 'llenarDetalleSGasto';
        $idSG=limpiar($_POST['idSolicGastoFM']);
        $idConcGasto=limpiar($_POST['ConceptoGastoSGFM']);
        $unidades=limpiar($_POST['NumDiasSGFM']);
        $vrUnidad=str_replace(',','',$_POST['ValorConceptoSGFM']); 
        $IdVereda=$_POST['IdVereda'];        
        $InsertandoDetalleSG=$InstSolicitudGasto->InsertarTPMaDetalle($idSG,$idConcGasto,$unidades,$vrUnidad,$IdVereda);
    break;

    // case 'BuscarIDSG';
    //     $IdSG=$_POST['IdSG'];
    //     $VerIdSG=$InstSolicitudGasto->BuscarSolicitudGastosxid($IdSG);
    //     echo json_encode($VerIdSG->fetch_array(MYSQLI_NUM));
    // break;


// modulo Tipo Novedades Material.
    case 'InsertTipoNovedadMater':
        $descTipoNovMater=primera_mayuscula(limpiar($_POST['descTipoNovMater']));
        $TiponovMaterinsertado=$InstTipoNovedadesMater->creartipoNovedadesMater($descTipoNovMater);
        if($TiponovMaterinsertado>0){
            echo 1;
        }else{
            echo 0;
        }
    break;

    case 'EditarTipoNovMater':
        $idTipoNovMater=limpiar($_POST['idTipoNovMater']);
        $descTipoNovMater=primera_mayuscula(limpiar($_POST['descTipoNovMater']));
        $DatosProgramaDB=$InstTipoNovedadesMater->buscartipoNovedadesMater($idTipoNovMater);
        $rowDB=$DatosProgramaDB->fetch_array(MYSQLI_BOTH);
        $diferenciadescTipoNovMater=strcmp($descTipoNovMater,$rowDB[1]);               
        $cambio=true;
        if ($diferenciadescTipoNovMater ==0) {
            echo 0;
            $cambio=false;
        } else {
            if($cambio){
                $actualizarprograma=$InstTipoNovedadesMater->EditarNovedadesMater($idTipoNovMater,$descTipoNovMater);
                echo 1;
            }else{
                echo 3;
            }
        }
    break;            

    case 'ListarDetalleSG':
        $IdSG=limpiar($_POST['IdSG']);
        $IdDOM = $_POST['IdDOM'];
            echo '<div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="table-responsive">
                        <!-- Row start -->
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading clearfix">
                                            <h3 class="panel-title">Detalle Solicitud de Gastos</h3>
                                        </div>
                                        
                                        <div class="panel-body">
                                            <div class="row">
                                                <table class="table table-striped  table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">Item</th>
                                                            <th>Vereda</th>
                                                            <th>Concepto Gasto</th>
                                                            <th class="text-center">Cantidad</th>
                                                            <th class="text-right">Valor</th>
                                                            <th class="text-right">Sub Total</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody class="items">';
                                                        $ListaDetalleSG=$InstSolicitudGasto->ListarDetallexidSolicitud($IdSG);
                                                        $items=1;
                                                        $suma=0;
                                                        if ($ListaDetalleSG->num_rows > 0) {
                                                            while($rowSG=$ListaDetalleSG->fetch_array()){
                                                                echo '
                                                                <tr>
                                                                    <td class="text-center">'.$items.'</td>
                                                                    <td>'.$rowSG[7].'</td>
                                                                    <td>'.$InstSolicitudGasto->getnombreconceptoGasto($rowSG[2]).'</td>
                                                                    <td class="text-center"> '.$rowSG[3].'</td>
                                                                    <td class="text-right"> $ '.number_format($rowSG[4]).'</td>
                                                                    <td class="text-right"> $ '.number_format((intval($rowSG[3])*intval($rowSG[4]))).'</td>';
                                                                    if($IdDOM == 'detall_modal'){
                                                                        echo '<td class="text-center">
                                                                            <a href="#" onclick="eliminar_item_DetalleSG('.$rowSG[0].','.$IdSG.');")>
                                                                                <span class="glyphicon glyphicon-trash" style="color: red;"></span>
                                                                            </a>
                                                                        </td>';
                                                                     }

                                                                echo '</tr>';
                                                                $items++;
                                                                $suma+=(intval($rowSG[3])*intval($rowSG[4]));
                                                            }
                                                        }
                                                        echo '
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <!-- Row end -->
                        
                        <div class="form-group col-lg-12 text-right">
                            <div style="width:70%; float:left;"><h4><label>TOTAL</label></h4></div>
                            <div style="width:30%; float:right;"><h4><b>$</b><label id="TotalSolicitudGastoSG">'.number_format($suma).'</label></h4></div>
                        </div>                        
                    <div>
                </div>';
    break;

    case 'getSolcitudGastoxAprobar':
        $IdSG=$_POST['IdSG'];
        $listaSolicGastos=$InstSolicitudGasto->BuscarSolicitudGastosxid($IdSG);
        echo json_encode($listaSolicGastos->fetch_array());
    break;

        // Modulo Aprobacion solicitud de gastos
    case 'InsertarAprobacionMultipleSolicitudGasto':
        require_once("../modulos/aprobacionSolicGasto/class/ClassaprobacionSolicGasto.php");
        $InstAprobacionSolictGasto= new Proceso_AprobacionSolicitudGastos($InstanciaDB);
        $SGparaAprobar=$_POST['SGparaAprobar'];
        $fecha=$_POST['fecha'];
        $responsable=$_POST['responsable'];
        $SGValoresSeleccion=$_POST['SGValoresSeleccion'];
        $observaciones=$_POST['observaciones'];
        $RegInsertado=0;
        for($i=0 ; $i< count($SGparaAprobar); $i++){
            $InsertarAprobacionSolicGastoCab=$InstAprobacionSolictGasto->InsertarAprobacionSolicitudGastos($SGparaAprobar[$i],$fecha,$responsable,$SGValoresSeleccion[$i],$observaciones);
            $CambioEstdoSG=$InstSolicitudGasto->CambiarEstadoSG($SGparaAprobar[$i],1);
            $RegInsertado++;
        }
        if($RegInsertado==count($SGparaAprobar)){
            echo 1;
        }else{
            echo 0;
        }

    break;

    case 'InsertarAprobacionSolicitudGasto':
        require_once("../modulos/aprobacionSolicGasto/class/ClassaprobacionSolicGasto.php");
        $InstAprobacionSolictGasto= new Proceso_AprobacionSolicitudGastos($InstanciaDB);
        
        $IdSolicitudGastoSG=$_POST['IdSolicitudGastoSG'];
        $FechaAprobacionSG=$_POST['FechaAprobacionSG'];
        $responsableSG=$_POST['responsableSG'];
        $VrAproSolicGastoSG=str_replace('$','',str_replace(',','',$_POST['VrAproSolicGastoSG']));

        $ObservacionesAprobacionSG=limpiar($_POST['ObservacionesAprobacionSG']);
        if($VrAproSolicGastoSG==''){
            $regSG=$InstSolicitudGasto->BuscarSolicitudGastosxid($IdSolicitudGastoSG);
            $rowSG=$regSG->fetch_array();
            $VrAproSolicGastoSG=$rowSG[9];
            $ObservacionesAprobacionSG="Se aprueba valor total de la solicitud de gastos";
        }
        
        $InsertarAprobacionSolicGastoCab=$InstAprobacionSolictGasto->InsertarAprobacionSolicitudGastos($IdSolicitudGastoSG,$FechaAprobacionSG,$responsableSG,$VrAproSolicGastoSG,$ObservacionesAprobacionSG);
        
        if($InsertarAprobacionSolicGastoCab>0){
            $CambioEstdoSG=$InstSolicitudGasto->CambiarEstadoSG($IdSolicitudGastoSG,1);
            echo 1;             
        }else{
        echo 0;
        }
    break;

    case 'EditarAprobacion':
        require_once("../modulos/AprobacionSolicGasto/class/ClassaprobacionSolicGasto.php");
        $InstAprobacionSG=new Proceso_AprobacionSolicitudGastos($InstanciaDB);
        $IdAprobacion=limpiar($_POST['IdAprobacion']);
        $IdSolicitud=limpiar($_POST['IdSolicitud']);
        $FechaAprobariz=limpiar($_POST['FechaAprobariz']);
        $usuarioAprobariz=primera_mayuscula(limpiar($_POST['usuarioAprobariz']));
        $valorAprobariz=limpiar($_POST['valorAprobariz']);

        $DatosProgramaDB=$InstAprobacionSG->BuscarAprobarSolicitudGastosxid($IdAprobacion);
        $rowDB=$DatosProgramaDB->fetch_array(MYSQLI_BOTH);
        $diferenciaAprobariz=strcmp($IdAprobacion,$rowDB[0]);               
        $cambio=true;
        if ($diferenciaAprobariz ==0) {
            echo 0;
            $cambio=false;
        } else {
            if($cambio){
                $actualizarprograma=$InstAprobacionSG->EditarAprobacSolicitudGastos($IdAprobacion,$IdSolicitud,$FechaAprobariz,$usuarioAprobariz,$valorAprobariz);
                echo 1;
            }else{
                echo 3;
            }
        }
    break;

    case 'BuscarAprobacionSG':
        require_once("../modulos/aprobacionSolicGasto/class/ClassaprobacionSolicGasto.php");
        $InstAprobacionSG=new Proceso_AprobacionSolicitudGastos($InstanciaDB);
        $IdSolicitudGastoSG=$_POST['idSG'];
        $ListaDocAprobacion=$InstAprobacionSG->BuscarAprobarSolicitudGastosxid($IdSolicitudGastoSG);
        echo json_encode($ListaDocAprobacion->fetch_array());
    break;

    case 'ListarCABSG':
        $IdSG=$_POST['IdSG'];
        $ListaCABSG=$InstSolicitudGasto->ObtenerCABdeIdSG($IdSG);
        echo json_encode($ListaCABSG->fetch_array());
    break;
    
    case 'ListarCABRG':
        require_once('../modulos/legalizaciongastos/class/ClassLegalizacionGastos.php');
        $InstLegalizacionGasto=new Proceso_LegalizacionGastos($InstanciaDB);
        $IdRG=$_POST['IdRG'];
        $ListarCABRG=$InstLegalizacionGasto->ListaCABLegalizacionGastoxIdRG($IdRG);
        echo json_encode($ListarCABRG->fetch_array());
    break;
    

    case 'ListarDetalleSGLegalizacion':
        require_once("../modulos/aprobacionSolicGasto/class/ClassaprobacionSolicGasto.php");
        require_once('../modulos/legalizaciongastos/class/ClassLegalizacionGastos.php');
        require_once('../modulos/solicitud_gastos/class/ClassSolicitudGastos.php');
        $InstSolicitudGasto= new Proceso_SolicitudGastos($InstanciaDB);
        $InstAprobacionSG=new Proceso_AprobacionSolicitudGastos($InstanciaDB);
        $InstLegalizacionGasto=new Proceso_LegalizacionGastos($InstanciaDB);
       
        
        $IdSG=limpiar($_POST['IdSG']);
        $IdRG=limpiar($_POST['IdRG']);
        $CABSG=$InstSolicitudGasto->ObtenerCABdeIdSG($IdSG)->fetch_array();
        $CodMunicipio=$CABSG[5];
        $ListaVeredasxMunicipio=$InstVereda->ListaVeredasxMunicipio($CodMunicipio);
        
        
        //busca la Aprobacion X la solicitud de Gastos para optener el valor  Aprobado
        $AprobacionGasto=$InstAprobacionSG->BuscarAprobarSolicitudGastosxid($IdSG);
        $ResulsetAprobacionGasto=$AprobacionGasto->fetch_array();
        $ValorAprobacionGasto=$ResulsetAprobacionGasto[4];
        
        //Busca los datos de CAB de la Aprobacion de gastos y obtiene el valor q hasta el momento esta Aprobado.
        $LegalizacionGasto=$InstLegalizacionGasto->ListaCABLegalizacionGastoxIdRG($IdRG);
        
        if($LegalizacionGasto->num_rows > 0){
            $ResulsetLegalizacionGasto=$LegalizacionGasto->fetch_array();
            $ValorLegalizacion=$ResulsetLegalizacionGasto[4];
        }else{
            $ValorLegalizacion=0;
        }
            echo '<div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="table-responsive">
                        <!-- Row start -->
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading clearfix">
                                            <h3 class="panel-title">Detalle Solicitud de Gastos</h3>
                                        </div>
                                        
                                        <div class="panel-body">
                                            <div class="row">
                                                <table class="table table-striped  table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">Item</th>
                                                            <th>Vereda</th>
                                                            <th>Concepto Gasto</th>
                                                            <th class="text-center">Cant</th>
                                                            <th class="text-center">Valor Legalizado</th>
                                                            <th class="text-center">
                                                                <span class="glyphicon glyphicon-cog" title="Config"></span> 
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody class="items">';
                                                        $ListaDetalleSG=$InstSolicitudGasto->ListarDetallexidSolicitud($IdSG);
                                                        $items=1;
                                                        $suma=0;
                                                        $valorDetalleRG=0;
                                                        if ($ListaDetalleSG->num_rows > 0) {
                                                            $tienerelacion=false;
                                                            while($rowSG=$ListaDetalleSG->fetch_array()){
                                                            if($rowSG[5]==1){
                                                                $tienerelacion=true;
                                                                $DetalleLegalizacionGasto=$InstLegalizacionGasto->ListaDetalleLegalizacionGastoxIddetalleSG($rowSG[0]);
                                                                $rowBRG=$DetalleLegalizacionGasto->fetch_array();
                                                                $idDetalleRG=$rowBRG[0];
                                                                $valorDetalleRG=$rowBRG[6];
                                                            }else{
                                                                $idDetalleRG=0;
                                                                $tienerelacion=false;
                                                                $valorDetalleRG=0;
                                                            }
                                                                $NombreConcepto=$InstSolicitudGasto->getnombreconceptoGasto($rowSG[2]);
                                                                $SubTotal=(intval($rowSG[3])*intval($rowSG[4]));
                                                            echo '
                                                                <tr>
                                                                    <td class="text-center">
                                                                        <div id="IdDetalleSG'.$rowSG[0].'" style="display:none;">'.$rowSG[0].'</div>
                                                                        <div id="numitem'.$idDetalleRG.'" style="display:none;">'.$items.'</div>
                                                                        <div id="numitem'.$rowSG[0].'">'.$items.'</div>
                                                                    </td>
                                                                    <td>
                                                                        <div id="IdVereda'.$rowSG[0].'" style="display:none;">'.$rowSG[6].'</div>
                                                                        <div id="Vereda '.$rowSG[0].'">'.$rowSG[7].'</div>
                                                                    </td>
                                                                    <td>
                                                                        <div id="NombreConcepto'.$rowSG[0].'">'.$NombreConcepto.'</div>
                                                                        <div id="IdConcepto'.$rowSG[0].'" style="display:none;">'.$rowSG[2].'</div>
                                                                        <div id="NombreConcepto'.$idDetalleRG.'" style="display:none;">'.$NombreConcepto.'</div>
                                                                    </td>
                                                                    <td class="text-center"><div id="numdias'.$rowSG[0].'"> '.$rowSG[3].'</div></td>
                                                                    <td class="text-center"><div id="ValorLegalizado'.$rowSG[0].'">$ '.number_format($valorDetalleRG).'</div></td>
                                                                    
                                                                    <th class="text-center">';
                                                                    
                                                                        if(!$tienerelacion){
                                                                            echo '<button type="button" class="btn btn-xs" onclick="mostrarlegalizaciondegastos('.$idDetalleRG.','.$rowSG[0].',0);" id="UpLoadLegalizacion" data-toggle="modal" data-target="#ModalUpLoadLegalizacion">
                                                                            <span class="glyphicon glyphicon-open" style="color:black;"></span>
                                                                            </button>';
                                                                        }else{
                                                                            echo '
                                                                            <button type="button" class="btn btn-xs" onclick="mostrarLGAdicionalxDetalleIdRG('.$idDetalleRG.');" id="UpLoadLegalizacion" data-toggle="modal" data-target="#ModalUpLoadLegalizacion" style="background:green;color:white;">
                                                                            <span class="glyphicon glyphicon glyphicon-check"></span>
                                                                            </button>';
                                                                        }
                                                                    echo '</th>
                                                                </tr>
                                                                ';
                                                                $items++;
                                                            }
                                                        }
                                                        echo '
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <!-- Row end -->

                        <!-- Lista Detalle de Conceptos Adicionales-->                            
                            <div id="DetalleConceptoAdicionales">';
                                    $DetalleLegalizacionGastoAdicionales=$InstLegalizacionGasto->ListaDetalleConceptosAdicionalesRGxIdRG($IdRG);
                                    if($DetalleLegalizacionGastoAdicionales->num_rows != 0){
                                        echo '
                                        <!-- Row start -->
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading clearfix">
                                                            <h3 class="panel-title">Concepto de Gasto Adicionales</h3>
                                                        </div>
                                                        
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-md-8 col-sm-8">
                                                                    <table class="table table-striped  table-hover">
                                                                        <thead>
                                                                            <tr>
                                                                                <th class="text-center">Item</th>
                                                                                <th class="text-left">Vereda</th>
                                                                                <th class="text-left">Concepto Gasto</th>
                                                                                <th class="text-center">Valor Legalizado</th>
                                                                                <th class="text-center">
                                                                                    <span class="glyphicon glyphicon-cog" title="Config"></span> 
                                                                                </th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody class="items">';
                                                                        
                                                                        while($rowRGAd=$DetalleLegalizacionGastoAdicionales->fetch_array()) {
                                                                            $NombreConcepto=$InstSolicitudGasto->getnombreconceptoGasto($rowRGAd[2]);
                                                                            echo '
                                                                            <tr>
                                                                                <td class="text-center">
                                                                                    <div id="numitem'.$rowRGAd[0].'">'.$items.'</div>
                                                                                    <div id="IdDetalleSG'.$rowRGAd[0].'" style="display:none;">'.$rowRGAd[0].'</div>
                                                                                </td>
                                                                                <td class="text-left">'.$rowRGAd[16].'</td>
                                                                                <td class="text-left">
                                                                                    <div id="NombreConcepto'.$rowRGAd[0].'">'.$NombreConcepto.'</div>
                                                                                    <div id="IdConcepto'.$rowRGAd[0].'" style="display:none;">'.$rowRGAd[2].'</div>
                                                                                </td>
                                                                                <td class="text-right"><div id="ValorLegalizado'.$rowRGAd[0].'">$ '.number_format($rowRGAd[6]).'</div></td>
                                                                                <th class="text-center">
                                                                                <button type="button" style="background:green;color:white;" class="btn btn-xs" onclick="mostrarLGAdicionalxDetalleIdRG('.$rowRGAd[0].');" id="UpLoadLegalizacion" data-toggle="modal" data-target="#ModalUpLoadLegalizacion">
                                                                                <span class="glyphicon glyphicon glyphicon-check"></span>
                                                                                </button>
                                                                                </th>
                                                                            </tr>
                                                                            ';
                                                                            
                                                                            $items++;
                                                                        }
                                                                        echo '</tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <!-- Row end -->';
                                    }
                        echo '</div>';                                   

                        $Saldo=$ValorAprobacionGasto-$ValorLegalizacion;
                        
                        echo '<!-- Row start -->
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading clearfix">
                                    <div class="col-sm-6">
                                    <h3 class="panel-title">Insertar Concepto de Gasto Adicionales</h3>
                                </div>

                                    </div>
                                    
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6">
                                            <div class="col-md-2" style="border:0px; padding:0px;"><label>Concepto:</label></div>
                                                <div class="col-lg-9">
                                                    <div class="input-group">
                                                        <select  class="form-control" name="ConceptoGastoRG" id="ConceptoGastoRG">
                                                            <option value=0> --- Seleccione un concepto de Gasto --- </option>';
                                                            $listaConceptoGasto=$InstConceptoGasto->ListarConceptoGastos();
                                                            while ($rowCG=$listaConceptoGasto->fetch_array()) {
                                                                echo "<option value='".$rowCG[0]."'>".$rowCG[2]."</option>";
                                                            }                
                                                                echo '
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-5 col-md-5">
                                                <div class="col-md-2" style="border:0px; padding:0px;"><label>Vereda:</label></div>
                                                <div class="col-lg-9">
                                                    <select class="form-control" name="CodVeredaSG" id="CodVeredaSG" style="width:280px;">
                                                        <option value="0"> -- Seleccione Vereda -- </option>';
                                                            while ($rowVer=$ListaVeredasxMunicipio->fetch_array(MYSQLI_BOTH)) {
                                                                echo "<option value='".$rowVer[0]."'>".$rowVer[1]."</option>";
                                                            }
                                            echo '
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-1 col-sm-1">
                                                <div class="input-group">
                                                    <button type="button" id="AgregarLegalizacionGasto" onclick="mostrarlegalizaciondegastos(0,0,1);" data-toggle="modal" data-target="#ModalUpLoadLegalizacion"><span class="glyphicon glyphicon-arrow-up"></span></button> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!-- Row end -->';

                        echo '
                        <div class="form-group col-lg-12 text-right">
                            <div style="width:70%; float:left;"><h4><label>Valor Aprobacion Gasto: </label></h4></div>
                            <div style="width:30%; float:right;"><h4><b></b><label id="TotalSolicitudGastoSG">$ '.number_format($ValorAprobacionGasto).'</label></h4></div>
                        </div>
                        <div class="form-group col-lg-12 text-right">
                            <div style="width:70%; float:left;"><h4><label>Valor Gastos Legalizados: </label></h4></div>
                            <div style="width:30%; float:right;"><h4><b></b><label id="TotalSolicitudGastoSG">$ '.number_format($ValorLegalizacion).'</label></h4></div>
                        </div>
                        <div class="form-group col-lg-12 text-right">
                            <div style="width:70%; float:left;"><h4><label>Saldo (Valor Aprobacion - Valor Legalizado): </label></h4></div>
                            <div style="border-top-style: double;border-top-width: 2px;width:30%; float:right; color:';
                            if($Saldo < 0 ){
                                echo 'red';
                            }else{
                                echo 'black';
                            }
                            echo ';"><h4><b></b><label id="TotalSolicitudGastoSG">$ '.number_format($Saldo).'</label></h4></div>
                        </div>
                    <div>
                </div>';
    break;

    case 'DetalleConceptosAdicionales':
        require_once('../modulos/legalizaciongastos/class/ClassLegalizacionGastos.php');
        $InstLegalizacionGasto=new Proceso_LegalizacionGastos($InstanciaDB);
        $IdRG=limpiar($_POST['IdRG']);
        

    break;

    case 'CargarResponsablesSelect2':
        $i=0;
        $IdSG=limpiar($_POST['IdSG']);
        $empleados = array();
        $Listaempleados=$InstSolicitudGasto->ListaResponsablesxIDsolicitud($IdSG);
        while($rowEMSG=$Listaempleados->fetch_array()){
            $empleados[$i]=intval($rowEMSG[2]);
            $i++;
        }
        echo json_encode($empleados);
    break;

    case 'GuardarLegalizacionSG':
        require_once("../modulos/legalizaciongastos/class/ClassLegalizacionGastos.php");
        $InstLegalizacionSG=new Proceso_LegalizacionGastos($InstanciaDB);
        $IdSG=$_POST['IdSG'];
        $ObservacionesLegalizacionGasto=limpiar($_POST['ObservacionesLegalizacionGasto']);
        $UsuarioLegalizacionGastos=$_POST['UsuarioLegalizacionGastos'];
        $FechaLegalizacionGastos=$_POST['FechaLegalizacionGastos'];
        $IDGuardadoLegalizacionSG=$InstLegalizacionSG->InsertarCabLegalizacionGasto($IdSG,$FechaLegalizacionGastos,$ObservacionesLegalizacionGasto,$UsuarioLegalizacionGastos);
        $CambioestadoSG=$InstSolicitudGasto->CambiarEstadoSG($IdSG,3);
        echo json_encode($IDGuardadoLegalizacionSG);
    break;

    case 'GuardarDetalleLegalizacion':
        require_once("../modulos/legalizaciongastos/class/ClassLegalizacionGastos.php");
        $InstLegalizacionSG=new Proceso_LegalizacionGastos($InstanciaDB);
        $IdRG=intval($_POST['IdRG']);
        $IdVereda=$_POST['IdVereda'];
        $IdConcepto=$_POST['IdConcepto'];
        $IdDetalleSG=$_POST['IdDetalleSG'];
        $IdDetalleRG=$_POST['IdDetalleRG'];
        $NitBeneficiario=limpiar($_POST['NitBeneficiario']);
        $NombreBeneficiario=limpiar($_POST['NombreBeneficiario']);
        $NumeroFactura=limpiar($_POST['NumeroFactura']);
        $ValorFactura=str_replace(',','',limpiar($_POST['ValorFactura']));
        $PagoTCD=$_POST['PagoTCD'];
        $EntregaRUT=$_POST['EntregaRUT'];
        $EntregaCedula=$_POST['EntregaCedula'];
        $Observaciones=limpiar($_POST['Observaciones']);
        $DireccionBeneficiario=limpiar($_POST['DireccionBeneficiario']);
        $TelefonoBeneficiario=limpiar($_POST['TelefonoBeneficiario']);
        $EntregaRUT=$_POST['EntregaRUT'];
        $EntregaCedula=$_POST['EntregaCedula'];
        $Input_imagen=$_POST['Input_imagen'];
        $Imagen_Cargada=$_POST['Imagen_Cargada'];
        $GuardarDetalleLegalizacion=0;
        //echo ("El valor del input:".$Input_imagen."<br>El valor de etiqueta img: ".$Imagen_Cargada);
        
        if($IdDetalleRG == 0){
            $GuardarDetalleLegalizacion=$InstLegalizacionSG->InsLineaDetalleReacionSG($IdRG,$IdConcepto,$NitBeneficiario,$NombreBeneficiario,$NumeroFactura,$ValorFactura,$PagoTCD,$Observaciones,$IdDetalleSG,$EntregaRUT,$EntregaCedula,$DireccionBeneficiario,$TelefonoBeneficiario,$IdVereda);
        }else{  
            $GuardarDetalleLegalizacion=$InstLegalizacionSG->UpdateDetalleLG($IdDetalleRG,$NitBeneficiario,$NombreBeneficiario,$NumeroFactura,$ValorFactura,$PagoTCD,$Observaciones,$EntregaRUT,$EntregaCedula,$DireccionBeneficiario,$TelefonoBeneficiario);
            if($GuardarDetalleLegalizacion > 0 ){
                $GuardarDetalleLegalizacion=$IdDetalleRG;
            }
        }
        $ActualizaValorenCabLegalizacion=$InstLegalizacionSG->UpdateValorCABRG($IdRG);

        if($GuardarDetalleLegalizacion>0){

            if($IdDetalleSG != 0){
                $CambioEstadoDetalle=$InstSolicitudGasto->CambiarEstadoDetalleSG($IdDetalleSG,1);
            }

            if($Input_imagen > 0){
                $ruta_Archivo = '../archivos_soporte/legalizacionGastos/'.$IdRG;
                $NombreTMP_Archivo=$_FILES['Soporte_Legalizacion']['tmp_name'];
                $Tamaño_Archivo=$_FILES['Soporte_Legalizacion']['size'];
                $target_file = $ruta_Archivo . basename($_FILES['Soporte_Legalizacion']["name"]);
                $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
                
                if (!file_exists($ruta_Archivo)) {
                        mkdir($ruta_Archivo, 0777, true);
                }
                
                $uploadfile = $ruta_Archivo."/".$GuardarDetalleLegalizacion."_".$NitBeneficiario.".".$imageFileType;
                
                if (move_uploaded_file($NombreTMP_Archivo, $uploadfile)) {
                    //Modifica ruta para Guardar de la imagen en la tabla del registro.
                    $RutaImagen='../../archivos_soporte/legalizacionGastos/'.$IdRG."/".$GuardarDetalleLegalizacion."_".$NitBeneficiario.".".$imageFileType;
                    $UpdateRutaImagen=$InstLegalizacionSG->UpdateRutaImagen($GuardarDetalleLegalizacion,$RutaImagen);
                    if($UpdateRutaImagen > 0){
                        echo 1;
                    }else{
                        echo 0;
                    }
                }else{
                    echo 0;        
                }
            }else{
                echo 1;
            }

        }else{
                echo 0;
        }
    break;

    case 'GetnombreconceptoLG':
        $IdConcepto=$_POST['IdConcepto'];
        $nombreConcepto=$InstSolicitudGasto->getnombreconceptoGasto($IdConcepto);
        echo json_encode($nombreConcepto);
    break;

    case 'MostrarDetalleLegalizacionSG':
        require_once("../modulos/legalizaciongastos/class/ClassLegalizacionGastos.php");
        $InstLegalizacionSG=new Proceso_LegalizacionGastos($InstanciaDB);
        $IddetalleLegalizacionSG=intval($_POST['IdDetalleRG']);
        $ListaDetalleLegalizacionGasto=$InstLegalizacionSG->BuscarLegalizacionGastoxIdDetalleSG($IddetalleLegalizacionSG);
        echo json_encode($ListaDetalleLegalizacionGasto->fetch_array(MYSQLI_NUM));
    break;

    case'ArchivarSG':
        require_once('../modulos/solicitud_gastos/class/ClassSolicitudGastos.php');
        $InstSG=new Proceso_SolicitudGastos($InstanciaDB);
        $IdSG=$_POST['IdSG'];
        $fechaarchivo=$_POST['fecha'];
        $IdUsuariologin=$_POST['IdUsuariologin'];
        $UpdateEstadoArchivarSG=$InstSG->ArchivarSG($IdSG,$IdUsuariologin,$fechaarchivo);
        echo 1;
    break;

    case'ArchivarmultiplesSG':
        require_once('../modulos/solicitud_gastos/class/ClassSolicitudGastos.php');
        $InstSG=new Proceso_SolicitudGastos($InstanciaDB);
        $data = json_decode(stripslashes($_POST['SGS']));
        $usuarioArchivo=$_POST['usuarioarchivo'];
        $fechaarchivo=$_POST['fechaarchivo'];
        
        foreach($data as $datav){
            $UpdateEstadoArchivarSG=$InstSG->ArchivarSG($datav,$usuarioArchivo,$fechaarchivo);
        }
        unset($datav);
        if($UpdateEstadoArchivarSG==count($data)){
            echo 1;
        }else{
            echo 0;
        }
        
    break;

    case 'mostrarDLGxDIDLG':
        require_once("../modulos/legalizaciongastos/class/ClassLegalizacionGastos.php");
        $InstLegalizacionRG=new Proceso_LegalizacionGastos($InstanciaDB);
        $IdDetalleRG=intval($_POST['IdDetalleRG']);
        $ListaDetalleRG=$InstLegalizacionRG->mostrarLegalizaciondegastosxDetalleIdRG($IdDetalleRG);
        echo json_encode($ListaDetalleRG->fetch_array(MYSQLI_NUM));
    break;

    case 'LlenaSelectMunicipioSegunDepartamento':
        $CodDepartamentoSG=limpiar($_POST['CodDepartamentoSG']);
        $ListaMunicipioSegunDepartamento=$InstMcpio->ListarMunicipiosxDepartamento($CodDepartamentoSG);
        $texto='<option value="0">---Seleccione Municipio ---</option>';
        while ($rowVer=$ListaMunicipioSegunDepartamento->fetch_array(MYSQLI_NUM)) {
            $texto.="<option value='".$rowVer[0]."'>".$rowVer[2]."</option>";
        }
        echo json_encode($texto);
    break;

    case 'CargarResponsablesSelect2_InformeInstalacion':
        require_once("../modulos/informeInstalacion/class/classInformeInstalacion.php");
        $InstinformeInstalacion=new Proceso_InformeInstalacion($InstanciaDB);        
            $i=0;
            $idRegistroEntregaColeccion=limpiar($_POST['idRegistroEntregaColeccion']);
            $empleados = array();
            $Listaempleados=$InstRegEntregaColeccion->ListarEmpledosxidRegistroEntregaColeccion($idRegistroEntregaColeccion);
            while($rowEMSG=$Listaempleados->fetch_array()){
                $empleados[$i]=intval($rowEMSG[2]);
                $i++;
            }
            echo json_encode($empleados);
    break;

    case 'PatrocinadorSegunMunicipio':
            require_once("../modulos/informeInstalacion/class/classInformeInstalacion.php");
            $InstinformeInstalacion=new Proceso_InformeInstalacion($InstanciaDB);
            $idMunicipio=limpiar($_POST['idMunicipio']);
            $ListaEntidadesxMunicipio=$InstinformeInstalacion->DonantesxMunicipio($idMunicipio);
            $texto='<option value="0">---Seleccione Patrocinador ---</option>';
            while ($rowVer=$ListaEntidadesxMunicipio->fetch_array(MYSQLI_BOTH)) {
                $texto.="<option value='".$rowVer[4]."'>".$rowVer[13]."</option>";
            }
            echo json_encode($texto);
    break;
    
    case 'LlenarSelectInstitucionesxVeredas':
        require_once("../modulos/institucion/class/classInstitucion.php");
        $InstInstitucion=new Proceso_Institucion($InstanciaDB);
        $IdVereda=limpiar($_POST['IdVereda']);
        $ListaInstitucionesxVereda=$InstInstitucion->ListaInstitucionxVereda($IdVereda);
        $texto='<option value="0">---Seleccione Institucion ---</option>';
        while ($rowVer=$ListaInstitucionesxVereda->fetch_array(MYSQLI_BOTH)) {
            $texto.="<option value='".$rowVer[0]."'>".$rowVer[2]."</option>";
        }
        echo json_encode($texto);
    break;

    case 'LlenaSelectInstitucionsegunVereda':
        $CodVereda=limpiar($_POST['CodVereda']);
        $ListaInstitucionSegunVereda=$InstInstitucion->listaInstitucionxVereda($CodVereda);
        $texto='<option value="0">---Seleccione Institucion ---</option>';
            while ($rowVer=$ListaInstitucionSegunVereda->fetch_array(MYSQLI_BOTH)) {
                $texto.="<option value='".$rowVer[0]."'>".$rowVer[2]."</option>";
            }
        echo json_encode($texto);
    break;
    
    //Asignacion Donantes
    
    case 'GuardarAsignacionDonante':
        require_once("../modulos/donantes/class/ClassDonantes.php");
        $InstDonantes= new Proceso_Donantes($InstanciaDB);

        $IdProyecto=$_POST['IdProyecto'];
        $fechaInicioDonante=$_POST['fechaInicioDonante'];
        $fechaFinalDonante=$_POST['fechaFinalDonante'];
        $idEntidad=$_POST['idEntidad'];
        $Cuantia=str_replace(',','',limpiar($_POST['Cuantia']));
        $TipoVinculacion=limpiar($_POST['TipoVinculacion']);
        $observaciones=limpiar($_POST['observaciones']);                   
        $CodMunicipio=$_POST['CodMunicipio'];

        $InsertarDonante=$InstDonantes->InsertDonante($IdProyecto,$fechaInicioDonante, 
        $fechaFinalDonante, $idEntidad,$Cuantia,$TipoVinculacion,$observaciones);
        
        if($InsertarDonante>0){
            for($i=0; $i<count($CodMunicipio); $i++){
                $InsertarResponsbleSG=$InstDonantes->InstdetalleMunicipio($InsertarDonante, $CodMunicipio[$i]);
            }
            echo 1;
        }else{
            echo 0;
        }
    break;

    case 'CargarMunicipiosAsignacionDonantes':
        require_once("../modulos/donantes/class/ClassDonantes.php");
        $InstDonantes= new Proceso_Donantes($InstanciaDB);
        $i=0;
        $IdasignacionDonantes=$_POST['IdasignacionDonantes'];
        $municipios = array();
        $Listamunicipiosdonantes=$InstDonantes->listadetallemunicipioxidasignaciondonante($IdasignacionDonantes);
        while($rowEMSG=$Listamunicipiosdonantes->fetch_array()){
            $municipios[$i]=intval($rowEMSG[2]);
            $i++;
        }
        echo json_encode($municipios);
    break;
    case 'EditarAsignacionDonante':
        require_once("../modulos/donantes/class/ClassDonantes.php");
        $InstDonantes= new Proceso_Donantes($InstanciaDB);

        $IdasignacionDonante=$_POST['IdReg'];
        $idProyectoAsignacionDonante=($_POST['IdProyecto']);
        
        $FechaInicioAsignacionDonante=limpiar($_POST['fechaInicioDonante']);
        $FechaFinalizacionAsignacionDonante=limpiar($_POST['fechaFinalDonante']);
        $idEntidadVinculadaAsignacionDonante=($_POST['idEntidad']);
        $CuantiaAsignacionDonante=str_replace(',','',limpiar($_POST['Cuantia']));
        $TipoVinculacionAsignacionDonante=limpiar($_POST['TipoVinculacion']);
        $MunicipiosDonante=($_POST['VerCodMunicipio']);
        
        $DatosProgramaDB=$InstDonantes->ListaDonantesxID($IdasignacionDonante);
        $rowDB=$DatosProgramaDB->fetch_array(MYSQLI_NUM);
        $diferenciaIdProyecto=strcmp($idProyectoAsignacionDonante,$rowDB[1]);
        $diferenciaFecha1=strcmp($FechaInicioAsignacionDonante,$rowDB[2]);
        $diferenciaFecha2=strcmp($FechaFinalizacionAsignacionDonante,$rowDB[3]);
        $diferenciaIdEntidad=strcmp($idEntidadVinculadaAsignacionDonante,$rowDB[4]);
        $diferenciaCuantia=strcmp($CuantiaAsignacionDonante,$rowDB[5]);
        $diferenciaTipoVinculacion=strcmp($TipoVinculacionAsignacionDonante,$rowDB[6]);
        
        $cambio=true;
        if ($diferenciaIdProyecto==0 && $diferenciaFecha1==0  && $diferenciaFecha2==0 && $diferenciaIdEntidad==00 && $diferenciaCuantia==00 && $diferenciaTipoVinculacion==00) {
            $cambio=false;
        }else{
            switch (true) {
                case ($diferenciaIdProyecto != 0):
                    $cambio=true;
                break;
                case ($diferenciaFecha1 != 0 ):
                    $cambio=true;
                break;
                case($diferenciaFecha2 != 0):
                    $cambio=true;
                break;
                case ($diferenciaIdEntidad != 0):
                    $cambio=true;
                break;
                case ($diferenciaCuantia != 0 ):
                    $cambio=true;
                break;
                case($diferenciaTipoVinculacion != 0):
                    $cambio=true;
                break;
                default:
                    $cambio=false;
                    break;
                }
        }
        if($cambio=true){
            $EditarAsignacionDonante=$InstDonantes->EditarAsignacionDonante($IdasignacionDonante,$idProyectoAsignacionDonante, $FechaInicioAsignacionDonante, $FechaFinalizacionAsignacionDonante, $idEntidadVinculadaAsignacionDonante,$CuantiaAsignacionDonante,$TipoVinculacionAsignacionDonante);
            $eliminarAsignacionDonantesMunicipios=$InstDonantes->BorrraregistrosAsignacionDonante($IdasignacionDonante);
            for($i=0; $i<count($MunicipiosDonante); $i++){
                    $InsertarmunicipiosDonante=$InstDonantes->InstdetalleMunicipio($IdasignacionDonante, $MunicipiosDonante[$i]);
            }
            
            echo 1;
        }else{
            echo 0;
        }           
    break;

    
    // modulo Alumnos
    case 'InsertAlumno':
        require_once("../modulos/alumnos/class/classAlumnos.php");
        $InstAlumnos=new Proceso_Alumnos($InstanciaDB);

        $codAlumno=limpiar($_POST['codAlumno']);
        $descAlumno=primera_mayuscula(limpiar($_POST['descAlumno']));
        $estado=limpiar($_POST['estado']);
        $fechanac=limpiar($_POST['fechanac']);
        $IdInstituc=limpiar($_POST['IdInstituc']);
        $alumnoExiste=$InstAlumnos->BuscarAlumnosxCod($codAlumno);

        if($alumnoExiste->num_rows==0){
                $Alumnoinsertado=$InstAlumnos->InsertAlumno($codAlumno,$descAlumno,$estado,$fechanac,$IdInstituc);
                if($Alumnoinsertado>0){
                    echo 1;
                }else{
                    echo 0;
                }
            }else{
                echo 0;
            }
    break;

    case 'EditarAlumno':
        require_once("../modulos/alumnos/class/classAlumnos.php");
        $InstAlumnos=new Proceso_Alumnos($InstanciaDB);
        $IdAlumno=limpiar($_POST['IdAlumno']);
        $codAlumno=limpiar($_POST['codAlumno']);
        $nombAlumno=primera_mayuscula(limpiar($_POST['nombAlumno']));
        $estAlumno=primera_mayuscula(limpiar($_POST['estAlumno']));
        $fechanac=limpiar($_POST['fechanac']);
        $idInstitucion=limpiar($_POST['idInstitucion']);
        
        $DatosProgramaDB=$InstAlumnos->BuscarAlumnos($IdAlumno);
        $rowDB=$DatosProgramaDB->fetch_array(MYSQLI_BOTH);
        $diferenciaIdAlumno=strcmp($IdAlumno,$rowDB[2]);              
        $cambio=true;
        if ($diferenciaIdAlumno==0){
        echo 0;
        $cambio=false;
        } else {
            if($cambio){
                $actualizarprograma=$InstAlumnos->EditarAlumno($IdAlumno,$codAlumno,$nombAlumno,$estAlumno,$fechanac,$idInstitucion);
                echo 1;
            }else{
                echo 3;
            }
        }
        
    break;

            // modulos Empleados

    case 'InsertEmpleado':
        require_once("../modulos/empleados/class/classEmpleados.php");
        $InstEmpleados=new Proceso_Empleados($InstanciaDB);
        $DocEmpleado=limpiar($_POST['DocEmpleado']);
        $nomEmpleado=primera_mayuscula(limpiar($_POST['nomEmpleado']));
        $telEmpleado=limpiar($_POST['telEmpleado']);
        $cargoEmpl=limpiar($_POST['cargoEmpl']);
        $idArea=limpiar($_POST['idArea']);
        $estadoEmple=limpiar($_POST['estadoEmple']);
        $emplExiste=$InstEmpleados->BuscarEmpleado($DocEmpleado);

        if($emplExiste==true){
                $Empleadoinsertado=$InstEmpleados->InsertEmpleado($DocEmpleado,$nomEmpleado,$telEmpleado,$cargoEmpl,$idArea,$estadoEmple);
                if($Empleadoinsertado>0){
                    echo 1;
                }else{
                    echo 0;
                }
            }else{
                echo 0;
            }
    break;

    case 'EditarEmpleado':
        require_once("../modulos/empleados/class/classEmpleados.php");
        $InstEmpleados=new Proceso_Empleados($InstanciaDB);

        $idEmpleado=limpiar($_POST['idEmpleado']);
        $DocEmpleado=limpiar($_POST['DocEmpleado']);
        $nomEmpleado=primera_mayuscula(limpiar($_POST['nomEmpleado']));
        $DocEmpleado=limpiar($_POST['DocEmpleado']);
        $cargoEmpl=limpiar($_POST['cargoEmpl']);
        $idArea=limpiar($_POST['idArea']);
        $estadoEmple=limpiar($_POST['estadoEmple']);
        $actualizarprograma=$InstEmpleados->EditarEmpleado($idEmpleado,$DocEmpleado,$nomEmpleado,$DocEmpleado,$cargoEmpl,$idArea,$estadoEmple);
        echo 1;
        
    break;


    //Modulo de area
    case 'InsertArea':
        require_once("../modulos/area/class/ClassArea.php");
        $InstArea=new Proceso_Area($InstanciaDB);

        $descArea=primera_mayuscula(limpiar($_POST['descArea']));
        $estadoarea=primera_mayuscula(limpiar($_POST['estadoarea']));
        $AreaInsertado=$InstArea->InserArea($descArea,$estadoarea);
        if($AreaInsertado>0){
        echo 1;
        }else{
            echo 0;
            }
    break;

    case 'EditarArea':
        require_once("../modulos/area/class/ClassArea.php");
        $InstArea=new Proceso_Area($InstanciaDB);
        
        $IdArea=$_POST['idArea'];
        $descArea=primera_mayuscula(limpiar($_POST['desArea']));
        $estadoarea=(limpiar($_POST['estadoArea']));
        $actualizarprograma=$InstArea->EditarArea($IdArea,$descArea,$estadoarea);
                
        if($actualizarprograma > 0){
            echo 1;
        }else{
            echo 0;
        }

    break;

    //Modulo Entrega Anteojos
    case 'InsertEntregaAnteojos':
        require_once("../modulos/entregaAnteojos/class/classEntregaAnteojos.php");
        $InstEntregaAnt=new Proceso_EntregaAnteojos($InstanciaDB);

        $IdResponsableEntr=primera_mayuscula(limpiar($_POST['IdResponsableEntr']));
        $idVdaBenef=primera_mayuscula(limpiar($_POST['idVdaBenef']));
        $mcpioEntrega=primera_mayuscula(limpiar($_POST['mcpioEntrega']));
        $fechaEntrega=primera_mayuscula(limpiar($_POST['fechaEntrega']));
        $beneficiario=primera_mayuscula(limpiar($_POST['beneficiario']));
        $telBeneficiario=primera_mayuscula(limpiar($_POST['telBeneficiario']));
        $correoBeneficiario=primera_mayuscula(limpiar($_POST['correoBeneficiario']));
        $personaRecibe=primera_mayuscula(limpiar($_POST['personaRecibe']));
        $tipoAnteojos=primera_mayuscula(limpiar($_POST['tipoAnteojos']));

        $EntregaInsertado=$InstEntregaAnt->InsertarEntregaAnteojos($IdResponsableEntr,$idVdaBenef,$mcpioEntrega,$fechaEntrega,$beneficiario,$telBeneficiario,
        $correoBeneficiario,$personaRecibe,$tipoAnteojos);
        if($EntregaInsertado>0){
        echo 1;
        }else{
        echo 0;
        }
    break;

    case 'EditarEntrega':
        require_once("../modulos/entregaAnteojos/class/classEntregaAnteojos.php");
        $InstEntregaAnt=new Proceso_EntregaAnteojos($InstanciaDB);
        $idEntrega=limpiar($_POST["idEntrega"]);
        $IdResponsableEntr=primera_mayuscula(limpiar($_POST['IdResponsableEntr']));
        $mcpioEntrega=primera_mayuscula(limpiar($_POST['mcpioEntrega']));
        $fechaEntrega=primera_mayuscula(limpiar($_POST['fechaEntrega']));
        $beneficiario=primera_mayuscula(limpiar($_POST['beneficiario']));
        $telBeneficiario=primera_mayuscula(limpiar($_POST['telBeneficiario']));
        $correoBeneficiario=primera_mayuscula(limpiar($_POST['correoBeneficiario']));
        $personaRecibe=primera_mayuscula(limpiar($_POST['personaRecibe']));
        $tipoAnteojos=primera_mayuscula(limpiar($_POST['tipoAnteojos']));

        $rowDB=$DatosProgramaDB->fetch_array(MYSQLI_BOTH);
        $diferenciadescEntr=strcmp($idEntrega,$rowDB[1]);               
        $cambio=true;
        if ($diferenciadescEntr==0) {
        echo 0;
        $cambio=false;
        } else {
        if($cambio){
            $actualizarprograma=$InstEntregaAnt->EditarArea($idEntrega,$IdResponsableEntr,$mcpioEntrega.$fechaEntrega,$beneficiario,$telBeneficiario,$correoBeneficiario,$personaRecibe,$tipoAnteojos);
        echo 1;
        }else{
        echo 3;
        }
        }
    break;


    // ************ Modulo informe de instalacion ***************

    case 'InsertInformeInstalac':
        require_once("../modulos/informeInstalacion/class/classInformeInstalacion.php");
        $InstInformInstal=new Proceso_InformeInstalacion($InstanciaDB);
        $departamento=($_POST['departamento']);
        $municipio=(($_POST['municipio']));
        $patrocinador=(($_POST['patrocinador']));
        $responsables=$_POST['responsables'];
        
        $fecha=limpiar($_POST['fecha']);
        $vereda=(($_POST['vereda']));
        $centroEducRural=(($_POST['centroEducRural']));
        $NroFamilProgram=($_POST['NroFamilProgram']);
        $NroFamilEntreg=($_POST['NroFamilEntreg']);
        $InstEducativas=(($_POST['InstEducativas']));
        $nombre_docente=primera_mayuscula(limpiar($_POST['nombre_docente']));
        $telefono_docente=limpiar($_POST['telefono_docente']);
        $correo_docente=limpiar($_POST['correo_docente']);
        $private_id=$_POST['private_id'];
        $observaciones=primera_mayuscula(limpiar($_POST['observaciones']));

        $InfInstalacInsertado=$InstInformInstal->InsertarInformeInstalacion($departamento,$municipio,$patrocinador,$fecha,
        $vereda,$centroEducRural,$NroFamilProgram,$NroFamilEntreg,$InstEducativas,$nombre_docente,$telefono_docente,$correo_docente,$observaciones,$private_id);

        if($InfInstalacInsertado>0){
                for($i=0; $i<count($responsables); $i++){
                    $InsertarDetalleInformeInst=$InstInformInstal->AgregarDetalleInfoInstalacion($InfInstalacInsertado,$responsables[$i],0,0,0);
                }
                $ListaDetalleInformeInstTMP=$InstInformInstal->ListarDetalleInformeInstTMP($private_id);
                while($rowtmpInfInfra=$ListaDetalleInformeInstTMP->fetch_array(MYSQLI_BOTH)){
                $InsertarDetalleInformeInst=$InstInformInstal->AgregarDetalleInfoInstalacion($InfInstalacInsertado,$rowtmpInfInfra[1],$rowtmpInfInfra[2],$rowtmpInfInfra[3],1);
                }
                $LimpiarTMPInformeInst=$InstInformInstal->vaciarTMPDetalleInformeInfra($private_id);
            echo 1;
        }else{
            echo 0;
        }

    break;


    case 'InsertarDetalleTMPInfInstalac':
        require_once('../modulos/informeInstalacion/class/classInformeInstalacion.php');
        $InstInformeInst=new Proceso_InformeInstalacion($InstanciaDB);
        
        $idMaterialInformeInst=$_POST['idMaterialInformeInst'];
        $idTipoPoblacionInformeInst=$_POST['idTipoPoblacionInformeInst'];
        $CantInformeInst=limpiar($_POST['CantInformeInst']);
        $private_id=$_POST['private_id'];

        $BuscarIdMaterialDetalleInformeInstTMP=$InstInformeInst->BuscarxIdMaterial($idMaterialInformeInst,$idTipoPoblacionInformeInst,$private_id);

        if($BuscarIdMaterialDetalleInformeInstTMP->num_rows > 0 ){
            $rowDRM=$BuscarIdMaterialDetalleInformeInstTMP->fetch_array();
            $CantInformeInst+=$rowDRM[3];
            $SumaCantxIdMateriInfInst=$InstInformeInst->UpdateCantInformeInstTMP($idMaterialInformeInst,$CantInformeInst,$idTipoPoblacionInformeInst,$private_id);
            if($SumaCantxIdMateriInfInst>0){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            $InsertarDetalleInformeInstalacionTMP=$InstInformeInst->InsertarDetalleInformeInstTMP($idMaterialInformeInst,$idTipoPoblacionInformeInst,$CantInformeInst,$private_id);
            if($InsertarDetalleInformeInstalacionTMP>0){
                echo 1;
            }else{
                echo 0;
            }
        } 
    break;

    case 'EliminarDetalleInformeInstTMP':
        require_once("../modulos/informeInstalacion/class/classInformeInstalacion.php");
        $InstInformInstal=new Proceso_InformeInstalacion($InstanciaDB);
        $private_id=$_POST['private_id'];
        $idRegDetalleInformeInstTMP=$_POST['idRegDetalleInformeInstTMP'];

        $EliminarDetalleInformeInstTMP=$InstInformInstal->EliminarDetalleInformeInstTMP($idRegDetalleInformeInstTMP,$private_id);
        if($EliminarDetalleInformeInstTMP > 0){echo 1;}else{echo 0;}
    break;

    case 'EliminarDetalleInformeInst':
        require_once("../modulos/informeInstalacion/class/classInformeInstalacion.php");
        $InstInformInstal=new Proceso_InformeInstalacion($InstanciaDB);
        
        $idRegDetalleInformeInst=$_POST['idRegDetalleInformeInst'];
        $EliminarDetalleInformeInst=$InstInformInstal->EliminarDetalleInformeInst($idRegDetalleInformeInst);
        if($EliminarDetalleInformeInst > 0){echo 1;}else{echo 0;}
    break;

    case 'MostrarInformeInstalacion';
        require_once("../modulos/informeInstalacion/class/classInformeInstalacion.php");
        $InstInformInstal=new Proceso_InformeInstalacion($InstanciaDB);
        $idInfInstalacion=$_POST['idInfInstalacion'];
        $ListarInformeInstalacion=$InstInformInstal->BuscarInformeInstalacionxId($idInfInstalacion);
        echo json_encode($ListarInformeInstalacion->fetch_array(MYSQLI_NUM));
    break;

    case 'MostrarDetalleInformeInstalacion':
        require_once("../modulos/informeInstalacion/class/classInformeInstalacion.php");
        $InstInformInstal=new Proceso_InformeInstalacion($InstanciaDB);
        $idInfInstalacion=$_POST['idInfInstalacion'];
        $FuenteInfoInformeInst=$_POST['FuenteInfoInformeInst'];
        $i=0;
        $items=1;
        
        
        if($FuenteInfoInformeInst==0){
            //El detalle de empleados o responsables
            $responsables=array();
            $ListarInformeInstalacionResponsables=$InstInformInstal->ListarDetalleInfoInstalacionxCABxFuente($idInfInstalacion,$FuenteInfoInformeInst);
            mysqli_data_seek($ListarInformeInstalacionResponsables,0);
            while($rowdInforInst=$ListarInformeInstalacionResponsables->fetch_array(MYSQLI_NUM)){
                $responsables[$i] = intval($rowdInforInst[2]);
                $i++;
            }

            echo json_encode($responsables);

        }else{
            //El detalle es de materiales
            $ListarInformeInstalacionMateriales=$InstInformInstal->ListarDetalleInfoInstalacionxCABxFuente($idInfInstalacion,$FuenteInfoInformeInst);
            mysqli_data_seek($ListarInformeInstalacionMateriales,0);
            $texto=null;
            while($rowdInforInst=$ListarInformeInstalacionMateriales->fetch_array(MYSQLI_NUM)){
                $texto.= '
                <tr>
                    <td class="text-center"><div id="'.$rowdInforInst[0].'">'.$items.'</div></td>
                    <td><div id="IdMaterialDetalleInformeInst'.$rowdInforInst[0].'">'.$rowdInforInst[6].'</div></td>
                    <td><div id="IdTipoPoblacionDetalleInformeInst'.$rowdInforInst[0].'">'.$rowdInforInst[7].'</div></td>
                    <td class="text-center"><div id="CantidadDetalleInformeInst'.$rowdInforInst[0].'"> '.$rowdInforInst[4].'</div></td>
                    <td class="text-center">
                    <a href="#" onclick="EliminarDetalleInformeInst('.$rowdInforInst[0].','.$rowdInforInst[1].');">
                    <span class="glyphicon glyphicon-trash" style="color: red;"></span>
                </a>
                    </td>
                </tr>
                ';
                $items++;
            }
            echo json_encode($texto);
        }
    break;

    case 'InsertDetalleInformeInst':
        require_once("../modulos/informeInstalacion/class/classInformeInstalacion.php");
        $InstInformInstal=new Proceso_InformeInstalacion($InstanciaDB);
        $idInfInstalacion=$_POST['idInfInstalacion'];
        $idMaterial_EmpleadoInformeInst=$_POST['idMaterial_EmpleadoInformeInst'];
        $idTipoPoblacionInformeInst=$_POST['idTipoPoblacionInformeInst'];
        $CantEntregadaInformeInst=limpiar($_POST['CantEntregadaInformeInst']);
        
        $InsertarDetalleInformeInst=$InstInformInstal->AgregarDetalleInfoInstalacion($idInfInstalacion,$idMaterial_EmpleadoInformeInst,
        $idTipoPoblacionInformeInst,$CantEntregadaInformeInst,1);
        if($InsertarDetalleInformeInst>0){echo 1;}else{echo 0;}
    break;

    case'ListarDetalleInformeInstTPM':
        require_once("../modulos/informeInstalacion/class/classInformeInstalacion.php");
        $InstInformInstal=new Proceso_InformeInstalacion($InstanciaDB);
        $private_id=$_POST['private_id'];
        $string="'".$private_id."'";
        $ListaDetalleInformeInst=$InstInformInstal->ListarDetalleInformeInstTMP($private_id);
        $items=1;
        $SumaCant=0;
        while($rowDInstTMP=$ListaDetalleInformeInst->fetch_array(MYSQLI_NUM)){
            
            echo '
            <tr>
                <td class="text-center"><div id="'.$rowDInstTMP[0].'">'.$items.'</div></td>
                <td><div id="IdMaterialDetalleInformeInstTMP'.$rowDInstTMP[0].'">'.$rowDInstTMP[5].'</div></td>
                <td><div id="IdTipoPoblacionDetalleInformeInstTMP'.$rowDInstTMP[0].'">'.$rowDInstTMP[6].'</div></td>
                <td class="text-center"><div id="CantidadDetalleInformeInstTMP'.$rowDInstTMP[0].'"> '.$rowDInstTMP[3].'</div></td>
                <td class="text-center">
                <a href="#" onclick="EliminarDetalleInformeInstTMP('.$string.','.$rowDInstTMP[0].');">
                <span class="glyphicon glyphicon-trash" style="color: red;"></span>
            </a>
                </td>
            </tr>
            ';
            $items++;
            $SumaCant+=$rowDInstTMP[3];
        }
    break;

    case 'EditarInfInstalac':
        require_once("../modulos/informeInstalacion/class/classInformeInstalacion.php");
        $InstInformInstal=new Proceso_InformeInstalacion($InstanciaDB);
        $idInfInstalac=$_POST['idInfInstalac'];
        $departamento=($_POST['departamento']);

        $municipio=(($_POST['municipio']));
        $responsables=(($_POST['responsables']));
        $patrocinador=(($_POST['patrocinador']));
        $fecha=limpiar($_POST['fecha']);
        $vereda=(($_POST['vereda']));
        $centroEducRural=(($_POST['centroEducRural']));
        $NroFamilProgram=limpiar($_POST['NroFamilProgram']);
        $NroFamilEntreg=limpiar($_POST['NroFamilEntreg']);
        $InstEducativas=(($_POST['InstEducativas']));
        $nombre_docente=primera_mayuscula(limpiar($_POST['nombre_docente']));
        $telefono_docente=limpiar($_POST['telefono_docente']);
        $correo_docente=limpiar($_POST['correo_docente']);
        $observaciones=primera_mayuscula(limpiar($_POST['observaciones']));

        $DatosProgramaDB=$InstInformInstal->BuscarInformeInstalacionxId($idInfInstalac);
        $rowDB=$DatosProgramaDB->fetch_array(MYSQLI_BOTH);
        $diferenciaMunicipio=strcmp($municipio,$rowDB[1]);
        $diferenciapatrocinador=strcmp($patrocinador,$rowDB[1]);
        $diferenciafecha=strcmp($fecha,$rowDB[1]);
        $diferenciavereda=strcmp($vereda,$rowDB[1]);
        $diferenciacentroEduc=strcmp($centroEducRural,$rowDB[1]);
        $diferencianumfamiliasprg=strcmp($NroFamilProgram,$rowDB[1]);
        $diferenciaNumFamiliasEntre=strcmp($NroFamilEntreg,$rowDB[1]);
        $diferenciaInstEduca=strcmp($InstEducativas,$rowDB[1]);
        
        $diferenciaObservaciones=strcmp($observaciones,$rowDB[1]);
        $diferenciaDepartamento=strcmp($departamento,$rowDB[1]);
        

        if($diferenciaMunicipio != 0 || $diferenciapatrocinador != 0 || $diferenciafecha !=0 || $diferenciavereda !=0 || $diferenciacentroEduc !=0 || $diferencianumfamiliasprg !=0 || $diferenciaNumFamiliasEntre !=0 || $diferenciaInstEduca !=0 || $diferenciaEducadores !=0 || $diferenciaObservaciones !=0 || $diferenciaDepartamento !=0 || $diferenciaBiblio !=0 ){
            $actualizarprograma=$InstInformInstal->EditarInformeInstalacion($idInfInstalac,$municipio,$patrocinador,$fecha,$vereda,
            $centroEducRural,$NroFamilProgram,$NroFamilEntreg,$InstEducativas,$nombre_docente,$telefono_docente,$correo_docente,$observaciones,$departamento);
            
            $responsablesBD=array();
            $i=0;

            $ResponsablesInformeInst=$InstInformInstal->ListarDetalleInfoInstalacionxCABxFuente($idInfInstalac,0);
            while($rowRES=$ResponsablesInformeInst->fetch_array(MYSQLI_BOTH)){
                $responsablesBD[$i]=$rowRES[2];
                $i++;
            }

            $diferencia=array_diff ($responsables,$responsablesBD);
            $diff1=count($diferencia);

            $diferencia2=array_diff($responsablesBD,$responsables);
            $diff2=count($diferencia2);
            
            if($diff1 > 0 || $diff2 > 0){
                $EliminarDetalleInformeInfraResponsables=$InstInformInstal->EliminarDetalleInformeInfraResponsables($idInfInstalac);
                for($m=0;$m<count($responsables);$m++){
                    $InseratrResponsableInformInt=$InstInformInstal->AgregarDetalleInfoInstalacion($idInfInstalac,$responsables[$m],0,0,0);
                }
            }
        }
            
        if($actualizarprograma>0){
            echo 1;
        }else{
            echo 0;
        }
    break;

// ------------------------------------------------------------------
// Modulo registro entrega coleccion
    case 'InsertRegEntrColec':
        require_once("../modulos/registroEntregaColecciones/class/classRegistroEntregaColeccion.php");
        $InstRegEntregaColeccion=new Proceso_registroEntrColeccion($InstanciaDB);

        $codRegEntColec=texto_mayusculas(limpiar($_POST['codRegEntColeccion']));
        $fechaEnt=$_POST['fecha'];
        $idDepartamento=$_POST['idDepartamento'];
        $idMunicipio=$_POST['idMunicipio'];
        $idVeredaEnt=$_POST['idVereda'];
        $idInstitEduc=$_POST['idInstitucionEducativa'];
        $responsable=$_POST['responsable'];
        $sede=$_POST['sede'];
        $docente=limpiar($_POST['docente']);
        
        $internetEsc=$_POST['internet'];
        $internetPub=$_POST['internetPublico'];
        $codTipoPoblacion=$_POST['codTipoPoblacion'];
        $codTipoMaterial=$_POST['codTipoMaterial'];
        $cantEntregada=limpiar($_POST['cantEntregada']);
        $imagenEntColec=limpiar($_POST['imagenEntColec']);
        
        $RegEntColeInsertado=$InstRegEntregaColeccion->InsertarRegEntColec($codRegEntColec,$fechaEnt,$idMunicipio,$idInstitEduc,$idVeredaEnt,$sede,$docente,
        $internetEsc,$internetPub,$codTipoPoblacion,$codTipoMaterial,$cantEntregada,$imagenEntColec,$idDepartamento);

        if($RegEntColeInsertado > 0){
            for($i=0; $i<count($responsable); $i++){
                $InsertarEmpleadoRegistroEC=$InstRegEntregaColeccion->InsertarEmpleadoRegistroEntregaColeccion($RegEntColeInsertado,$responsable[$i]);
            }
            echo 1;
        }else{
            echo 0; 
        }
    break;

    case 'CargarResponsablesSelect2_RegEntregaColeccion':
            require_once("../modulos/registroEntregaColecciones/class/classRegistroEntregaColeccion.php");
            $InstRegEntregaColeccion=new Proceso_registroEntrColeccion($InstanciaDB);        
            $i=0;
            $idRegistroEntregaColeccion=limpiar($_POST['idRegistroEntregaColeccion']);
            $empleados = array();
            $Listaempleados=$InstRegEntregaColeccion->ListarEmpledosxidRegistroEntregaColeccion($idRegistroEntregaColeccion);
            while($rowEMSG=$Listaempleados->fetch_array()){
                $empleados[$i]=intval($rowEMSG[2]);
                $i++;
            }
            echo json_encode($empleados);
    break;

    case 'EditarRegEntrColec':
        require_once("../modulos/registroEntregaColecciones/class/classRegistroEntregaColeccion.php");
        $InstRegEntregaColeccion=new Proceso_registroEntrColeccion($InstanciaDB);
        $idRegistrosEColeccion=$_POST['idRegistrosEColeccion'];
        $RegistroEColeccionEdit=$InstRegEntregaColeccion->ListarregEntregColeccxidRegistro($idRegistrosEColeccion);
        echo json_encode($RegistroEColeccionEdit->fetch_array(MYSQLI_NUM));
    break;

    case 'InsertarEncuestaInfra':
        require_once("../modulos/encuestainfra/class/classEncuestraIfra.php");
        $InstEncuestaInfra=new Proceso_EncuestaInfra($InstanciaDB);
        $IdInfraestructura=$_POST['IdInfraestructura'];
        $fechaEInfra=($_POST['fechaEInfra']);
        $CodDepartamentoEInfra=$_POST['CodDepartamentoEInfra'];
        $CodMunicipioEInfra=($_POST['CodMunicipioEInfra']);
        $CodVeredaEInfra=($_POST['CodVeredaEInfra']);
        $IER_CEREInfra=primera_mayuscula(limpiar($_POST['IER_CEREInfra']));
        $IER_CERLegalEInfra=($_POST['IER_CERLegalEInfra']);
        $NumEEInfra=limpiar($_POST['NumEEInfra']);
        $ModoAccesoEInfra=primera_mayuscula(limpiar($_POST['ModoAccesoEInfra']));
        $HayHuertaEscolarEInfra=limpiar($_POST['HayHuertaEscolarEInfra']);
        $UsoHuertaEInfra=primera_mayuscula(limpiar($_POST['UsoHuertaEInfra']));
        $HayAguaPotableEInfra=limpiar($_POST['HayAguaPotableEInfra']);
        $FuenteAguaBanosEInfra=limpiar($_POST['FuenteAguaBanosEInfra']);
        $UsoAbastecimientoAguaEInfra=$_POST['UsoAbastecimientoAguaEInfra'];
        $FuenteAbasAguaEInfra=($_POST['FuenteAbasAguaEInfra']);
        $ObservacionesAcueductoybanosEInfra=limpiar($_POST['ObservacionesAcueductoybanosEInfra']);
        $TipoMaterialTechoEinfra=limpiar($_POST['TipoMaterialTechoEinfra']);
        $EstadoMaterialTechoEinfra=limpiar($_POST['EstadoMaterialTechoEinfra']);
        $TipoMaterialParedesEinfra=limpiar($_POST['TipoMaterialParedesEinfra']);
        $EstadoMaterialParedesEinfra=limpiar($_POST['EstadoMaterialParedesEinfra']);
        $HayAlcantarilladoEInfra=limpiar($_POST['HayAlcantarilladoEInfra']);                
        $EstadoAlcantarilladoEInfra=limpiar($_POST['EstadoAlcantarilladoEInfra']);               
        $HayPozoSepticoEInfra=limpiar($_POST['HayPozoSepticoEInfra']);
        $EstadoPozoSepticoEinfra=limpiar($_POST['EstadoPozoSepticoEinfra']);
        $EleDotacionEInfra=($_POST['EleDotacionEInfra']);
        $EstadoEleDotacionEInfra=limpiar($_POST['EstadoEleDotacionEInfra']);                
        $ObservacionesTecnologiaEInfra=limpiar($_POST['ObservacionesTecnologiaEInfra']);
        $ObservacionesOtrosEInfra=limpiar($_POST['ObservacionesOtrosEInfra']);
        $nombre_docente=limpiar($_POST['nombre_docente']);
        $telefono_docente=limpiar($_POST['telefono_docente']);
        $correo_docente=limpiar($_POST['correo_docente']);
        $HayElecEInfra=$_POST['HayElecEInfra'];
        $EstadoElecEInfra=$_POST['EstadoElecEInfra'];
        
        if($IdInfraestructura==0){
            $IDEncuentaNuevaInsertada=$InstEncuestaInfra->InsertarEncuentraInfra(
                $fechaEInfra,
                $CodMunicipioEInfra,
                $CodVeredaEInfra,
                $IER_CEREInfra,
                $IER_CERLegalEInfra,
                $nombre_docente,
                $telefono_docente,
                $correo_docente,
                $NumEEInfra,
                $ModoAccesoEInfra,
                $TipoMaterialTechoEinfra,
                $EstadoMaterialTechoEinfra,
                $TipoMaterialParedesEinfra,
                $EstadoMaterialParedesEinfra,
                $HayAguaPotableEInfra,
                $FuenteAguaBanosEInfra,
                $UsoAbastecimientoAguaEInfra,
                $ObservacionesAcueductoybanosEInfra,
                $HayAlcantarilladoEInfra,
                $EstadoAlcantarilladoEInfra,
                $HayPozoSepticoEInfra,
                $EstadoPozoSepticoEinfra,
                $HayElecEInfra,
                $EstadoElecEInfra,
                $EstadoEleDotacionEInfra,
                $HayHuertaEscolarEInfra,
                $UsoHuertaEInfra,
                $ObservacionesTecnologiaEInfra,
                $ObservacionesOtrosEInfra,
                $CodDepartamentoEInfra
                );
            for($i=0; $i<count($FuenteAbasAguaEInfra); $i++){
                $InsertarFuenteAbasAguaEInfra=$InstEncuestaInfra->InseratrDetalleEncuestaInfra($IDEncuentaNuevaInsertada,$FuenteAbasAguaEInfra[$i],0);
            }
            for($j=0; $j<count($EleDotacionEInfra); $j++){
                $InsertarEleDotacionEInfra=$InstEncuestaInfra->InseratrDetalleEncuestaInfra($IDEncuentaNuevaInsertada,$EleDotacionEInfra[$j],1);
            }
        }else{
            $IDEncuentaNuevaInsertada=$InstEncuestaInfra->UpdateEncuestrInfra(
                $IdInfraestructura,
                $fechaEInfra,
                $CodMunicipioEInfra,
                $CodVeredaEInfra,
                $IER_CEREInfra,
                $IER_CERLegalEInfra,
                $nombre_docente,
                $telefono_docente,
                $correo_docente,
                $NumEEInfra,
                $ModoAccesoEInfra,
                $TipoMaterialTechoEinfra,
                $EstadoMaterialTechoEinfra,
                $TipoMaterialParedesEinfra,
                $EstadoMaterialParedesEinfra,
                $HayAguaPotableEInfra,
                $FuenteAguaBanosEInfra,
                $UsoAbastecimientoAguaEInfra,
                $ObservacionesAcueductoybanosEInfra,
                $HayAlcantarilladoEInfra,
                $EstadoAlcantarilladoEInfra,
                $HayPozoSepticoEInfra,
                $EstadoPozoSepticoEinfra,
                $HayElecEInfra,
                $EstadoElecEInfra,
                $EstadoEleDotacionEInfra,
                $HayHuertaEscolarEInfra,
                $UsoHuertaEInfra,
                $ObservacionesTecnologiaEInfra,
                $ObservacionesOtrosEInfra,
                $CodDepartamentoEInfra
            );
        }                
        if($IDEncuentaNuevaInsertada>0){
            echo 1;
        }else{
            echo 0;
        }
    break;

    case 'BuscarEncuestaInfra':
        require_once("../modulos/encuestainfra/class/classEncuestraIfra.php");
        $InstEncuestaInfra=new Proceso_EncuestaInfra($InstanciaDB);
        $idEncuestaInfraestructura=$_POST['IdCodigoEInfra'];
        $EncuestraInfra=$InstEncuestaInfra->ListarEncuestaInfraxIdReg($idEncuestaInfraestructura);
        echo json_encode($EncuestraInfra->fetch_array(MYSQLI_NUM));
        
    break;

    case 'CargarDetalleSelect2EInfra':
            require_once("../modulos/encuestainfra/class/classEncuestraIfra.php");
            $InstEncuestaInfra=new Proceso_EncuestaInfra($InstanciaDB);
            $i=0;
            $IdEInfra=$_POST['IdEInfra'];
            $ValorfiltroCampo=$_POST['ValorfiltroCampo'];
            $arrayselect2EInfra = array();
            $Listaarrayselect2=$InstEncuestaInfra->ListarDettaleEInfraxIdPerteneceaCampoEI($IdEInfra,$ValorfiltroCampo);
            while($rowEMSG=$Listaarrayselect2->fetch_array()){
                $arrayselect2EInfra[$i]=intval($rowEMSG[2]);
                $i++;
            }
            echo json_encode($arrayselect2EInfra);
    break;

        //Modulo de tipoPoblacion
    case 'insertarTipoPoblacion':
        require_once("../modulos/tipo_poblacion/class/ClassTipoPoblacion.php");
        $InstTipoPoblacion=new Proceso_TipoPoblacion($InstanciaDB);
        $descTipoPoblacion=primera_mayuscula(limpiar($_POST['descTipoPoblacion']));
        $TipoPoblacioninsertado=$InstTipoPoblacion->InserttipoPoblacion($descTipoPoblacion);
        if($TipoPoblacioninsertado>0){
        echo 1;
        }else{
        echo 0;
        }
    break;

    case 'EditarTipoPoblacion':
        require_once("../modulos/tipo_poblacion/class/ClassTipoPoblacion.php");
        $InstTipoPoblacion=new Proceso_TipoPoblacion($InstanciaDB);

        $idTipoPoblac=limpiar($_POST['idTipoPoblac']);
        $desTipoPoblac=primera_mayuscula(limpiar($_POST['desTipoPoblac']));
        $DatosProgramaDB=$InstTipoPoblacion->BuscarTipoPoblacion($idTipoPoblac);
        $rowDB=$DatosProgramaDB->fetch_array(MYSQLI_BOTH);
        $diferenciadescTipPoblac=strcmp($desTipoPoblac,$rowDB[1]);               
        $cambio=true;
        if ($diferenciadescTipPoblac ==0) {
            echo 0;
            $cambio=false;
        } else {
            if($cambio){
                $actualizarprograma=$InstTipoPoblacion->EditartipoPoblacion($idTipoPoblac,$desTipoPoblac);
                echo 1;
            }else{
                echo 3;
            }
        }
    break;

    // Modulo ASistencia a Capacitacion Instalacion
    case 'InsertAsistCapacInstal':
        require_once("../modulos/asistCapacInstalacion/class/classAsistenciaCapacInstalacion.php");
        $InstAsistCapacInstal=new Proceso_asistCapacInstalacion($InstanciaDB);

        $CodFormAsisCapac=limpiar($_POST['CodFormAsisCapac']);
        $fecha=limpiar($_POST['fecha']);
        $idTipoTaller=limpiar($_POST['idTipoTaller']);
        $idMunicipio=limpiar($_POST['idMunicipio']);
        $idResponsable=limpiar($_POST['idResponsable']);
        $LugarAsistCapac=primera_mayuscula(limpiar($_POST['LugarAsistCapac']));
        $CantAsistentes=limpiar($_POST['CantAsistentes']);
        $idReciboMaterial=limpiar($_POST['idReciboMaterial']);
        $ObservAsistencCapac=primera_mayuscula(limpiar($_POST['ObservAsistencCapac']));
        
        $existe_CodFormAsisCapac=$InstAsistCapacInstal->buscarCodRegAsisCapa($CodFormAsisCapac);
        if($existe_CodFormAsisCapac->num_rows > 0){
            echo 2; // Si el registo Cod de formulario ya existe
        }else{

            $AsistCapacInstal=$InstAsistCapacInstal->InsertAsistCapacInstal($CodFormAsisCapac,$fecha,$idTipoTaller,$idMunicipio,$idResponsable,$LugarAsistCapac,$CantAsistentes,$idReciboMaterial,$ObservAsistencCapac);
            if($AsistCapacInstal>0){
                echo 1;
            }else{
                echo 0;
            }
        }
        
    break;

    case 'editAsistCapacInstal':
        require_once("../modulos/asistCapacInstalacion/class/classAsistenciaCapacInstalacion.php");
        $InstAsistCapacInstal=new Proceso_asistCapacInstalacion($InstanciaDB);
    
        $idReg=$_POST['idReg'];
        $CodFormAsisCapac=limpiar($_POST['CodFormAsisCapac']);
        $fecha=limpiar($_POST['fecha']);
        $idTipoTaller=limpiar($_POST['idTipoTaller']);
        $idMunicipio=limpiar($_POST['idMunicipio']);
        $idResponsable=limpiar($_POST['idResponsable']);
        $LugarAsistCapac=primera_mayuscula(limpiar($_POST['LugarAsistCapac']));
        $CantAsistentes=limpiar($_POST['CantAsistentes']);
        $idReciboMaterial=limpiar($_POST['idReciboMaterial']);
        $ObservAsistencCapac=primera_mayuscula(limpiar($_POST['ObservAsistencCapac']));


        $existe_CodFormAsisCapac=$InstAsistCapacInstal->buscarCodRegAsisCapa($CodFormAsisCapac);

        if($existe_CodFormAsisCapac->num_rows > 0){
            $row=$existe_CodFormAsisCapac->fetch_array(MYSQLI_NUM);
            if($row[0]==$idReg){
                $actualizarprograma=$InstAsistCapacInstal->editAsistCapacInstal($idReg,$CodFormAsisCapac,$fecha,$idTipoTaller,$idMunicipio,$idResponsable,$LugarAsistCapac,$CantAsistentes,$idReciboMaterial,$ObservAsistencCapac);
                if($actualizarprograma>0){
                    echo 1;
                }else{
                    echo 0;
                }
            }else{
               echo 2; // Si el registo Cod de formulario ya existe 
            }
            
        }else{
            $actualizarprograma=$InstAsistCapacInstal->editAsistCapacInstal($idReg,$CodFormAsisCapac,$fecha,$idTipoTaller,$idMunicipio,$idResponsable,$LugarAsistCapac,$CantAsistentes,$idReciboMaterial,$ObservAsistencCapac);
            if($actualizarprograma>0){
                echo 1;
            }else{
                echo 0;
            }
        }
    break;


        
    // Modulo registro entrega coleccion
        case 'InsertRegEntrColec':
                require_once("../modulos/registroEntregaColecciones/class/classRegistroEntregaColeccion.php");
                $InstRegEntColec=new Proceso_registroEntrColeccion($InstanciaDB);
                $idDepartamento=$_POST['idDepartamento'];
                $codRegEntColeccion=limpiar($_POST['codRegEntColeccion']);
                $fecha=primera_mayuscula(limpiar($_POST['fecha']));
                $idMunicipio=primera_mayuscula(limpiar($_POST['idMunicipio']));
                $idInstitucionEducativa=limpiar($_POST['idInstitucionEducativa']);
                $idVereda=limpiar($_POST['idVereda']);
                $responsable=limpiar($_POST['responsable']);
                $sede=limpiar($_POST['sede']);
                $docente=primera_mayuscula(limpiar($_POST['docente']));
                $telDocente=primera_mayuscula(limpiar($_POST['telDocente']));
                $internet=limpiar($_POST['internet']);
                $internetPublico=limpiar($_POST['internetPublico']);
                $codTipoPoblacion=limpiar($_POST['codTipoPoblacion']);
                $codTipoMaterial=limpiar($_POST['codTipoMaterial']);
                $cantEntregada=limpiar($_POST['cantEntregada']);
                $imagenEntColec=primera_mayuscula(limpiar($_POST['imagenEntColec']));
            
                $RegEntColeInsertado=$InstRegEntColec->InsertarRegEntColec($codRegEntColeccion,$fecha,$idMunicipio,
                $idInstitucionEducativa,$idVereda,$responsable,$sede,$docente,$telDocente,$internet,$internetPublico,
                $codTipoPoblacion,$codTipoMaterial,$cantEntregada,$imagenEntColec,$idDepartamento);
                if($RegEntColeInsertado>0){
                    echo 1;
                }else{
                    echo 0;
                }
        break;

        case 'editRegEntColeccion':    
            require_once("../modulos/registroEntregaColecciones/class/classRegistroEntregaColeccion.php");
            $InstRegEntColec=new Proceso_registroEntrColeccion($InstanciaDB);
            
            $idRegEntColec=limpiar($_POST['idRegEntColec']);
            $codRegEntColeccion=limpiar($_POST['codRegEntColeccion']);
            $fecha=primera_mayuscula(limpiar($_POST['fecha']));
            $idMunicipio=primera_mayuscula(limpiar($_POST['idMunicipio']));
            $idInstitucionEducativa=limpiar($_POST['idInstitucionEducativa']);
            $idVereda=limpiar($_POST['idVereda']);
            $responsable=limpiar($_POST['responsable']);
            $sede=limpiar($_POST['sede']);
            $docente=primera_mayuscula(limpiar($_POST['docente']));
            $telDocente=primera_mayuscula(limpiar($_POST['telDocente']));
            $internet=limpiar($_POST['internet']);
            $internetPublico=limpiar($_POST['internetPublico']);
            $codTipoPoblacion=limpiar($_POST['codTipoPoblacion']);
            $codTipoMaterial=limpiar($_POST['codTipoMaterial']);
            $cantEntregada=limpiar($_POST['cantEntregada']);
            $imagenEntColec=primera_mayuscula(limpiar($_POST['imagenEntColec']));

            $actualizarprograma=$InstRegEntColec->EditarInformeInstalacion($idRegEntColec,$codRegEntColeccion,$fecha,
            $idMunicipio,$idInstitucionEducativa,$idVereda,$responsable,$sede,$docente,$telDocente,$internet,$internetPublico,
            $codTipoPoblacion,$codTipoMaterial,$cantEntregada,$imagenEntColec);
            if($actualizarprograma>0){
                echo 1;
            }else{
                echo 0;
            }
        break;
        // ***** Modulo Materiales **********+
        case 'InsertarMaterialesR':
            require_once("../modulos/materialesR/class/ClassMaterialesR.php");
            $InstMaterialesR=new Proceso_MaterialesR($InstanciaDB);
            $CodTipoMaterialR=$_POST['CodTipoMaterialR'];
            $CodMaterialR=texto_mayusculas($_POST['CodMaterialR']);
            $DescripcionMaterialR=primera_mayuscula(limpiar($_POST['DescripcionMaterialR']));
            $CantidadMaterialR=$_POST['CantidadMaterialR'];
            $CostounitMaterialR=$_POST['CostounitMaterialR'];
            $InsertarMaterial=$InstMaterialesR->InsertarMaterialR($CodMaterialR,$DescripcionMaterialR,$CantidadMaterialR,$CostounitMaterialR,$CodTipoMaterialR);
            echo $InsertarMaterial;
        break;

        case 'EditarMaterialesR':
            require_once("../modulos/materialesR/class/ClassMaterialesR.php");
            $EditMaterialesR=new Proceso_MaterialesR($InstanciaDB);
            $idArchivodeMaterial=$_POST['idArchivodeMaterial'];
            $CodTipoMaterialR=$_POST['CodTipoMaterialR'];
            $CodMaterialR=texto_mayusculas(limpiar($_POST['CodMaterialR']));
            $DescripcionMaterialR=primera_mayuscula(limpiar($_POST['DescripcionMaterialR']));
            $CantidadMaterialR=$_POST['CantidadMaterialR'];
            $CostounitMaterialR=$_POST['CostounitMaterialR'];
            $InsertarMaterial=$EditMaterialesR->EditarMaterialR($idArchivodeMaterial,$CodMaterialR,$DescripcionMaterialR,$CantidadMaterialR,$CostounitMaterialR,$CodTipoMaterialR);
            echo $InsertarMaterial;
        break;

// ******** Modulo Requisicion de Materiales ****************

        case 'ListarDetalleRequisicionMTMP':
            require_once("../modulos/materialesR/class/ClassMaterialesR.php");
            require_once("../modulos/requisicion_materiales/class/ClassRequisicionMateriales.php");
            $InstMaterialesR=new Proceso_MaterialesR($InstanciaDB);
            $InstRequisicion=new Proceso_RequisicionMateriales($InstanciaDB);
            $ListaMateriales=$InstMaterialesR->listarMaterialesR();
            $private_id=$_POST['private_id'];
            $ListaDetalleRequisicionTMP=$InstRequisicion->ListarDetalleRequisicionesTMP($private_id);
                echo '<div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="table-responsive">
                        <table class="table table-striped  table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center">Item</th>
                                    <th >Materiales</th>
                                    <th class="text-center">Cantidad Req.</th>                                
                                    <th class="text-center">Eliminar</th>
                                </tr>
                            </thead>
                            <tbody class="items">';
                        $items=1;
                        $sumaCantidadRequeridas=0;
                        if ($ListaDetalleRequisicionTMP->num_rows > 0) {
                            while($rowTMP=$ListaDetalleRequisicionTMP->fetch_array()){
                                $string="'".$private_id."'";
                            echo '
                                <tr>
                                    <td class="text-center">'.$items.'</td>
                                    <td>'.$rowTMP[4].'</td>
                                    <td class="text-center"> '.number_format($rowTMP[2]).'</td>
                                   
                                    <td class="text-center">
                                        <a href="#" onclick="BorrarDetalleRequisicionMTMP('.$rowTMP[0].','.$string.');")>
                                            <span class="glyphicon glyphicon-trash" style="color: red;"></span>
                                        </a>
                                    </td>
                                </tr>';
                                $items++;
                                $sumaCantidadRequeridas+=(intval($rowTMP[2]));
                            }
                        }
                        echo '
                            </tbody>
                        </table>
                            <!-- Row start -->
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading clearfix">
                                                <h3 class="panel-title">Insertar Materiales</h3>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-4 col-sm-4">
                                                            <div class="input-group">
                                                                <select  class="form-control" name="MaterialRequisicion" id="MaterialRequisicion">
                                                                    <option value=0> --- Seleccione el Material requerido --- </option>';
                                                                    while ($rowCG=$ListaMateriales->fetch_array()) {
                                                                        echo "<option value='".$rowCG[0]."'>".$rowCG[2]."</option>";
                                                                    }                
                                                                        echo '
                                                                </select>
                                                            </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                        <div class="input-group">
                                                            <input class="form-control" type="number" id="CantiRequeridaM" min="1" max="15" placeholder="Cantidad Requerida">
                                                            <span class="input-group-addon">00</span>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-md-1 col-sm-1">
                                                        <div class="input-group">
                                                            <button type="button" id="AgregarDetalleRequisicionM" onclick="InsertarDetalleRequisicionMTMP();"><span class="glyphicon glyphicon-arrow-up"></span></button> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <!-- Row end -->
                                <div class="form-group col-lg-12 text-right">
                                    <div style="width:90%; float:left;"><h4><label>Total Cantidades Requeridas: </label></h4></div>
                                    <div style="width:10%; float:right;"><h4><label id="TotalCantidadesRequisicionM">'.number_format($sumaCantidadRequeridas).'</label></h4></div>
                                    <div id="mgEliminarDetalleRequisicionMTMP"></div>
                                </div>                        
                        <div>
                    </div>
                </div>';
        break;
        
        case 'InsertarDetalleRequisicionTMP':
            require_once('../modulos/requisicion_materiales/class/ClassRequisicionMateriales.php');
            $InstRequisicionM=new Proceso_RequisicionMateriales($InstanciaDB);
            
            $MaterialRequisicion=$_POST['MaterialRequisicion'];
            $CantiRequeridaM=$_POST['CantiRequeridaM'];
            // $UndxCajaRequisicionM=$_POST['UndxCajaRequisicionM'];
            $private_id=$_POST['private_id'];
            $BuscarIdMaterialRM=$InstRequisicionM->BuscarRequisicionMXIdMaterialTPM($MaterialRequisicion,$private_id);

            if($BuscarIdMaterialRM->num_rows > 0 ){
                $rowDRM=$BuscarIdMaterialRM->fetch_array();
                $CantiRequeridaM+=$rowDRM[2];
                $SumaCantxIdMaterialRM=$InstRequisicionM->UpdateCantMaterialRMTMP($MaterialRequisicion,$CantiRequeridaM, $private_id);
                if($SumaCantxIdMaterialRM>0){
                    echo 1;
                }else{
                    echo 0;
                }
            }else{
                $InsertarDetalleRequisicionM=$InstRequisicionM->InsertarDetalleRequisicionTMP($MaterialRequisicion,$CantiRequeridaM, $private_id);
                if($InsertarDetalleRequisicionM>0){
                    echo 1;
                }else{
                    echo 0;
                }
            } 
        break;                                                   
        case 'EliminaridDetalleRequisicionMTMP':
            require_once('../modulos/requisicion_materiales/class/ClassRequisicionMateriales.php');
            $InstRequisicionM=new Proceso_RequisicionMateriales($InstanciaDB);
            $idDetalleRequisicionMTMP=$_POST['idDetalleRequisicionMTMP'];
            $private_id=$_POST['private_id'];
            $EliminarDetalleRequisicionMTMP=$InstRequisicionM->BorrarDetalleRequisicionMTMP($idDetalleRequisicionMTMP,$private_id);
            echo 1;
        break;

        case'InsertarRequisicionMateriales':
            require_once('../modulos/requisicion_materiales/class/ClassRequisicionMateriales.php');
            $InstRequisicionM=new Proceso_RequisicionMateriales($InstanciaDB);
            $FechaRequisicionM=$_POST['FechaRequisicionM'];
            $ProyectoRequisicionM=$_POST['ProyectoRequisicionM'];
            $EmpleadosRequisicionM=$_POST['EmpleadosRequisicionM'];
            
            $TotalCantRequisicionM=str_replace(',','',$_POST['TotalCantRequisicionM']);
            $private_id=$_POST['private_id'];
            $IdInsertadoRequisicionM=$InstRequisicionM-> InsertarRequisicionMateriales($FechaRequisicionM,$ProyectoRequisicionM,$EmpleadosRequisicionM,$TotalCantRequisicionM);
            if($IdInsertadoRequisicionM>0){
                $ListaDetalleRequisicionTMP=$InstRequisicionM->ListarDetalleRequisicionesTMP($private_id);
                while($rowTMP=$ListaDetalleRequisicionTMP->fetch_array()){
                    $InsertarDetalleRequisicionM=$InstRequisicionM->InsertarDetalleRequisicionM($IdInsertadoRequisicionM,$rowTMP[1],$rowTMP[2],$rowTMP[3]);
                    $BorrarRegistrosTMPRM=$InstRequisicionM->vaciarTMPDetalleRequisicionMaterial($private_id);
                }
                echo 1;
            }else{
                echo 0;
            }
        break;

        case 'obtenerCABRequisicion':
            require_once('../modulos/requisicion_materiales/class/ClassRequisicionMateriales.php');
            $InstRequisicionM=new Proceso_RequisicionMateriales($InstanciaDB);
            $IdRequisicionMaterial=$_POST['IdRequisicionMaterial'];
            $ListaRequisicion=$InstRequisicionM->BuscarRequisicion($IdRequisicionMaterial);
            echo json_encode($ListaRequisicion->fetch_array());

        break;

        case 'VerRequisicionMateriales':
            require_once('../modulos/requisicion_materiales/class/ClassRequisicionMateriales.php');
            require_once("../modulos/materialesR/class/ClassMaterialesR.php");
            $InstRequisicionM=new Proceso_RequisicionMateriales($InstanciaDB);
            $InstMaterialesR=new Proceso_MaterialesR($InstanciaDB);
            $ListaMateriales=$InstMaterialesR->listarMaterialesR();
            $IdRequisicionMateriales=$_POST['IdRequisicionMateriales'];
            $desde=$_POST['desde'];
            
            $ListaDetallexRequisicionMateriales=$InstRequisicionM->ListarDetalleRMxIdRequisicionM($IdRequisicionMateriales);
            echo '<div class="col-lg-12 col-md-12 col-sm-12">
            <div class="table-responsive">
                <table class="table table-striped  table-hover">
                    <thead>
                        <tr>
                            <th class="text-center">Item</th>
                            <th >Materiales</th>
                            <th class="text-center">Cantidad Req.</th>
                            // <th class="text-center">Unidad x Caja</th>';
                            if($desde==1){
                                echo '
                                <th class="text-center">Eliminar</th>';
                            }
                            echo '
                        </tr>
                    </thead>
                    <tbody class="items">';
                        $items=1;
                        $sumaCantidadRequeridas=0;
                        if ($ListaDetallexRequisicionMateriales->num_rows > 0) {
                            while($rowTMP=$ListaDetallexRequisicionMateriales->fetch_array()){
                            echo '
                                <tr>
                                    <td class="text-center">'.$items.'</td>
                                    <td>'.$rowTMP[9].'</td>
                                    <td class="text-center"> '.number_format($rowTMP[3]).'</td>
                                    // <td class="text-center"> '.number_format($rowTMP[6]).'</td>
                                    <td class="text-center">';

                                    if($desde==1){
                                        echo '
                                        <a href="#" onclick="BorrarDetalleRequisicionM('.$rowTMP[0].','.$IdRequisicionMateriales.');")>
                                            <span class="glyphicon glyphicon-trash" style="color: red;"></span>
                                        </a>';
                                    }
                                    echo '</td>
                                </tr>';
                                $items++;
                                $sumaCantidadRequeridas+=(intval($rowTMP[3]));
                            }
                        }
                        echo '
                            </tbody>
                        </table>
                            <!-- Row start -->
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading clearfix">
                                                <h3 class="panel-title">Insertar Materiales</h3>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-4 col-sm-4">
                                                            <div class="input-group">
                                                                <select  class="form-control" name="MaterialRequisicion" id="MaterialRequisicion">
                                                                    <option value=0> --- Seleccione el Material requerido --- </option>';
                                                                    while ($rowCG=$ListaMateriales->fetch_array()) {
                                                                        echo "<option value='".$rowCG[0]."'>".$rowCG[2]."</option>";
                                                                    }                
                                                                        echo '
                                                                </select>
                                                            </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                        <div class="input-group">
                                                            <input class="form-control" type="number" id="CantiRequeridaM" min="1" max="15" placeholder="Cantidad Requerida">
                                                            <span class="input-group-addon">00</span>
                                                        </div>
                                                    </div>
                                                    // <div class="col-md-4 col-sm-4">
                                                    //     <div class="input-group">
                                                    //         <span class="input-group-addon">$</span>
                                                    //         <input  class="form-control" type="number" id="UndxCajaRequisicionM" min="1" max="1500000" placeholder="Unidad x Caja">
                                                    //         <span class="input-group-addon">00</span>
                                                    //     </div>
                                                    // </div>';
                                                   
                                                    if($desde==1){
                                                        echo '<div class="col-md-1 col-sm-1">
                                                                <div class="input-group">
                                                                    <button type="button" id="AgregarDetalleRequisicionM" onclick="InsertarDetalleRequisicionM('.$IdRequisicionMateriales.');"><span class="glyphicon glyphicon-arrow-up"></span></button> 
                                                                </div>
                                                            </div>';
                                                    }
                                                    
                                                echo '
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <!-- Row end -->
                                <div class="form-group col-lg-12 text-right">
                                    <div style="width:90%; float:left;"><h4><label>Total Cantidades Requeridas: </label></h4></div>
                                    <div style="width:10%; float:right;"><h4><label id="TotalCantidadesRequisicionM">'.number_format($sumaCantidadRequeridas).'</label></h4></div>
                                </div>                        
                        <div>
                    </div>
                </div>
                ';
        break;

        case 'InsertarDetalleRequisicionMateriales':
           
            require_once('../modulos/requisicion_materiales/class/ClassRequisicionMateriales.php');
            $InstRequisicionM=new Proceso_RequisicionMateriales($InstanciaDB);
            $IdRequisicionMaterial=$_POST['IdRequisicionMaterial'];
            $IdMaterial=$_POST['MaterialRequisicion'];
            $CantiRequeridaM=$_POST['CantiRequeridaM'];
            $UndxCajaRequisicionM=$_POST['UndxCajaRequisicionM'];
            $BuscarIdMaterialRequisicionM=$InstRequisicionM->BuscarRequisicionMXIdMaterial($IdMaterial,$IdRequisicionMaterial);
            
            if($BuscarIdMaterialRequisicionM->num_rows > 0 ){ 
                $rowDRM=$BuscarIdMaterialRequisicionM->fetch_array();
                echo "Cantidad: ".$rowDRM[4];
                $CantiRequeridaM+=$rowDRM[4];
                $UpdateDetalleRM=$InstRequisicionM->UpdateCantMaterialM($IdRequisicionMaterial,$IdMaterial,$CantiRequeridaM,$UndxCajaRequisicionM);
                if($UpdateDetalleRM>0){
                    echo 1;   
                }else{
                    echo 0;
                }
            }else{
                $InsertarDetalleRM=$InstRequisicionM->InsertarDetalleRequisicionM($IdRequisicionMaterial,$IdMaterial,$CantiRequeridaM,$UndxCajaRequisicionM);
                if($InsertarDetalleRM>0){
                    echo 1;   
                }else{
                    echo 0;
                }
            }
                                                    
        break;

        case 'BorrarDetalleRequisicionM':
            
            $IdDetalleRequisicion=$_POST['IdDetalleRequisicion'];
            require_once('../modulos/requisicion_materiales/class/ClassRequisicionMateriales.php');
            $InstRequisicionM=new Proceso_RequisicionMateriales($InstanciaDB);
            $BorraLineaDetalleRequisicion=$InstRequisicionM->BorrarDetalleRequisicionM($IdDetalleRequisicion);
            if($BorraLineaDetalleRequisicion>0){
                echo 1;
            }else{
                echo 0;
            }
            

        break;

        //Modulo de Recibo de Materiales

        case 'MostrarDetalleParaReciboMateriales':
            
            require_once('../modulos/requisicion_materiales/class/ClassRequisicionMateriales.php');
            require_once("../modulos/materialesR/class/ClassMaterialesR.php");
            require_once("../modulos/tipo_novedades/class/classTipoNovedadesMaterial.php");
            $InstRequisicionM=new Proceso_RequisicionMateriales($InstanciaDB);
            $InstMaterialesR=new Proceso_MaterialesR($InstanciaDB);
            $InstTipoNonedadesM=new Proceso_TipoNovedadesMaterial($InstanciaDB);
            $ListaMateriales=$InstMaterialesR->listarMaterialesR();
            $ListaTipoNonedadesM=$InstTipoNonedadesM->listatipoNovedadesMater();
            $IdRequisicionMaterial=$_POST['IdRequisicionMaterial'];

            
            $ListaDetallexRequisicionMateriales=$InstRequisicionM->ListarDetalleRMxIdRequisicionM($IdRequisicionMaterial);
            
            echo '<div class="col-lg-12 col-md-12 col-sm-12">
            <div class="table-responsive">
                <table class="table table-striped  table-hover">
                    <thead>
                        <tr>
                            <th class="text-center">Item</th>
                            <th >Materiales</th>
                            <th class="text-center">Cantidad Req.</th>
                            <th class="text-center">Unidad x Caja</th>
                            <th class="text-center">Cant Recibida</th>
                            <th class="text-center">Tipo Novedad</th>
                            <th class="text-center">Diferencia</th>';
                            
                        echo '
                        </tr>
                    </thead>
                    <tbody class="items">';
                        $InputsCantRecibo=null;
                        $ListaDetallesM=array();
                        $items=1;
                        $sumaCantidadRequeridas=0;
                        if ($ListaDetallexRequisicionMateriales->num_rows > 0) {
                            $InputsCantRecibo="'";
                            while($rowTMP=$ListaDetallexRequisicionMateriales->fetch_array()){
                                $OrigenIngreso=$rowTMP[6];
                                $ListaDetallesM[]=$rowTMP[6];
                                if($items>1){
                                    $InputsCantRecibo.="||";
                                }
                                $InputsCantRecibo.=$rowTMP[0]."||";
                                
                                echo '
                                <tr>
                                    <td class="text-center">'.$items.'</td>
                                    <td>'.$rowTMP[10].'</td>
                                    <td class="text-center" id="CantRequerida'.$rowTMP[0].'">'.number_format($rowTMP[4]).'</td>
                                    <td class="text-center" id="UndxCaja">'.number_format($rowTMP[7]).'</td>
                                    <td class="text-center">

                                    <input class="form-control" type="text" id="CantReciboxitemM'.$rowTMP[0].'" onkeyup="DifCantRecibidas_CantRequ('.$rowTMP[0].')"'; 
                                    
                                    if($OrigenIngreso==1){

                                        echo ' value="'.$rowTMP[5].'"';
                                    }

                                    echo 'placeholder="Cantidad Recibida">';
                                        
                                    echo '
                                    </td>
                                    <td>
                                        <div class="form-group text-right">
                                            <select  class="form-control" name="TipoNonedadesReciboM'.$rowTMP[0].'" id="TipoNonedadesReciboM'.$rowTMP[0].'">
                                                <option value=0>-- Seleccione Tipo Novedad --</option>';
                                                mysqli_data_seek($ListaTipoNonedadesM, 0);
                                                while ($rowCG=$ListaTipoNonedadesM->fetch_array()) {
                                                    echo "<option value='".$rowCG[0]."'>".$rowCG[1]."</option>";
                                                }                
                                                    echo '
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group text-right">
                                            <div style="width:75%; float:left;">
                                                <input  class="form-control" type="text"'; 
                                                        if($OrigenIngreso==1){
                                                            echo ' value="'.($rowTMP[4]-$rowTMP[5]).'" disabled';
                                                        }
                                                echo ' id="CantTipoNovedadReciboM'.$rowTMP[0].'" placeholder="Diferencia Cant">
                                            </div>';
                                        if($OrigenIngreso==1){
                                            echo '
                                            <div style="width:20%; float:right;">
                                                <a href="#" onclick="BorrarDetalleRequisicionM('.$rowTMP[0].');")>
                                                    <span class="glyphicon glyphicon-trash" style="color: red;"></span>
                                                </a>
                                            </div>';
                                        }

                                    echo '
                                        </div>
                                    </td>';

                                    
                                echo '
                                </tr>';
                                
                                $InputsCantRecibo.="CantReciboxitemM".$rowTMP[0]."||".$rowTMP[4];
                                $items++;
                                $sumaCantidadRequeridas+=(intval($rowTMP[4]));
                            }
                            $InputsCantRecibo.="'";
                            echo'
                            <tr>
                                <td class="text-right" colspan="7">
                                    <div class="form-group text-right">
                                        <button type="button" class="form-control btn-info" style="width:40%" id="ReciboCompleto" onclick="AutocompletarRecibo('.$InputsCantRecibo.');"><b>Autocompletar Recibo  <span class="glyphicon glyphicon-circle-arrow-right"></span></b></button>
                                    </div>
                                </td>
                            </tr>
                            ';
                        }

                        echo '
                                
                            </tbody>
                        </table>
                            <!-- Row start -->
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading clearfix">
                                                <h3 class="panel-title">Insertar Material <b>Adicional</b></h3>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-4 col-sm-4">
                                                            <div class="input-group">
                                                                <select  class="form-control" name="MaterialRequisicion" id="MaterialRequisicion">
                                                                    <option value=0> --- Seleccione el Material requerido --- </option>';
                                                                    while ($rowCG=$ListaMateriales->fetch_array()) {
                                                                        echo "<option value='".$rowCG[0]."'>".$rowCG[2]."</option>";
                                                                    }                
                                                                        echo '
                                                                </select>
                                                            </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                        <div class="input-group">
                                                            <input class="form-control" type="number" id="CantRecibidaM" min="1" max="15" placeholder="Cantidad Requerida">
                                                            <span class="input-group-addon">00</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 col-sm-4">
                                                        <div class="input-group">
                                                            
                                                            <input  class="form-control" type="number" id="UndxCajaRequisicionM" min="1" max="1500000" placeholder="Unidad x Caja">
                                                            <span class="input-group-addon">00</span>
                                                        </div>
                                                    </div>
                                                   <div class="col-md-1 col-sm-1">
                                                            <div class="input-group">
                                                                <button type="button" id="AgregarDetalleReciboM" onclick="InserDetalleAdicional_desdeReciboM('.$IdRequisicionMaterial.');"><span class="glyphicon glyphicon-arrow-up"></span></button> 
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <!-- Row end -->
                                <div class="form-group col-lg-12 text-right">
                                    <div style="width:90%; float:left;"><h4><label>Total Cantidades Requeridas: </label></h4></div>
                                    <div style="width:10%; float:right;"><h4><label id="TotalCantidadesRequisicionM">'.number_format($sumaCantidadRequeridas).'</label></h4></div>
                                </div>                    
                        <div>
                    </div>
                </div>
                
                <div class="modal-footer" id="FooterBotonesModal">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" onclick="guardarReciboMateriales('.$InputsCantRecibo.');" class="btn btn-primary">Guardar</button>  
                </div>';
        break;

        case 'InserDetalleAdicional_desdeReciboM':
            require_once('../modulos/requisicion_materiales/class/ClassRequisicionMateriales.php');
            require_once("../modulos/materialesR/class/ClassMaterialesR.php");
            require_once("../modulos/tipo_novedades/class/classTipoNovedadesMaterial.php");
            $InstRequisicionM=new Proceso_RequisicionMateriales($InstanciaDB);
            $InstMaterialesR=new Proceso_MaterialesR($InstanciaDB);
            $InstTipoNonedadesM=new Proceso_TipoNovedadesMaterial($InstanciaDB);
            $IdRequisicionMaterial=$_POST['IdRequisicionMaterial'];
            $IdMaterial=$_POST['MaterialRequisicion'];
            $CantRecibidaM=$_POST['CantRecibidaM'];
            $UndxCajaRequisicionM=$_POST['UndxCajaRequisicionM'];
            $BuscarIdMaterialRequisicionM=$InstRequisicionM->BuscarRequisicionMXIdMaterial($IdMaterial,$IdRequisicionMaterial);

            if($BuscarIdMaterialRequisicionM->num_rows > 0 ){ 
                $rowDRM=$BuscarIdMaterialRequisicionM->fetch_array();
                $CantRecibidaM+=$rowDRM[5];
                $UpdateDetalleRM=$InstRequisicionM->UpdateCantMaterialenRecibo($IdRequisicionMaterial,$IdMaterial,$CantRecibidaM,$UndxCajaRequisicionM);
                if($UpdateDetalleRM>0){
                    echo 1;   
                }else{
                    echo 0;
                }
            }else{
                $InsAdicionalxReciboM=$InstRequisicionM->InsertarDetalleAdicionalRecibo($IdRequisicionMaterial,$IdMaterial,$CantRecibidaM,$UndxCajaRequisicionM);
            }
            if($InsAdicionalxReciboM>0){
                echo 1;
            }else{
                echo 0;
            }
        break;

        case 'InserCabRecibo':
            require_once('../modulos/requisicion_materiales/class/ClassRequisicionMateriales.php');
            $InstRequisicionM=new Proceso_RequisicionMateriales($InstanciaDB);
            $IdRequisicionMaterial=$_POST['IdRequisicionMaterial'];
            $FechaReciboMaterial=$_POST['FechaReciboMaterial'];
            $IdProyecto=$_POST['IdProyecto'];
            $IdEmpleadosRecibo=$_POST['IdEmpleadosRecibo'];

            $InsertarCabRecibo=$InstRequisicionM->InsertarCabReciboM($IdRequisicionMaterial,$IdEmpleadosRecibo,$FechaReciboMaterial,$IdProyecto,0);

            if($InsertarCabRecibo > 0){
                echo $InsertarCabRecibo;
            }else{
                echo 0;
            }

        break;
        case 'UpdateRecibo':
            require_once('../modulos/requisicion_materiales/class/ClassRequisicionMateriales.php');
            $InstRequisicionM=new Proceso_RequisicionMateriales($InstanciaDB);
            $IdDetalleMaterial=$_POST['IdDetalleMaterial'];
            $CantRecibidas=$_POST['CantRecibidas'];
            $IdTipoNovedad=$_POST['tipoNovedad'];
            $CantTipoNovedad=$_POST['CantTipoNovedad'];
            $IdReciboMaterial=$_POST['response'];
            $UpdateRecibo=$InstRequisicionM->UpdateDetalleGuardarRecibo($IdDetalleMaterial,$IdReciboMaterial,$CantRecibidas,$IdTipoNovedad,$CantTipoNovedad);
            
            if($UpdateRecibo > 0){
                echo 1;
            }else{
                echo 0;
            }

        break;
        case 'UpdateEstadoRequisicion':
            require_once('../modulos/requisicion_materiales/class/ClassRequisicionMateriales.php');
            $InstRequisicionM=new Proceso_RequisicionMateriales($InstanciaDB);
            $IdRequisicionMaterial=$_POST['IdRequisicionMaterial'];
            $estadoRequisicion=$_POST['estadoRequisicion'];
            $UpdateEstadoRequisicion=$InstRequisicionM->UpdateEstadoRequisicion($IdRequisicionMaterial,$estadoRequisicion);
            if($UpdateEstadoRequisicion > 0){
                echo 1;
            }else{
                echo 0;
            }
        break;

        case 'SubirImagen':
            $NitBeneficiario=$_REQUEST['NitBeneficiario'];
            $NombreBeneficiario=$_POST['NombreBeneficiario'];
            $DireccionBeneficiario=$_POST['DireccionBeneficiario'];
            $TelefonoBeneficiario=$_POST['TelefonoBeneficiario'];
            $NumeroFactura=$_POST['NumeroFactura'];
            $ValorFactura=$_POST['ValorFactura'];
            $PagoTCD=$_POST['PagoTCD'];
            $EntregaRUT=$_POST['EntregaRUT'];
            $EntregaCedula=$_POST['EntregaCedula'];
            $Observaciones=$_POST['Observaciones'];
            // $msgSubirImagen=$_FILES['msgSubirImagen'];
            $tipoImagen=substr($_FILES['msgSubirImagen']['type'],-3,3);
            
            $ruta = '../imgrelaciongastos/'.$NombreBeneficiario;
            
            if (!file_exists($ruta)) {
                mkdir($ruta, 0777, true);
            }
               
            $uploadfile = $ruta."/".$NitBeneficiario.".".$tipoImagen;
            echo $_FILES['msgSubirImagen']['tmp_name'];
            echo $_FILES['msgSubirImagen']['name'];
            echo $_FILES['msgSubirImagen']['type'];
            echo $_FILES['msgSubirImagen']['size'];
            if (move_uploaded_file($_FILES['msgSubirImagen']['tmp_name'], $uploadfile)) {
                    echo "Ha subido";
                } else {
                    
                    http_response_code(500); echo 'Here is some more debugging info:'; print_r($_FILES);
                } 
        break;
        // ********* Modulo Anticipos *******
        case 'InsertarAnticipo':
                require_once('../modulos/anticipos/class/classAnticipos.php');
                $InstAnticipos=new Proceso_Anticipos($InstanciaDB);
                $FechaAnticipo=$_POST['FechaAnticipo'];
                $ResponsableAnticipoEn=$_POST['ResponsableAnticipoEn'];
                $ResponsableAnticipoRe=$_POST['ResponsableAnticipoRe'];
                $DocEquivalenteAnticipo=texto_mayusculas($_POST['DocEquivalente']);
                $ValorAnticipo=str_replace(',','',$_POST['ValorAnticipo']);
                $ValorAnticipo=str_replace('$','',$ValorAnticipo);

                $NotasAnticipos=$_POST['Notasanticipos'];
                $IdSGAnticipo=$_POST['IdSG'];
                $InsertarAnticipo=$InstAnticipos->InsertarAnticipo($FechaAnticipo,$ResponsableAnticipoEn,$ResponsableAnticipoRe,$IdSGAnticipo,$DocEquivalenteAnticipo,$ValorAnticipo,$NotasAnticipos);
                if($InsertarAnticipo>0){
                    $cambioEstadoSG=$InstSolicitudGasto->CambiarEstadoSG($IdSGAnticipo,2);
                    if($cambioEstadoSG>0){
                        echo 1;
                    }else{
                        echo 0;
                    }
                }else{
                    echo 0;
                }
        break;

        case 'GetTarifaTipoConcepto':
            require_once('../modulos/solicitud_gastos/class/ClassSolicitudGastos.php');
            $InstSG=new Proceso_SolicitudGastos($InstanciaDB);
            $idMunicipio=$_POST['idMunicipio'];
            $idConceptoGasto=$_POST['idConceptoGasto'];
            $BuscarTarifa=$InstSG->GetTarifa($idConceptoGasto,$idMunicipio);
            echo json_encode($BuscarTarifa->fetch_array(MYSQLI_NUM));
        break;

        case 'BuscarTipoConcepto':
            require_once('../modulos/conceptos_gastos/class/classConceptoGastos.php');
            $InsTipoGasto=new Proceso_ConceptoGastos($InstanciaDB);
            $idConceptodeGasto=$_POST['idConcepto'];
            $ListadeConcepto=$InsTipoGasto->BuscarConceptoGastoxid($idConceptodeGasto);
            echo json_encode($ListadeConcepto->fetch_array(MYSQLI_NUM));
        break;

        case 'UploadFiles':
            require_once("../modulos/filemodulos/class/classFileModulos.php");
            $InstFileModulos=new Proceso_FileModulos($InstanciaDB);    
            $CarpetaOrigen=$_POST['CarpetaOrigen'];
            $idDoc=$_POST['idDoc'];
            $ruta_Archivo = '../archivos_soporte/'.$CarpetaOrigen.'/'.$idDoc.'/';
            $NombreTMP_Archivo=$_FILES['FileUpload']['tmp_name'];
            $Tamaño_Archivo=$_FILES['FileUpload']['size'];
            
            if (!file_exists($ruta_Archivo)) {
                mkdir($ruta_Archivo, 0777, true);
            }

            $target_file = $ruta_Archivo . basename($_FILES['FileUpload']["name"]);
            $FileType = pathinfo($target_file,PATHINFO_EXTENSION);

            if (move_uploaded_file($NombreTMP_Archivo,$target_file)){
                $PathFile='../../archivos_soporte/'.$CarpetaOrigen.'/'.$idDoc.'/'.$_FILES['FileUpload']['name'];
                $InsertUrlFila=$InstFileModulos->InsertarUrlFile($CarpetaOrigen,$idDoc,$PathFile,$_FILES['FileUpload']['name'],$FileType);
            }                
            
        break;

        case 'DelFiles':
            require_once("../modulos/filemodulos/class/classFileModulos.php");
            $InstFileModulos=new Proceso_FileModulos($InstanciaDB);
            
             
            $idFileDel=$_POST['idFileDel'];
            $RowFile=$InstFileModulos->getrowxidfiles($idFileDel);
            $row=$RowFile->fetch_array(MYSQLI_NUM);
            $urlfile="../archivos_soporte/".$row[1]."/".$row[2]."/".$row[4];
            $directorio="../archivos_soporte/".$row[1]."/".$row[2];
            $BorradoFile=unlink($urlfile);
            
            if($BorradoFile){
                $DelFile=$InstFileModulos->DelFile($idFileDel);
                $num = count(glob($directorio.'/{*.*}',GLOB_BRACE));
                if ($num == 0){
                    rmdir($directorio);
                }
            }else{
                $DelFile=false;
                $num=0;
            }
            $eliminacionArchivos=array($DelFile,$num);
            echo json_encode($eliminacionArchivos);
        break;

        case 'ListFiles':
            require_once("../modulos/filemodulos/class/classFileModulos.php");
            $InstFileModulos=new Proceso_FileModulos($InstanciaDB);
            $idDoc=$_POST['idDoc'];
            $moduloorigen=$_POST['moduloorigen'];
            $ListaFiles=$InstFileModulos->ListFilexOrigen_idDoc($moduloorigen,$idDoc);
            echo json_encode($ListaFiles->fetch_all());
        break;
        default:
            break;
        }
    } else {
         include('../vista/indexvista.php');
    }

?>