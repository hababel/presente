
<!DOCTYPE html>
<html lang="en">
	<head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>LOGIN SIGLA V.1</title>
        <meta name="generator" content="Bootply" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link href="assets/css/bootstrap.css" rel="stylesheet" />
        <link href="assets/css/font-awesome.css" rel="stylesheet" />
        <link href="assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
        <link href="assets/css/custom.css" rel="stylesheet" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
        <script type="text/javascript" src="assets/js/funcionesjs.js"></script>
        <script src="assets/js/jquery-1.10.2.js"></script>   
        <script src="assets/js/bootstrap.min.js"></script> 
        <script src="assets/js/jquery.metisMenu.js"></script>
        <script src="assets/js/morris/raphael-2.1.0.min.js"></script>
        <script src="assets/js/morris/morris.js"></script>
        <script src="assets/js/custom.js"></script>
	</head>
	<body>

<!--login modal-->

        <div id="loginModal" class="modal show" tabindex="-1" role="dialog" style="overflow-y: scroll;" aria-hidden="true">
            <div class="alert alert-info" align="center"><strong>SIGLA V.1 </strong></div><br>
                <form name="form1" method="post" action="" onkeypress="" class="form col-md-12 center-block">
                    <div class="modal-dialog">
                        <div class="modal-content">    
                            <div class="modal-body">
                                <center><img src="img/empresa.png" width="100" height="150"></center><br>
                                <div class="input-group input-group-lg">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                    <input type="text" name="Email" id="Email" class="form-control input-lg" placeholder="Email" autocomplete="off" required autofocus>
                                </div>
                                <br>
                                <div class="input-group input-group-lg">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                    <input type="password" name="Clave" id="Clave" class="form-control input-lg" placeholder="Password" autocomplete="off" required>
                                </div>
                                <br>
                                <div class="form-group">
                                    <div align="right">
                                        <button class="btn btn-primary btn-lg btn-block" type="button" onclick="login()"><i class="glyphicon glyphicon-log-in"></i> <strong>Conectar</strong></button>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <a data-toggle="modal" href="#modalrecuperarclave" id="olvidarclave" onclick="paraolvidarclave();">Olvide la clave</a>
                                </div>
                </form>
            </div> 
        </div>
        <br>
        <div id="msg" class="alert alert-warning"> 
            <div class="alert alert-warning" align="center">
        </div>
       

<!--modal para recuperacion de clave-->
    <div class="modal fade" id="modalrecuperarclave" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="border-bottom:0px;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 class="text-center">Recuperar Clave</h3>
                </div>
                <div class="modal-body">
                     
                    <div class="text-center">
                        <p>Escriba su correo electrónico para recuperar la clave.</p>
                        <div class="panel-body">
                            <div class="form-group">
                                <input class="form-control input-lg" placeholder="usuario@ejemplo.com" name="email" id="emailparaclave" type="email" autocomplete="off">
                            </div>
                            <button class="btn btn-lg btn-primary btn-block" onclick="enviopararecuperacion();">Enviar</button>
                        </div>
                    </div>
                            
                   <br>
                   
                   <div class="modal-footer text-center" id= "msgresetpass" style="border-top:0px; display:none;">
                        <div id="cabmodalmsg" class="alert alert-light">
                            <div id="titulomsgrecuperarclave" class="alert-heading"></div>
                            <div id="msgrecuperarclave">
                                <p class="mb-0"></a></p>
                            </div>
                        </div>
                   </div> 
                   <div class="text-right">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                   </div>
                </div>
                
            </div>
        </div>
    </div>


	</body>
</html>

<script>
$(document).ready(function () {
    $('#msgresetpass').hide();
    $('#emailparaclave').val(null);
    $('#msgrecuperarclave').html('');
    $('#titulomsgrecuperarclave').html('');
});

function paraolvidarclave(){
    $('#emailparaclave').val(null);
    $('#msgrecuperarclave').html('');
    $('#titulomsgrecuperarclave').html('');
    $('#msgresetpass').hide();
}

    function enviopararecuperacion(){

        $('#cabmodalmsg').removeClass().addClass('alert-light');
        
        var emailparaclave=$('#emailparaclave').val();
        var parametros={emailparaclave};
        $('#msgresetpass').show();
        $('#msgrecuperarclave').html('<div class="loading"><img src="img/2.gif"><br/><br>Un momento, por favor...</div>');
        $('#titulomsgrecuperarclave').html('');
        
        $.ajax({
            type: "POST",
            url: "/secretos/modulos/usuarios/usuarioslogica.php?accion=RecuperarClave",
            data: parametros,
            // dataType: "json",
            success: function (response) {
                
                $('#msgrecuperarclave').html('');
                $('#titulomsgrecuperarclave').html('');
                
                if (response){ 
                    $('#msgrecuperarclave').html('<p>La instruccion para recuperar la clave fue enviada con exito a tu bandeja de correo.</p>');
                    $('#titulomsgrecuperarclave').html('<span class="glyphicon glyphicon-check" style="color:green"> </span> <b>Correo enviado !</b>');
                    $('#cabmodalmsg').removeClass().addClass('alert-success');
                }else{
                    $('#msgrecuperarclave').html('<p>Ha ocurrido un error en el envio a tu correo.</p>Por favor verifica que escribiste el correo correcto.');
                    $('#titulomsgrecuperarclave').html('<span class="glyphicon glyphicon-info-sign" style="color:red"> </span> <b>Error !</b>');
                    $('#cabmodalmsg').removeClass().addClass('alert-danger');
                }
            }
        });
    }
</script>