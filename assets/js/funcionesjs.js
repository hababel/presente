function AlertModalmsg(msgTipoAlert,msgTitulo,msgTexto,NameDOMdialog){

  switch (msgTipoAlert) {
    case 'verde':
      msgTipoAlert='alert alert-success';
      break;

      case 'rojo':
      msgTipoAlert='alert alert-danger';
      break;

      case 'azul':
      msgTipoAlert='alert alert-info';
      break;

      case 'amarillo':
        msgTipoAlert='alert alert-warning';
      break;
  
    default:
      break;
  }

  var modalHtml =`
    <div class="modal fade" id="ModalErrores" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document" id="modal_dialog_Errores">
          <div class="modal-content">
            <div class="modal-body">
                <div class="`+msgTipoAlert+`" role="alert">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
                    <b><span aria-hidden="true">&times;</span></b>
                  </button>
                  <strong>`+msgTitulo+`</strong><br><br>`+msgTexto+`.
                </div>
            </div> 
          </div>
        </div>
    </div>  
      `;

    $("#"+NameDOMdialog).html(modalHtml);
    // display
    $("#ModalErrores").modal('show');
    
}

function AlertConfirmacion(msgTitulo,msgTexto,botonaceptar,NameDOMdialog){

  var modalHtml =`
    <div class="modal fade" id="ModalConfirmacion" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document" id="modal_dialog_Errores">
          <div class="modal-content">
            <div class="modal-body">
                <div class="alert alert-warning" role="alert">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
                    <b><span aria-hidden="true">&times;</span></b>
                  </button>
                  <h4><strong><b>`+msgTitulo+`</b></strong></h4><p>`+msgTexto+`.</p>
                  <br> 
                    `+botonaceptar+`
                  <button type="button btn" class="btn btn-default" data-dismiss="modal" aria-label="Close">Cancelar</button>
                </div>
            </div> 
          </div>
        </div>
    </div>`;

    $("#"+NameDOMdialog).html(modalHtml);
    // display
    $("#ModalConfirmacion").modal('show');
}

function login() {
    var email = $('#Email').val();
    var pass = $('#Clave').val(); 
    var params = {email,pass};
    var url = "logica/logica.php?accion=login";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msg").html("<div class='alert alert-dismissible alert-success'><strong>Redireccionando !!</strong></div>");
        location.href="modulos/principal/principal.php";
      } else {
        $("#msg").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Las credenciales son incorrectas.</div>");
        location.reload(); 
      }
    });
    $("#msg").delay(3000).fadeOut(300);
  }

function EditPrograma(){
    var IdPrograma=$('#formeditProgramaid').val();
    var CodigoProgramaEdit = $('#formeditProgramaCodigo').val();
    var DescProgramaEdit = $('#formeditProgramaDescripcion').val();
    var EstadoProgramaEdit = $('#formeditProgramaEstado').val();
    if (DescProgramaEdit === "" || CodigoProgramaEdit==="") {
      $("#msgProgramaEdit").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> El campo descripcion debe estar lleno</div>");
    } else {
      var params = {IdPrograma,CodigoProgramaEdit,DescProgramaEdit,EstadoProgramaEdit};
      var url = "../../logica/logica.php?accion=EditPrograma";
      $.ajax({
        url: url,
        type: 'POST',
        cache: false,
        dataType: 'json',
        data: params,
      }).done(function(result) {
        if(result == 1){
          $("#msgProgramaEdit").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
          location.reload(); 
        } else if (result == 3) {
          $("#msgProgramaEdit").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
        } else {
          $("#msgProgramaEdit").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
        }
      });
  }
  $("#msgProgramaEdit").delay(3000).fadeOut(300);
  return;
}


function EditarUsuario() {  
  var IdUsuarioFM=$('#IdUsuarioFM').val();
  var NombreUsuarioFM=$('#NombreUsuariof').val();
  var EstadoUsuarioFM=$('#EstadoUsuario').val();
  if (NombreUsuarioFM === "") {
    $("#msgEditUsuario").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var params = {IdUsuarioFM,NombreUsuarioFM,EstadoUsuarioFM};
      var url = "../../logica/logica.php?accion=EditUsuario";
      $.ajax({
        url: url,
        type: 'POST',
        cache: false,
        dataType: 'json',
        data: params,
      }).done(function(result) {
        if(result == 1){
          $("#msgEditUsuario").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
          location.reload(); 
        } else if (result == 3) {
          $("#msgEditUsuario").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
        } else {
          $("#msgEditUsuario").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en los datos del usuario, no hay nada que cambiar</div>");
        }
      });
  }
  $("#msgEditUsuario").delay(3000).fadeOut(300);
}

function formeditUsuario(datoUsuario){
  deditI=datoUsuario.split('||');
  $('#IdUsuarioFM').val(deditI[0]);
  $('#NombreUsuariof').val(deditI[1]);
  $('#CorreoUsuario').val(deditI[2]);
  $("#EstadoUsuario option[value="+ deditI[4] +"]").attr("selected",true);
}

  function formeditPrograma(datoPrograma){
    deditI=datoPrograma.split('||');
    $('#formeditProgramaid').val(deditI[0]);
    $('#formeditProgramaCodigo').val(deditI[1]);
    $('#formeditProgramaDescripcion').val(deditI[2]);
    $("#formeditProgramaEstado option[value="+ deditI[3] +"]").attr("selected",true);
  }

  

  function formeditPlanCuentas(datoPrograma){
    deditI=datoPrograma.split('||');
    $('#IdPlanCuentasFM').val(deditI[0]);
    $('#CodigoPlanCuentasFM').val(deditI[1]);
    $('#descPlanCuentasFM').val(deditI[2]);
    $("#EstadoPlanCuentasFM option[value="+ deditI[3] +"]").attr("selected",true);
  }

  function formeditConceptoGasto(dato2Programa){
    deditI=dato2Programa.split('||');
    $('#idConceptodeGastoFM').val(deditI[0]);
    $('#CodigoConceptoGastoFM').val(deditI[1]);
    $('#DesConceptoGastoFM').val(deditI[2]);
    if (deditI[3]==1) {
      $('#TarifaSNConceptoGastoFM').prop('checked',true);
    } else {
      $('#TarifaSNConceptoGastoFM').prop('checked',false);
    }
    $("#TipoGastoConceptoGastoFM option[value="+ deditI[7] +"]").attr("selected",true);
    $("#PlanCuentasConceptoGastoFM option[value="+ deditI[8] +"]").attr("selected",true);
    $("#EstadoConceptoGastoFM option[value="+ deditI[6] +"]").attr("selected",true);
  }

  function formeditTipoMaterial(datoPrograma){
    deditI=datoPrograma.split('||');
    $('#IdTipoMaterialFM').val(deditI[0]);
    $('#descTipoMaterialFM').val(deditI[1]);
  }

  function formeditTipoNovedadesPlan(datoPrograma){
    deditI=datoPrograma.split('||');
    $('#IdTipoNovedadesPlanFM').val(deditI[0]);
    $('#descTipoNovedadesPlanFM').val(deditI[1]);
  }

  function formeditCentroCostosP(datoPrograma){
    deditI=datoPrograma.split('||');
    $('#IdCentroCostosPFM').val(deditI[0]);
    $('#CodigoCentroCostosPFM').val(deditI[1]);
    $('#descCentroCostosPFM').val(deditI[2]);
  }

  function formeditCentroCostosH(datoPrograma){
    deditI=datoPrograma.split('||');
    $('#IdCentroCostosHFM').val(deditI[0]);
    $('#CodigoCentroCostosHFM').val(deditI[1]);
    $('#descCentroCostosHFM').val(deditI[2]);
    $("#CodCentroCostosPFM option[value="+ deditI[3] +"]").attr("selected",true);
  }

  function formeditEntidades(datoPrograma){
    deditI=datoPrograma.split('||');
    $('#IdEntidadesFM').val(deditI[0]);
    $('#NitEntidadesFM').val(deditI[1]);
    $('#NombreEntidadesFM').val(deditI[2]);
  }

  
  function formeditProyecto(datoPrograma){
    deditI=datoPrograma.split('||');
    $('#IdProyectoFM').val(deditI[0]);
    $('#DescProyectoFM').val(deditI[1]);
    $("#idCentrodeCostosHijoFM option[value="+ deditI[2] +"]").attr("selected",true);
    $("#idProgramaFM option[value="+ deditI[3] +"]").attr("selected",true);
  }

  function formeditDotaciones(datoPrograma){
    deditI=datoPrograma.split('||');
    $('#IdDotacionesFM').val(deditI[0]);
    $('#descDotacionesFM').val(deditI[1]);
  }

  function formeditFuentes(datoPrograma){
      deditI=datoPrograma.split('||');
      $('#IdFuentesFM').val(deditI[0]);
      $('#descFuentesFM').val(deditI[1]);
  }

  function formeditClasificacionC(datoPrograma){
    deditI=datoPrograma.split('||');
    $('#IdClasificacionCFM').val(deditI[0]);
    $('#descClasificacionCFM').val(deditI[1]);
  }

  function formmasinfoContacto(datoPrograma){
    deditI=datoPrograma.split('||');
      $('#NombreMI').html(deditI[2]);
      $('#TelefonoContactoMI').html(deditI[4]);
      $('#CelularContactoMI').html(deditI[5]);
      $('#emailContactoMI').html(deditI[6]);
      $('#RegionMI').html(deditI[7]);
      $('#idClasificacionMI').html(deditI[1]);
      $('#CargoContactoMI').html(deditI[3]);
      $('#idDepartamentoMI').html(deditI[8]);
      $('#idTipoMunicipioMI').html(deditI[9]);
  }

   
  function formeditContacto(datoPrograma){
    deditI=datoPrograma.split('||');
      $('#idContactoFM').val(deditI[0]);
      $('#NombreFM').val(deditI[2]);
      $('#TelefonoContactoFM').val(deditI[4]);
      $('#CelularContactoFM').val(deditI[5]);
      $('#emailContactoFM').val(deditI[6]);
      $('#CargoContactoFM').val(deditI[3]);
      $("#idClasificacionFM option[value="+ deditI[1] +"]").attr("selected",true);
      $("#idDepartamentoFM option[value="+ deditI[8] +"]").attr("selected",true);
      $("#idTipoMunicipioFM option[value="+ deditI[9] +"]").attr("selected",true);
      $("#idRegionFM option[value="+ deditI[7] +"]").attr("selected",true);
      
  }

  function formeditInstitucion(datoPrograma){
    deditI=datoPrograma.split('||');
    $('#idInstitucionFM').val(deditI[0]);
    $('#CodDaneInstitucionFM').val(deditI[1]);
    $('#NombreInstitucionFM').val(deditI[2]);
    $("#idTipoInstitucionFM option[value="+ deditI[3] +"]").attr("selected",true);
    $("#idVeredaFM option[value="+ deditI[4] +"]").attr("selected",true);
  }


  // Javier Octubre 25


  function formeditdpto(datoPrograma){
    deditI=datoPrograma.split('||');
    $('#idDptoFM').val(deditI[0]);
    $('#CodDANEDptoFM').val(deditI[1]);
    $('#DescDptoFM').val(deditI[2]);
  }

  function formeditRegion(datoPrograma){
    deditI=datoPrograma.split('||');
    $('#idRegionFM').val(deditI[0]);
    $('#descRegionFM').val(deditI[1]);
  }

  function formeditActividad(datoPrograma){
    deditI=datoPrograma.split('||');
    $('#idActividadFM').val(deditI[0]);
    $('#descActividadFM').val(deditI[1]);
    $('#undTiempActividadFM').val(deditI[2]);
  }

  function formeditTipoInst(datoPrograma){
    deditI=datoPrograma.split('||');
    $('#idTipoInstFM').val(deditI[0]);
    $('#descTipoInstFM').val(deditI[1]);
  }

  function formeditTipoMcpio(datoPrograma){
    deditI=datoPrograma.split('||');
    $('#idTipoMcpioFM').val(deditI[0]);
    $('#CodTipoMcpioFM').val(deditI[1]);
    $('#descTipoMcpioFM').val(deditI[2]);

  }

  function formeditTipoEncInf(datoPrograma){
    deditI=datoPrograma.split('||');
    $('#idTipoEncInfFM').val(deditI[0]);
    $('#descTipoEncInfFM').val(deditI[1]);
  }
 
  function formeditProceso(datoPrograma){
    deditI=datoPrograma.split('||');
    $('#idProcesoFM').val(deditI[0]);
    $('#descProcesoFM').val(deditI[1]);
  }

  function formeditTipoTaller(datoPrograma){
    deditI=datoPrograma.split('||');
    $('#idTipoTallerFM').val(deditI[0]);
    $('#descTipoTallerFM').val(deditI[1]);
    $('#estadoTipoTallerFM').val(deditI[2]);
  }

  function formeditTipoMaterial(datoPrograma){
    deditI=datoPrograma.split('||');
    $('#IdTipoMaterialFM').val(deditI[0]);
    $('#descTipoMaterialFM').val(deditI[1]);
  }

  function InsertPrograma(){
    var codigoPrograma=$('#codigoprograma').val();
    var descPrograma = $('#descPrograma').val();
    var estado = $('#estado').val();
    if (descPrograma === "" && codigoPrograma=="") {
      $("#msgPrograma").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> El campo descripcion debe estar lleno</div>");
    } else {
      var params = {descPrograma,estado,codigoPrograma};
      var url = "../../logica/logica.php?accion=InsertPrograma";
      $.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        data: params,
        }).done(function(result) {
          if(result == 1){
            $("#msgPrograma").html("<div class='alert alert-dismissible alert-success'><strong>Insertado Con Exito !!</strong></div>");
            location.reload(); 
          } else if (result == 3) {
            $("#msgPrograma").html("<div class='alert alert-dismissible alert-warning'><strong>La descripcion ya existe.</strong></div>");
          } else {
            $("#msgPrograma").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong>No fue posible insertar el Dato. Comuniquese con soporte.</div>");
            return;
          }
        
        });
    }
    $("#msgPrograma").delay(3000).fadeOut(300);
  }

  function InsertTipoGasto() {
    var CodTipoGasto=$('#CodTipoGasto').val();
    var DescTipoGasto=$('#DescTipoGasto').val();
    var EstadoTipoGasto=$('#EstadoTipoGasto').val();
    var TparaVereda=$('#TparaVereda').attr('checked');

    if (CodTipoGasto === "" && DescTipoGasto === "") {
      $("#msgTipoGastoNuevo").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos 'Nombre, Correo y Clave' son obligatorios</div>");
    } else {
      var params = {CodTipoGasto,DescTipoGasto,EstadoTipoGasto,TparaVereda};
      var url = "../../logica/logica.php?accion=InsertTipoGasto";
      $.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        data: params,
      }).done(function(result) {
        if(result == 1){
          $("#msgTipoGastoNuevo").html("<div class='alert alert-dismissible alert-success'><strong>Tipo de gasto insertado Con Exito !!</strong></div>");
          location.reload(); 
        } else if (result == 3) {
          $("#msgTipoGastoNuevo").html("<div class='alert alert-dismissible alert-warning'><strong>Esecoigo de tipo de gasto ya existe.</strong></div>");
        } else {
          $("#msgTipoGastoNuevo").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong>No fue posible insertar el dato, por favor comuniquese con soporte tecnico.</div>");
          return;
        }
      });
    }
    $("#msgTipoGastoNuevo").delay(3000).fadeOut(300);
  }

  function formeditTipoGasto(datoPrograma){
    deditI=datoPrograma.split('||');
    $('#IdTipoGastoFM').val(deditI[0]);
    $('#CodigoTipoGastoFM').val(deditI[1]);
    $('#descTipoGastoFM').val(deditI[2]);
    $("#EstadoTipoGastoFM option[value="+ deditI[3] +"]").attr("selected",true);
    if (deditI[4] == 1) {
      $('#TparaVeredaFM').prop('checked',true).change();
    }else{
      $('#TparaVeredaFM').prop('checked',false).change();
    }
  }

  function EditarTipoGastoFM(){
    var IdTipoGastoFM=$('#IdTipoGastoFM').val();
    var CodigoTipoGastoFM = $('#CodigoTipoGastoFM').val();
    var descTipoGastoFM = $('#descTipoGastoFM').val();
    var EstadoTipoGastoFM = $('#EstadoTipoGastoFM').val();
    var TparaVeredaFM =$('#TparaVeredaFM').is(':checked');

    if (CodigoTipoGastoFM === "" || descTipoGastoFM === "") {
      $("#msgEditTipoGastoFM").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
    } else {
      var params = {IdTipoGastoFM,CodigoTipoGastoFM,descTipoGastoFM,EstadoTipoGastoFM,TparaVeredaFM};
      var url = "../../logica/logica.php?accion=EditTipoGasto";
      $.ajax({
        url: url,
        type: 'POST',
        cache: false,
        dataType: 'json',
        data: params,
      }).done(function(result) {
        if(result == 1){
          $("#msgEditTipoGastoFM").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
          location.reload(); 
        } else if (result == 3) {
          $("#msgEditTipoGastoFM").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
        } else {
          $("#msgEditTipoGastoFM").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa</div>");
        }
      });
  }
  $("#msgEditTipoGastoFM").delay(3000).fadeOut(300);
  return;
}


// Insertar nueva Cuenta Contable en Plan de Cuentas
function InsertPLanCuenta(){
  var CodPlanCuentas=$('#CodPlanCuentas').val();
  var DescPlanCuentas=$('#DescPlanCuentas').val();
  var EstadoPlanCuentas=$('#EstadoPlanCuentas').val();
  if (CodPlanCuentas === "" && DescPlanCuentas === "") {
    $("#msgPlanCuentasNuevo").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos 'Nombre, Correo y Clave' son obligatorios</div>");
  } else {
    var params = {CodPlanCuentas,DescPlanCuentas,EstadoPlanCuentas};
    var url = "../../logica/logica.php?accion=InsertPlanCuentas";
    $.ajax({
      url: url,
      type: 'POST',
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgPlanCuentasNuevo").html("<div class='alert alert-dismissible alert-success'><strong>Cuenta insertada Con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgPlanCuentasNuevo").html("<div class='alert alert-dismissible alert-warning'><strong>Ese Codigo de Cuenta ya existe.</strong></div>");
      } else {
        $("#msgPlanCuentasNuevo").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong>No fue posible insertar el dato, por favor comuniquese con soporte tecnico.</div>");
        return;
      }
    });
  }
  $("#msgPlanCuentasNuevo").delay(3000).fadeOut(300);
}

function EditarPlanCuentasFM(){
  var IdPlanCuentasFM=$('#IdPlanCuentasFM').val();
  var CodigoPlanCuentasFM = $('#CodigoPlanCuentasFM').val();
  var descPlanCuentasFM = $('#descPlanCuentasFM').val();
  var EstadoPlanCuentasFM = $('#EstadoPlanCuentasFM').val();
  if (CodigoPlanCuentasFM === "" || descPlanCuentasFM==="") {
    $("#msgEditTipoGastoFM").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var params = {IdPlanCuentasFM,CodigoPlanCuentasFM,descPlanCuentasFM,EstadoPlanCuentasFM};
    var url = "../../logica/logica.php?accion=EditPlanCuentas";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgEditTipoGastoFM").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgEditTipoGastoFM").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgEditTipoGastoFM").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
}
$("#msgEditTipoGastoFM").delay(3000).fadeOut(300);
return;
}


// Proceso Insertar Concepto de Gasto
function InsertConceptoGasto(){
  var CodigoConceptoGasto=$('#CodigoConceptoGasto').val();
  var DesConceptoGasto=$('#DesConceptoGasto').val();
  var TarifaSNConceptoGasto=$('#TarifaSNConceptoGasto').val();
  var TipoGastoConceptoGasto=$('#TipoGastoConceptoGasto').val();
  var PlanCuentasConceptoGasto=$('#PlanCuentasConceptoGasto').val();
  var EstadoConceptoGasto=$('#EstadoConceptoGasto').val();
  if (CodigoConceptoGasto === "" && DesConceptoGasto === "") {
    $("#msgConceptoGastoNuevo").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos 'Codigo y Descripcion' son obligatorios</div>");
  } else {
    var params = {CodigoConceptoGasto,DesConceptoGasto,TarifaSNConceptoGasto,TipoGastoConceptoGasto,PlanCuentasConceptoGasto,EstadoConceptoGasto};
    var url = "../../logica/logica.php?accion=InsertConceptoGasto";
    $.ajax({
      url: url,
      type: 'POST',
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgConceptoGastoNuevo").html("<div class='alert alert-dismissible alert-success'><strong>Concepto de gasto insertado Con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgConceptoGastoNuevo").html("<div class='alert alert-dismissible alert-warning'><strong>Ese Codigo de Concepto de gasto ya existe.</strong></div>");
      } else {
        $("#msgConceptoGastoNuevo").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong>No fue posible insertar el dato, por favor comuniquese con soporte tecnico.</div>");
        return;
      }
    });
  }
  $("#msgConceptoGastoNuevo").delay(3000).fadeOut(300);
}

//Editar Concepto de Gasto
function EditarConceptoGastoFM(){
  var idConceptodeGastoFM=$('#idConceptodeGastoFM').val();
  var CodigoConceptoGastoFM = $('#CodigoConceptoGastoFM').val();
  var DesConceptoGastoFM = $('#DesConceptoGastoFM').val();
  var TarifaSNConceptoGastoFM = $('#TarifaSNConceptoGastoFM').prop('checked');
  var TipoGastoConceptoGastoFM = $('#TipoGastoConceptoGastoFM').val();
  var PlanCuentasConceptoGastoFM = $('#PlanCuentasConceptoGastoFM').val();
  var EstadoConceptoGastoFM = $('#EstadoConceptoGastoFM').val();
  if (CodigoConceptoGastoFM === "" || DesConceptoGastoFM==="") {
    $("#msgEditConceptoGasto").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var params = {idConceptodeGastoFM,CodigoConceptoGastoFM,DesConceptoGastoFM,TarifaSNConceptoGastoFM,TipoGastoConceptoGastoFM,PlanCuentasConceptoGastoFM,EstadoConceptoGastoFM};
    var url = "../../logica/logica.php?accion=EditConceptoGasto";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgEditConceptoGasto").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgEditConceptoGasto").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgEditConceptoGasto").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
}
$("#msgEditConceptoGasto").delay(3000).fadeOut(300);
return;
}


function insertarDpto(){
  var codDaneDpto= $('#codDANEDpto').val();
  var descripDpto=$('#descripDpto').val();
  if(codDaneDpto=="" || descripDpto==""){
    $("#msgDpto").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  }else{
    var parametros={codDaneDpto,descripDpto};
    $.ajax({
      url: "../../logica/logica.php?accion=insertarDpto",
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: parametros,
    }).done(function(result) {
      if(result == 1){
        location.reload(); 
      } else if (result == 3) {
        $("#msgDpto").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgDpto").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No fue posible insertar el dato, por favor comuniquese con soporte tecnico.</div>");
      }
    });
  }
}


function InsertTipoMaterial() {
  
  var DescTipoMaterial=$('#DescTipoMaterial').val();
  
  if (DescTipoMaterial === "") {
    $("#msgTipoMaterialNuevo").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos 'Nombre, Correo y Clave' son obligatorios</div>");
  } else {
    var params = {DescTipoMaterial};
    var url = "../../logica/logica.php?accion=InsertTipoMaterial";
    $.ajax({
      url: url,
      type: 'POST',
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgTipoMaterialNuevo").html("<div class='alert alert-dismissible alert-success'><strong>Tipo de gasto insertado Con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgTipoMaterialNuevo").html("<div class='alert alert-dismissible alert-warning'><strong>Esecoigo de tipo de gasto ya existe.</strong></div>");
      } else {
        $("#msgTipoMaterialNuevo").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong>No fue posible insertar el dato, por favor comuniquese con soporte tecnico.</div>");
        return;
      }
    });
  }
  $("#msgTipoMaterialNuevo").delay(3000).fadeOut(300);
}


function EditarTipoMaterialFM(){
  var IdTipoMaterialFM=$('#IdTipoMaterialFM').val();
  var descTipoMaterialFM = $('#descTipoMaterialFM').val();
  if (descTipoMaterialFM==="") {
    $("#msgEditTipoMaterialFM").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var params = {IdTipoMaterialFM,descTipoMaterialFM};
    var url = "../../logica/logica.php?accion=EditTipoMaterial";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgEditTipoMaterialFM").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgEditTipoMaterialFM").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgEditTipoMaterialFM").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
}
$("#msgEditTipoMaterialFM").delay(3000).fadeOut(300);
return;
}


function InsertTipoNovedadesPlan() {
  
  var DescTipoNovedadesPlan=$('#DescTipoNovedadesPlan').val();
  
  if (DescTipoNovedadesPlan === "") {
    $("#msgTipoNovedadesPlanNuevo").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos 'Nombre, Correo y Clave' son obligatorios</div>");
  } else {
    var params = {DescTipoNovedadesPlan};
    var url = "../../logica/logica.php?accion=InsertTipoNovedadesPlan";
    $.ajax({
      url: url,
      type: 'POST',
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgTipoNovedadesPlanNuevo").html("<div class='alert alert-dismissible alert-success'><strong>Tipo de gasto insertado Con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgTipoNovedadesPlanNuevo").html("<div class='alert alert-dismissible alert-warning'><strong>Esecoigo de tipo de gasto ya existe.</strong></div>");
      } else {
        $("#msgTipoNovedadesPlanNuevo").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong>No fue posible insertar el dato, por favor comuniquese con soporte tecnico.</div>");
        return;
      }
    });
  }
  $("#msgTipoNovedadesPlanNuevo").delay(3000).fadeOut(300);
}


function EditarTipoNovedadesPlanFM(){
  var IdTipoNovedadesPlanFM=$('#IdTipoNovedadesPlanFM').val();
  var descTipoNovedadesPlanFM = $('#descTipoNovedadesPlanFM').val();
  if (descTipoNovedadesPlanFM==="") {
    $("#msgEditTipoNovedadesPlanFM").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var params = {IdTipoNovedadesPlanFM,descTipoNovedadesPlanFM};
    var url = "../../logica/logica.php?accion=EditTipoNovedadesPlan";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgEditTipoNovedadesPlanFM").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgEditTipoNovedadesPlanFM").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgEditTipoNovedadesPlanFM").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
}
$("#msgEditTipoNovedadesPlanFM").delay(3000).fadeOut(300);
return;
}


function InsertCentroCostosP() {
  var CodCentroCostosP=$('#CodCentroCostosP').val();
  var DescCentroCostosP=$('#DescCentroCostosP').val();
  
  if (DescCentroCostosP === "") {
    $("#msgCentroCostosPNuevo").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos 'Nombre, Correo y Clave' son obligatorios</div>");
  } else {
    var params = {DescCentroCostosP,CodCentroCostosP};
    var url = "../../logica/logica.php?accion=InsertCentroCostosP";
    $.ajax({
      url: url,
      type: 'POST',
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgCentroCostosPNuevo").html("<div class='alert alert-dismissible alert-success'><strong>Tipo de gasto insertado Con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgCentroCostosPNuevo").html("<div class='alert alert-dismissible alert-warning'><strong>Esecoigo de tipo de gasto ya existe.</strong></div>");
      } else {
        $("#msgCentroCostosPNuevo").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong>No fue posible insertar el dato, por favor comuniquese con soporte tecnico.</div>");
        return;
      }
    });
  }
  $("#msgCentroCostosPNuevo").delay(3000).fadeOut(300);
}

function EditarCentroCostosPFM(){
  var IdCentroCostosPFM=$('#IdCentroCostosPFM').val();
  var CodigoCentroCostosPFM = $('#CodigoCentroCostosPFM').val();
  var descCentroCostosPFM = $('#descCentroCostosPFM').val();
  
  if (CodigoCentroCostosPFM === "" || descCentroCostosPFM==="") {
    $("#msgEditCentroCostosPFM").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var params = {IdCentroCostosPFM,CodigoCentroCostosPFM,descCentroCostosPFM};
    var url = "../../logica/logica.php?accion=EditCentroCostosP";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgEditCentroCostosPFM").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgEditCentroCostosPFM").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgEditCentroCostosPFM").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
}
$("#msgEditCentroCostosPFM").delay(3000).fadeOut(300);
return;
}

function InsertCentroCostosH(){
  var CodCentroCostosP=$('#CodCentroCostosP').val();
  var CodCentroCostosH = $('#CodCentroCostosH').val();
  var DescCentroCostosH = $('#DescCentroCostosH').val();
  
  if (CodCentroCostosH === "" || DescCentroCostosH==="") {
    $("#msgCentroCostosHNuevo").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var params = {CodCentroCostosP,CodCentroCostosH,DescCentroCostosH};
    var url = "../../logica/logica.php?accion=InsertCentroCostosH";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgCentroCostosHNuevo").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgCentroCostosHNuevo").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgCentroCostosHNuevo").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
}
$("#msgCentroCostosHNuevo").delay(3000).fadeOut(300);
return;
}


function EditarCentroCostosHFM(){
  var IdCentroCostosHFM=$('#IdCentroCostosHFM').val();
  var CodigoCentroCostosHFM = $('#CodigoCentroCostosHFM').val();
  var descCentroCostosHFM = $('#descCentroCostosHFM').val();
  var CodCentroCostosPFM=$('#CodCentroCostosPFM').val();
  if (CodigoCentroCostosHFM === "" || descCentroCostosHFM==="") {
    $("#msgEditCentroCostosHFM").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var params = {IdCentroCostosHFM,CodigoCentroCostosHFM,descCentroCostosHFM,CodCentroCostosPFM};
    var url = "../../logica/logica.php?accion=EditCentroCostosH";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgEditCentroCostosHFM").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgEditCentroCostosHFM").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgEditCentroCostosHFM").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
}
$("#msgEditCentroCostosHFM").delay(3000).fadeOut(300);
return;
}


function InsertEntidades(){
  var NitEntidad=$('#NitEntidad').val();
  var Nombreentidad = $('#Nombreentidad').val();
   
  if (NitEntidad === "" || Nombreentidad==="") {
    $("#msgEntidadesNuevo").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var params = {NitEntidad,Nombreentidad};
    var url = "../../logica/logica.php?accion=InsertEntidad";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgEntidadesNuevo").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgEntidadesNuevo").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgEntidadesNuevo").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
}
$("#msgEntidadesNuevo").delay(3000).fadeOut(300);
return;
}

function EditarEntidadesFM(){
  var IdEntidadesFM=$('#IdEntidadesFM').val();
  var NitEntidadesFM = $('#NitEntidadesFM').val();
  var NombreEntidadesFM = $('#NombreEntidadesFM').val();
  
  if (NitEntidadesFM === "" || NombreEntidadesFM==="") {
    $("#msgEditEntidadesFM").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var params = {IdEntidadesFM,NitEntidadesFM,NombreEntidadesFM};
    var url = "../../logica/logica.php?accion=EditarEntidades";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgEditEntidadesFM").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgEditEntidadesFM").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgEditEntidadesFM").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
}
$("#msgEditEntidadesFM").delay(3000).fadeOut(300);
return;
}


function InsertProyecto(){
  var DescProyecto=$('#DescProyecto').val();
  var idCentrodeCostosHijo = $('#idCentrodeCostosHijo').val();
  var idPrograma=$('#idPrograma').val();
   
  if (DescProyecto === "") {
    $("#msgProyectoNuevo").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var params = {DescProyecto,idCentrodeCostosHijo,idPrograma};
    var url = "../../logica/logica.php?accion=InsertProyecto";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgProyectoNuevo").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgProyectoNuevo").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgProyectoNuevo").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
}
$("#msgProyectoNuevo").delay(3000).fadeOut(300);
return;
}


function EditarProyectoFM(){
  var IdProyectoFM=$('#IdProyectoFM').val();
  var DescProyectoFM=$('#DescProyectoFM').val();
  var idCentrodeCostosHijoFM = $('#idCentrodeCostosHijoFM').val();
  var idProgramaFM=$('#idProgramaFM').val();
  
  if (DescProyectoFM === "") {
    $("#msgEditProyectoFM").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var params = {IdProyectoFM,DescProyectoFM,idCentrodeCostosHijoFM,idProgramaFM};
    var url = "../../logica/logica.php?accion=EditarProyecto";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgEditProyectoFM").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgEditProyectoFM").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgEditProyectoFM").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
}
$("#msgEditProyectoFM").delay(3000).fadeOut(300);
return;
}

function InsertDotaciones() {
  
  var DescDotaciones=$('#DescDotaciones').val();
  
  if (DescDotaciones === "") {
    $("#msgDotacionesNuevo").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos 'Nombre, Correo y Clave' son obligatorios</div>");
  } else {
    var params = {DescDotaciones};
    var url = "../../logica/logica.php?accion=InsertDotaciones";
    $.ajax({
      url: url,
      type: 'POST',
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgDotacionesNuevo").html("<div class='alert alert-dismissible alert-success'><strong>Tipo de gasto insertado Con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgDotacionesNuevo").html("<div class='alert alert-dismissible alert-warning'><strong>Esecoigo de tipo de gasto ya existe.</strong></div>");
      } else {
        $("#msgDotacionesNuevo").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong>No fue posible insertar el dato, por favor comuniquese con soporte tecnico.</div>");
        return;
      }
    });
  }
  $("#msgDotacionesNuevo").delay(3000).fadeOut(300);
}


function EditarDotacionesFM(){
  var IdDotacionesFM=$('#IdDotacionesFM').val();
  var descDotacionesFM = $('#descDotacionesFM').val();
  if (descDotacionesFM==="") {
    $("#msgEditDotacionesFM").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var params = {IdDotacionesFM,descDotacionesFM};
    var url = "../../logica/logica.php?accion=EditDotaciones";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgEditDotacionesFM").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgEditDotacionesFM").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgEditDotacionesFM").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
}
$("#msgEditDotacionesFM").delay(3000).fadeOut(300);
return;
}



function InsertFuentes() {
  
  var DescFuentes=$('#DescFuentes').val();
  
  if (DescFuentes === "") {
    $("#msgFuentesNuevo").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos 'Nombre, Correo y Clave' son obligatorios</div>");
  } else {
    var params = {DescFuentes};
    var url = "../../logica/logica.php?accion=InsertFuentes";
    $.ajax({
      url: url,
      type: 'POST',
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgFuentesNuevo").html("<div class='alert alert-dismissible alert-success'><strong>Tipo de gasto insertado Con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgFuentesNuevo").html("<div class='alert alert-dismissible alert-warning'><strong>Esecoigo de tipo de gasto ya existe.</strong></div>");
      } else {
        $("#msgFuentesNuevo").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong>No fue posible insertar el dato, por favor comuniquese con soporte tecnico.</div>");
        return;
      }
    });
  }
  $("#msgFuentesNuevo").delay(3000).fadeOut(300);
}


function EditarFuentesFM(){
  var IdFuentesFM=$('#IdFuentesFM').val();
  var descFuentesFM = $('#descFuentesFM').val();
  if (descFuentesFM==="") {
    $("#msgEditFuentesFM").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var params = {IdFuentesFM,descFuentesFM};
    var url = "../../logica/logica.php?accion=EditFuentes";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgEditFuentesFM").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgEditFuentesFM").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgEditFuentesFM").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
}
$("#msgEditFuentesFM").delay(3000).fadeOut(300);
return;
}


function InsertClasificacionC() {
  
  var DescClasificacionC=$('#DescClasificacionC').val();
  
  if (DescClasificacionC === "") {
    $("#msgClasificacionCNuevo").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos 'Nombre, Correo y Clave' son obligatorios</div>");
  } else {
    var params = {DescClasificacionC};
    var url = "../../logica/logica.php?accion=InsertClasificacionC";
    $.ajax({
      url: url,
      type: 'POST',
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgClasificacionCNuevo").html("<div class='alert alert-dismissible alert-success'><strong>Tipo de gasto insertado Con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgClasificacionCNuevo").html("<div class='alert alert-dismissible alert-warning'><strong>Esecoigo de tipo de gasto ya existe.</strong></div>");
      } else {
        $("#msgClasificacionCNuevo").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong>No fue posible insertar el dato, por favor comuniquese con soporte tecnico.</div>");
        return;
      }
    });
  }
  $("#msgClasificacionCNuevo").delay(3000).fadeOut(300);
}


function EditarClasificacionCFM(){
  var IdClasificacionCFM=$('#IdClasificacionCFM').val();
  var descClasificacionCFM = $('#descClasificacionCFM').val();
  if (descClasificacionCFM==="") {
    $("#msgEditClasificacionCFM").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var params = {IdClasificacionCFM,descClasificacionCFM};
    var url = "../../logica/logica.php?accion=EditClasificacionC";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgEditClasificacionCFM").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgEditClasificacionCFM").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgEditClasificacionCFM").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
}
$("#msgEditClasificacionCFM").delay(3000).fadeOut(300);
return;
}


function InsertContacto() {
  var Nombre=$('#Nombre').val();
  var idClasificacion=$('#idClasificacion').val();
  var CargoContacto=$('#CargoContacto').val();
  var TelefonoContacto=$('#TelefonoContacto').val();
  var CelularContacto=$('#CelularContacto').val();
  var emailContacto=$('#emailContacto').val();
  var idRegion=$('#idRegion').val();
  var idDepartamento=$('#idDepartamento').val();
  var idTipoMunicipio=$('#idTipoMunicipio').val();
  if (Nombre=="" && CargoContacto=="" && TelefonoContacto=="" && CelularContacto=="" && emailContacto == "") {
    $("#msgContactoNuevo").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos 'Nombre, Correo y Clave' son obligatorios</div>");
  } else {
    var params = {Nombre,idClasificacion,CargoContacto,TelefonoContacto,CelularContacto,emailContacto,idRegion,idDepartamento,idTipoMunicipio};
    var url = "../../logica/logica.php?accion=InsertContacto";
    $.ajax({
      url: url,
      type: 'POST',
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgContactoNuevo").html("<div class='alert alert-dismissible alert-success'><strong>Tipo de gasto insertado Con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgContactoNuevo").html("<div class='alert alert-dismissible alert-warning'><strong>Esecoigo de tipo de gasto ya existe.</strong></div>");
      } else {
        $("#msgContactoNuevo").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong>No fue posible insertar el dato, por favor comuniquese con soporte tecnico.</div>");
        return;
      }
    });
  }
  $("#msgContactoNuevo").delay(3000).fadeOut(300);
}


function EditarContactoFM(){
  var idContactoFM=$('#idContactoFM').val();
  var NombreFM=$('#NombreFM').val();
  var idClasificacionFM=$('#idClasificacionFM').val();
  var CargoContactoFM=$('#CargoContactoFM').val();
  var TelefonoContactoFM=$('#TelefonoContactoFM').val();
  var CelularContactoFM=$('#CelularContactoFM').val();
  var emailContactoFM=$('#emailContactoFM').val();
  var idRegionFM=$('#idRegionFM').val();
  var idDepartamentoFM=$('#idDepartamentoFM').val();
  var idTipoMunicipioFM=$('#idTipoMunicipioFM').val();
  if (NombreFM=="" && CargoContactoFM=="" && TelefonoContactoFM=="" && CelularContactoFM=="" && emailContactoFM == "") {
    $("#msgEditContactoFM").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos 'Nombre, Correo y Clave' son obligatorios</div>");
  } else {
    var params = {idContactoFM,NombreFM,idClasificacionFM,CargoContactoFM,TelefonoContactoFM,CelularContactoFM,emailContactoFM,idRegionFM,idDepartamentoFM,idTipoMunicipioFM};
    var url = "../../logica/logica.php?accion=EditarContacto";
    $.ajax({
      url: url,
      type: 'POST',
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgEditContactoFM").html("<div class='alert alert-dismissible alert-success'><strong>Tipo de gasto insertado Con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgEditContactoFM").html("<div class='alert alert-dismissible alert-warning'><strong>Esecoigo de tipo de gasto ya existe.</strong></div>");
      } else {
        $("#msgEditContactoFM").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong>No fue posible insertar el dato, por favor comuniquese con soporte tecnico.</div>");
        return;
      }
    });
  }
$("#msgEditContactoFM").delay(3000).fadeOut(300);
return;
}


function InsertInstitucion(){
  var CodDaneInstitucion=$('#CodDaneInstitucion').val();
  var NombreInstitucion = $('#NombreInstitucion').val();
  var idTipoInstitucion=$('#idTipoInstitucion').val();
  var idMcpio=$('#idMcpio').val();
  var idVereda=$('#idVereda').val();
  if (CodDaneInstitucion=="" && NombreInstitucion=="") {
    $("#msgInstitucionNuevo").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var params = {CodDaneInstitucion,NombreInstitucion,idTipoInstitucion,idMcpio,idVereda};
    var url = "../../logica/logica.php?accion=InsertInstitucion";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgInstitucionNuevo").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Insertado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgInstitucionNuevo").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos ingresados ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgInstitucionNuevo").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
}
$("#msgInstitucionNuevo").delay(3000).fadeOut(300);
return;
}

function EditarInstitucionFM(){
  var idInstitucionFM=$('#idInstitucionFM').val();
  var CodDaneInstitucionFM=$('#CodDaneInstitucionFM').val();
  var NombreInstitucionFM=$('#NombreInstitucionFM').val();
  var idTipoInstitucionFM=$('#idTipoInstitucionFM').val();
  var idMcpioFM=$('#idMcpioFM').val();
  var idVeredaFM=$('#idVeredaFM').val();
  if (CodDaneInstitucionFM=="" || NombreInstitucionFM==="") {
    $("#msgEditInstitucion").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var params = {idInstitucionFM,CodDaneInstitucionFM,NombreInstitucionFM,idTipoInstitucionFM,idMcpioFM,idVeredaFM};
    var url = "../../logica/logica.php?accion=EditInstitucion";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgEditInstitucion").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgEditInstitucion").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else if(result==0){
        $("#msgEditInstitucion").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
}
$("#msgEditInstitucion").delay(3000).fadeOut(300);
return;
}

function EditarDpto(){
  var IdDpto=$('#idDptoFM').val();
  var codDANEDpto=$('#CodDANEDptoFM').val();
  var descDpto = $('#DescDptoFM').val();
  if (IdDpto===""||codDANEDpto==""||descDpto=="") {
    $("#msgEditDpto").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var params = {IdDpto,codDANEDpto,descDpto};
    var url = "../../logica/logica.php?accion=EditarDpto";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgEditDpto").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgEditDpto").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgEditDpto").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
}
$("#msgEditDpto").delay(3000).fadeOut(300);
return;
}

// Modulo de actividad -->
function InsertActividad(){
  var descripcionactividad=$('#descripcionactividad').val();
  var unidadtiempoac=$('#unidadtiempoac').val();
  if(descripcionactividad=="" || unidadtiempoac==""){
    $("#msgInsActividad").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  }else{
    var parametros={descripcionactividad,unidadtiempoac};
    $.ajax({
      url: "../../logica/logica.php?accion=InsertarActividad",
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: parametros,
    }).done(function(result) {
      if(result == 1){
        $("#msgInsActividad").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Guardado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgInsActividad").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgInsActividad").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No fue posible insertar el dato, por favor comuniquese con soporte tecnico.</div>");
      }
    });
  }
}

function EditarActividad(){
  var IdActividad=$('#idActividadFM').val();
  var descActividad=$('#descActividadFM').val(); 
  var unTiemActividad=$('#undTiempActividadFM').val(); 
  if (IdActividad==="" || descActividad=="" || unTiemActividad=="") {
    $("#msgEditActividad").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var params = {IdActividad,descActividad,unTiemActividad};
    var url = "../../logica/logica.php?accion=EditarActividad";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgEditActividad").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgEditActividad").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgEditActividad").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
}
$("#msgEditActividad").delay(3000).fadeOut(300);
return;
}

// Modulo de tipo de municipio -->
function InsertTipoMunicipio(){
  var codTipoMcpio=$('#codTipoMcpio').val();
  var descTipoMcpio=$('#descTipoMcpio').val();
  
  if(codTipoMcpio=="" || descTipoMcpio==""){
    $("#msgTipoMcpio").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  }else{
    var parametros={codTipoMcpio,descTipoMcpio};
    $.ajax({
      url: "../../logica/logica.php?accion=insertarTipoMcpio",
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: parametros,
    }).done(function(result) {
      if(result == 1){
        $("#msgTipoMcpio").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Guardado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgTipoMcpio").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgTipoMcpio").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No fue posible insertar el dato, por favor comuniquese con soporte tecnico.</div>");
      }
    });
  }
}

function EditarTipoMcpio(){
  var IdTipoMcpio=$('#idTipoMcpioFM').val();
  var descTipoMcpio = $('#descTipoMcpioFM').val();
  var codTipoMcpio=$('#CodTipoMcpioFM').val();
  if (IdTipoMcpio===""||descTipoMcpio=="") {
    $("#msgEditTipoMcpio").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var params = {codTipoMcpio,IdTipoMcpio,descTipoMcpio};
    var url = "../../logica/logica.php?accion=EditarTipoMcpio";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgEditTipoMcpio").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgEditTipoMcpio").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgEditTipoMcpio").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
}
$("#msgEditTipoMcpio").delay(3000).fadeOut(300);
return;
}


// Modulo de region municipio -->
function InsertRegion(){
  
  var descripcionRegion=$('#descripcionRegion').val();
  if(descripcionRegion==""){
    $("#msgRegion").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  }else{
    var parametros={descripcionRegion};
    $.ajax({
      url: "../../logica/logica.php?accion=InsertRegion",
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: parametros,
    }).done(function(result) {
      if(result == 1){
        $("#msgRegion").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Guardado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgRegion").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgRegion").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No fue posible insertar el dato, por favor comuniquese con soporte tecnico.</div>");
      }
    });
  }
}

function EditarRegion(){
  var idRegion=$('#idRegionFM').val();
  var descRegion=$('#descRegionFM').val();
  if (descRegion===""|| idRegion=="") {
    $("#msgEditRegion").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var params = {descRegion, idRegion};
    var url = "../../logica/logica.php?accion=EditarRegion";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgEditRegion").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgEditRegion").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgEditRegion").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
}
$("#msgEditRegion").delay(3000).fadeOut(300);
return;
}

// Modulo de Tipo Instituciones -->
function insertarTipoInsti(){
  var descTipoInsti=$('#descTipoInsti').val();
  if(descTipoInsti==""){
    $("#msgTipoInstituc").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  }else{
    var parametros={descTipoInsti};
    $.ajax({
      url: "../../logica/logica.php?accion=insertarTipoInsti",
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: parametros,
    }).done(function(result) {
      if(result == 1){
        $("#msgTipoInstituc").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Guardado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgTipoInstituc").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgTipoInstituc").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No fue posible insertar el dato, por favor comuniquese con soporte tecnico.</div>");
      }
    });
  }
}

function EditarTipoInst(){
  var idTipoInst=$('#idTipoInstFM').val();
  var descTipoInst=$('#descTipoInstFM').val();
  if (idTipoInst===""|| descTipoInst=="") {
    $("#msgEditTipoInst").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var params = {idTipoInst, descTipoInst};
    var url = "../../logica/logica.php?accion=EditarTipoInst";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgEditTipoInst").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgEditTipoInst").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgEditTipoInst").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
}
$("#msgEditTipoInst").delay(3000).fadeOut(300);
return;
}


// Modulo de Tipo Encuesta de infraestructura -->
function insertarTipoEncInf(){
  var descTipoEncInf=$('#descTipoEncInf').val();
  if(descTipoEncInf==""){
    $("#msgTipoEncInf").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  }else{
    var parametros={descTipoEncInf};
    $.ajax({
      url: "../../logica/logica.php?accion=insertarTipoEncInf",
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: parametros,
    }).done(function(result) {
      if(result == 1){
        $("#msgTipoEncInf").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Guardado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgTipoEncInf").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgTipoEncInf").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No fue posible insertar el dato, por favor comuniquese con soporte tecnico.</div>");
      }
    });
  }
}

function EditarTipoEncInf(){
  var idTipoEncInf=$('#idTipoEncInfFM').val();
  var descTipoEncInf=$('#descTipoEncInfFM').val();
  if (idTipoEncInf===""|| descTipoEncInf=="") {
    $("#msgEditTipoEncInf").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var params = {idTipoEncInf, descTipoEncInf};
    var url = "../../logica/logica.php?accion=EditarTipoEncInf";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgEditTipoEncInf").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgEditTipoEncInf").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgEditTipoEncInf").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
}
$("#msgEditTipoEncInf").delay(3000).fadeOut(300);
return;
}

// Modulo de Procesos -->
function InsertProcesos(){
  var descProceso=$('#descProceso').val();
  if(descProceso=="" ){
    $("#msgProceso").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  }else{
    var parametros={descProceso};
    $.ajax({
      url: "../../logica/logica.php?accion=InsertProcesos",
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: parametros,
    }).done(function(result) {
      if(result == 1){
        $("#msgProceso").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Guardado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgProceso").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgProceso").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No fue posible insertar el dato, por favor comuniquese con soporte tecnico.</div>");
      }
    });
  }
}

function EditarProceso(){
  var idProceso=$('#idProcesoFM').val();
  var descProceso=$('#descProcesoFM').val();
  if (idProceso===""|| descProceso=="") {
    $("#msgEditProceso").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var params = {idProceso, descProceso};
    var url = "../../logica/logica.php?accion=EditarProceso";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgEditProceso").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgEditProceso").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgEditProceso").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
}
$("#msgEditProceso").delay(3000).fadeOut(300);
return;
}

// Modulo de Tipo de taller -->
function InsertTipoTaller(){
  var descTipoTaller=$('#descTipoTaller').val();
  var estadoTipoTaller=$('#estadoTipoTaller').val();
  if(descTipoTaller==""|| estadoTipoTaller=="" ){
    $("#msgtipotaller").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  }else{
    var parametros={descTipoTaller,estadoTipoTaller};
    $.ajax({
      url: "../../logica/logica.php?accion=InsertTipoTaller",
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: parametros,
    }).done(function(result) {
      if(result == 1){
        $("#msgtipotaller").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Guardado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgtipotaller").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgtipotaller").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No fue posible insertar el dato, por favor comuniquese con soporte tecnico.</div>");
      }
    });
  }
}

function EditarTipoTaller(){
  var IdTipoTaller=$('#idTipoTallerFM').val();
  var descTipoTaller=$('#descTipoTallerFM').val();
  var estadoTipoTaller = $('#estadoTipoTallerFM').val();
  if (IdTipoTaller===""||descTipoTaller==""||estadoTipoTaller=="") {
    $("#msgEditTipoTaller").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var params = {IdTipoTaller,descTipoTaller,estadoTipoTaller};
    var url = "../../logica/logica.php?accion=EditarTipoTaller";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgEditTipoTaller").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgEditTipoTaller").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgEditTipoTaller").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
}
$("#msgEditTipoTaller").delay(3000).fadeOut(300);
return;
}

// Modulo de Colecciones -->
function formeditColeccion(datoPrograma){
  deditI=datoPrograma.split('||');
  $('#idColeccionFM').val(deditI[0]);
  $('#descColeccionFM').val(deditI[1]);
}

function InsertColeccion(){
  var descColeccion=$('#descColeccion').val();
  var idEntColeccion=$('#idEntColeccion').val();
  if(descColeccion==""|| idEntColeccion=="" ){
    $("#msgColeccion").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  }else{
    var parametros={descColeccion,idEntColeccion};
    $.ajax({
      url: "../../logica/logica.php?accion=InsertColeccion",
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: parametros,
    }).done(function(result) {
      if(result == 1){
        $("#msgColeccion").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Guardado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgColeccion").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgColeccion").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No fue posible insertar el dato, por favor comuniquese con soporte tecnico.</div>");
      }
    });
  }
}

function EditarColeccion(){
  var idColeccion=$('#idColeccionFM').val();
  var DescColeccion=$('#descColeccionFM').val();
  if (idColeccion===""|| DescColeccion=="") {
    $("#msgEditColeccion").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var params = {idColeccion, DescColeccion};
    var url = "../../logica/logica.php?accion=EditarColeccion";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgEditColeccion").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgEditColeccion").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgEditColeccion").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
}
$("#msgEditColeccion").delay(3000).fadeOut(300);
return;
}

// Modulo de tipo Tarifa -->
function formeditTipoTarifa(datoPrograma){
  deditI=datoPrograma.split('||');
  $('#idTipoTarifaFM').val(deditI[0]);
  $('#descTipoTarifaFM').val(deditI[1]);
}

function InsertTipoTarifa(){
  var descTipoTarifa=$('#descTipoTarifa').val();
  if(descTipoTarifa==""){
    $("#msgTipoTarifa").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  }else{
    var parametros={descTipoTarifa};
    $.ajax({
      url: "../../logica/logica.php?accion=InsertTipoTarifa",
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: parametros,
    }).done(function(result) {
      if(result == 1){
        $("#msgTipoTarifa").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Guardado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgTipoTarifa").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgTipoTarifa").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No fue posible insertar el dato, por favor comuniquese con soporte tecnico.</div>");
      }
    });
  }
}

function EditarTipoTarifa(){
  var idTipoTarifa=$('#idTipoTarifaFM').val();
  var descTipoTarifa=$('#descTipoTarifaFM').val();
  if (idTipoTarifa===""|| descTipoTarifa=="") {
    $("#msgEditTipoTarifa").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var params = {idTipoTarifa, descTipoTarifa};
    var url = "../../logica/logica.php?accion=EditarTipoTarifa";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgEditTipoTarifa").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgEditTipoTarifa").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgEditTipoTarifa").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
}
$("#msgEditTipoTarifa").delay(3000).fadeOut(300);
return;
}

// *********** Modulo de Vereda *******************


function formeditVereda(datoPrograma){
  deditI=datoPrograma.split('||');
  $('#TituloModalVereda').html(`
  <div id="idVereda" style="display:none;">`+deditI[0]+`</div>
  Editar Vereda - # `+ deditI[0]);
  $('#descVereda').val(deditI[1]);
  $('#IdMcpio').val(deditI[2]).change();
  $('#ValorMovilidadVereda').val(deditI[3]);
  $('#FooterModalVereda').html(`
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
    <button type="button" class="btn btn-primary" onclick="UpdateVereda();">Actualizar</button>
  `);
}

function UpdateVereda(){

  var idVereda=$('#idVereda').text();
  var descVereda=$('#descVereda').val();
  var IdMcpio=$('#IdMcpio').val();
  var ValorMovilidadVereda=$('#ValorMovilidadVereda').val();
  if (descVereda == '' || IdMcpio == 0) {
    $("#msgVereda").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var params = {idVereda,descVereda,IdMcpio,ValorMovilidadVereda};
    var url = "../../logica/logica.php?accion=EditarVereda";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgVereda").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgVereda").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgVereda").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
}
$("#msgEditVereda").delay(3000).fadeOut(300);
return;
}

function InsertVereda(){
  var descVereda=$('#descVereda').val();
  var IdMcpio=$('#IdMcpio').val();
  if(descVereda=="" || IdMcpio==""){
    $("#msgVereda").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  }else{
    var parametros={descVereda,IdMcpio};
    $.ajax({
      url: "../../logica/logica.php?accion=InsertVereda",
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: parametros,
    }).done(function(result) {
      if(result == 1){
        $("#msgTipoMcpio").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Guardado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgTipoMcpio").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgTipoMcpio").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No fue posible insertar el dato, por favor comuniquese con soporte tecnico.</div>");
      }
    });
  }
}




// Modulo de Municipio -->
function formeditMunicipio(datoPrograma){
  deditI=datoPrograma.split('||');
  $('#IdMcpioFM').val(deditI[0]);
  $('#codDANEMcpioFM').val(deditI[1]);
  $('#descMcpioFM').val(deditI[2]);
  $("#idRegionFM option[value="+ deditI[3]+"]").attr("selected", true);
  $("#idTipoMcpioFM option[value="+ deditI[4]+"]").attr("selected", true);
  $("#idTipoTarifaFM option[value="+ deditI[5]+"]").attr("selected", true);
  $("#idDptoFM option[value="+ deditI[6]+"]").attr("selected", true);
}

function InsertMunicipio(){
  var codMunicipio=$('#codMunicipio').val();
  var descMunicipio=$('#descMunicipio').val();
  var idRegion=$('#idRegion').val();
  var idTipoMcpio=$('#idTipoMcpio').val();
  var idTipoTarifa=$('#idTipoTarifa').val();
  var idDpto=$('#idDpto').val();
  if(codMunicipio==""||descMunicipio==""||idRegion==""||idTipoMcpio==""||idTipoTarifa==""||idDpto==""){
    $("#msgMunicipio").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  }else{
    var parametros={codMunicipio,descMunicipio,idRegion,idTipoMcpio,idTipoTarifa,idDpto};
    $.ajax({
      url: "../../logica/logica.php?accion=InsertMunicipio",
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: parametros,
    }).done(function(result) {
      if(result == 1){
        $("#msgMunicipio").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Guardado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgMunicipio").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgMunicipio").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No fue posible insertar el dato, por favor comuniquese con soporte tecnico.</div>");
      }
    });
  }
}

function EditarMunicipio(){
  var IdMcpio=$('#IdMcpioFM').val();
  var codMunicipio=$('#codDANEMcpioFM').val();
  var descMunicipio=$('#descMcpioFM').val();
  var idRegion=$('#idRegionFM').val();
  var idTipoMcpio=$('#idTipoMcpioFM').val();
  var idTipoTarifa=$('#idTipoTarifaFM').val();
  var idDpto=$('#idDptoFM').val();

  if (codMunicipio==""||descMunicipio=="") {
    $("#msgEditMunicipio").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var params = {IdMcpio,codMunicipio,descMunicipio,idRegion,idTipoMcpio,idTipoTarifa,idDpto};
    var url = "../../logica/logica.php?accion=EditarMunicipio";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgEditMunicipio").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgEditMunicipio").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgEditMunicipio").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
}
$("#msgEditMunicipio").delay(3000).fadeOut(300);
return;
}


function guardarSolicitudGasto(){
  var IdUsuario=$('#UsuarioCreacion').text();
  var IdSolicitudGastoSG=$('#IdSolicitudGastoSG').text();
  var fecha=$('#fechadatetimepicker').text();
  var CodProyectoSG=$('#CodProyectoSG').val();
  var CodProcesoSG=$('#CodProcesoSG').val();
  var CodActividadSG=$('#CodActividadSG').val();
  var CodMunicipioSG=$('#CodMunicipioSG').val();
  var CodDepartamentoSG=$('#CodDepartamentoSG').val();
  var CodVeredaSG=$('#CodVeredaSG').val();
  var CodEntidadSG=$('#CodEntidadSG').val();
  var FechaHoraSalidaSG=$('#FechaHoraSalidaSG').val();
  var FechaHoraRegresoSG=$('#FechaHoraRegresoSG').val();
  var responsableSG=$('#responsableSG').val();
  var TotalSolicitudGastoSG=$('#TotalSolicitudGastoSG').text();
  var ObservacionesSG=$('#ObservacionesSG').val();
  
  if (CodProyectoSG=='00' || CodProcesoSG=='00' || CodActividadSG=='00' || CodMunicipioSG=='00' || CodEntidadSG=='' 
  || responsableSG=='' ||  FechaHoraSalidaSG=='' || FechaHoraRegresoSG=='' || CodVeredaSG=='0') {
    $("#msgSolicitudGasto").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var params = {IdSolicitudGastoSG,fecha,CodProyectoSG,CodProcesoSG,CodActividadSG,CodMunicipioSG,CodEntidadSG,
      FechaHoraSalidaSG,FechaHoraRegresoSG,responsableSG,TotalSolicitudGastoSG,
      ObservacionesSG,CodDepartamentoSG,CodVeredaSG,IdUsuario};
    var url = "../../logica/logica.php?accion=InsertarSolicitudGasto";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgSolicitudGasto").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
        location.reload();
      } else if (result == 3) {
        $("#msgSolicitudGasto").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgSolicitudGasto").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
  }
  $("#msgSolicitudGasto").delay(3000).fadeOut(300);
  return;
}

// modal nueva editar solicitud de gasto  /// 

function formeditSolicitudGasto(datoPrograma){

  $('#Mostrar_dpo_editSG').show();
  $('#Mostrar_mcpo_editSG').show();

  $('#Mostrar_dpo_InsertSG').hide();
  $('#Mostrar_mcpo_insertSG').hide();

$('#input_mcpo_editSG').val(datoPrograma[5]);

  $('#Titulo_modal').html(`<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h3 align="center" class="modal-title" id="myModalLabel">Editar Solicitud de Gastos</h3>
      <div id="msgSolicitudGasto"></div>
      <div class="form-group">
          <h3 class="modal-title" id="myModalLabel">Solicitud de Gastos - # <label id="idSolicitudgastoFM">`+datoPrograma[0]+`</Label></h3>          
      </div> `);

  $('#AgrgarGastoTMP').attr('onclick','InsertarDetalle()');
  $('#Conjunto_botone_Footmodal').html(`<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      <button type="button" onclick=editarSolicitudGasto(); class="btn btn-primary">Actualizar SG</button>`);

  $('#detall_modal').html(`
  <div id="mostrarDetalleSGEditar"></div>
      <div id="msgDetalleSolicitudGastoLista2"></div>`);
      $('#Nombre_dpto_editSG').html(datoPrograma[14]);
      $('#Nombre_mcpo_editSG').html(datoPrograma[13]);

  $("#CodProyectoSG").val(datoPrograma[2]);
  $('#CodProcesoSG').val(datoPrograma[3]);
  $('#CodActividadSG').val(datoPrograma[4]).change();
  $('#FechaHoraSalidaSG').val(datoPrograma[6]);
  $('#FechaHoraRegresoSG').val(datoPrograma[7]);
  $('#ObservacionesSG').val(datoPrograma[10]);
  $('#fechadatetimepicker').val(datoPrograma[1]);
  
  CargarResponsablesenSelect2Edit(datoPrograma[0]);
  CargarEntidadesSelect2Edit(datoPrograma[0]);
  mostrarDetalleSG(datoPrograma[0],'detall_modal');
  VeredaSegunMunicipio('input_mcpo_editSG','CodVeredaSG');
}

function CargarResponsablesenSelect2Edit(IdSG){
  var params = {IdSG};
    var url = "../../logica/logica.php?accion=CargarResponsablesSelect2";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
        $('#responsableSG').val(result);
        $('#responsableSG').trigger('change');
        $('#responsableSG').select2({
          disabled: false
        });
    });
}

function CargarResponsablesS2(IdReg,NameDOM){
  var params = {IdReg};
    var url = "../../logica/logica.php?accion=CargarResponsablesSelect2";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
        $('#'+NameDOM).val(result);
        $('#'+NameDOM).trigger('change');
        $('#'+NameDOM).select2({
          disabled: false
        });
    });
}

function CargarEntidadesSelect2Edit(IdSG){
  var params = {IdSG};
    var url = "../../logica/logica.php?accion=CargarEntidadesSelect2";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(resulte) {
        $('#CodEntidadSG').val(resulte);
        $('#CodEntidadSG').trigger('change');
        $('#CodEntidadSG').select2({
          disabled: false
        });
    });
}

function editarSolicitudGasto(){
  var IdSolicitudGastoSG=$('#idSolicitudgastoFM').text();
  var CodProyectoSG=$('#CodProyectoSG').val();
  var CodActividadSG=$('#CodActividadSG').val();
  var CodMunicipioSG=$('#CodMunicipioSG').val();
  var CodDepartamentoSG=$('#CodDepartamentoSG').val();
  var CodVeredaSG=$('#CodVeredaSG').val();
  var CodEntidadSG=$('#CodEntidadSG').val();
  var FechaHoraSalidaSG=$('#FechaHoraSalidaSG').val();
  var FechaHoraRegresoSG=$('#FechaHoraRegresoSG').val();
  var responsableSG=$('#responsableSG').val();
  var TotalSolicitudGastoSG=$('#TotalSolicitudGastoSG').text();
  var ObservacionesSG=$('#ObservacionesSG').val();

  if (CodProyectoSG=='00' || CodProcesoSG=='00' || CodActividadSG=='00' || CodMunicipioSG=='00' || CodEntidadSG=='' || fecha==''
    || responsableSG=='' ||  FechaHoraSalidaSG=='' || FechaHoraRegresoSG=='' || CodVeredaSG=='0') {
        $("#msgSolicitudGasto").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
      } else {
        var params = {IdSolicitudGastoSG,CodProyectoSG,CodActividadSG,CodMunicipioSG,CodEntidadSG,
          FechaHoraSalidaSG,FechaHoraRegresoSG,responsableSG,TotalSolicitudGastoSG,ObservacionesSG,CodDepartamentoSG,
          CodVeredaSG};
        var url = "../../logica/logica.php?accion=editarSolicitudGasto";
          // console.log('funciones');
          // console.log(params);
        $.ajax({
          url: url,
          type: 'POST',
          cache: false,
          dataType: 'json',
          data: params,
        }).done(function(result) {
          if(result == 1){
            $("#msgSolicitudGasto").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
            location.reload();
          } else if (result == 3) {
            $("#msgSolicitudGasto").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
          } else {
            $("#msgSolicitudGasto").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
          }
        });
      }
      $("#msgSolicitudGasto").delay(3000).fadeOut(300);
      return;
}

function VerSolicitudGasto(IdSG,datoresponsables){
  $('#ListaDetalleSG').val("");
  $('#VerAprobarizacionSG').val("");
  var texto;
  
  var parametros={IdSG};
  $.ajax({
    type: "POST",
    url: "../../logica/logica.php?accion=BuscarIDSG",
    data: parametros,
    dataType: "json",
    success: function (deditI) {
      
      $('#VerAprobacionSG').hide();
      $('#msgBotonAccionSG').html(texto);
      $('#VerIdSolicitudGastoSG').html(deditI[0]);
      $('#VerFechaSolicitudGastoSG').html(deditI[1]);
      $('#VerCodDepartamentoSGdiv').html(deditI[14]);
      
      $('#VerCodMunicipioSGdiv').html(deditI[13]);
      $('#VerCodProyectoSGdiv').html(deditI[15]);
      $('#VerCodProcesoSGdiv').html(deditI[17]);
      $('#VerCodActividadSGdiv').html(deditI[16]);
      $('#VerFechaHoraSalidaSGdiv').html(deditI[6]);
      $('#VerFechaHoraRegresoSGdiv').html(deditI[7]);

      if (deditI[18] != null) {
        var ValorAprobadoSG=formatNumber.new("$"+deditI[18]);
        $('#VerValorAprobadoSGdiv').html(ValorAprobadoSG);
      } else {
        $('#VerValorAprobadoSGdiv').html('Sin Aprobacion');
      }
      if (deditI[21] != null) {
        $('#VerDocAnticipoSGdiv').html("#"+deditI[19]+"<br>Doc Equiv.: "+deditI[20]+"<br>Valor: $"+formatNumber.new(deditI[21]));
        mostrarAprobacionSolicitudGasto(deditI[0]);  
      } else {
        $('#VerDocAnticipoSGdiv').html('pendiente Anticipo');
      }
      var totalSG=formatNumber.new(deditI[9]);
      $('#VerValorTotaldiv').html('$  '+ totalSG);
      
      deditR=datoresponsables.split('||');
      $('#VerresponsableSGdiv').val(deditR);
      $('#VerresponsableSGdiv').trigger('change');
      $('#VerresponsableSGdiv').select2({
        disabled: true
      });
      mostrarDetalleSG(deditI[0],'ListaDetalleSG');
      CargarEntidadesSelect2(deditI[0]);
    }
  });

    
}

function CargarResponsablesenSelect2(IdSG){
  var params = {IdSG};
    var url = "../../logica/logica.php?accion=CargarResponsablesSelect2";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      alert(result);
        $('#VerresponsableSGdiv2').val(result);
        $('#VerresponsableSGdiv2').trigger('change');
        $('#VerresponsableSGdiv2').select2({
          disabled: true
        });
    });
}

function CargarEntidadesSelect2(IdSG){
  var params = {IdSG};
    var url = "../../logica/logica.php?accion=CargarEntidadesSelect2";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(resulte) {
      alert(resulte);
        $('#VerentidadesSGdiv').val(resulte);
        $('#VerentidadesSGdiv').trigger('change');
        $('#VerentidadesSGdiv').select2({
          disabled: true
        });
    });
}

// fin editar solicitud de gasto

// function VerSolicitudGasto(IdSG,datoresponsables){
//   $('#ListaDetalleSG').val("");
//   $('#VerAprobarizacionSG').val("");
//   var texto;
  
//   var parametros={IdSG};
//   $.ajax({
//     type: "POST",
//     url: "../../logica/logica.php?accion=BuscarIDSG",
//     data: parametros,
//     dataType: "json",
//     success: function (deditI) {
      
//       $('#VerAprobacionSG').hide();
//       // if (deditI[8]==0) {
//       //   texto='<button Title="Aprobar Gasto" type="button" class="btn-default btn" onclick="AprobarSolicitudGastodesdeSolicitudes('+deditI[0]+')" class="btn btn-default btn-sm" data-toggle="modal" data-target="#NuevaAprobarSolicitudGasto"><span class="glyphicon glyphicon-arrow-right" style="color:green;"></span> <span style="color:green;">$</span></button>';
//       // } else {
//       //   texto='<button Title="Ver Aprobacion" type="button" class="btn-default btn" onclick="ShowVerAprobacionSolicitudGasto();"><span style="color:green;">$</span></button><button Title="Aprobar Gasto" type="button"class="btn-default btn" onclick="LegalizarSolicitudGasto();"><span class="glyphicon glyphicon-arrow-right" style="color:rgb(255, 128, 0);"></span> <span class="glyphicon glyphicon-new-window" style="color:rgb(255, 128, 0);"></span></button>';
//       // }

//       // $('#msgBotonAccionSG').html(texto);
//       $('#VerIdSolicitudGastoSG').html(deditI[0]);
//       $('#VerFechaSolicitudGastoSG').html(deditI[1]);
//       $('#VerCodDepartamentoSGdiv').html(deditI[15]);
//       $('#VerCodVeredaSGdiv').html(deditI[16]);
//       $('#VerCodMunicipioSGdiv').html(deditI[14]);
//       $('#VerCodProyectoSGdiv').html(deditI[17]);
//       $('#VerCodProcesoSGdiv').html(deditI[19]);
//       $('#VerCodActividadSGdiv').html(deditI[18]);
//       $('#VerFechaHoraSalidaSGdiv').html(deditI[6]);
//       $('#VerFechaHoraRegresoSGdiv').html(deditI[7]);

//       if (deditI[20] != null) {
//         var ValorAprobadoSG=formatNumber.new("$"+deditI[20]);
//         $('#VerValorAprobadoSGdiv').html(ValorAprobadoSG);
//       } else {
//         $('#VerValorAprobadoSGdiv').html('Sin Aprobacion');
//       }
//       if (deditI[21] != null) {
//         $('#VerDocAnticipoSGdiv').html("#"+deditI[21]+"<br>Doc Equiv.: "+deditI[22]+"<br>Valor: $"+formatNumber.new(deditI[23]));
//         mostrarAprobacionSolicitudGasto(deditI[0]);  
//       } else {
//         $('#VerDocAnticipoSGdiv').html('pendiente Anticipo');
//       }
//       var totalSG=formatNumber.new(deditI[9]);
//       $('#VerValorTotaldiv').html('$  '+ totalSG);
      
//       deditR=datoresponsables.split('||');
//       $('#VerresponsableSGdiv').val(deditR);
//       $('#VerresponsableSGdiv').trigger('change');
//       $('#VerresponsableSGdiv').select2({
//         disabled: true
//       });
//       mostrarDetalleSG(deditI[0],'ListaDetalleSG');
//       CargarEntidadesSelect2(deditI[0]);
//     }
//   });

    
// }

function CargarResponsablesenSelect2(IdSG,Name_DOM){
  var params = {IdSG};

    var url = "../../logica/logica.php?accion=CargarResponsablesSelect2";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
        $('#'+Name_DOM).val(result).change();
        $('#'+Name_DOM).trigger('change');
        $('#'+Name_DOM).select2({
          disabled: true
        });
    });
}

function CargarEntidadesSelect2(IdSG){
  var params = {IdSG};
    var url = "../../logica/logica.php?accion=CargarEntidadesSelect2";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(resulte) {
        $('#VerentidadesSGdiv').val(resulte);
        $('#VerentidadesSGdiv').trigger('change');
        $('#VerentidadesSGdiv').select2({
          disabled: true
        });
    });
}

// ******** Modulo aprobacion Solicitud Gastos *********

function CargarEntidadesSelect2_VerAprovacion(IdSG){
  var params = {IdSG};
    var url = "../../logica/logica.php?accion=CargarEntidadesSelect2";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(resulte) {
        $('#VerentidadesSGdiv2').val(resulte);
        $('#VerentidadesSGdiv2').trigger('change');
        $('#VerentidadesSGdiv2').select2({
          disabled: true
        });
    });

}

function mostrarAprobacionSolicitudGasto(idSG){
    var params = {idSG};
    var url = "../../logica/logica.php?accion=BuscarAprobacionSG";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      var 
            texto='<div class="col-md-6 col-sm-6 col-xs-6">';
              texto+='<div class="panel panel-default">';
                  texto+='<divclass="panel-heading clearfix">';
                      texto+='<h3 class="panel-title">Detalle Aprobacion </h3> - # '+result[1];
                      texto+='<div id="close">';
                      texto+='<button Title="Cerrar" type="button" class="btn-default btn" onclick="HideVerAprobacionSolicitudGasto();">cerrar</button>';
                      texto+='</div>';
                  texto+='</div>';
                  texto+='<div class="panel-body">'
                    texto+='<table class="table table-hover">';
                      texto+='<tbody>';
                        texto+='<tr class="table-light">';
                          texto+='<th scope="row">Id Doc Aprobacion</th>';
                          texto+='<td><div>'+result[0]+'</div></td>';
                        texto+='</tr>';
                        texto+='<tr class="table-light">';
                          texto+='<th scope="row">Fecha Documento</th>';
                          texto+='<td><div>'+result[2]+'</div></td>';
                        texto+='</tr>';
                        texto+='<tr class="table-light">';
                          texto+='<th scope="row">Usuario</th>';
                          texto+='<td><div>'+result[3]+'</div></td>';
                        texto+='</tr>';
                        texto+='<tr class="table-light">';
                          texto+='<th scope="row">Valor Aprobacionado</th>';
                          texto+='<td><div>$ '+formatNumber.new(result[4])+'</div></td>';
                        texto+='</tr>';
                      texto+='</tbody>';
                    texto+='</table> ';
                  texto+='</div>';
              texto+='</div>';
          texto+='</div>';
      texto+='</div>';
      $('#VerAprobacionSG').html(texto);
    });
}

function AprobarSolicitudGastodesdeSolicitudes(IdSG){
    var codempleado=$("#empleadologin").val();
    $("#IdSolicitudGastoSG2").val(IdSG).change();
    $('#IdSolicitudGastoSG2').attr('disabled', 'disabled');
    $('#SelectMultipleResponsables').show();
    $('#responsableSG2').val(0).change();
    $('#VrAproSolicGastoSG').val(null);
    $('#ObservacionesAprobacionSG').val(null);
    $('#responsableSG2').val(codempleado).change();
    AprobarSolicitudGasto2(IdSG);
}

function AprobarSolicitudGasto2(IdSG){
  if (IdSG != "00") {
    
  var parametros={IdSG};

 $.ajax({
   url: "../../logica/logica.php?accion=BuscarIDSG",
   type: "POST",
   data: parametros,
   dataType: 'json',
   
   success: function(deditI){
     
     $('#VerCodDepartamentoSGdivApr').html(deditI[15]);
     $('#VerCodVeredaSGdivApr').html(deditI[16]);
     $('#VerCodMunicipioSGdivApr').html(deditI[14]);
     $('#VerCodProyectoSGdivApr').html(deditI[17]);
     $('#VerCodProcesoSGdivApr').html(deditI[19]);
     $('#VerCodActividadSGdivApr').html(deditI[18]);
     $('#VerFechaHoraSalidaSGdivApr').html(deditI[6]);
     $('#VerFechaHoraRegresoSGdivApr').html(deditI[7]);

      if (deditI[20] != null) {
        var ValorAprobadoSG=formatNumber.new("$"+deditI[20]);
        $('#VerValorAprobadoSGdivApr').html(ValorAprobadoSG);
      } else {
        $('#VerValorAprobadoSGdivApr').html('Sin Aprobacion');
      }
      if (deditI[21] != null) {
       $('#VerDocAnticipoSGdivApr').html("#: "+deditI[21]+"/<strong> Doc Equiv:</strong> "+deditI[22]+"/<strong> Valor:</strong> $"+deditI[23]);
      } else {
       $('#VerDocAnticipoSGdivApr').html('pendiente Anticipo');
      }
     var totalSG=formatNumber.new(deditI[9]);
     $('#VerValorTotaldivApr').html('$  '+ totalSG);
     
     CargarEntidadesSelect2_VerAprovacion(IdSG);
     CargarResponsablesenSelect2(IdSG,'VerresponsableSGdiv2');
     mostrarDetalleSG(deditI[0],'ListaDetalleSGApr')
   }
 });
}
}      
  
function AprobacionSGMultiple(SGparaAprobar,SGValoresSeleccion){

  var responsable=$("#responsableSGAprobacionMultiple").val();
  
  if (responsable == 0 || responsable == '') {
    AlertModalmsg('amarillo','Atención','Falta seleccionar el empledo que aprueba, por favor verifique','MsgModalSG');
    // $("#msgSolicitudGasto").html("<div class='alert alert-dismissible alert-warning'><strong>Se debe seleccionar los datos obligatorios.  Intenta nuevamente</strong></div>");
  } else {

    var fecha=$("#fechadatetimepicker").html();
    var observaciones=$("#ObservacionSGMultiple").val();

    if (observaciones=='') {
      observaciones='Se aprueba el valor total de la solicitud de gastos';
    }
    
    parametros={SGparaAprobar,fecha,responsable,SGValoresSeleccion,observaciones};
    $.ajax({
      type: "POST",
      url: '../../logica/logica.php?accion=InsertarAprobacionMultipleSolicitudGasto',
      data: parametros,
      success: function (response) {
        if (response==1) {
          AlertModalmsg('verde','Aprobación Exitosa !','Documentos SG aprobados con Exito !','MsgModalSG');
          $("#ModalUpLoadLegalizacion").modal('hide');//ocultamos el modal
          $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
          $('.modal-backdrop').remove();//eliminamos el backdrop del modal
          location.reload();
        } else {
          AlertModalmsg('rojo','Error:','Por favor comuniquese con soporte','MsgModalSG');
        }
      }
    });
  }
}



function mostrarDetalleSG(IdSG,IdDOM) {
  
  var parametros={IdSG,IdDOM};
  $.ajax({
      url:'../../logica/logica.php?accion=ListarDetalleSG',
      type: "POST",
      data: parametros,
      beforeSend: function(objeto){
        $('#'+IdDOM).html('Cargando...');
      },
      success: function(data){
                $("#"+IdDOM).html(data);
      }
  });   
}


var formatNumber = {
  separador: ",", // separador para los miles
  sepDecimal: '.', // separador para los decimales
  formatear:function (num){
  num +='';
  var splitStr = num.split('.');
  var splitLeft = splitStr[0];
  var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
  var regx = /(\d+)(\d{3})/;
  while (regx.test(splitLeft)) {
  splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
  }
  return this.simbol + splitLeft +splitRight;
  },
  new:function(num, simbol){
    this.simbol = simbol ||'';
    return this.formatear(num);
  }
 }


function formeditTipoNovMater(datoPrograma){
  deditI=datoPrograma.split('||');
  $('#idTipoNovMaterFM').val(deditI[0]);
  $('#descTipoNovMaterFM').val(deditI[1]);
}

function InsertTipoNovedadMater(){
  var descTipoNovMater=$('#descTipoNovMater').val();
  if(descTipoNovMater==""){
    $("#msgTipoNovMater").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  }else{
    var parametros={descTipoNovMater};
    $.ajax({
      url: "../../logica/logica.php?accion=InsertTipoNovedadMater",
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: parametros,
    }).done(function(result) {
      if(result == 1){
        $("#msgTipoNovMater").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Guardado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgTipoNovMater").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgTipoNovMater").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No fue posible insertar el dato, por favor comuniquese con soporte tecnico.</div>");
      }
    });
  }
}

function EditarTipoNovMater(){
  var idTipoNovMater=$('#idTipoNovMaterFM').val();
  var descTipoNovMater=$('#descTipoNovMaterFM').val();
  if (idTipoNovMater===""|| descTipoNovMater=="") {
    $("#msgEditTipoNovMater").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var params = {idTipoNovMater, descTipoNovMater};
    var url = "../../logica/logica.php?accion=EditarTipoNovMater";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgEditTipoNovMater").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgEditTipoNovMater").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgEditTipoNovMater").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
}
$("#msgEditTipoNovMater").delay(3000).fadeOut(300);
return;
}

// function MostrarSolicitudGastoxid() {
//   $('#IdSolicitudGastoSG').change(function(){    
//     var IdSG = $("#IdSolicitudGastoSG option:selected").val();
//     var params = {IdSG};
//     var url = "../../logica/logica.php?accion=getSolcitudGastoxAprobar";
//     $.ajax({
//       url: url,
//       type: 'POST',
//       cache: false,
//       dataType: 'json',
//       data: params,
//     }).done(function(result) {
               
//     });

//   })
// }

// *************** Inicio Modulo Aprobacion solicitud Gastos ***************

function guardarAprobacionSolicitudGasto(){
  var IdSolicitudGastoSG=$('#IdSolicitudGastoSG2').val();
  var FechaAprobacionSG=$('#fechadatetimepicker').text();
  var responsableSG=$('#responsableSG2').val();
  var VrAproSolicGastoSG=$('#VrAproSolicGastoSG').val();
  var ObservacionesAprobacionSG=$('#ObservacionesAprobacionSG').val();
  if (responsableSG==0) {
    $("#msgAprobacionSolicitudGasto").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Debe seleccionar el emplado que aprueba</div>");
  } else {
    var params = {IdSolicitudGastoSG,FechaAprobacionSG,responsableSG,VrAproSolicGastoSG,ObservacionesAprobacionSG};
    var url = "../../logica/logica.php?accion=InsertarAprobacionSolicitudGasto";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgAprobacionSolicitudGasto").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
        location.reload();
      } else if (result == 3) {
        $("#msgAprobacionSolicitudGasto").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgAprobacionSolicitudGasto").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
  }
  $("#msgAprobacionSolicitudGasto").delay(3000).fadeOut(300);
  return;
}

function AprobarSolicitudGasto(datoPrograma){
  deditI=datoPrograma.split('||'); 
  $('#NuevaAprobacionSolicitudGasto').modal('show');
  $('#IdSolicitudGastoSG2 option[value='+ deditI[0] +']').attr('selected',true);
  $('#idTipoNovMaterFM').val(deditI[0]);
  $('#descTipoNovMaterFM').val(deditI[1]);
}

function formeditAprobacionSolicGasto(datoPrograma){
  deditI=datoPrograma.split('||');
  $('#idAprobacionFM').val(deditI[0]);
  $('#idSolicGastoFM').val(deditI[1]);
  $('#FechaAprobacionFM').val(deditI[2]);
  $('#UsuarioLegFM').val(deditI[3]);
  $('#ValorAprobacionFM').val(deditI[4]);
}

function EditarAprobacion(){
  var IdAprobacion=$('#idAprobacionFM').val();
  var IdSolicitud=$('#idSolicGastoFM').val();
  var FechaAprobacion = $('#FechaAprobacionFM').val();
  var usuarioAprobacion=$('#UsuarioLegFM').val();
  var valorAprobacion = $('#ValorAprobacionFM').val();
  if (valorAprobacion==="") {
    $("#msgAprobacionEdit").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var params = {IdAprobacion,IdSolicitud,FechaAprobacion,usuarioAprobacion,valorAprobacion};
    var url = "../../logica/logica.php?accion=EditarAprobacion";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgAprobacionEdit").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgAprobacionEdit").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgAprobacionEdit").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
}
$("#msgAprobacionEdit").delay(3000).fadeOut(300);
return;
}

function mostrarCABSGAprobacion(IdSG) {
  var parametros={IdSG};
  // $("#CabSG").show();
  $.ajax({
      url:'../../logica/logica.php?accion=ListarCABSG',
      type: "POST",
      data: parametros,
      cache: false,
      dataType: 'json',
      success: function(data){
          texto=`<div class="table-responsive">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading clearfix">
            
                        Solicitud de Gasto: <label id="IdSG">`+data[0]+`</label> - Fecha elaboracion: `+data[1]+` - Usuario creacion: ?? 
                    </div>
                    <div class="panel-body">
                        <div class="row col-md-6 col-sm-6">
                            <table class="table table-hover">
                                <tbody>
                                <tr class="table-light">
                                  <th scope="row" style="width:100px">Departamento</th>
                                  <td><div name="VerCodDepartamentoSGdiv" id="VerDepartamentoSGdiv">`+data[18]+`</div></td>
                                </tr>
                                    <tr class="table-light">
                                      
                                        <th scope="row" style="width:100px">Municipio</th>
                                        <td><div name="VerCodMunicipioSGdiv" id="VerCodMunicipioSGdiv">`+data[14]+`</div></td>
                                    </tr>
                                    <tr class="table-light">
                                        <th scope="row">Proyecto</th>
                                        <td><div name="VerCodProyectoSGdiv" id="VerCodProyectoSGdiv">`+data[15]+`</div></td>
                                    </tr>
                                    <tr class="table-light">
                                        <th scope="row">Proceso</th>
                                        <td><div name="VerCodProcesoSGdiv" id="VerCodProcesoSGdiv">`+data[16]+`</div></td>
                                    </tr>
                                    <tr class="table-light">
                                        <th scope="row">Actividad</th>
                                        <td><div name="VerCodActividadSGdiv" id="VerCodActividadSGdiv">`+data[17]+`</div></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="row col-md-6 col-sm-6">
                            <table class="table table-hover">
                                <tbody>
                                    <tr class="table-light">
                                        <th scope="row" style="width:170px">Fecha/Hora Salida</th>
                                        <td><div name="VerFechaHoraSalidaSGdiv" id="VerFechaHoraSalidaSGdiv">`+data[6]+`</div></td>
                                    </tr>
                                    <tr class="table-light">
                                        <th scope="row">Fecha/Hora Regreso</th>
                                        <td><div name="VerFechaHoraRegresoSGdiv" id="VerFechaHoraRegresoSGdiv">`+data[7]+`</div></td>
                                    </tr>
                                    <tr class="table-light">
                                        <th scope="row">Cant Colección</th>
                                        <td><div name="VerCantColeccionSGdiv" id="VerCantColeccionSGdiv">`+data[8]+`</div></td>
                                    </tr>
                                    <tr class="table-light">
                                        <th scope="row">Tipo Colección</th>
                                        <td><div name="VerTipoColeccionSGdiv" id="VerTipoColeccionSGdiv"></div>`+data[9]+`</td>
                                    </tr>
                                          
                                </tbody>
                            </table>
                          </div>      
                </div>
            </div>
        </div>`;
        $('#CabSG').html(texto);
      }
  });
}

function mostrarCABSG(IdSG) {
  var parametros={IdSG};
  $("#CabSG").show();
  $.ajax({
      url:'../../logica/logica.php?accion=BuscarIDSG',
      type: "POST",
      data: parametros,
      cache: false,
      dataType: 'json',
      success: function(deditI){
        $('#VerIdSolicitudGastoSG').html(deditI[0]);
        $('#VerFechaSolicitudGastoSG').html(deditI[1]);
        $('#VerCodDepartamentoSGdiv').html(deditI[14]);
        $('#CodMunicipio').html(deditI[5]);
        
        $('#VerCodMunicipioSGdiv').html(deditI[13]);
        $('#VerCodProyectoSGdiv').html(deditI[15]);
        $('#VerCodProcesoSGdiv').html(deditI[17]);
        $('#VerCodActividadSGdiv').html(deditI[16]);
        $('#VerFechaHoraSalidaSGdiv').html(deditI[6]);
        $('#VerFechaHoraRegresoSGdiv').html(deditI[7]);

        if (deditI[18] != null) {
          var ValorAprobadoSG=formatNumber.new("$"+deditI[18]);
          $('#VerValorAprobadoSGdiv').html(ValorAprobadoSG);
        } else {
          $('#VerValorAprobadoSGdiv').html('Sin Aprobacion');
        }

        if (deditI[21] != null) {
          $('#VerDocAnticipoSGdiv').html("<strong>#: </strong>"+deditI[19]+"/<strong> Doc Equiv:</strong> "+deditI[20]+"/<strong> Valor:</strong> $"+formatNumber.new(deditI[21]));
          mostrarAprobacionSolicitudGasto(deditI[0]);  
        } else {
          $('#VerDocAnticipoSGdiv').html('pendiente Anticipo');
        }

        var totalSG=formatNumber.new(deditI[9]);
        $('#VerValorTotaldiv').html('$  '+ totalSG);
       
        CargarResponsablesenSelect2(deditI[0],'VerresponsableSGdiv');
        CargarEntidadesSelect2(deditI[0]);
      }
  });
}


//************** Inicio Legalizacion de Gastos ****************

function mostrarCABRG(IdRG){
  
  var parametros={IdRG};
  $.ajax({
    url:'../../logica/logica.php?accion=ListarCABRG',
    type: "POST",
    data: parametros,
    cache: false,
    dataType: 'json',
    success: function(data){
      texto=`
      <div style="display:none;">
        <div id="IdLegalizacion">`+data[0]+`</div>
        <div id="FechaLegalizacion">`+data[2]+`</div>
        <div id="ObservacionesLegalizacion">`+data[3]+`</div>
        <div id="TotalLegalizacion">`+data[4]+`</div>
        <div id="UsuarioLegalizacion">`+data[5]+`</div>
      </div>`;
      $('#CABRG').html(texto);
    }
  });
}

function guardarNuevaLegalizacionSG(IdSG) {

  
  var UsuarioLegalizacionGastos=$('#VerresponsableSGdivLeg').val();
  var ObservacionesLegalizacionGasto=$('#ObservacioneLegalizacionSGdiv').val();
  var FechaLegalizacionGastos=$('#fechadatetimepicker').text();
  var parametros={IdSG,FechaLegalizacionGastos,UsuarioLegalizacionGastos,ObservacionesLegalizacionGasto};
    $.ajax({
      url:'../../logica/logica.php?accion=GuardarLegalizacionSG',
      type: "POST",
      data: parametros,
      cache: false,
      dataType: 'json',     
        success: function(data)
        {
          $('#DocumentoSG').show();
          $('#CabIdSG_LG').show();
          mostrarCABSG(IdSG);
          mostrarCABRG(data);
          mostrarDetalleSGLegalizacion(IdSG,data);
          $('#ConceptoGastoLegalizacion').html(data);
          $('#NuevaLegalizacionG').attr("disabled", true);
        }
      });
  }

  function mostrarDetalleSGLegalizacion(IdSG,IdRG) {
    var IddetalleRG= $('#IdDetalleRG').text();
    var parametros={IdSG,IdRG,IddetalleRG};

    $.ajax({
        url:'../../logica/logica.php?accion=ListarDetalleSGLegalizacion',
        type: "POST",
        data: parametros,
        beforeSend: function(objeto){
          $('#ListaDetalleSGLegalizacion').html('Cargando...');
      },success: function(data){
          $("#ListaDetalleSGLegalizacion").html(data).fadeIn('slow');
      }
    });
}

function mostrarLGAdicionalxDetalleIdRG(IdDetalleRG){
  $("#ImagenSoporteFisico").attr("src",'');
  var parametros={IdDetalleRG};
  $.ajax({
    url:'../../logica/logica.php?accion=mostrarDLGxDIDLG',
    type: "POST",
    data: parametros,
    dataType: 'json',
    success: function(data){
      
        texto=`
          <table class="table table-striped table-hover">
            <tr>
              <td align="center">Item</td>
              <td align="left">Concepto</td>
              <td align="left">Vereda</td>
            </tr>
            <tr>
              <td align="center">`+$('#numitem'+IdDetalleRG).text()+`</td>
              <td align="left"><div id="NombreConcepto">`+$('#NombreConcepto'+IdDetalleRG).text()+`</div></td>
              <td align="left">`+data[16]+`</td>
              </tr>
          </table>`;
          $('#ResumenSG').html(texto);
          $('#NitBeneficiario').val(data[3]);
          $('#NombreBeneficiario').val(data[4]);
          $('#NumeroFactura').val(data[5]);
          $('#ValorFactura').val(formatNumber.new(data[6]));
          $('#Observaciones').val(data[8]);
          $("#PagoTCD").val(data[7]);
          $("#DireccionBeneficiario").val(data[11]);
          $("#TelefonoBeneficiario").val(data[12]);
          $("#EntregaRUT").val(data[13]);
          $("#EntregaCedula").val(data[14]);
          // $("#ImagenSoporteFisico").attr("src",data[9]);
          
            if (data[9]==null) {
              $("#ImagenSoporteFisico").attr("src",'../../img/iconos/sin_imagen.jpg').change();
            } else {
              $("#ImagenSoporteFisico").attr("src",data[9]).change();
            }
            
          $('#modal_footer_DetalleLG').html(`
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" id="GuardarLegalizacion" name="GuardarLegalizacion" onclick="GuardarDetalleLegalizacion(`+IdDetalleRG+`);" class="btn btn-primary">
                <span class="fa fa-save"></span><span class="hidden-xs"> Guardar</span>
            </button>
          `);
      }
  });
}

function mostrarlegalizaciondegastos(IdDetalleRG,IdDetalleSG,DetalleRGNuevo){
  $('#ResumenSG').html('');
  if (DetalleRGNuevo==0) {  //DetalleRGNuevo=0 : Se agregara una legalizacion se un concepto que se agrego desde la solicitud
    var item= '#numitem'+IdDetalleSG;
    var nombreconcepto='#NombreConcepto'+IdDetalleSG;
    var numdias='#numdias'+IdDetalleSG;
    var IdConcepto='#IdConcepto'+IdDetalleSG;
    var IdVereda= '#IdVereda'+IdDetalleSG;
    var NombreVereda= '#Vereda'+IdDetalleSG;
    var IdVereda=$(IdVereda).text();
    var NombreVereda=$(NombreVereda).text();
    var numitem=$(item).text();
    var NombreConcepto=$(nombreconcepto).text();
    var numdias=$(numdias).text();
    var IdConcepto=$(IdConcepto).text();

    var parametros={IdDetalleRG};
  } else { // DetalleRGNuevo=1 : Significa que se insertara una legalizacion de gastos nueva agregando un concepto que no estaba antes
    var IdConcepto=$("#ConceptoGastoRG option:selected").val();
    var NombreConcepto=$("#ConceptoGastoRG option:selected").text();
    var IdVereda=$("#CodVeredaSG").val();
    var IdDetalleSG='null';
    var parametros={IdConcepto}
  }
  
  if (IdConcepto == 0 || IdVereda == 0) {
    $("#AvisoForm").html("<div class='alert alert-dismissible alert-warning'><strong>Debe seleccionar un concepto y la vereda </strong></div>");
    $('#GuardarLegalizacion').attr("disabled", true);
    $("#body_form").hide();
  } else {
    $("#AvisoForm").remove();
    $('#GuardarLegalizacion').attr("disabled", false);
    $("#body_form").show();
  }
  
  if (IdDetalleRG == 0) { //Si el detalle de Legalizacion no tiene registros, se vacian imputs para que se inserte info
    var texto=`
    <div id="IDConceptoRG" style="display:none;">`+IdConcepto+`</div>
    <div id="IDDetalleSGRG" style="display:none;">`+IdDetalleSG+`</div>
    <div id="IdVereda" style="display:none;">`+IdVereda+`</div>`;

   
    texto+=`
      <table class="table table-striped table-hover">
        <tr>`;
        if (DetalleRGNuevo==0) {
          texto+=`<td align="center">Item</td>`;
        }
          texto+=`<td align="left">Concepto</td>`;
        if (DetalleRGNuevo==0) {
          texto+=`<td align="center">Cant</td>`;
        }

        texto+=`</tr>
        <tr>`;
        if (DetalleRGNuevo==0) {
          texto+=`<td align="center">`+numitem+`</td>`;
        }
          texto+=`<td align="left">`+NombreConcepto+`</td>`;
          if (DetalleRGNuevo==0) {
            texto+=`<td align="center">`+numdias+`</td>`;
          }
        texto+=`</tr>
      </table>`;
    
    $('#ResumenSG').html(texto);
    $('#NitBeneficiario').val('');
    $('#NombreBeneficiario').val('');
    $('#NumeroFactura').val('');
    $('#ValorFactura').val('');
    $('#Observaciones').val('');
    $("#DireccionBeneficiario").val('');
    $("#TelefonoBeneficiario").val('');
    $("#PagoTCD").val(0);
    $("#EntregaRUT").val(0);
    $("#EntregaCedula").val(0);
    $('#Soporte_Legalizacion').val(null);
    $('#GuardarLegalizacion').attr("enabled", true);
    $("#ImagenSoporteFisico").attr("src",null).change();

    $('#modal_footer_DetalleLG').html(`
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      <button type="button" id="GuardarLegalizacion" name="GuardarLegalizacion" onclick="GuardarDetalleLegalizacion(0);" class="btn btn-primary">
          <span class="fa fa-save"></span><span class="hidden-xs"> Guardar</span>
      </button>
    `);

  } else { // Si el detalle de Legalizacion Existe, estos se muestran en los inputs

    $.ajax({
      url:'../../logica/logica.php?accion=MostrarDetalleLegalizacionSG',
      type: "POST",
      data: parametros,
      dataType: 'json'
      ,success: function(data){
        var texto=`
            <div id="IdDetalleLG" style="display:none;">`+data[0]+`</div>
            <div id="IDDetalleSGRG" style="display:none;">`+data[10]+`</div>
            <table class="table table-striped table-hover">
              <tr>
                <td align="center">Item</td>
                <td align="left">Concepto</td>
                <td align="center">Cant</td>

              </tr>
              <tr>
                <td align="center">`+numitem+`</td>
                <td align="left">`+data[15]+`</td>
                <td align="center">`+numdias+`</td>
            </tr>
            </table>
                `;
            $('#ResumenSG').html(texto);
            $('#NitBeneficiario').val(data[3]);
            $('#NombreBeneficiario').val(data[4]);
            $('#NumeroFactura').val(data[5]);
            $('#ValorFactura').val(formatNumber.new(data[6]));
            $('#Observaciones').val(data[8]);
            $("#DireccionBeneficiario").val(data[11]);
            $("#TelefonoBeneficiario").val(data[12]);
            $("#PagoTCD").val(data[7]);
            $("#EntregaRUT").val(data[13]);
            $("#EntregaCedula").val(data[14]);
            alert(data[9]);
            if (data[9]=='') {
              $("#ImagenSoporteFisico").attr("src",'../../img/iconos/sin_imagen.jpg').change();
            } else {
              $("#ImagenSoporteFisico").attr("src",data[9]).change();
            }
            
        }
    });
    $('#modal_footer_DetalleLG').html(`
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <button type="button" id="GuardarLegalizacion" name="GuardarLegalizacion" onclick="GuardarDetalleLegalizacion(`+IdDetalleRG+`);" class="btn btn-primary">
        <span class="fa fa-save"></span><span class="hidden-xs"> Guardar</span>
    </button>
  `);
  }
}

function GetnombreconceptoLG(IdConcepto){
  var parametros={IdConcepto};
  $.ajax({
    type: "POST",
    url: "../../logica/logica.php?accion=GetnombreconceptoLG",
    data: parametros,
    dataType: "json",
    success: function (data) {
      $('#NombreConcepto').html(data);
    }
  });
}

function GuardarDetalleLegalizacion(IdDetalleRG) {
  if ($("#Soporte_Legalizacion")[0].files.length == 0 && $('#ImagenSoporteFisico').prop('src')=='') {
    var Input_imagen= 0;
  } else {
    var Input_imagen= $("#Soporte_Legalizacion")[0].files.length;
    var Imagen_Cargada=$('#ImagenSoporteFisico').prop('src');
  }
    var IdRG=$('#IdLegalizacion').text();
    var IdSG=$('#IdSG').text();
    var IdVereda=$('#IdVereda').text();
    var IdDetalleSG=$('#IDDetalleSGRG').text();
    var IdConcepto=$('#IDConceptoRG').text();
    var NitBeneficiario=$('#NitBeneficiario').val();
    var NombreBeneficiario=$('#NombreBeneficiario').val();
    var DireccionBeneficiario=$('#DireccionBeneficiario').val();
    var TelefonoBeneficiario=$('#TelefonoBeneficiario').val();
    var NumeroFactura=$('#NumeroFactura').val();
    var ValorFactura=$('#ValorFactura').val();

    if (NitBeneficiario=="" || NombreBeneficiario=="" || NumeroFactura=="" || ValorFactura=="" || DireccionBeneficiario=="" || TelefonoBeneficiario=="") {
      AlertModalmsg('amarillo','Atención!','Faltan campos por llenar.  Por favor verifique','MsgLG');
      $("#msgLegalizacionSolicitudGasto").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
    } else {
      var formData = new FormData();
      
      formData.append('Soporte_Legalizacion', $("#Soporte_Legalizacion")[0].files[0]);
      formData.append('IdRG',IdRG);
      formData.append('IdSG',IdSG);
      formData.append('IdVereda',IdVereda);
      formData.append('IdDetalleSG',IdDetalleSG);
      formData.append('IdDetalleRG',IdDetalleRG);
      formData.append('IdConcepto',IdConcepto);
      formData.append('NitBeneficiario', $('#NitBeneficiario').prop('value'));
      formData.append('NombreBeneficiario', $('#NombreBeneficiario').prop('value'));
      formData.append('DireccionBeneficiario', $('#DireccionBeneficiario').prop('value'));
      formData.append('TelefonoBeneficiario', $('#TelefonoBeneficiario').prop('value'));
      formData.append('NumeroFactura', $('#NumeroFactura').prop('value'));
      formData.append('ValorFactura', $('#ValorFactura').prop('value'));
      formData.append('PagoTCD', $('#PagoTCD').prop('value'));
      formData.append('EntregaRUT', $('#EntregaRUT').prop('value'));
      formData.append('EntregaCedula', $('#EntregaCedula').prop('value'));
      formData.append('Observaciones', $('#Observaciones').prop('value'));
      formData.append('Input_imagen', Input_imagen);
      formData.append('Imagen_Cargada', Imagen_Cargada);

      // var parametros={IdRG,IdDetalleSG,IdConcepto,NitBeneficiario,NombreBeneficiario,DireccionBeneficiario,TelefonoBeneficiario,NumeroFactura,ValorFactura,PagoTCD,Observaciones,EntregaRUT,EntregaCedula,formadata};
      $.ajax({
        url:'../../logica/logica.php?accion=GuardarDetalleLegalizacion',
        type: "POST",
        data: formData,
        // dataType: 'json'
        contentType: false,
        cache: false,
        processData:false
        ,success: function(data){
            if (data==1) {
              $("#ModalUpLoadLegalizacion").modal('hide');//ocultamos el modal
              $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
              $('.modal-backdrop').remove();//eliminamos el backdrop del modal
              $('#DocumentoSG').show();
              mostrarDetalleSGLegalizacion(IdSG,IdRG);
              AlertModalmsg('verde','Exito !','Registro guardado con exito','MsgLG');
              // $("#msgLegalizacionSolicitudGasto").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Legalizacion Gasto, Insertado con Exito !!</strong></div>");
            } else {
              AlertModalmsg('rojo','Error:','Por favor comuniquese con soporte','MsgLG');
              // $("#msgLegalizacionSolicitudGasto").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No fue posible Insertarlo, comuniquese con SOPORTE</div>");
            }
        }
      });
    }
    // $("#msgLegalizacionSolicitudGasto").delay(3000).fadeOut(300);
      return;
  
}

function AbrirLegalizacion(IdSG){
  
  $(location).attr('href',"../legalizaciongastos/legalizardetallegasto.php?IdSG="+IdSG);
  
}

function ArchivarSG(IdSG){
  var fecha=$("#fechadatetimepicker").html();
  var IdUsuariologin=$("#IdUsuariologin").val();
  $.ajax({
    type: "POST",
    url: '../../logica/logica.php?accion=ArchivarSG',
    data: {IdSG,fecha,IdUsuariologin},
    // dataType: "dataType",
    success: function (response) {
      
      if (response == 1) {
        var $miModal1 = $('#ModalMensajeArchivarSG');
        $miModal1.modal('show');
        setTimeout(function() { $miModal1.modal('toggle'); }, 4000); 
        $(location).attr('href','../../modulos/legalizaciongastos/indexLegalizacionGastos.php');
      } else {
        
      }
    }
  });
}

//************** Fin Legalizacion de Gastos ****************


// ************* Inicio Asignacion Donantes ***************


  function InsertAsignacionDonntes() {
    $('#VerCodMunicipio').select2({
      disabled: false
    });
    var IdProyecto=$('#IdProyecto').val();
    var fechaInicioDonante=$('#fechaInicioDonante').val();
    var fechaFinalDonante=$('#fechaFinalDonante').val();
    var idEntidad=$('#idEntidad').val();
    var Cuantia=$('#Cuantia').val();
    var TipoVinculacion=$('#TipoVinculacion').val();
    var observaciones=$('#observaciones').val();
    var CodMunicipio=$('#VerCodMunicipio').val();
    var parametros={IdProyecto,fechaInicioDonante,fechaFinalDonante,idEntidad,Cuantia,TipoVinculacion,observaciones,CodMunicipio};
    $.ajax({
      url:'../../logica/logica.php?accion=GuardarAsignacionDonante',
      type: "POST",
      data: parametros
      ,success: function(data){
        if (data==1) {
          $("#msgInsertDonantes").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Asignacion Donante, Insertado con Exito !!</strong></div>");
          location.reload();
        } else {
          $("#msgInsertDonantes").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No fue posible Insertarlo, comuniquese con SOPORTE</div>");
        }
      }
    });
  }

  // Editar asignacion donantes
  function formeditdonante(datoPrograma){
    $('#IdProyecto').val(null);
    $('#fechaInicioDonante').val(null);
    $('#fechaFinalDonante').val(null);
    $('#idEntidad').val(null);
    $('#Cuantia').val(null);
    $('#TipoVinculacion').val(null);
    $('#VerCodMunicipio').val(null);
    $('#IdProyecto').attr("disabled",false);
    $('#fechaInicioDonante').attr("disabled",false);
    $('#fechaFinalDonante').attr("disabled",false);
    $('#idEntidad').attr("disabled",false);
    $('#Cuantia').attr("disabled",false);
    $('#TipoVinculacion').attr("disabled",false);
    $('#VerCodMunicipio').attr("disabled",false);
    deditI=datoPrograma.split('||');
    var IdReg=deditI[0];
    $('#TituloAsignacionDonante').html(`Edicion de Asignacion de Donante - #`+IdReg);
    $('#FootAsignacionDonantes').html(`
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            <button type="button" onclick="EditarDonante(`+IdReg+`);" class="btn btn-primary">Guardar</button>`);
    
    $("#IdProyecto").val(deditI[1]);
    $('#fechaInicioDonante').val(deditI[2]);
    $('#fechaFinalDonante').val(deditI[3]);
    $("#idEntidad").val(deditI[4]);
    $('#Cuantia').val(formatNumber.new(deditI[5]));
    $('#TipoVinculacion').val(deditI[6]);
    
    CargarmunicpiosSelect2(deditI[0]);

    $('#VerCodMunicipio').select2({
      disabled: false
    });
    
  }

  // Ver asignacion donantes
  function VerDonantes(datoPrograma){
    deditI=datoPrograma.split('||');
    var IdReg=deditI[0];
    $('#TituloAsignacionDonante').html(`Ver Donante - #`+IdReg);
    $('#FootAsignacionDonantes').html(`
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
    `);
    $('#IdProyecto').val(null);
    $('#fechaInicioDonante').val(null);
    $('#fechaFinalDonante').val(null);
    $('#idEntidad').val(null);
    $('#Cuantia').val(null);
    $('#TipoVinculacion').val(null);
    $('#VerCodMunicipio').val(null);
    $('#IdProyecto').val(deditI[1]).attr("disabled", true);
    $('#fechaInicioDonante').val(deditI[2]).attr("disabled", true);
    $('#fechaFinalDonante').val(deditI[3]).attr("disabled", true);
    $('#idEntidad').val(deditI[4]).attr("disabled", true);
    $('#Cuantia').val(deditI[5]).attr("disabled", true);
    $('#TipoVinculacion').val(deditI[6]).attr("disabled", true);

    
    CargarmunicpiosSelect2(deditI[0]);

    $('#VerCodMunicipio').select2({
      disabled: true
    });
    
  }

  // cargar municipios para editar
  function CargarmunicpiosSelect2(IdasignacionDonantes){
    var params = {IdasignacionDonantes};
      var url = "../../logica/logica.php?accion=CargarMunicipiosAsignacionDonantes";
      $.ajax({
        url: url,
        type: 'POST',
        cache: false,
        dataType: 'json',
        data: params,
      }).done(function(result) {
          $('#VerCodMunicipio').val(result);
          $('#VerCodMunicipio').trigger('change');
          // $('#VerCodMunicipio').select2({
          //   disabled: false
          // });
      });
  }


  // editar asignacion de donantes 
  function EditarDonante(IdReg) {
    
    var IdProyecto=$('#IdProyecto').val();
    var fechaInicioDonante=$('#fechaInicioDonante').val();
    var fechaFinalDonante=$('#fechaFinalDonante').val();
    var idEntidad=$('#idEntidad').val();
    var Cuantia=$('#Cuantia').val();
    var TipoVinculacion=$('#TipoVinculacion').val();
    var VerCodMunicipio=$('#VerCodMunicipio').val();

    var parametros={IdReg,IdProyecto,fechaInicioDonante,fechaFinalDonante,idEntidad,Cuantia,TipoVinculacion,VerCodMunicipio};
    $.ajax({
      url:'../../logica/logica.php?accion=EditarAsignacionDonante',
      type: "POST",
      data: parametros,
      cache: false,
        dataType: 'json'
      ,success: function(data){
        if (data==1) {
          $("#msgInsertDonantes").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Asignacion Donante, Editado con Exito !!</strong></div>");
          location.reload();
        } else {
          $("#msgInsertDonantes").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No fue posible editarlo, comuniquese con SOPORTE</div>");
        }
      }
    });
  }

// ************* FIN Asignacion Donantes ***************

// Modulo de Alumno -->
function formeditAlumno(datoPrograma){
  deditI=datoPrograma.split('||');
  $('#IdAlumnoFM').val(deditI[0]);
  $('#codAlumnoFM').val(deditI[1]);
  $('#descAlumnoFM').val(deditI[2]);
  $('#estadoFM').val(deditI[3]);
  $('#fechanacimientoaFM').val(deditI[4]).change();
  $("#IdInstitucFM").val(deditI[5]).change();
}

function InsertAlumno(){
  var codAlumno=$('#codAlumno').val();
  var descAlumno=$('#descAlumno').val();
  var estado=$('#estado').val();
  var fechanac=$('#fechanacimientoa').val();
  var IdInstituc=$('#IdInstitucion').val();

  if(codAlumno==""||descAlumno==""||fechanac=="" ||IdInstituc==0 ){
    
    $("#msgAlumno").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  }else{
    var parametros={codAlumno,descAlumno,estado,fechanac,IdInstituc};
    $.ajax({
      url: "../../logica/logica.php?accion=InsertAlumno",
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: parametros,
    }).done(function(result) {
      if(result == 1){
        $("#msgAlumno").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Guardado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgAlumno").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgAlumno").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No fue posible insertar el dato, por favor comuniquese con soporte tecnico.</div>");
      }
    });
  }
  setTimeout(function() {
    $("#msgAlumno").html('');
  },3000);
}

function EditarAlumno(){
  var IdAlumno=$('#IdAlumnoFM').val();
  var codAlumno=$('#codAlumnoFM').val();
  var nombAlumno=$('#descAlumnoFM').val();
  var estAlumno=$('#estadoFM').val();
  var fechanac=$('#fechanacimientoaFM').val();
  var idInstitucion=$('#IdInstitucFM').val();

  if (codAlumno==""||nombAlumno==""||fechanac=="") {
    $("#msgEditAlumno").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  
  } else {
    var params = {IdAlumno,codAlumno,nombAlumno,estAlumno,fechanac,idInstitucion};
    var url = "../../logica/logica.php?accion=EditarAlumno";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgEditAlumno").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgEditAlumno").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgEditAlumno").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
}
  setTimeout(function() {
    $("#msgEditAlumno").html('');
  },3000);
}

// Modulo de Empleado -->
function formeditEmpleado(datoPrograma){
  deditI=datoPrograma.split('||');
  $('#IdEmpleadoFM').val(deditI[0]);
  $('#DocEmpleadoFM').val(deditI[1]);
  $('#nomEmpleadoFM').val(deditI[2]);
  $('#telEmpleadoFM').val(deditI[3]);
  $('#cargoEmplFM').val(deditI[4]);
  $("#idAreaFM option[value="+ deditI[5]+"]").attr("selected", true);
  $('#estadoEmpleFM').val(deditI[6]);
  $("#idUsuarioEmplFM option[value="+ deditI[7]+"]").attr("selected", true);
}

function InsertEmpleado(){
  var DocEmpleado=$('#DocEmpleado').val();
  var nomEmpleado=$('#nomEmpleado').val();
  var telEmpleado=$('#telEmpleado').val();
  var cargoEmpl=$('#cargoEmpl').val();
  var idArea=$('#idArea').val();
  var estadoEmple=$('#estadoEmple').val();
  var idUsuarioEmpl=$('#idUsuarioEmpl').val();
  if(DocEmpleado==""||nomEmpleado==""||telEmpleado==""||cargoEmpl==""||idArea==""||estadoEmple==""||idUsuarioEmpl==""){
    $("#msgEmpleado").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  }else{
    var parametros={DocEmpleado,nomEmpleado,telEmpleado,cargoEmpl,idArea,estadoEmple,idUsuarioEmpl};
    $.ajax({
      url: "../../logica/logica.php?accion=InsertEmpleado",
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: parametros,
    }).done(function(result) {
      if(result == 1){
        $("#msgEmpleado").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Guardado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgEmpleado").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgEmpleado").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No fue posible insertar el dato, por favor comuniquese con soporte tecnico.</div>");
      }
    });
  }

  setTimeout(function() {
    $("#msgEmpleado").html('');
  },3000);
}

function EditarEmpleado(){
  var idEmpleado=$('#IdEmpleadoFM').val();
  var DocEmpleado=$('#DocEmpleadoFM').val();
  var nomEmpleado=$('#nomEmpleadoFM').val();
  var telEmpleado=$('#telEmpleadoFM').val();
  var cargoEmpl=$('#cargoEmplFM').val();
  var idArea=$('#idAreaFM').val();
  var estadoEmple=$('#estadoEmpleFM').val();
  var idUsuarioEmpl=$('#idUsuarioEmplFM').val();

  if(DocEmpleado==""||nomEmpleado==""||telEmpleado==""||cargoEmpl==""){
    $("#msgEditEmpleado").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var params = {idEmpleado,DocEmpleado,nomEmpleado,telEmpleado,cargoEmpl,idArea,estadoEmple,idUsuarioEmpl};
    var url = "../../logica/logica.php?accion=EditarEmpleado";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgEditEmpleado").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgEditEmpleado").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgEditEmpleado").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
}
  setTimeout(function() {
    $("#msgEditEmpleado").html('');
  },3000);
}

//Modulo de Area
function InsertArea(){
  var descArea=$('#descArea').val();
  var estadoarea=$('#estadoarea').val();
  if(descArea=="" || estadoarea==""){
    $("#msgArea").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  }else{
    var parametros={descArea,estadoarea};
    $.ajax({
      url: "../../logica/logica.php?accion=InsertArea",
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: parametros,
    }).done(function(result) {
      if(result == 1){
        $("#msgArea").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Guardado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgArea").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgArea").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No fue posible insertar el dato, por favor comuniquese con soporte tecnico.</div>");
      }
    });
  }
  setTimeout(function() {
    $("#msgArea").html('');
  },3000);
}

function formeditArea(datoPrograma){
  deditI=datoPrograma.split('||');
  $('#IdAreaFM').val(deditI[0]);
  $('#desAreaFM').val(deditI[1]);
  $('#estadoAreaFM').val(deditI[2]).trigger('change');
}

function EditarArea(){
  var idArea=$('#IdAreaFM').val();
  var desArea=$('#desAreaFM').val();
  var estadoArea=$('#estadoAreaFM').val();
  
  if (desArea == "") {
    AlertModalmsg('amarillo','Atencion !','Los campos no pueden estar vacios','Mensaje_Area');
  } else {
    var params = {idArea,desArea,estadoArea};
    var url = "../../logica/logica.php?accion=EditarArea";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        AlertModalmsg('verde','Editado con Exito !','Registro editado con exito.','Mensaje_Area');
        location.reload(); 
      } else{
        AlertModalmsg('rojo','Error: ','Por favor comuniquese con soporte !','Mensaje_Area');
      } 
    });
  }
  $("#msgEditArea").delay(3000).fadeOut(300);
}

//Modulo entrega de anteojos
function insertEntrega(datoPrograma){
  deditI=datoPrograma.split('||');
  $('#MunicipioIn').val(deditI[3]);
}

function guardarEntregaAnteojos(){
  var IdResponsableEntr=$('#IdResponsableEntr').val();
  var idVdaBenef=$('#idVdaBenef').val();
  var mcpioEntrega=$('#Municipio').val();
  var fechaEntrega=$('#fechaEntrega').val();
  var beneficiario=$('#beneficiario').val();
  var telBeneficiario=$('#telBeneficiario').val();
  var correoBeneficiario=$('#correoBeneficiario').val();
  var personaRecibe=$('#personaRecibe').val();
  var tipoAnteojos=$('#tipoAnteojos').val();
  
  if (IdResponsableEntr==0 || idVdaBenef==0 || mcpioEntrega==0 || fechaEntrega==0 || beneficiario==0 || telBeneficiario==0 ||  correoBeneficiario==0 || personaRecibe==0 || tipoAnteojos==0) {
    $("#msgEntregaAnteojos").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var params = {IdResponsableEntr,idVdaBenef,mcpioEntrega,fechaEntrega,beneficiario,telBeneficiario,correoBeneficiario,personaRecibe,tipoAnteojos};
    var url = "../../logica/logica.php?accion=InsertEntregaAnteojos";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgEntregaAnteojos").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
        location.reload();
      } else if (result == 3) {
        $("#msgEntregaAnteojos").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgEntregaAnteojos").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
  }
  $("#msgEntregaAnteojos").delay(3000).fadeOut(300);
  return;
}

function formatEditarEntrega(datoPrograma){
  deditI=datoPrograma.split('||');
  $('#IdEntregaFM').val(deditI[0]);
  $('#IdResponsableFM').val(deditI[1]);
  $('#idVdaBenefFM').val(deditI[2]);
  $('#MunicipioFM').val(deditI[3]);
  $('#fechaEntregaFM').val(deditI[4]);
  $('#beneficiarioFM').val(deditI[5]);
  $('#telBeneficiarioFM').val(deditI[6]);
  $('#correoBeneficiarioFM').val(deditI[7]);
  $('#personaRecibeFM').val(deditI[8]);
  $('#tipoAnteojosFM').val(deditI[9]);
}

function EditarEntrega(){
  var idEntrega=$('#IdEntregaFM').val();
  var IdResponsableEntr=$('#IdResponsableFM').val();
  var idVdaBenef=$('#idVdaBenefFM').val();
  var mcpioEntrega=$('#MunicipioFM').val();
  var fechaEntrega=$('#fechaEntregaFM').val();
  var beneficiario=$('#beneficiarioFM').val();
  var telBeneficiario=$('#telBeneficiarioFM').val();
  var correoBeneficiario=$('#correoBeneficiarioFM').val();
  var personaRecibe=$('#personaRecibeFM').val();
  var tipoAnteojos=$('#tipoAnteojosFM').val();
  if (IdResponsableEntr===""|| idVdaBenef==""||mcpioEntrega==""||fechaEntrega==""||beneficiario==""||telBeneficiario==""||correoBeneficiario==""||personaRecibe==""||tipoAnteojos=="") {
    $("#msgEditEntrega").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var params = {idEntrega,IdResponsableEntr,idVdaBenef,mcpioEntrega,fechaEntrega,beneficiario,telBeneficiario,correoBeneficiario,personaRecibe,tipoAnteojos};
    var url = "../../logica/logica.php?accion=EditarEntrega";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgEditEntrega").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgEditEntrega").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgEditEntrega").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
}
$("#msgEditEntrega").delay(3000).fadeOut(300);
return;
}

// *************************  Modulo informe de instalacion  ****************************

function guardarInformeInstalacion(private_id){  
  var fecha=$('#fechaIns').val();
  var departamento=$('#CodDepartamentoSelect').val();
  var municipio=$('#municipio').val();
  var vereda=$('#idVereda').val();
  var responsables=$('#responsables').val();
  var patrocinador=$('#patrocinador').val();
  var centroEducRural=$('#centroEducRural').val();
  var NroFamilProgram=$('#NroFamilProgram').val();
  var NroFamilEntreg=$('#NroFamilEntreg').val();
  var InstEducativas=$('#InstEducativas').val();
  var nombre_docente=$('#nombre_docente').val();
  var telefono_docente=$('#telefono_docente').val();
  var correo_docente=$('#correo_docente').val();
  var observaciones=$('#observaciones').val();

  if (fecha==''||centroEducRural==''||NroFamilProgram==''||NroFamilEntreg==''||
  InstEducativas==''|| departamento==0 || municipio ==0 || vereda == 0 || responsables ==0 || patrocinador == 0) {
      AlertModalmsg('amarillo','ATENCION:','Los campos no pueden estar vacios.  Verifique por favor','ModalError_InformeInst')
    } else {
    var params = {private_id,responsables,departamento,municipio,patrocinador,fecha,vereda,
      centroEducRural,NroFamilProgram,NroFamilEntreg,InstEducativas,nombre_docente,telefono_docente,correo_docente,observaciones};
    var url = "../../logica/logica.php?accion=InsertInformeInstalac";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        
        AlertModalmsg('verde','Insertado !','Registro insertado con exito.','ModalError_InformeInst')
        location.reload();
      } else {
        AlertModalmsg('rojo','ERROR:',' Hubo un error comunicate con Soporte','ModalError_InformeInst')
        
      }
    });
  }
  $("#msginformeInstalc").delay(3000).fadeOut(300);
  return;
}


function ListaInstitucionesxVereda(IdVereda){
  var IdVereda=$('#'+IdVereda).val();
  var parametro={IdVereda};
  $.ajax({
    type: "POST",
    url: "../../logica/logica.php?accion=LlenarSelectInstitucionesxVeredas",
    data: parametro,
    dataType: "json",
    success: function (response) {
      $('#InstEducativas').html(response).change();
    }
  });
}


function formatEditInfInstalacion(data){
  
      $('#TituloModalInformeInst').html("Editar Informe de Instalacion - # <b>"+data[0]+"</b>");
      $('#agregarDetalle').html('<button type="button" id="AgregarMaterialInformeInstTMP" onclick="InsertDetalleInformeInst('+data[0]+')"><span class="glyphicon glyphicon-arrow-up"></span></button>');
      $('#modal-footerInfInsNuevo').html('');
      $('#modal-footerInfInsEditar').html(`
        
      <button type="button" onclick="EditarInformeInstalacion('+data[0]+');" class="btn btn-primary">Actualizar</button>
        `);
      $('#fechaIns').val(data[3]);
       $('#CodDepartamentoSelect').val(data[13]).trigger('change');
      $('#municipio').val(data[1]).trigger('change');           
      $('#idVereda').val(data[4]).trigger('change');
      $('#patrocinador').val(data[2]).trigger('change');
      $('#InstEducativas').val(data[8]).change();
      $('#centroEducRural').val(data[5]).trigger('change');
      $('#NroFamilProgram').val(data[6]).trigger('change');
      $('#NroFamilEntreg').val(data[7]).trigger('change');
      $('#nombre_docente').val(data[9]).trigger('change');
      $('#telefono_docente').val(data[10]).trigger('change');
      $('#correo_docente').val(data[11]).trigger('change');
      $('#observaciones').val(data[12]).trigger('change');

      ListarDetalleInformeInstalacion(data[0],0);
      ListarDetalleInformeInstalacion(data[0],1);  

  // alert(data[1])

  $("#municipio option[value="+ data[1] +"]").attr("selected",true).trigger("change");

  // alert(data[4])
  // alert(data[2])
  $("#idVereda option[value="+ data[4] +"]").attr("selected",true).trigger("change");
  $("#patrocinador option[value="+ data[2] +"]").attr("selected",true).trigger("change");

}

function InsertDetalleInformeInst(idInfInstalacion){
  
  var idMaterial_EmpleadoInformeInst=$('#idMaterialInformeInst').val();
  var idTipoPoblacionInformeInst=$('#idTipoPoblacionInformeInst').val();
  var CantEntregadaInformeInst=$('#CantInformeInst').val();
  
  if (idMaterial_EmpleadoInformeInst=='' ||idTipoPoblacionInformeInst=='' || CantEntregadaInformeInst=='') {
      $("#msgInsertDetalleInformeInstTPM").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var parametros={idInfInstalacion,idMaterial_EmpleadoInformeInst,idTipoPoblacionInformeInst,CantEntregadaInformeInst};
      var url="../../logica/logica.php?accion=InsertDetalleInformeInst";
      $.ajax({
        type: "POST",
        url: url,
        data: parametros,
        // dataType: "json",
        success: function (response) {
          if (response==1) {
            ListarDetalleInformeInstalacion(idInfInstalacion,1);
          } else {
            $("#msgInsertDetalleInformeInstTPM").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Por faqvor comuniquese con Soporte</div>");
          }
        }
      });
  }

}

function EliminarDetalleInformeInst(idRegDetalleInformeInst,idInfInstalacion){
  var parametros={idRegDetalleInformeInst};
  $.ajax({
    type: "POST",
    url: "../../logica/logica.php?accion=EliminarDetalleInformeInst",
    data: parametros,
    // dataType: "dataType",
    success: function (response) {
      if (response !=0) {
        ListarDetalleInformeInstalacion(idInfInstalacion,1);
      }
    }
  });

}

function ListarDetalleInformeInstalacion(idInfInstalacion,FuenteInfoInformeInst){
  var parametros={idInfInstalacion,FuenteInfoInformeInst}
  $.ajax({
    type: "POST",
    url: "../../logica/logica.php?accion=MostrarDetalleInformeInstalacion",
    data: parametros,
    dataType: "json",
    success: function (response) {
      if (FuenteInfoInformeInst==0) {
          $('#responsables').val(response);
          $('#responsables').trigger('change');
      } else {
          $('#items').html(response).trigger('change');
      }
    }
  });
}

function EditarInformeInstalacion(idInfInstalac){
  var fecha=$('#fechaIns').val();
  var departamento=$('#CodDepartamentoSelect').val();
  var municipio=$('#municipio').val();
  var vereda=$('#idVereda').val();
  var responsables=$('#responsables').val();
  var patrocinador=$('#patrocinador').val();
  var centroEducRural=$('#centroEducRural').val();
  var NroFamilProgram=$('#NroFamilProgram').val();
  var NroFamilEntreg=$('#NroFamilEntreg').val();
  var InstEducativas=$('#InstEducativas').val();
  var biblioteca=$('#BibliotecaInformeInst').val();
  var observaciones=$('#observaciones').val();
  var nombre_docente=$('#nombre_docente').val();
  var telefono_docente=$('#telefono_docente').val();
  var correo_docente=$('#correo_docente').val();

  if (fecha==''||centroEducRural==''||NroFamilProgram==''||NroFamilEntreg==''||
  InstEducativas==''|| biblioteca=='' || departamento==0 || municipio ==0 || vereda == 0 || responsables ==0 || patrocinador == 0) {
    $("#msginformeInstalc").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var params = {idInfInstalac,departamento,municipio,responsables,patrocinador,fecha,vereda,centroEducRural,NroFamilProgram,NroFamilEntreg,InstEducativas,nombre_docente,telefono_docente,correo_docente,observaciones,biblioteca};
    var url = "../../logica/logica.php?accion=EditarInfInstalac";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgEditInfInstalac").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgEditInfInstalac").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgEditInfInstalac").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
}
$("#msgEditInfInstalac").delay(3000).fadeOut(300);
return;
}

function EliminarDetalleInformeInstTMP(private_id,idRegDetalleInformeInstTMP){
  var parametros={private_id,idRegDetalleInformeInstTMP};
  var url= "../../logica/logica.php?accion=EliminarDetalleInformeInstTMP";
  $.ajax({
    type: "POST",
    url: url,
    data: parametros,
    // dataType: "json",
    success: function (response) {
      if (response !=0) {
        ListarDetalleInformeInstTPM(private_id);
    }
      
    }
  });
}

 function ListarDetalleInformeInstTPM(private_id){
  var parametros={private_id};
  var url = "../../logica/logica.php?accion=ListarDetalleInformeInstTPM";
  $.ajax({
    type: "POST",
    url: url,
    data: parametros,
    // dataType: "json",
    success: function (DTMP) {
      if (DTMP != null) {
            $('#items').html(DTMP).trigger('change');
        } 
    }
  });
 }



function InsertDetalleInformeInstTPM(private_id){
  var idMaterialInformeInst=$('#idMaterialInformeInst').val();
  var idTipoPoblacionInformeInst=$('#idTipoPoblacionInformeInst').val();
  var CantInformeInst=$('#CantInformeInst').val();
  if (idMaterialInformeInst==0 || idTipoPoblacionInformeInst==0 || CantInformeInst=='') {
    $("#msgInsertDetalleInformeInstTPM").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var parametros ={private_id, idMaterialInformeInst,idTipoPoblacionInformeInst,CantInformeInst};
    var url = "../../logica/logica.php?accion=InsertarDetalleTMPInfInstalac";
    $.ajax({
      type: "POST",
      url: url,
      data: parametros,
      dataType: "json",
      success: function (DTMP) {
          if (DTMP == 1) {
            ListarDetalleInformeInstTPM(private_id);
          } else {
            $("#msgInsertDetalleInformeInstTPM").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
          }
      }
    });
  }
  $("#msgInsertDetalleInformeInstTPM").delay(3000).fadeOut(300);
}


function FechaActual(){
  const fecha = new Date();            
  let anio = fecha.getFullYear();
  let mes = fecha.getMonth()+1;
  let dia = fecha.getDate();
  if(dia < 10){
      dia='0'+dia;
    }
    if(mes < 10){
      mes='0'+mes;
    }
  var FechaActual=`${dia}/${mes}/${anio}`;

  return FechaActual;
}

function NuevoInformeInstalacion(private_id){
  $('#fechaIns').val(FechaActual()).trigger('change');
  $('#TituloModalInformeInst').html("Nuevo Informe de Instalacion").trigger('change');
  $('#items').html('').trigger('change');
  $('#responsables').val(null).trigger('change');
  $('#CodDepartamentoSelect').val(0).trigger('change');
  $('#centroEducRural').val('').trigger('change');
  $('#NroFamilProgram').val('').trigger('change');
  $('#NroFamilEntreg').val('').trigger('change');
  $('#BibliotecaInformeInst').val('').trigger('change');
  $('#observaciones').val('').trigger('change');
  $('#nombre_docente').val('').trigger('change');
  $('#telefono_docente').val('').trigger('change');
  $('#correo_docente').val('').trigger('change');
  $('#agregarDetalle').html('<button type="button" id="AgregarMaterialInformeInstTMP" onclick="InsertDetalleInformeInstTPM(\''+private_id+'\')"><span class="glyphicon glyphicon-arrow-up"></span></button>').trigger('change');
  $('#modal-footerInfInsEditar').html('');
  $('#modal-footerInfInsNuevo').html(`
      <button type="button" onclick='guardarInformeInstalacion("`+private_id+`");' class="btn btn-primary">Guardar</button>
    `);
  ListarDetalleInformeInstTPM(private_id);
}


// FIN Informe de Instalacion -----------------------------------------------------


// ****************** Modulo Registro Entrega Coleccion ************************

function guardarRegEntregaColec(){
  var municipio=$('#municipio').val();
  var responsables=$('#responsables').val();
  var patrocinador=$('#patrocinador').val();
  var fecha=$('#fechaIns').val();
  var Vereda=$('#idVereda').val();
  var centroEducRural=$('#centroEducRural').val();
  var NroFamilProgram=$('#NroFamilProgram').val();
  var NroFamilEntreg=$('#NroFamilEntreg').val();
  var InstEducativas=$('#InstEducativas').val();
  var educadores=$('#educadores').val()
  var anteojos1_5=$('#anteojos1_5').val();
  var anteojos2_5=$('#anteojos2_5').val();
  var guias=$('#guias').val();
  var observaciones=$('#observaciones').val();

  if (fecha==''||centroEducRural==''||NroFamilProgram==''||NroFamilEntreg==''||InstEducativas==''||educadores=='') {
    $("#msgRegEntColec").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
    } else {
    var params = {municipio,responsables,patrocinador,fecha,Vereda,centroEducRural,NroFamilProgram,NroFamilEntreg,InstEducativas,educadores,
      anteojos1_5,anteojos2_5,guias,observaciones};
    var url = "../../logica/logica.php?accion=InsertInformeInstalac";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgRegEntColec").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
        location.reload();
      } else if (result == 3) {
        $("#msgRegEntColec").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgRegEntColec").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
  }
  $("#msgRegEntColec").delay(3000).fadeOut(300);
  return;
}

function formatEditRegEntColeccion(datoPrograma){
  deditI=datoPrograma.split('||');
  $('#idInfInstalacFM').val(deditI[0]);
  $('#municipioFM').val(deditI[1]);
  $('#responsablesFM').val(deditI[2]);
  $('#patrocinadorFM').val(deditI[3]);
  $('#fechaFM').val(deditI[4]);
  $('#VeredaFM').val(deditI[5]);
  $('#centroEducRuralFM').val(deditI[6]);
  $('#NroFamilProgramFM').val(deditI[7]);
  $('#NroFamilEntregFM').val(deditI[8]);
  $('#InstEducativasFM').val(deditI[9]);
  $('#educadoresFM').val(deditI[10]);
  $('#anteojos1_5FM').val(deditI[11]);
  $('#anteojos2_5FM').val(deditI[12]);
  $('#guiasFM').val(deditI[13]);
  $('#observacionesFM').val(deditI[14]);
}

  function EditarRegEntColeccion(){
  var idInfInstalac=$('#idInfInstalacFM').val();
  var municipio=$('#municipioFM').val();
  var responsables=$('#responsablesFM').val();
  var patrocinador=$('#patrocinadorFM').val();
  var fecha=$('#fechaFM').val();
  var Vereda=$('#VeredaFM').val();
  var centroEducRural=$('#centroEducRuralFM').val();
  var NroFamilProgram=$('#NroFamilProgramFM').val();
  var NroFamilEntreg=$('#NroFamilEntregFM').val();
  var InstEducativas=$('#InstEducativasFM').val();
  var educadores=$('#educadoresFM').val();
  var anteojos1_5=$('#anteojos1_5FM').val();
  var anteojos2_5=$('#anteojos2_5FM').val();
  var guias=$('#guiasFM').val();
  var observaciones=$('#observacionesFM').val();
  if (NroFamilProgram=="" ||NroFamilEntreg=="" || InstEducativas=="" ||educadores=="" ) {
    $("#msgEditInfInstalac").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var params = {idInfInstalac,municipio,responsables,patrocinador,fecha,Vereda,centroEducRural,NroFamilProgram,NroFamilEntreg,InstEducativas,educadores,
      anteojos1_5,anteojos2_5,guias,observaciones};
    var url = "../../logica/logica.php?accion=EditarInfInstalac";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgEditInfInstalac").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgEditInfInstalac").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgEditInfInstalac").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
}
$("#msgEditInfInstalac").delay(3000).fadeOut(300);
return;
}


//Modulo Encuesta de Infraestructura

function NuevaEInfra(){

  TituloEncInfra
  $('#TituloEncInfra').html('Nueva Encuestra Infraestructura<div id="IdInfraestructura" style="display:none;">0</div>');
  
  $('#Q_A').trigger("reset");
  $('input[name="EstadoMaterialTechoEinfra"]').prop('checked', false);
  $('input[name="EstadoMaterialParedesEinfra"]').prop('checked', false);
  $('input[name="EstadoAlcantarilladoEInfra"]').prop('checked', false);
  $('input[name="EstadoPozoSepticoEinfra"]').prop('checked', false);
  $('input[name="EstadoEleDotacionEInfra"]').prop('checked', false);
  $('input[name="EstadoEleDotacionEInfra"]').prop('checked', false);
  $('#FuenteAbasAguaEInfra').val(null).change();
  $('#EleDotacionEInfra').val(null).change();
}

function InsertarEncuestraInfra(){
  var fechaEInfra=$('#fechaEInfra').val();
  var CodMunicipioEInfra=$('#CodMunicipioEInfra').val();
  var CodVeredaEInfra=$('#CodVeredaEInfra').val();
  var IER_CEREInfra=$('#IER_CEREInfra').val();
  var IER_CERLegalEInfra=$('#IER_CERLegalEInfra').val();
  var NumEEInfra=$('#NumEEInfra').val();
  var ModoAccesoEInfra=$('#ModoAccesoEInfra').val();
  var HayHuertaEscolarEInfra=$('#HayHuertaEscolarEInfra').val();
  var UsoHuertaEInfra=$('#UsoHuertaEInfra').val();
  var HayAguaPotableEInfra=$('#HayAguaPotableEInfra').val();
  var FuenteAguaBanosEInfra=$('#FuenteAguaBanosEInfra').val();
  var UsoAbastecimientoAguaEInfra=$('#UsoAbastecimientoAguaEInfra').val();
  var FuenteAbasAguaEInfra=$('#FuenteAbasAguaEInfra').val();
  var TipoMaterialTechoEinfra=$('#TipoMaterialTechoEinfra').val();
  var EstadoMaterialTechoEinfra=$('input:radio[name=EstadoMaterialTechoEinfra]:checked').val();
  var EstadoElecEInfra=$('input:radio[name=EstadoElecEInfra]:checked').val();
  var TipoMaterialParedesEinfra=$('#TipoMaterialParedesEinfra').val();
  var EstadoMaterialParedesEinfra=$('input:radio[name=EstadoMaterialParedesEinfra]:checked').val();
  var HayAlcantarilladoEInfra=$('#HayAlcantarilladoEInfra').val();
  var EstadoAlcantarilladoEInfra=$('input:radio[name=EstadoAlcantarilladoEInfra]:checked').val();
  var HayPozoSepticoEInfra=$('#HayPozoSepticoEInfra').val();
  var EstadoPozoSepticoEinfra=$('input:radio[name=EstadoPozoSepticoEinfra]:checked').val();
  var EleDotacionEInfra=$('#EleDotacionEInfra').val();
  var EstadoEleDotacionEInfra=$('input:radio[name=EstadoEleDotacionEInfra]:checked').val();
  var ObservacionesAcueductoybanosEInfra=$('#ObservacionesAcueductoybanosEInfra').val();
  var ObservacionesTecnologiaEInfra=$('#ObservacionesTecnologiaEInfra').val();
  var ObservacionesOtrosEInfra=$('#ObservacionesOtrosEInfra').val();
  var CodDepartamentoEInfra=$('#CodDepartamentoEInfra').val();
  var IdInfraestructura=$('#IdInfraestructura').text();
  var nombre_docente=$('#nombre_docente').val();
  var telefono_docente=$('#telefono_docente').val();
  var correo_docente=$('#correo_docente').val();
  var HayElecEInfra=$('#HayElecEInfra').val();
  var EstadoElecEInfra=$('input:radio[name=EstadoElecEInfra]:checked').val();

  if(EstadoMaterialTechoEinfra== undefined || 
      EstadoMaterialParedesEinfra==undefined || 
      EstadoElecEInfra==undefined || 
      EstadoAlcantarilladoEInfra==undefined || 
      EstadoPozoSepticoEinfra==undefined || 
      EstadoEleDotacionEInfra==undefined || 
      fechaEInfra==""||
      CodDepartamentoEInfra==0||
      CodMunicipioEInfra==0||
      CodVeredaEInfra==0||
      IER_CEREInfra==""||
      NumEEInfra==""||
      ModoAccesoEInfra==""||
      UsoHuertaEInfra==""||
      UsoAbastecimientoAguaEInfra==""||
      FuenteAbasAguaEInfra==""||
      TipoMaterialTechoEinfra==""||
      TipoMaterialParedesEinfra==""||
      EleDotacionEInfra==""
    ){
      AlertModalmsg('amarillo','Datos incompletos','Hay datos que se deben ser llenados.  Por favor llene los datos solicitados.','Mensajes');
  }else{
    var params = {fechaEInfra,
                  CodMunicipioEInfra,
                  CodVeredaEInfra,
                  IER_CEREInfra,
                  IER_CERLegalEInfra,
                  NumEEInfra,
                  ModoAccesoEInfra,
                  HayHuertaEscolarEInfra,
                  UsoHuertaEInfra,
                  HayAguaPotableEInfra,
                  FuenteAguaBanosEInfra,
                  UsoAbastecimientoAguaEInfra,
                  FuenteAbasAguaEInfra,
                  TipoMaterialTechoEinfra,
                  EstadoMaterialTechoEinfra,
                  TipoMaterialParedesEinfra,
                  EstadoMaterialParedesEinfra,
                  HayAlcantarilladoEInfra,
                  EstadoAlcantarilladoEInfra,
                  HayPozoSepticoEInfra,
                  EstadoPozoSepticoEinfra,
                  EleDotacionEInfra,
                  EstadoEleDotacionEInfra,
                  ObservacionesAcueductoybanosEInfra,
                  ObservacionesTecnologiaEInfra,
                  ObservacionesOtrosEInfra,
                  CodDepartamentoEInfra,
                  IdInfraestructura,
                  nombre_docente,
                  telefono_docente,
                  correo_docente,
                  HayElecEInfra,
                  EstadoElecEInfra};
        var url = "../../logica/logica.php?accion=InsertarEncuestaInfra";
        $.ajax({
          url: url,
          type: 'POST',
          cache: false,
          dataType: 'json',
          data: params,
        }).done(function(result) {
          if(result == 1){
            AlertModalmsg('verde','Insertado.','Registro insertado con exito !!','Mensajes');
            location.reload(); 
          } else {
            AlertModalmsg('rojo','Error.','Por favor, comuniquese con Soporte','Mensajes');
          }
        }); 
  }
}

function VeredaSegunMunicipio(CodMunicipioSelect,DestinoMuestra){
  var CodMunicipioSelect=$('#'+CodMunicipioSelect).val();
  var parametros={CodMunicipioSelect};
  $.ajax({
      type: "POST",
      url: "../../logica/logica.php?accion=LlenaSelectVeredasSegunMunicipio",
      data: parametros,
      cache: false,
      dataType: "json",
      success: function (data) {
          $('#'+DestinoMuestra).html(data);
          $('#'+DestinoMuestra).change();
      }
  });
}

function PatrocinadorXmunicipio(idMunicipio){
  var idMunicipio=$('#'+idMunicipio).val();
  var parametro={idMunicipio};
  $.ajax({
    type: "POST",
    url: "../../logica/logica.php?accion=PatrocinadorSegunMunicipio",
    data: parametro,
    dataType: "json",
    success: function (response) {
      $('#patrocinador').html(response).change();
    }
  });
}

function MunicipiosegunDepartamento(CodDepartamentoSelect,DestinoMuestra){
  var CodDepartamentoSG=$('#'+CodDepartamentoSelect).val();
  var parametros={CodDepartamentoSG};
  $.ajax({
    type: "POST",
    url: "../../logica/logica.php?accion=LlenaSelectMunicipioSegunDepartamento",
    data: parametros,
    cache: false,
    dataType: "json",
    success: function (data) {
      $('#'+DestinoMuestra).html(data);
      $('#'+DestinoMuestra).trigger('change');
    }
  });
}

function formeditEncuestaInfra(rowEInfra){
  
      // MunicipiosegunDepartamento(response[27],'CodMunicipioEInfra',response[2]);
      $('#IdInfraestructura').show();
      $('#IdInfraestructura').html(rowEInfra[0]);
      $('#fechaEInfra').val(rowEInfra[1]).trigger('change');
      $('#CodDepartamentoEInfra').val(rowEInfra[30]).trigger('change');
      $("#CodVeredaEInfra").val(rowEInfra[3]).trigger('change');
      $('#IER_CEREInfra').val(rowEInfra[4]);
      $('#IER_CERLegalEInfra').val(rowEInfra[5]);
      $("#nombre_docente").val(rowEInfra[6]);
      $("#telefono_docente").val(rowEInfra[8]);
      $("#correo_docente").val(rowEInfra[7]);
      $('#NumEEInfra').val(rowEInfra[9]);
      $('#ModoAccesoEInfra').val(rowEInfra[10]);
      $('#HayHuertaEscolarEInfra').val(rowEInfra[26]);
      $('#UsoHuertaEInfra').val(rowEInfra[27]);
      $('#HayAguaPotableEInfra').val(rowEInfra[15]);
      $('#FuenteAguaBanosEInfra').val(rowEInfra[16]);
      $('#UsoAbastecimientoAguaEInfra').val(rowEInfra[17]);
      $('#TipoMaterialTechoEinfra').val(rowEInfra[11]);
      $('#TipoMaterialParedesEinfra').val(rowEInfra[13]);

      $('input[name="EstadoMaterialTechoEinfra"][value="'+rowEInfra[12]+'"]').prop('checked',true).trigger('change');
      $('input[name="EstadoMaterialParedesEinfra"][value="'+rowEInfra[14]+'"]').prop('checked',true).trigger('change');
      $('input[name="EstadoElecEInfra"][value="'+rowEInfra[24]+'"]').prop('checked',true).trigger('change');
      $('input[name="EstadoAlcantarilladoEInfra"][value="'+rowEInfra[20]+'"]').prop('checked',true).trigger('change');
      $('input[name="EstadoPozoSepticoEinfra"][value="'+rowEInfra[22]+'"]').prop('checked',true).trigger('change');
      $('input[name="EstadoEleDotacionEInfra"][value="'+rowEInfra[25]+'"]').prop('checked',true).trigger('change');

      $('#HayElecEInfra').val(rowEInfra[23]).trigger('onchange');
      $('#HayAlcantarilladoEInfra').val(rowEInfra[19]).trigger('onchange');
      $('#HayPozoSepticoEInfra').val(rowEInfra[21]).trigger('onchange');
      $('#ObservacionesAcueductoybanosEInfra').val(rowEInfra[18]);
      $('#ObservacionesTecnologiaEInfra').val(rowEInfra[28]);
      $('#ObservacionesOtrosEInfra').val(rowEInfra[29]);
      CargarDetalleSelect2(rowEInfra[0],0);
      CargarDetalleSelect2(rowEInfra[0],1);
      $('.Select2').trigger('change');
      $('#TituloEncInfra').html('Editar Encuestra Infraestructura # - '+rowEInfra[0]+'<div id="IdInfraestructura" style="display:none;">'+rowEInfra[0]+'</div>');
}

function CargarDetalleSelect2(IdEInfra,ValorfiltroCampo){
  var params = {IdEInfra,ValorfiltroCampo};
    var url = "../../logica/logica.php?accion=CargarDetalleSelect2EInfra";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(ValorfiltroCampo==0){
        $('#FuenteAbasAguaEInfra').val(result);
        $('#FuenteAbasAguaEInfra').trigger('change');
      } else {
        $('#EleDotacionEInfra').val(result);
        $('#EleDotacionEInfra').trigger('change');
      } 
    });

}

//Modulo de Tipo Poblacion
function InsertTipoPoblacion(){
  var descTipoPoblacion=$('#descTipoPoblacion').val();
  if(descTipoPoblacion==""){
    $("#msgTipoPoblacion").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  }else{
    var parametros={descTipoPoblacion};
    $.ajax({
      url: "../../logica/logica.php?accion=insertarTipoPoblacion",
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: parametros,
    }).done(function(result) {
      if(result == 1){
        $("#msgTipoPoblacion").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Guardado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgTipoPoblacion").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgTipoPoblacion").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No fue posible insertar el dato, por favor comuniquese con soporte tecnico.</div>");
      }
    });
  }
}

function formeditTipoPoblacion(datoPrograma){
  deditI=datoPrograma.split('||');
  $('#IdTipoPoblacFM').val(deditI[0]);
  $('#desTipoPoblacFM').val(deditI[1]);
}

function EditarTipoPoblacion(){
  var idTipoPoblac=$('#IdTipoPoblacFM').val();
  var desTipoPoblac=$('#desTipoPoblacFM').val();
  if (desTipoPoblac==="") {
    $("#msgEditTipoPoblac").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var params = {idTipoPoblac,desTipoPoblac};
    var url = "../../logica/logica.php?accion=EditarTipoPoblacion";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgEditTipoPoblac").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgEditTipoPoblac").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgEditTipoPoblac").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
}
$("#msgEditTipoPoblac").delay(3000).fadeOut(300);
return;
}

// Modulo Asistencia a Capacitacion Instalacion


function NuevoAsistCapacitInstal(){
  $('#CodFormAsisCapac').val('').trigger('change');
  $('#fechaAsist').val(FechaActual()).trigger('change');
  $('#idTipoTaller').val(0).trigger('change');
  $('#idMunicipio').val(0).trigger('change');
  $('#idResponsable').val('').trigger('change');
  $('#LugarAsistCapac').val('').trigger('change');
  $('#CantAsistentes').val('').trigger('change');
  $('#idReciboMaterial').val('').trigger('change');
  $('#ObservAsistencCapacEsc').val('').trigger('change');
  $('#TituloModal').html("Nuevo Asistencia Capacitacion Instalacion");
  $('#BotonesAsistenciaCapacitacion').html(`
    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
    <button type="button" onclick="guardarAsistCapacInstal();" class="btn btn-primary">Guardar</button>
  `);
}

  function guardarAsistCapacInstal(){

      var CodFormAsisCapac=$('#CodFormAsisCapac').val();
      var fecha=$('#fechaAsist').val();
      var idTipoTaller=$('#idTipoTaller').val();
      var idMunicipio=$('#idMunicipio').val();
      var idResponsable=$('#idResponsable').val();
      var LugarAsistCapac=$('#LugarAsistCapac').val();
      var CantAsistentes=$('#CantAsistentes').val();
      var idReciboMaterial=$('#idReciboMaterial').val();
      var ObservAsistencCapac=$('#ObservAsistencCapacEsc').val();

    if ($.isNumeric(CantAsistentes)) {   
      if (CodFormAsisCapac==""||fecha==""||LugarAsistCapac==''||CantAsistentes=='') {
        $("#msgAsistCapacInst").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
        } else {
        var params = {CodFormAsisCapac,fecha,idTipoTaller,idMunicipio,idResponsable,LugarAsistCapac,CantAsistentes,idReciboMaterial,ObservAsistencCapac};
          var url = "../../logica/logica.php?accion=InsertAsistCapacInstal";
          
        $.ajax({
          url: url,
          type: 'POST',
          cache: false,
          dataType: 'json',
          data: params,
        }).done(function(result) {
          if(result == 1){
            $("#msgAsistCapacInst").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Guardado con Exito !!</strong></div>");
            location.reload();
          } else if (result == 3) {
            $("#msgAsistCapacInst").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
          } else {
            $("#msgAsistCapacInst").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
          }
        });
      }
      $("#msgAsistCapacInst").delay(3000).fadeOut(300);
      return;
    }else{
      $('#CantAsistentes').css('border-color','#FF0000');     
    }
  }

  function formatEditAsistCapacInstal(datoPrograma){

    deditI=datoPrograma.split('||');
    var idReg=deditI[0];

    $('#TituloModal').html("Editar Asistencia Capacitacion Instalacion - #"+idReg);

    $('#BotonesAsistenciaCapacitacion').html(`
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					<button type="button" onclick="EditarAsistCapacInstal(`+idReg+`);" class="btn btn-primary">Guardar</button>
				`);
    $('#CodFormAsisCapac').val(deditI[1]);
    $('#fechaAsist').val(deditI[2]);
    $('#idTipoTaller').val(deditI[4]).trigger('change');
    $('#idMunicipio').val(deditI[3]).trigger('change');
    $('#idResponsable').val(deditI[5]).trigger('change');
    $('#LugarAsistCapac').val(deditI[6]);
    $('#CantAsistentes').val(deditI[7]);
    $('#idReciboMaterial').val(deditI[8]);
    $('#ObservAsistencCapacEsc').val(deditI[9]);
  }

  function EditarAsistCapacInstal(idReg){

      var CodFormAsisCapac=$('#CodFormAsisCapac').val();
      var fecha=$('#fechaAsist').val();
      var idTipoTaller=$('#idTipoTaller').val();
      var idMunicipio=$('#idMunicipio').val();
      var idResponsable=$('#idResponsable').val();
      var LugarAsistCapac=$('#LugarAsistCapac').val();
      var CantAsistentes=$('#CantAsistentes').val();
      var idReciboMaterial=$('#idReciboMaterial').val();
      var ObservAsistencCapac=$('#ObservAsistencCapacEsc').val();

    if ($.isNumeric(CantAsistentes)) {
      if (CodFormAsisCapac==""||fecha==""||LugarAsistCapac==''||CantAsistentes=='') {
        $("#msgAsistCapacInst").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
      } else {
        var params = {idReg,CodFormAsisCapac,fecha,idTipoTaller,idMunicipio,idResponsable,LugarAsistCapac,CantAsistentes,idReciboMaterial,ObservAsistencCapac};
        var url = "../../logica/logica.php?accion=editAsistCapacInstal";
        $.ajax({
          url: url,
          type: 'POST',
          cache: false,
          dataType: 'json',
          data: params,
        }).done(function(result) {
          if(result == 1){
            $("#msgAsistCapacInst").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
            location.reload(); 
          } else if (result == 2) {
            $("#msgAsistCapacInst").html("<div class='alert alert-dismissible alert-warning'><strong>El Codigo de registro que deseas cambiar ya existen.  Intenta nuevamente</strong></div>");
          } else {
            $("#msgAsistCapacInst").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
          }
        });
      }
    }
      $("#msgAsistCapacInst").delay(3000).fadeOut(300);
      return;
  }

// Modulo Registro Entrega de Coleccion
function guardarregEntregaColec(){
  var codRegEntColeccion=$('#codRegEntColec').val();
  var fecha=$('#fechaEnt').val();
  var idDepartamento=$('#idDepartamento').val();
  var idMunicipio=$('#idMunicipio').val();
  var idInstitucionEducativa=$('#idInstitEduc').val();
  var idVereda=$('#idVeredaEnt').val();
  var responsable=$('#responsable').val();
  var sede=$('#sede').val();
  var docente=$('#docente').val();
  
  var internet=$('#internetEsc').val()
  var internetPublico=$('#internetPub').val();
  var codTipoPoblacion=$('#codTipoPoblacion').val();
  var codTipoMaterial=$('#codTipoMaterial').val();
  var cantEntregada=$('#cantEntregada').val();
  var imagenEntColec=$('#imagenEntColec').val();
  
  if (docente==''||responsable==''||cantEntregada==""||imagenEntColec==""||idDepartamento=="0") {
    $("#msgRegEntColec").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
    } else {
    var params = {codRegEntColeccion,fecha,idMunicipio,idInstitucionEducativa,idVereda,responsable,sede,docente,internet,internetPublico,codTipoPoblacion,codTipoMaterial,cantEntregada,imagenEntColec,idDepartamento};
    var url = "../../logica/logica.php?accion=InsertRegEntrColec";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgRegEntColec").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
        location.reload();
      } else if (result == 3) {
        $("#msgRegEntColec").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgRegEntColec").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
  }
  $("#msgRegEntColec").delay(3000).fadeOut(300);
  return;
}

function formatEditRegEntColec(idRegistrosEColeccion){
  var parametros={idRegistrosEColeccion};
  var url = "../../logica/logica.php?accion=EditarRegEntrColec";
  $.ajax({
    type: "POST",
    url: url,
    cache: false,
    data: parametros,
    dataType: "json",
    success: function (response) {
      $('#idRegEntColec').show();
      $('#idRegEntColec').html('Id #  '+response[0]);
      $('#codRegEntColec').val(response[1]);
      $('#fechaEnt').val(response[2]);
      $('#idDepartamento').val(response[14]).trigger('change.select2').change();
      
      // $('#idMunicipio').val(response[3]).trigger('change.select2').change();
     
      $('#idVeredaEnt').val(response[5]).trigger('change.select2').change();
      $('#idInstitEduc').val(response[4]).trigger('change.select2').change();
      $('#sede').val(response[6]).change();
      $('#docente').val(response[7]);
      $('#internetEsc').val(response[8]);
      $('#internetPub').val(response[9]);
      $('#codTipoPoblacion').val(response[10]);
      $('#codTipoMaterial').val(response[11]);
      $('#cantEntregada').val(response[12]);
      $('#imagenEntColec').val(response[13]);

      var idRegistroEntregaColeccion=response[0];
      var parametros2={idRegistroEntregaColeccion};
      $.ajax({
        type: "POST",
        url: "../../logica/logica.php?accion=CargarResponsablesSelect2_RegEntregaColeccion",
        data: parametros2,
         dataType: "json",
        success: function (responseE) {
          $('#responsable').val(responseE).change();
          $('#responsable').trigger('change');
        }
      }); 
    }
  });
  $('#idMunicipio').append(response[3]).trigger('change');
}

function EditarRegEntColec(){
  alert($('#idDepartamento').val());
    var idRegEntColec=$('#idRegEntColecFM').val();
    var codRegEntColec=$('#codRegEntColecFM').val();
    var fecha=$('#fechaEntFM').val();
    var idDepartamento=$('#idDepartamento').val();
    var idMunicipio=$('#idMunicipioFM').val();
    var idInstitucionEducativa=$('#idInstitEducFM').val();
    var idVereda=$('#idVeredaEntFM').val();
    var responsable=$('#responsableFM').val();
    var sede=$('#sedeFM').val();
    var docente=$('#docenteFM').val();
   
    var internet=$('#internetEscFM').val()
    var internetPublico=$('#internetPubFM').val();
    var codTipoPoblacion=$('#codTipoPoblacionFM').val();
    var codTipoMaterial=$('#codTipoMaterialFM').val();
    var cantEntregada=$('#cantEntregadaFM').val();
    var imagenEntColec=$('#imagenEntColecFM').val();
  if (docente=="" || cantEntregada=="" ||imagenEntColec=="" || idDepartamento=="0") {
    $("#msgEditRegEntColec").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var params = {idRegEntColec,codRegEntColec,fecha,idMunicipio,idInstitucionEducativa,idVereda,responsable,sede,docente,internet,internetPublico,codTipoPoblacion,codTipoMaterial,cantEntregada,imagenEntColec,idDepartamento};
    var url = "../../logica/logica.php?accion=editRegEntColeccion";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msgEditRegEntColec").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
        location.reload(); 
      } else if (result == 3) {
        $("#msgEditRegEntColec").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
      } else {
        $("#msgEditRegEntColec").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en el programa, no hay nada que editar</div>");
      }
    });
}
$("#msgEditRegEntColec").delay(3000).fadeOut(300);
return;
}

// *************** Modulo Materiales R *************

function paraeditarMaterialR(datos){

    datosMR=datos.split('||');
    $('.VerMaterialesR').show();
    $('#TituloModalMaterialesR').html('Editar Material');
    $('#BotonAccionMaterialesR').html(`
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" id="BotonAccionMaterialesR" onclick="editarMaterialR(`+datosMR[0]+`);" class="btn btn-primary">Guardar Cambios</button>
    `);
    $("#CodMaterialR").val(datosMR[1]).prop('disabled',false);
    $("#DescripcionMaterialR").val(datosMR[2]).prop('disabled',false);
    $("#CantidadMaterialR").val(datosMR[3]).prop('disabled',false);
    $("#CostounitMaterialR").val(datosMR[7]).prop('disabled',false);
    $("#CodTipoMaterialR").val(datosMR[8]).prop('disabled',false).trigger('change');
    $('#CostoTotalMaterialesR').val('$ '+formatNumber.new(datosMR[7]*datosMR[3])).prop('disabled',true);
    
    
    $('#CostounitMaterialR').keyup(function(){
      $('#CostoTotalMaterialesR').val("$"+formatNumber.new(parseInt($("#CostounitMaterialR").val()) * parseInt($("#CantidadMaterialR").val()))).prop('disabled',true);
    });

    $("#CantidadMaterialR").keyup(function(){ 
      $('#CostoTotalMaterialesR').val("$"+formatNumber.new(parseInt($("#CostounitMaterialR").val()) * parseInt($("#CantidadMaterialR").val()))).prop('disabled',true);
    });
}


function editarMaterialR(datos){
    var idArchivodeMaterial=datos;
    var CodTipoMaterialR=$("#CodTipoMaterialR").val();
    var CodMaterialR=$("#CodMaterialR").val();
    var DescripcionMaterialR=$("#DescripcionMaterialR").val();
    var CantidadMaterialR=$("#CantidadMaterialR").val();
    var CostounitMaterialR=$("#CostounitMaterialR").val();
    if (CodTipoMaterialR==''|| CodMaterialR==''|| DescripcionMaterialR==''|| CantidadMaterialR==''|| CostounitMaterialR=='') {
      $("#msgMaterialR").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
    } else {
      var parametros={idArchivodeMaterial,CodTipoMaterialR,CodMaterialR,DescripcionMaterialR,CantidadMaterialR,CostounitMaterialR}
      var url = "../../logica/logica.php?accion=EditarMaterialesR";
      $.ajax({
        type: "POST",
        url: url,
        data: parametros,
        dataType: "json",
        cache: false,
        success: function (data) {
            if (data==0) {
              $("#msgMaterialR").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Por fAvor Comuniquese con Soporte</div>");
            } else {
              $("#msgMaterialR").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>"); 
              location.reload();            
            }
        }
      });
    }

}

function NuevoMaterialR(){
  $('#TituloModalMaterialesR').html("Nuevo Material");
  $('#BotonAccionMaterialesR').html('<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button> <button type="button" id="BotonAccionMaterialesR" onclick="guardarMaterialR();" class="btn btn-primary">Guardar</button>');
  $('.VerMaterialesR').hide();
  $("#CodMaterialR").val('').prop('disabled',false);
  $("#DescripcionMaterialR").val('').prop('disabled',false);
  $("#CantidadMaterialR").val(1).prop('disabled',false);
  $("#CostounitMaterialR").val(1).prop('disabled',false);
  $("#CodTipoMaterialR").val('').prop('disabled',false);
  $('#CostoTotalMaterialesR').val('').prop('disabled',false);
  $('#SaldoMaterialesR').val('').prop('disabled',false);
  
}

function guardarMaterialR(){
  var CodTipoMaterialR=$("#CodTipoMaterialR").val();
  var CodMaterialR=$("#CodMaterialR").val();
  var DescripcionMaterialR=$("#DescripcionMaterialR").val();
  var CantidadMaterialR=$("#CantidadMaterialR").val();
  var CostounitMaterialR=$("#CostounitMaterialR").val();
  if (CodTipoMaterialR==''|| CodMaterialR==''|| DescripcionMaterialR==''|| CantidadMaterialR==''|| CostounitMaterialR=='')  {
    $("#msgMaterialR").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
    var parametros={CodTipoMaterialR,CodMaterialR,DescripcionMaterialR,CantidadMaterialR,CostounitMaterialR}
    var url = "../../logica/logica.php?accion=InsertarMaterialesR";
    $.ajax({
      type: "POST",
      url: url,
      data: parametros,
      dataType: "json",
      cache: false,
      success: function (data) {
          if (data==0) {
            $("#msgMaterialR").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Por fAvor Comuniquese con Soporte</div>");
          } else {
            $("#msgMaterialR").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>"); 
            location.reload();            
          }
      }
    });
  }
}

function VerMaterialesR(datos){
  $('#TituloModalMaterialesR').html("Ver Material");
  $('#BotonAccionMaterialesR').html('<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>');
  datosMR=datos.split('||');
  $('.VerMaterialesR').show();
  // $('#SaldoMaterialesR').val(formatNumber.new(datosMR[4]-datosMR[5])).prop('disabled',true);
  $('#CostoTotalMaterialesR').val('$ '+formatNumber.new(datosMR[7]*datosMR[3])).prop('disabled',true);
  $("#CodMaterialR").val(datosMR[1]).prop('disabled',true);
  $("#DescripcionMaterialR").val(datosMR[2]).prop('disabled',true);
  $("#CantidadMaterialR").val(datosMR[3]).prop('disabled',true);
  $("#CostounitMaterialR").val('$ '+formatNumber.new(datosMR[7])).prop('disabled',true);
  $("#CodTipoMaterialR").val(datosMR[8]).prop('disabled',true);
  $('#CodTipoMaterialR').trigger('change');
}

// ********* Modulo Requisicion de Materiales *************


function NuevaRequisicionMateriales(){
  var textbotones=`
  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
  <button type="button" onclick="guardarRequisicionMeteriales();" class="btn btn-primary">Guardar</button>
  `;
  $('#TituloModalRequisicionM').html('Nueva Requisicion de Materiales');
  $('#ProyectoRequisicionM').val('');
  $('#EmpleadosRequisicionM').val('');
  $('#FooterBotonesModal').html(textbotones);
  mostrardetalleRequisicionMTMP();
}

function guardarRequisicionMeteriales(){
  var FechaRequisicionM=$('#FechaRequisicionM').val();
  var ProyectoRequisicionM=$('#ProyectoRequisicionM').val();
  var EmpleadosRequisicionM=$('#EmpleadosRequisicionM').val();
  var TotalCantRequisicionM=$('#TotalCantidadesRequisicionM').text();
  var private_id=$('#private_id').text();
  if (FechaRequisicionM == "" || ProyectoRequisicionM == 0  || EmpleadosRequisicionM == 0 ) {
      $("#msgRequisicionMateriales").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
      var parametros={FechaRequisicionM,ProyectoRequisicionM,EmpleadosRequisicionM,TotalCantRequisicionM,private_id};
      var url = "../../logica/logica.php?accion=InsertarRequisicionMateriales";
      $.ajax({
        type: "POST",
        url: url,
        data: parametros,
        // dataType: "json",
        cache: false,
        success: function (response) {
          if (response == 0) {
             $("#msgRequisicionMateriales").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Por fAvor Comuniquese con Soporte</div>");
          } else {
            $("#msgRequisicionMateriales").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");            
            $('#NuevaRequisicionMateriales').removeData("modal").modal({backdrop: 'static', keyboard: false});
            location.reload(); 
          }
        }
      });
  }
  $("#msgRequisicionMateriales").delay(3000).fadeOut(300);
}

function EditarRequisicionMateriales(datos,desde){
  datosVRM=datos.split('||');
  var textbotones=`
  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
  `;
  $('#TituloModalRequisicionM').html('Editar Requisicion de Materiales<b># '+datosVRM[0]+'</b>');
  $('#FechaRequisicionM').val(datosVRM[1]);
  $('#ProyectoRequisicionM').val(datosVRM[2]);
  $('#EmpleadosRequisicionM').val(datosVRM[3]);
  $('#ProyectoRequisicionM').change();
  $('#EmpleadosRequisicionM').change();
  $('#FooterBotonesModal').html(textbotones);
  var IdRequisicionMateriales=datosVRM[0];
  var parametros={IdRequisicionMateriales,desde};
  var url="../../logica/logica.php?accion=VerRequisicionMateriales";
  $.ajax({
    type: "POST",
    url: url,
    data: parametros,
    cache: false,
    // dataType: "json",
    success: function (datosR) {
      $("#DetalleRequisicionM").html(datosR);
    }
  });
}

function VerRequisicionMateriales(datos){
  datosVRM=datos.split('||');
  $('#TituloModalRequisicionM').html('Ver Requisicion de Materiales            <b># '+datosVRM[0]+'</b>');
  $('#FechaRequisicionM').val(datosVRM[1]);
  $('#ProyectoRequisicionM').val(datosVRM[2]);
  $('#EmpleadosRequisicionM').val(datosVRM[3]);
  $('#ProyectoRequisicionM').change();
  $('#EmpleadosRequisicionM').change();
  IdRequisicionMateriales=datosVRM[0];
  var desde='verRM';
  var parametros={IdRequisicionMateriales,desde};
  var url="../../logica/logica.php?accion=VerRequisicionMateriales";
  $.ajax({
    type: "POST",
    url: url,
    data: parametros,
    cache: false,
    // dataType: "json",
    success: function (datosR) {
      $("#DetalleRequisicionM").html(datosR);
    }
  });
}

function InsertarDetalleRequisicionM(IdRequisicionMaterial){
  var MaterialRequisicion=$('#MaterialRequisicion').val();
  var CantiRequeridaM=$('#CantiRequeridaM').val();
  var UndxCajaRequisicionM=$('#UndxCajaRequisicionM').val();
  var parametros={IdRequisicionMaterial,MaterialRequisicion,CantiRequeridaM,UndxCajaRequisicionM};
  var url="../../logica/logica.php?accion=InsertarDetalleRequisicionMateriales";
  $.ajax({
    type: "POST",
    url: url,
    data: parametros,
    cache: false,
    // dataType: "json",
    success: function (response) {
      CargarDetalleRequisicionM(IdRequisicionMaterial);
    }
  });
}

function CargarDetalleRequisicionM(IdRequisicionMateriales){
  var desde=1;
  var parametros={IdRequisicionMateriales,desde};
  var url="../../logica/logica.php?accion=VerRequisicionMateriales";
  $.ajax({
    type: "POST",
    url: url,
    data: parametros,
    cache: false,
    // dataType: "json",
    success: function (datosR) {
      $("#DetalleRequisicionM").html(datosR);
    }
  });
}

function BorrarDetalleRequisicionM(IdDetalleRequisicion,IdRequisicionMaterial){

  var parametros = {IdDetalleRequisicion};
    $.ajax({
        url: '../../logica/logica.php?accion=BorrarDetalleRequisicionM',
        type: 'POST',
        data: parametros,
        success: function(data) {
          CargarDetalleRequisicionM(IdRequisicionMaterial);
        }
    });
}


// ********* Modulo Recibo de Materiales *************


function NuevoReciboMateriales(){
 
  $('#TituloModalReciboM').html('Nuevo Recibo de Materiales');
  $('#ProyectoReciboM').val(0);
  $('#EmpleadosReciboM').val(0);
  
}

function obtenerCABrequisicionSelect(IdRequisicionMaterial){
  var parametrosCAB={IdRequisicionMaterial};
  var url="../../logica/logica.php?accion=obtenerCABRequisicion";
  $.ajax({
    type: "POST",
    url: url,
    data: parametrosCAB,
    dataType: "json",
    success: function (respuesta) {
      $('#EmpleadosReciboM').val(respuesta[3]);
      $('#EmpleadosReciboM').trigger('change');
      $('#ProyectoReciboM').val(respuesta[2]);
      $('#ProyectoReciboM').trigger('change');
    }
  });
}


function mostrardetalleparaReciboM(IdRequisicionMaterial){
  var parametros={IdRequisicionMaterial};
  var url="../../logica/logica.php?accion=MostrarDetalleParaReciboMateriales";
  $.ajax({
    type: "POST",
    url: url,
    data: parametros,
    cache: false,
    // dataType: "json",
    success: function (datosR) {
      $("#DetalleReciboM").html(datosR);
    }
  });
}

function AutocompletarRecibo(datos){
  datosReciboMAutoc=datos.split('||');
  
  for (index = 0; index < datosReciboMAutoc.length; index+=3) {
    if (datosReciboMAutoc[index+2] != 0) {
      $('#'+datosReciboMAutoc[index+1]).val(datosReciboMAutoc[index+2]);
      $('#CantTipoNovedadReciboM'+datosReciboMAutoc[index]).val(0);
      $('#TipoNonedadesReciboM'+datosReciboMAutoc[index]).prop('disabled', true);
      $('#TipoNonedadesReciboM'+datosReciboMAutoc[index]).trigger('change'); 
    }
     
  }

}

function DifCantRecibidas_CantRequ(IdDetalle){
  var Cant_Requerida=parseInt($('#CantRequerida'+IdDetalle).text());
  var Cant_Recibida=parseInt($('#CantReciboxitemM'+IdDetalle).val());
  var DiffRequeridaRecibida=(Cant_Requerida-Cant_Recibida);

    $('#CantTipoNovedadReciboM'+IdDetalle).val(DiffRequeridaRecibida);
    $('#TipoNonedadesReciboM'+IdDetalle).prop('disabled', false);
    $('#TipoNonedadesReciboM'+IdDetalle).trigger('change');
}

function InserDetalleAdicional_desdeReciboM(IdRequisicionMaterial){
    var MaterialRequisicion=$('#MaterialRequisicion').val();
    var CantRecibidaM=$('#CantRecibidaM').val();
    var UndxCajaRequisicionM=$('#UndxCajaRequisicionM').val();
    if (MaterialRequisicion=='' || CantRecibidaM=='' || UndxCajaRequisicionM == '') {
        $("#msgReciboMateriales").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
    } else {
      var parametros={IdRequisicionMaterial,MaterialRequisicion,CantRecibidaM,UndxCajaRequisicionM};
      var url="../../logica/logica.php?accion=InserDetalleAdicional_desdeReciboM";
      $.ajax({
        type: "POST",
        url: url,
        data: parametros,
        cache: false,
        success: function (response) {
          if (response == 0) {
            $("#msgRequisicionMateriales").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Por fAvor Comuniquese con Soporte</div>");
         } else {
           $("#msgRequisicionMateriales").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
           mostrardetalleparaReciboM(IdRequisicionMaterial);
         }
        }
      });
  }
}

function guardarReciboMateriales(data){
  //Guardar Encabezado en recibo materiales
  datosReciboM=data.split('||');

  var IdRequisicionMaterial=$("#RequisicionesReciboM").val();
  var FechaReciboMaterial=$("#FechaReciboM").val();
  var IdProyecto=$("#ProyectoReciboM").val();
  var IdEmpleadosRecibo=$("#EmpleadosReciboM").val();
  var DiferenciaTotalCant=0;
  if (FechaReciboMaterial == 0 || IdProyecto == 0 || IdEmpleadosRecibo == null) {
    $("#msgReciboMateriales").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
  } else {
      var parametros={IdRequisicionMaterial,FechaReciboMaterial,IdProyecto,IdEmpleadosRecibo};
      var url="../../logica/logica.php?accion=InserCabRecibo";
      $.ajax({
        type: "POST",
        url: url,
        data: parametros,
        // cache: false,
        // dataType: "dataType",
        success: function (response) {
          
           if (response == 0) {
               $("#msgReciboMateriales").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Por favor Comuniquese con Soporte</div>");
           } else {
              var url="../../logica/logica.php?accion=UpdateRecibo";
              
              for (i = 0; i < datosReciboM.length; i+=3) {
                var IdDetalleMaterial = datosReciboM[i];
                var CantRecibidas=$("#CantReciboxitemM"+datosReciboM[i]).val();
                var tipoNovedad=$("#TipoNonedadesReciboM"+datosReciboM[i]).val();
                var CantTipoNovedad=$("#CantTipoNovedadReciboM"+datosReciboM[i]).val();
                var param={IdDetalleMaterial,CantRecibidas,tipoNovedad,CantTipoNovedad,response};
                DiferenciaTotalCant+=parseInt(CantTipoNovedad);
                $.ajax({
                  type: "POST",
                  url: url,
                  data: param,
                  // dataType: "dataType",
                });
              }
            $("#msgReciboMateriales").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Guardado con Exito !!</strong></div>");
            $('#NuevoReciboMateriales').removeData("modal").modal({backdrop: 'static', keyboard: false});
            location.reload();
          }
        }
      });
      UpdateEstadoIdRequisicion(IdRequisicionMaterial,DiferenciaTotalCant);
  }

}

function UpdateEstadoIdRequisicion(IdRequisicionMaterial,DiferenciaTotalCant){

    if (DiferenciaTotalCant == 0) {
      estadoRequisicion=1;
    } else {
      estadoRequisicion=2;
    }

    var urle="../../logica/logica.php?accion=UpdateEstadoRequisicion";
    var parametrosUpdate={IdRequisicionMaterial,estadoRequisicion};
    
    $.ajax({
      type: "POST",
      url: urle,
      data:parametrosUpdate,
      cache: false,
       dataType: "json",
      success: function (response) {
        
      }
    });
}


  function GuardarImagenes() {
    var formData = new FormData();
    formData.append('msgSubirImagen', $("#Imagen_usuario")[0].files[0]);
    formData.append('NitBeneficiario', $('#NitBeneficiario').prop('value'));
    formData.append('NombreBeneficiario', $('#NombreBeneficiario').prop('value'));
    formData.append('DireccionBeneficiario', $('#DireccionBeneficiario').prop('value'));
    formData.append('TelefonoBeneficiario', $('#TelefonoBeneficiario').prop('value'));
    formData.append('NumeroFactura', $('#NumeroFactura').prop('value'));
    formData.append('ValorFactura', $('#ValorFactura').prop('value'));
    formData.append('PagoTCD', $('#PagoTCD').prop('value'));
    formData.append('EntregaRUT', $('#EntregaRUT').prop('value'));
    formData.append('EntregaCedula', $('#EntregaCedula').prop('value'));
    formData.append('Observaciones', $('#Observaciones').prop('value'));
    
    $.ajax({
        url: "../../logica/logica.php?accion=SubirImagen",
        type: 'POST',
        
        data: formData,
        contentType: false,
        cache: false,
        processData:false,
    success: function(data) {
            
            
            },
   
    });
};


//************************ Modulo Anticipos ********************* 28/03/2019 Anibal Benitez


function ParaAnticipoNuevo(){
  var codempleado=$("#empleadologin").val();
  $('#modal-footer_Anticipos').html(`
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
    <button type="button" id="Boton_Guardar_anticipo" class="btn btn-primary" onclick="GuardarAnticipo();">Guardar</button>
  `).change();
  $('#Boton_Guardar_anticipo').attr('disabled', true);
  $('#TituloModalAnticipos').html('Nuevo Anticipo a SG').change();
  $('#DocEquivalente').val(null);
  $('#DocEquivalente').attr('disabled',true);
  $('#ValorAnticipo').val(null);
  $('#ValorAnticipo').attr('disabled',true);
  $('#Notasanticipos').val(null);
  $('#Notasanticipos').attr('disabled',true);
  $('#ResponsableAnticipoEn').val(0).change();
  $('#ResponsableAnticipoEn').val(codempleado).change();
  $('#ResponsableAnticipoEn').attr('disabled',true);
  $('#ResponsableAnticipoRe').val(0).change();
  $('#ResponsableAnticipoRe').attr('disabled',true);
  $('#IdSG').val(0).change();

}

function GuardarAnticipo(){

    var FechaAnticipo=$('#FechaAnticipo').val();
    var ResponsableAnticipoEn=$('#ResponsableAnticipoEn').val();
    var ResponsableAnticipoRe=$('#ResponsableAnticipoRe').val();
    var DocEquivalente=$('#DocEquivalente').val();
    var ValorAnticipo=$('#ValorAnticipo').val();
    var Notasanticipos=$('#Notasanticipos').val();
    var IdSG=$('#IdSG').val();

    if (ResponsableAnticipoEn=='0' || ResponsableAnticipoEn==null || DocEquivalente=='' || ResponsableAnticipoRe=='0' || ResponsableAnticipoRe==null) {
      AlertModalmsg('amarillo','Datos obligatorios','Los datos de Responsables o Documento Equivalente son obligatorios','msgAnticiposNuevo');
    } else {
      var parametros={FechaAnticipo,ResponsableAnticipoEn,ResponsableAnticipoRe,DocEquivalente,ValorAnticipo,Notasanticipos,IdSG};
      $.ajax({
        type: "POST",
        url: "../../logica/logica.php?accion=InsertarAnticipo",
        data: parametros,
        dataType: "json",
        success: function (response) {
          if(response == 1){
            AlertModalmsg('verde','Anticipo Guardado !!','Anticipo guardado con <b>Exito !!</b>','msgAnticiposNuevo');
            location.reload();
          }else{
            AlertModalmsg('rojo','Error técnico','Por favor comunicate con soporte','msgAnticiposNuevo');
          }
        }
      });
    }
}

function AnticipodesdeIdSG(IdSG,ValorAprobado){
  var codempleado=$("#empleadologin").val();
  $('#modal-footer_Anticipos').html(`
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
    <button type="button" class="btn btn-primary" onclick="GuardarAnticipo();">Guardar</button>
  `).change();

  $('#TituloModalAnticipos').html('Nuevo Anticipo a Solicitudes de Gasto (SG)').change();
  $('#IdSG').val(IdSG).change();
  $('#ValorAnticipo').val(ValorAprobado).change();
  $('#DocEquivalente').val(null);
  $('#Notasanticipos').val(null);
  $('#ResponsableAnticipoEn').val(codempleado).change();
  $('#ResponsableAnticipoRe').val(0).change();
}

function MASK(form, n, mask, format) {
  if (format == "undefined") format = false;
  if (format || NUM(n)) {
    dec = 0, point = 0;
    x = mask.indexOf(".")+1;
    if (x) { dec = mask.length - x; }

    if (dec) {
      n = NUM(n, dec)+"";
      x = n.indexOf(".")+1;
      if (x) { point = n.length - x; } else { n += "."; }
    } else {
      n = NUM(n, 0)+"";
    } 
    for (var x = point; x < dec ; x++) {
      n += "0";
    }
    x = n.length, y = mask.length, XMASK = "";
    while ( x || y ) {
      if ( x ) {
        while ( y && "#0.".indexOf(mask.charAt(y-1)) == -1 ) {
          if ( n.charAt(x-1) != "-")
            XMASK = mask.charAt(y-1) + XMASK;
          y--;
        }
        XMASK = n.charAt(x-1) + XMASK, x--;
      } else if ( y && "$0".indexOf(mask.charAt(y-1))+1 ) {
        XMASK = mask.charAt(y-1) + XMASK;
      }
      if ( y ) { y-- }
    }
  } else {
     XMASK="";
  }
  if (form) { 
    form.value = XMASK;
    if (NUM(n)<0) {
      form.style.color="#FF0000";
    } else {
      form.style.color="#000000";
    }
  }
  return XMASK;
}

function NUM(s, dec) {
  for (var s = s+"", num = "", x = 0 ; x < s.length ; x++) {
    c = s.charAt(x);
    if (".-+/*".indexOf(c)+1 || c != " " && !isNaN(c)) { num+=c; }
  }
  if (isNaN(num)) { num = eval(num); }
  if (num == "")  { num=0; } else { num = parseFloat(num); }
  if (dec != undefined) {
    r=.5; if (num<0) r=-r;
    e=Math.pow(10, (dec>0) ? dec : 0 );
    return parseInt(num*e+r) / e;
  } else {
    return num;
  }
}

function fparavereda(idConcepto){
  var parametros={idConcepto};
  $.ajax({
    type: "POST",
    url: "../../logica/logica.php?accion=BuscarTipoConcepto",
    data: parametros,
    dataType: "json",
    success: function (responsev) {
      if(responsev[9] == 1){
        
        var CodVeredaSG=$('#CodVeredaSG').val();
        if (CodVeredaSG == 0 || CodVeredaSG == "") {
            $('#mensajeWarning').html(`
              <button type="button" class="close" data-dismiss="alert">&times;</button>
              <strong>Atencion:</strong> Se debe seleccionar una Vereda
            `).change();
            var $miModal = $('#ModalMensajewarningSG');
            $miModal.modal('show');
            setTimeout(function() { $miModal.modal('toggle');
                //$('.modal-backdrop').remove();
              }, 4000);
        } else {
          var parametrosv={CodVeredaSG,idConcepto};
          $.ajax({
            type: "POST",
            url: "../../logica/logica.php?accion=GetTarifaVereda",
            data: parametrosv,
            dataType: "json",
            success: function (response) {
              if (response == 0) {
                $('#ValorConceptoSG').val(0).change();
              } else {
                $('#ValorConceptoSG').val(response[3]).change();
              }
              
            }
          });
        }
      }else{
        var idMunicipio=$('#CodMunicipioSG').val();
        if (idMunicipio == 0 || idMunicipio == null) {
            $('#mensajeWarning').html(`
              <button type="button" class="close" data-dismiss="alert">&times;</button>
              <strong>Atencion:</strong> Se debe seleccionar un Municipio
            `).change();
    
            var $miModal = $('#ModalMensajewarningSG');
              $miModal.modal('show');
              setTimeout(function() {
                  $miModal.modal('toggle');
                  // $('.modal-backdrop').remove();
                }, 4000);
        }else{
          var parametros={idConcepto,idMunicipio};
          $.ajax({
            type: "POST",
            url: "../../logica/logica.php?accion=GetTarifaTipoConcepto",
            data: parametros,
            dataType: "json",
            success: function (response) {
              if (response == null) {
                $('#ValorConceptoSG').val(0);
              } else {
                $('#ValorConceptoSG').val(response[8]);
              }
            }
          });
        }
      }
    }
  });
}

function TraerTarifa(obj){
  $('#ValorConceptoSG').val(0).change();
  fparavereda($(obj).val());
  
}

function validarImagen(obj){
  
  if(obj.length == 0){

      var $miModal = $('#ModalMensajeCamposVacios');
      $miModal.modal('show');
      setTimeout(function() { $miModal.modal('toggle'); }, 4000);
      $('#GuardarLegalizacion').attr("disabled", true).change();
  
  }else{
    var uploadFile = obj.files[0];
      if (!window.FileReader) {
          alert('El navegador no soporta la lectura de archivos');
          return;
      }

      if (!(/\.(jpg|png|jpeg)$/i).test(uploadFile.name)) {
          alert('El archivo a adjuntar no es una imagen');
      }else{
          var img = new Image();
          img.onload = function () {
              if (uploadFile.size > 4194304) { // 4194304 bytes o 4096 Kb o 4 MB
                var $miModal1 = $('#ModalMensajewarningImagen');
                $miModal1.modal('show');
                setTimeout(function() { $miModal1.modal('toggle'); }, 4000);
                $('#Soporte_Legalizacion').val(null);
                $('#GuardarLegalizacion').attr("disabled", true);         
            }else{
                $('#GuardarLegalizacion').attr("disabled", false).change();
            }
          };
        img.src = URL.createObjectURL(uploadFile);
        $('#ImagenSoporteFisico').attr("src",img.src).change();
      }
  }
}