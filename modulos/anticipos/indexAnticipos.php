<?php 
	$IdRecurso=39;
	include_once('../principal/CABparapermisos.php');
	
	/*
        Forma de colocar permisos en cada modulo.

        Funcion: colocar_permiso($IdUsuario,$accion,$IdRecurso)

        $IdUsuario: $IdUsuariologin=$_SESSION['id'];
        $accion: 1-Consultar, 2-Editar,3-Insertar,4-Eliminar,5-Listar - > Este nuemro verifica la columna de la tabal que relaciona el recurso_perfil
		$IdRecurso: Cod del recurso - Columna: CodRecurso de la tabla recurso.
		Retorna el valor que hay en l aBD (1-SI tiene permiso,0-NO tiene permiso)    
	*/
	if($_SESSION['autentic']){
        if($Permisos_Listar==1 || $useradmon){
            $IdUsuariologin=$_SESSION['id'];
            require_once("../conn_BD.php");
            require_once('../anticipos/class/classAnticipos.php');
            require_once('../solicitud_gastos/class/ClassSolicitudGastos.php');
            require_once('../empleados/class/classEmpleados.php');
            require_once("../entidades/class/classEntidades.php");
            require_once("../../modulos/funciones.php");
            $InstanciaDB=new Conexion();
            $InstAnticipos=new Proceso_Anticipos($InstanciaDB);
            $InstSG=new Proceso_SolicitudGastos($InstanciaDB);
            $InstEmpleados=new Proceso_Empleados($InstanciaDB);
            $InstEntidades=new Proceso_Entidades($InstanciaDB);
            $ListaSGxAnticipo=$InstSG->ListarSolicitudGastosxEstado(1);
            $ListaAnticipos=$InstAnticipos->ListarAnticipos();
            $listaEmpleados=$InstEmpleados->ListarEmpleados();
            $listaEntidades=$InstEntidades->ListarEntidades();      
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- <head> -->
    <?php include_once('../headScript.php'); ?>
<!-- </head> -->        
<body>
    <div id="wrapper">
	<?php 
		include_once('../headWeb.php');
		include_once("../../menu/m_principal.php");
	?>
        <div id="page-wrapper" >
            <div id="page-inner">						                
                 <!-- /. ROW  -->               
                <div class="panel-body" align="right">
				<?php if( $Permisos_Insertar==1 || $useradmon ){ //Permiso para Insertar o Crear ?>                                                                                 
					<button type="button" class="btn btn-success btn-circle" data-toggle="modal" data-target="#ModalNuevoAnticipo" onclick="ParaAnticipoNuevo();">
						<i class="fa fa-plus fa-2x"></i>
					</button>
				<?php } ?>
                </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading" style="height:55px;">
                            Solicitudes de Gastos <b>X Generar Anticipo</b>
                            <div style="float:right;"><button type="button" class="btn btn-primary"><span class="badge">
                                        <?php echo $ListaSGxAnticipo->num_rows;?></span></button></div>
                        </div>
                        <div class="panel-body">
                        <?php 
                            if ($ListaSGxAnticipo->num_rows > 0) { ?>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <th>#</th>
                                        <th>Fecha Solicitud</th>
                                        <th>Municipio</th>
                                        <th>Actividad</th>
                                        <th>Valor Aprobado</th>
                                        
                                        <th><span class='glyphicon glyphicon-cog' title='Config'></span></th>
                                    </thead>
                                    <tbody>
                                        <?php
                                        
                                            while($row=$ListaSGxAnticipo->fetch_array()){
                                            // $datos=$row[0]."||".$row[1]."||".$row[2]."||".$row[3]."||".$row[4]."||".$row[5]."||".$row[6]."||".$row[7]."||".$row[8]."||".$row[9]."||".$row[10]."||".$row[11]."||".$row[12]."||".$row[13]."||".$row[14]."||".$row[15]."||".$row[16]."||".$row[17]."||".$row[18]."||".$row[19]."||".$row[20]."||".$row[21]."||".$row[22]."||".$row[23];

                                                $ListaResponsables=$InstSG->ListaResponsablesxIDsolicitud($row[0]);
                                                $datosResponsables='';
                                                while ($rowR=$ListaResponsables->fetch_array()) {
                                                    $datosResponsables=$datosResponsables."||".$rowR[2];
                                                }
                                        ?>
                                        <tr class="odd gradeX">
                                            <td><?php echo $row[0]; ?></td>
                                            <td><?php echo $row[1]; ?></td>
                                            <td><?php echo $row[13]; ?></td>
                                            <td><?php echo $row[16]; ?></td>
                                            <td><?php echo '$'.number_format($row[18]); ?></td>
                                            <td>
                                                <?php if($Permisos_Consultar==1 || $useradmon) { //Permisos para Consultar/Ver Registro ?>
                                                    <button title="Ver" onclick="VerSolicitudGasto('<?php echo $row[0];?>','<?php echo $datosResponsables;?>')"
                                                        class="btn btn-default btn-sm" data-toggle="modal" data-target="#VerSolicitudGasto"><span
                                                            class="glyphicon glyphicon-info-sign" style="color:blue;"></span></button>
                                                <?php } ?>
                                                <?php if( $Permisos_Insertar==1 || $useradmon ){ //Permiso para Insertar o Crear ?>    
                                                    <button type="button" data-toggle="modal" data-target="#ModalNuevoAnticipo" class="btn btn-default btn-sm" onclick="AnticipodesdeIdSG(<?php echo $row[0];?>,<?php echo $row[18];?>)">
                                                    <span class="glyphicon glyphicon-arrow-right" style="color:rgb(255, 128, 0);"></span>
                                                        <span class="glyphicon glyphicon-new-window" style="color:rgb(255, 128, 0);"></span></button>
                                                <?php } ?>    
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <?php } else { ?>
                                <div class="alert alert-dismissible alert-info">
                                    <strong>Buen trabajo!</strong> <b class="alert-link"> - No hay </b> Solicitudes de Gatos pendientes para Anticipos.
                                </div>
                        <?php } ?>
                        </div>
                    </div>
                </div>
            </div>                                
        </div>               
    </div>
  </div>

    <?php 
        include_once("modalinsanticipo.php");
        include_once("../solicitud_gastos/VerSG.php");
    ?>

    <div class="modal fade" id="ModalMensajesuccess" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="alert alert-dismissible alert-success">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <b>Eureka !!</b>: Anticipo Insertado Correctamente !
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="ModalMensajewarning" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div id="MensajeaMostrar">
                            <div class="alert alert-dismissible alert-warning">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <b>Hubo un error !!</b>: Comuniquese con el administrador !
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="Modalwarning" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div id="MensajeaMostrar">
                            <div class="alert alert-dismissible alert-warning">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <b>Atencion !!</b>: No pueden estar campos vacios !
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    <script src="../../assets/js/jquery-1.10.2.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script src="../../assets/js/jquery.metisMenu.js"></script>
    <script src="../../assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="../../assets/js/dataTables/dataTables.bootstrap.js"></script>
	<script src="../../assets/js/jasny-bootstrap.min.js"></script>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
                
                $('#ResponsableAnticipoRe').select2({
                    dropdownParent: $("#ModalNuevoAnticipo")
                });
                
                $('#ResponsableAnticipoEn').select2({
                    dropdownParent: $("#ModalNuevoAnticipo")
                });

                $('#VerresponsableSGdiv').select2();
                $('#VerentidadesSGdiv2').select2();

                $('#IdSG').select2({
                    dropdownParent: $("#ModalNuevoAnticipo")
                });

                $('#IdSG').change(function () { 
                    var IdSG=$(this).val();
                    var parametros={IdSG};
                    $.ajax({
                        type: "POST",
                        url: '../../logica/logica.php?accion=getSolcitudGastoxAprobar',
                        data: parametros,
                        dataType: "json",
                        success: function (response) {
                            $('#ValorAnticipo').val('$ '+formatNumber.new(response[18])).change();
                            $('#Boton_Guardar_anticipo').attr('disabled', false);
                            $('#DocEquivalente').attr('disabled',false);
                            $('#ValorAnticipo').attr('disabled',false);
                            $('#Notasanticipos').attr('disabled',false);
                            $('#ResponsableAnticipoEn').attr('disabled',false);
                            $('#ResponsableAnticipoRe').attr('disabled',false);
                        }
                    });
                    
                });
            });
    </script>
         <!-- CUSTOM SCRIPTS -->
    <script src="../../assets/js/custom.js"></script>
</body>
</html>
<?php
        }else{
            include_once('../principal/sinpermisolistar.php');         
            }
	}else{
		header('Location:../../php_cerrar.php');
	}
?>
