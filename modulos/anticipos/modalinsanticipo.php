<!--  Modal Nuevo Anticipos-->
        <?php $idempleado=$_SESSION['idempleado']; ?>
        <div class="modal fade" id="ModalNuevoAnticipo" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<form name="form2" method="post" enctype="multipart/form-data" action="">											
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>															
							<h3 align="center" class="modal-title" id="TituloModalAnticipos"></h3>
						</div>
						<div id="msgAnticiposNuevo"></div>
						<div class="panel-body">
                            
                            <?php 
                                if ($ListaSGxAnticipo->num_rows > 0) {
                            ?>

                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class="control-label">Fecha</label>
                                <div class='input-group date datetime' id='datetimepicker1' style="width:100%">
                                    <input id="FechaAnticipo" size="16" type="text" class="form-control" autocomplete="off" value="<?php echo $fechadatetimepicker;?>">
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                                    <?php
                                        $TipoCalendario='Fecha';
                                        include('../../Insertfecha.php');
                                    ?>
                            </div>


                            <div class="form-group col-sm-6">
                                <label>Solicitud de Gastos</label><br>
                                <select class="form-control" name="IdSG" id="IdSG" style="width:100%">
                                    <option values='0'></option>
                                    <?php
                                        mysqli_data_seek($ListaSGxAnticipo, 0);
                                        while ($rowEM=$ListaSGxAnticipo->fetch_array(MYSQLI_BOTH)) { 
                                            echo "<option value='".$rowEM[0]."'>#".$rowEM[0]."- $ ".number_format($rowEM[18])."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                              <label for="">Documento Equivalente</label>
                              <input type="text" class="form-control" name="DocEquivalente" id="DocEquivalente" aria-describedby="helpId" >
                            </div>

                            <div class="form-group col-sm-6">
                              <label for="">Valor Anticipo</label>
                              <input type="text" class="form-control" name="ValorAnticipo" id="ValorAnticipo" onchange="MASK(this,this.value,'-$##,###,##0',1)">
                            </div>
                        </div>           
                        <div class="row">

                             <div class="form-group col-sm-6">
                                <label>Responsable - Entrega</label><br>          
                                <select class="form-control" name="ResponsableAnticipoEn" id="ResponsableAnticipoEn" style="width:100%">
                                    <option values='0'> </option>
                                    <?php
                                        mysqli_data_seek($listaEmpleados, 0);
                                        while ($rowEM=$listaEmpleados->fetch_array(MYSQLI_BOTH)) { 
                                            echo "<option value='".$rowEM[0]."'>".$rowEM[2]."</option>";
                                        }
                                        
                                    ?>
                                </select>
                                <input type="hidden" name="empleadologin" id="empleadologin" value="<?php echo $idempleado; ?>">
                            </div>

                            <div class="form-group col-sm-6">
                                <label class="col-sm-9">Responsable - Recibe</label><i class="fas fa-hand-holding-usd fa-lg col-sm-3"></i>       
                                <select class="form-control" name="ResponsableAnticipoRe" id="ResponsableAnticipoRe" style="width:100%">
                                    <option values='0'> </option>
                                    <?php
                                        mysqli_data_seek($listaEmpleados, 0);
                                        while ($rowEM=$listaEmpleados->fetch_array(MYSQLI_BOTH)) { 
                                            echo "<option value='".$rowEM[0]."'>".$rowEM[2]."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-12">
                              <label for="">Notas</label>
                              <textarea class="form-control" name="Notasanticipos" id="Notasanticipos" rows="3" maxlength="100" style="resize: none;"></textarea>
                            </div>
                        </div>
                            <?php 
                                } else {?>
                                    <div class="alert alert-dismissible alert-info">
                                        <strong>Buen trabajo!</strong> <b class="alert-link"> - No hay </b> Solicitudes de Gatos pendientes por Anticipo.
                                    </div>
                            <?php } ?>

                        </div>
						<div class="modal-footer" id="modal-footer_Anticipos"></div>										 
					</div>
				</div>
			</form>
		</div>
	<!-- End Modal Nuevo Anticipos-->