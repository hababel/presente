<?php 

class Proceso_Anticipos {

    function __construct($InstanciaDB){
        $this->ConnxClass=$InstanciaDB;
     }

    function InsertarAnticipo($FechaAnticipo,$ResponsableAnticipoEn,$ResponsableAnticipoRe,$IdSGAnticipo,$DocEquivalenteAnticipo,$ValorAnticipo,$NotasAnticipos){
    $sql="INSERT INTO anticipos (FechaAnticipo, ResponsableAnticipoEn, ResponsableAnticipoRe, IdSGAnticipo, DocEquivalenteAnticipo, ValorAnticipo, NotasAnticipos)
    VALUES ('".$FechaAnticipo."', ".$ResponsableAnticipoEn.",".$ResponsableAnticipoRe." ,".$IdSGAnticipo.", '".$DocEquivalenteAnticipo."', ".$ValorAnticipo.",'".$NotasAnticipos."')";
    // echo $sql;
    $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
    return $this->resultado;        
    }

    function ListarAnticipos(){
        $sql="SELECT
        anticipos.IdAnticipos,
        anticipos.FechaAnticipo,
        anticipos.ResponsableAnticipoEn,
        anticipos.ResponsableAnticipoRe,
        anticipos.IdSGAnticipo,
        anticipos.DocEquivalenteAnticipo,
        anticipos.ValorAnticipo,anticipos.NotasAnticipos,
        QuienE.NombreEmpleado AS QuienRecibe,
        QuienR.NombreEmpleado AS QuienEntrega
        FROM anticipos
        INNER JOIN empleado AS QuienE ON anticipos.ResponsableAnticipoEn=QuienE.idEmpleado
        INNER JOIN empleado as QuienR ON anticipos.ResponsableAnticipoRe=QuienR.idEmpleado";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado; 
    }

    function BuscarAnticiposxId($IdAnticipo){
        $sql="SELECT IdAnticipos, FechaAnticipo, ResponsableAnticipoEn, ResponsableAnticipoRe, IdSGAnticipo, DocEquivalenteAnticipo,ValorAnticipo, NotasAnticipos
        FROM anticipos WHERE IdAnticipos=".$IdAnticipo;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
        
    }

    function BuscarAnticipoxIdSG($IdSG){
        $sql="SELECT IdAnticipos, FechaAnticipo, ResponsableAnticipoEn, ResponsableAnticipoRe, IdSGAnticipo, DocEquivalenteAnticipo,ValorAnticipo, NotasAnticipos
        FROM anticipos WHERE IdSGAnticipo=".$IdSG;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function BuscarAnticiposxDocEquivalente($DocEquivalenteAnticipo){
        $sql="SELECT IdAnticipos, FechaAnticipo, ResponsableAnticipoEn, ResponsableAnticipoRe, IdSGAnticipo, DocEquivalenteAnticipo,ValorAnticipo, NotasAnticipos
        FROM anticipos WHERE DocEquivalenteAnticipo=".$DocEquivalenteAnticipo;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }
}


?>