<?php 
	 $IdRecurso=36;
     include_once('../principal/CABparapermisos.php');
     
     /*
         Forma de colocar permisos en cada modulo.
     
         Funcion: colocar_permiso($IdUsuario,$accion,$IdRecurso)
     
         $IdUsuario: $IdUsuariologin=$_SESSION['id'];
         $accion: 1-Consultar, 2-Editar,3-Insertar,4-Eliminar,5-Listar - > Este nuemro verifica la columna de la tabal que relaciona el recurso_perfil
         $IdRecurso: Cod del recurso - Columna: CodRecurso de la tabla recurso.
         Retorna el valor que hay en la columna de la BD (1-SI tiene permiso,0-NO tiene permiso)    
     */
     if($_SESSION['autentic']){
         if($Permisos_Listar==1 || $useradmon){
		 require_once("../conn_BD.php");
         require_once('class/ClassTipoTalleres.php');
		 $InstanciaDB=new Conexion();
		 $insTipoTall=new Proceso_TipoTaller($InstanciaDB);
		 $LastidTipoTall=$insTipoTall->getlastidTipoTaller();
		 $ListaTipoTall=$insTipoTall->ListaTipoTaller();
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- <head> -->
    <?php include_once('../headScript.php'); ?>
<!-- </head> -->        
<body>
    <div id="wrapper">
        <?php 
            include_once('../headWeb.php');
            include_once("../../menu/m_principal.php");
        ?>
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="panel-body" align="right">  
                    <?php if( $Permisos_Insertar==1 || $useradmon ){ //Permiso para Insertar o Crear ?>
                        <button type="button" class="btn btn-success btn-circle" data-toggle="modal" data-target="#modalinsertPrograma">
                            <i class="fa fa-plus fa-2x"></i>
                        </button>
                    <?php } ?>
                </div>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Tipo De Talleres
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">                                            	                               
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>ID Tipo de Talleres</i></th>
                                        <th>Descripcion  Tipo Talleres</th>
                                        <th>Estado de tipo de Talleres</th>
                                        <th><span class='glyphicon glyphicon-cog' title='Config'></span></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        while($row=$ListaTipoTall->fetch_array()){
                                        $datos=$row[0]."||".$row[1]."||".$row[2];
                                    ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $row[0]; ?></td>
                                        <td><?php echo $row[1]; ?></td>
                                        <td><?php echo $row[2]; ?></td> 
                                        <td class="center">
                                            <?php if($Permisos_Consultar==1 || $useradmon) { //Permisos para Consultar/Ver Registro ?>
                                                <button title="Edit" onclick="formeditTipoTaller('<?php echo $datos;?>')" class="btn btn-default btn-sm" data-toggle="modal" data-target="#modaleditPrograma"><span class="glyphicon glyphicon-pencil"></span></button>
                                            <?php } ?>
                                        </td>
                                    </tr> 
                                    <?php } ?>
                                </tbody>									
                            </table>							
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal de insertar tipo de taller nueva -->
        <div class="modal fade" id="modalinsertPrograma" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title" id="exampleModalLabel">Nuevo Tipo de taller
                        </h3>
                        <div class="col-md-10">
                            <div id="msgtipotaller"></div>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">Descripcion Tipo taller</label>
                            <input type="text" class="form-control  form-control-sm" name="descTipoTaller" id="descTipoTaller" aria-describedby="Descripcion Tipo Taller" autocomplete="off" autofocus require>
                        </div>
                        <div class="form-group">
                            <label for="">Estado taller</label>
                            <select class="form-control" name="estadoTipoTaller" id="estadoTipoTaller">
                                <option value=1>Activo</option>
                                <option value=0>No Activo</option>
                    
                            </select>
                        </div>        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-primary" onclick="InsertTipoTaller();">Guardar</button>
                    </div>
                </div>
            </div>  
        </div>
    <!-- Fin modal de insertar Tipo de taller nuevo -->
                    
    <!-- Modal para Editar Tipo Taller-->
        <div class="modal fade" id="modaleditPrograma" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel">Editar Tipo Taller</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" action="" method="post">
                        <fieldset>
                            <div id="msgEditTipoTaller"></div>
                            <div class="form-group">
                                <label>Id Tipo Taller</label>
                                <input id="idTipoTallerFM" name="idTipoTallerFM" type="text" placeholder="formeditTipoTallerId" class="form-control" autocomplete="off" disabled>
                            </div>                           
                            <div class="form-group">
                                <label>descripcion Tipo Taller</label>
                                <input id="descTipoTallerFM" name="descTipoTallerFM" type="text" placeholder="formeditTipoTallerDesc" class="form-control" autocomplete="off" required>
                            </div>
                            <div class="form-group">
                                <label>estado tipo Taller</label>
                                <input id="estadoTipoTallerFM" name="estadoTipoTallerFM" type="text" placeholder="formeditEstadoTipoTaller" class="form-control" autocomplete="off" required>
                            </div>
                        </fieldset>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <?php if($Permisos_Editar==1 || $useradmon) { //Permisos para Editar/Modificar Registro ?>
                        <button class="btn btn-primary" onclick="EditarTipoTaller();">Grabar</button>
                    <?php } ?>
                </div>
                </div>
            </div>
        </div>
        <!-- /Modal para Editar Tipo Taller -->

    <script src="../../assets/js/jquery-1.10.2.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script src="../../assets/js/jquery.metisMenu.js"></script>
    <script src="../../assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="../../assets/js/dataTables/dataTables.bootstrap.js"></script>
	<script src="../../assets/js/jasny-bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });

            $('#modalinsertPrograma').on('shown.bs.modal', function(){
            $('#descTipoTaller').trigger('focus'); })

            $('#modaleditPrograma').on('shown.bs.modal', function(){
            $('#descTipoTallerFM').trigger('focus'); })
            
    </script>
         <!-- CUSTOM SCRIPTS -->
    <script src="../../assets/js/custom.js"></script>
</body>
</html>
<?php }else{
            include_once('../principal/sinpermisolistar.php');         
        }
    }else{
		 header('Location:../../php_cerrar.php');
     }
     ?>