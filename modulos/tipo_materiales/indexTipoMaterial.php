<?php 
	$IdRecurso=32;
	include_once('../principal/CABparapermisos.php');
	
	/*
		Forma de colocar permisos en cada modulo.
	
		Funcion: colocar_permiso($IdUsuario,$accion,$IdRecurso)
	
		$IdUsuario: $IdUsuariologin=$_SESSION['id'];
		$accion: 1-Consultar, 2-Editar,3-Insertar,4-Eliminar,5-Listar - > Este nuemro verifica la columna de la tabal que relaciona el recurso_perfil
		$IdRecurso: Cod del recurso - Columna: CodRecurso de la tabla recurso.
		Retorna el valor que hay en l aBD (1-SI tiene permiso,0-NO tiene permiso)    
	*/
	if($_SESSION['autentic']){
		if($Permisos_Listar==1 || $useradmon){
		require_once("../conn_BD.php");
		require_once('../tipo_materiales/class/ClassTipoMaterial.php');
		$InstanciaDB=new Conexion();
		$InstTipoMaterial=new Proceso_TipoMaterial($InstanciaDB);
		$ListaTipoMaterial=$InstTipoMaterial->listatipomaterial();
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- <head> -->
    <?php include_once('../headScript.php'); ?>
<!-- </head> -->        
<body>
    <div id="wrapper">
	<?php 
		include_once('../headWeb.php');
		include_once("../../menu/m_principal.php");
	?>
        <div id="page-wrapper" >
            <div id="page-inner">						                
                 <!-- /. ROW  -->               
                <div class="panel-body" align="right">                                                                                 
					<?php if( $Permisos_Insertar==1 || $useradmon ){ //Permiso para Insertar o Crear ?> 
						<button type="button" class="btn btn-success btn-circle" data-toggle="modal" data-target="#ModalNuevoTipoMaterial">
							<i class="fa fa-plus fa-2x"></i>
						</button>
					<?php } ?>
																					
</div>
<div class="row">
<div class="col-md-12">
	<!-- Advanced Tables -->
	<div class="panel panel-primary">
		<div class="panel-heading">
				Tipo de Material
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover" id="dataTables-example">
					<thead>
						<tr>
							<th>Id</th>
							<th>Descripcion</th>
							<th><span class='glyphicon glyphicon-cog' title='Config'></span>
						</tr>
					</thead>
					<tbody>
						<?php
							while ($row=$ListaTipoMaterial->fetch_array()) {
								$datosTipoMaterial=$row[0]."||".$row[1];
							?>
						<tr class="odd gradeX">
							<td><?php echo $row[0]; ?></td>
							<td><?php echo $row[1]; ?></td>                                                                                  
							<td class="center">
								<?php if($Permisos_Consultar==1 || $useradmon) { //Permisos para Consultar/Ver Registro ?>
									<div class="btn-group">
										<button type="button" onclick="formeditTipoMaterial('<?php echo $datosTipoMaterial;?>')" class="btn btn-default btn-sm" data-toggle="modal" data-target="#ModalEditarTipoMaterialFM"><span class="glyphicon glyphicon-pencil"></span></button>
									</div>
								<?php } ?>
							</td>
						</tr>
							<?php }?>       	
						
		<!--  Modal Nuevo Tipo de Material-->
			<div class="modal fade" id="ModalNuevoTipoMaterial" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<form name="form2" method="post" enctype="multipart/form-data" action="">											
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>															
								<h3 align="center" class="modal-title" id="myModalLabel">Nuevo Tipo de Material</h3>
							</div>
							<div id="msgTipoMaterialNuevo"></div>
							<div class="panel-body">
							<div class="row">                                       
								<div class="col-md-6">
									
									<div class="form-group">
										<label for="">Descripcion Tipo de Material</label>
										<input class="form-control" name="DescTipoMaterial" id="DescTipoMaterial" aria-describedby="emailHelpId" autocomplete="off" required>
									</div>
									
								</div>
																										
							</div> 
							</div> 
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
								<button type="button" onclick="InsertTipoMaterial();" class="btn btn-primary">Guardar</button>
							</div>										 
						</div>
					</div>
				</form>
			</div>
		<!-- End Modal Nuevo Tipo de Material-->

			<!--  Modal Editar Tipo de Material-->
			<div class="modal fade" id="ModalEditarTipoMaterialFM" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>															
								<h3 align="center" class="modal-title" id="myModalLabel">Actualizar Tipo de Material</h3>
								<div id="msgEditTipoMaterialFM"></div>
							</div>
							<div class="panel-body">
								<div class="row">                                       
									<div class="col-md-6">																												
										<label>ID</label>
										<input class="form-control" id="IdTipoMaterialFM" name="IdTipoMaterialFM" disabled>
										<label>Descripcion Tipo de Material</label>
										<input class="form-control" id="descTipoMaterialFM" name="descTipoMaterialFM" autocomplete="off" required>
										
										
									</div>                                                                       
								</div> 
							</div> 
							<div class="modal-footer">
								<div align="right">
									<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
									<?php if($Permisos_Editar==1 || $useradmon) { //Permisos para Editar/Modificar Registro ?>
										<button type="submit" class="btn btn-primary" onclick="EditarTipoMaterialFM();">Guardar</button>
									<?php } ?>
								</div> 
							</div>										 
						</div>
					</div>
				</div>
			<!-- End Modal Editar TipoMaterial-->
					</tbody>									
				</table>							
			</div>                            
		</div>
	</div>
                    <!--End Advanced Tables -->
                </div>
            </div>                                
        </div>               
    </div>
  </div>
         <!-- /. PAGE WRAPPER  -->
     <!-- /. WRAPPER  -->
    <script src="../../assets/js/jquery-1.10.2.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script src="../../assets/js/jquery.metisMenu.js"></script>
    <script src="../../assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="../../assets/js/dataTables/dataTables.bootstrap.js"></script>
	<script src="../../assets/js/jasny-bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });

			$('#ModalNuevoTipoMaterial').on('shown.bs.modal', function(){
			$('#DescTipoMaterial').trigger('focus');})

			$('#ModalEditarTipoMaterialFM').on('shown.bs.modal', function(){
			$('#descTipoMaterialFM').trigger('focus'); })
			
    </script>
         <!-- CUSTOM SCRIPTS -->
    <script src="../../assets/js/custom.js"></script>
</body>
</html>
<?php }else{
            include_once('../principal/sinpermisolistar.php');         
        }
	}else{
		header('Location:../../php_cerrar.php');
	}
?>