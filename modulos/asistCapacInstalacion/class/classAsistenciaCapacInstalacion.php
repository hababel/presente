<?php 

Class Proceso_asistCapacInstalacion{

    function __construct($InstanciaDB){
        $this->ConnxClass=$InstanciaDB;
     }

    function ListarAsistCapacInstalacion(){
        $sql="SELECT asistenciacapacitacionesinstalacion.*,  
        tipotaller.DescTipoTaller,
        municipio.NombreMunicipio,
        empleado.NombreEmpleado
        FROM asistenciacapacitacionesinstalacion
        INNER JOIN tipotaller ON asistenciacapacitacionesinstalacion.idTipoTaller=tipotaller.idTipoTaller
        INNER JOIN municipio ON asistenciacapacitacionesinstalacion.idMunicipio=municipio.idMunicipio
        INNER JOIN empleado ON asistenciacapacitacionesinstalacion.idResponsable=empleado.idEmpleado";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function InsertAsistCapacInstal($CodFormAsisCapac,$FechaAsistCapac,$idTipoTaller,$idMunicipio,
    $idResponsable,$LugarAsistCapac,$CantAsistentes,$idReciboMaterial,$ObservAsistencCapac){
        $sql="INSERT INTO asistenciacapacitacionesinstalacion (CodFormAsistenciaCapacitaciones,FechaAsistenciaCapacitaciones,
        idTipoTaller,idMunicipio,idResponsable,LugarAsistenciaCapacitaciones,CantAsistentes,idReciboMaterial,ObservacionesAsistenciaCapacitaciones)
         VALUES ('".$CodFormAsisCapac."','".$FechaAsistCapac."',".$idTipoTaller.",".$idMunicipio.",".$idResponsable.",
         '".$LugarAsistCapac."',".$CantAsistentes.",'".$idReciboMaterial."','".$ObservAsistencCapac."')";

         
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);  
        return $this->resultado;
    }

    function editAsistCapacInstal($idReg,$CodFormAsisCapac,$FechaAsistCapac,$idTipoTaller,$idMunicipio,
    $idResponsable,$LugarAsistCapac,$CantAsistentes,$idReciboMaterial,$ObservAsistencCapac){
       $sql="UPDATE asistenciacapacitacionesinstalacion 
       SET CodFormAsistenciaCapacitaciones='".$CodFormAsisCapac."',FechaAsistenciaCapacitaciones='".$FechaAsistCapac."',
        idTipoTaller=".$idTipoTaller.",idMunicipio=".$idMunicipio.",idResponsable=".$idResponsable.",
        LugarAsistenciaCapacitaciones='".$LugarAsistCapac."',CantAsistentes=".$CantAsistentes.",
        idReciboMaterial='".$idReciboMaterial."',ObservacionesAsistenciaCapacitaciones='".$ObservAsistencCapac."'
        WHERE idAsistenciaCapacitaciones=".$idReg;
        // echo $sql;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function buscarCodRegAsisCapa($CodFormAsisCapac){
        $sql="SELECT * FROM asistenciacapacitacionesinstalacion WHERE asistenciacapacitacionesinstalacion.CodFormAsistenciaCapacitaciones='".$CodFormAsisCapac."'";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }
}
?> 