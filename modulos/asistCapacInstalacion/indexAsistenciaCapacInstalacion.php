<?php 
	session_start();
	if($_SESSION['autentic']){
		require_once("../conn_BD.php");
		require_once("../tipo_talleres/class/ClassTipoTalleres.php");
		require_once("../municipios/class/ClassMunicipios.php");
		require_once("../empleados/class/classEmpleados.php");		
		require_once("class/classAsistenciaCapacInstalacion.php");
		require_once("../institucion/class/classInstitucion.php");
		require_once("../veredas/class/classVeredas.php");
		require_once("../funciones.php");

		require_once("../filemodulos/class/classFileModulos.php"); // Clase para adjuntar archivos

		$InstanciaDB=new Conexion();
		$InstMcpios=new Proceso_Municipios($InstanciaDB);
		$InstEmpleados=new Proceso_Empleados($InstanciaDB);		
		$InstAsistTallInstal=new Proceso_asistCapacInstalacion($InstanciaDB);
		$InstTipoTall=new Proceso_TipoTaller($InstanciaDB);
		$InstInstitucion=new Proceso_Institucion($InstanciaDB);
		$InstVeredas=new Proceso_Vereda($InstanciaDB);
		$InstFileModulos= new Proceso_FileModulos($InstanciaDB); // Instancia para adjuntar archivos

		$ListaMcpios=$InstMcpios->ListaMunicipio();
		$listaEmpleados=$InstEmpleados->ListarEmpleados();	
		$ListaTipoTall=$InstTipoTall->ListaTipoTaller();
		$ListaAsistenciTall=$InstAsistTallInstal->ListarAsistCapacInstalacion();
		$ListaInstitucion=$InstInstitucion->listaInstitucion();
		$ListaVeredas=$InstVeredas->listaVereda();

		$rutaActual = getcwd();	//Obtener ruta actual													
		$DirActual=basename($rutaActual); //Obtener ruta absoluta actual

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- <head> -->
    <?php include_once('../headScript.php'); ?>

<!-- </head> -->        
<body>
    <div id="wrapper">
		<?php 
			include_once('../headWeb.php');
			include_once("../../menu/m_principal.php");
		?>
		<div id="page-wrapper" >
			<div id="page-inner">						                
				<!-- /. ROW  -->
				<div class="panel-body" align="right">                                                                                 
					<button type="button" class="btn btn-success btn-circle" onclick="NuevoAsistCapacitInstal();" data-toggle="modal" data-target="#ModalNuevoAsistCapacitInstal">
						<i class="fa fa-plus fa-2x"></i>
					</button>																							
				</div> 
				<div class="row">
					<div class="col-md-12">
						<!-- Advanced Tables -->
						<div class="panel panel-primary">
							<div class="panel-heading">
									Asistencia Capacitacion Instalacion
							</div>
							<div class="panel-body">
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover" id="dataTables-example">
										<thead>
											<tr>
												<th>Codigo</th>
												<th>Fecha</th>
												<th>Tipo Taller</th>
												<th>Municipio</th>											
												<th>Responsable</th>
												<th>Lugar</th>
												<th>Cant Asistentes</th>																							
												<th><span class='glyphicon glyphicon-cog' title='Config'></span>
											</tr>
										</thead>
										<tbody>
											<?php
												while ($row=$ListaAsistenciTall->fetch_array()) {
													$datosEntrega=$row[0]."||".$row[1]."||".$row[2]."||".$row[3]."||".$row[4]."||".$row[5]."||".$row[6]."||".$row[7]."||".$row[8]."||".$row[9]."||".$row[10]."||".$row[11]."||".$row[12];
												?>
											<tr class="odd gradeX">
											<td><?php echo $row[1]; ?></td>
												<td><?php echo $row[2]; ?></td>
												<td><?php echo $row[10]; ?></td>
												<td><?php echo $row[11]; ?></td>
												<td><?php echo $row[12]; ?></td>
												<td><?php echo $row[6]; ?></td>				
												<td><?php echo $row[7]; ?></td>      								             
												<td class="center">
													<div class="btn-group">
														<button type="button" onclick="formatEditAsistCapacInstal('<?php echo $datosEntrega;?>')" class="btn btn-default btn-sm" data-toggle="modal" data-target="#ModalNuevoAsistCapacitInstal"><span class="glyphicon glyphicon-pencil"></span></button>

														<!-- Boton para subir archivos -->
															<?php 
																$ListaFilexDoc=$InstFileModulos->ListFilexOrigen_idDoc($DirActual,$row[0]);
																$CantFileDoc=$ListaFilexDoc->num_rows;
																
															?>
															
															<button type="button"
																onclick="AbrilModalUploadFile('<?php echo $DirActual; ?>','<?php echo $row[0];?>')"
																class="btn btn-default btn-sm" data-toggle="modal"
																data-target="#UploadFile" >
																
																<span id="ImagenFiles<?php echo $row[0];?>" <?php if($CantFileDoc>0){ echo 'style="color:DodgerBlue;"';}else{echo 'style="color:Gray;"';}?> class="glyphicon glyphicon-paperclip"></span>
															</button>
														<!-- Fin Boton adjuntar archivos -->

													</div>
												</td>
											</tr>
												<?php }?>   
										</tbody>									
									</table>							
								</div>                            
							</div>
						</div>
			    	</div>    
		    	</div>               
	    	</div> 
     	</div> 	

	<!--  Modal Nuevo Registro Asistencia Capacitacion -->
		<div class="modal fade" id="ModalNuevoAsistCapacitInstal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">					
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>															
								<h3 align="center" class="modal-title" id="TituloModal"></h3>
							</div>
							<div id="msgAsistCapacInst"></div>
							<div class="panel-body">	
								<div class="row col-sm-5">
									<div class="form-group">
										<label class="control-label"for="">Codigo Registro</label>
										<input class="form-control" name="Codigo" id="CodFormAsisCapac" aria-describedby="" placeholder="Codigo" autocomplete="off" required>
									</div>	
									<div class="form-group">
										<label class="control-label">Fecha</label>
											<div class='input-group date datetime' id='datetimepicker1'>
												<input type='text' class="form-control" id="fechaAsist" autocomplete="off">
												<span class="input-group-addon">
													<span class="glyphicon glyphicon-calendar"></span>
												</span>
											</div>
									</div>
									
									<?php
									$TipoCalendario='Fecha';
									include('../../Insertfecha.php');?>
									<div class="form-group">
									<label  class="control-label">Tipo Taller</label>
											<select class="form-control js-example-basic-single" id="idTipoTaller" name="TipoTaller" style="width:100%;">		
														<?php 
															while ($rowTipTal=$ListaTipoTall->fetch_array()) {
																echo "<option value='".$rowTipTal[0]."'>".$rowTipTal[1]."</option>";
															}
														?>
											</select>
									</div>	
									<div class="form-group">
									<label  class="control-label">Municipio</label>
											<select class="form-control js-example-basic-single" id="idMunicipio" name="Municipio" style="width:100%;">
														<?php 
															while ($rowMcpio=$ListaMcpios->fetch_array()) {
																echo "<option value='".$rowMcpio[0]."'>".$rowMcpio[2]." -- ".$rowMcpio[7]."</option>";
															}
														?>
											</select>
									</div>   																															
									
								</div> 	

							<div class="row col-sm-2"></div>
							<div class="row col-sm-5">
								<div class="form-group">
										<label  class="control-label" >Responsable</label>
											<select class="form-control js-example-basic-single" id="idResponsable" name="Responsable" style="width:100%;">
												<?php 
													while ($rowEmp=$listaEmpleados->fetch_array()) {
														echo "<option value='".$rowEmp[0]."'>".$rowEmp[2]."</option>";
													}
												?>
											</select>
									</div>
								<div class="form-group">
									<label class="control-label"for="">Sitio de Reunion</label>
									<input class="form-control" name="LugarAsistCapac" id="LugarAsistCapac" aria-describedby="" placeholder="sitioReunion" autocomplete="off" required>
								</div>
								<div class="form-group">
									<label class="control-label"for="">Cantidad Asistentes</label>
									<input type="number" min="0"  class="form-control" name="CantAsistentes" id="CantAsistentes" placeholder="Cant Asistentes" autocomplete="off" required>
								</div>
								<div class="form-group">
									<label class="control-label"for="">Material entregado</label>
									<input class="form-control" name="Recibo Material" id="idReciboMaterial" aria-describedby="" placeholder="Material" autocomplete="off" required>
								</div>						
							</div>
							<div class="row col-sm-12">
									<div class="form-group">
										<label class="control-label" for="">Observaciones</label>
										<textarea class="form-control" name="ObservAsistencCapacEsc" id="ObservAsistencCapacEsc" rows="2" placeholder="Observaciones" autocomplete="off" style="resize: none;width:100%;" required></textarea>
									</div>
								</div>		                                                         
							</div> 
						<div class="modal-footer" id="BotonesAsistenciaCapacitacion">
							
						</div>										 
					</div>
				</div>
		</div>
			
	<!-- End Modal Nuevo Registro Entrega Coleccion-->

       
  
 	<?php include_once ('../filemodulos/indexFilemodulos.php'); //Codigo modal de adjuntar archivos ?> 

    <script src="../../assets/js/jquery-1.10.2.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script src="../../assets/js/jquery.metisMenu.js"></script>
    <script src="../../assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="../../assets/js/dataTables/dataTables.bootstrap.js"></script>
	<script src="../../assets/js/jasny-bootstrap.min.js"></script>
	<script src="../../assets/js/custom.js"></script>
	<script>
            $(document).ready(function () {
				$('#dataTables-example').dataTable();
				
			});			
    </script>
</body>
</html>
<?php 
	}else{
		header('Location:../../php_cerrar.php');
	}
?>