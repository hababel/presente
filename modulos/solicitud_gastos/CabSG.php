<!-- Encabezado de SG -->
    
    <!-- Row start -->
        <div id="CabIdSG">
            <div class="table-responsive">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="panel panel-default">
                    <div class="panel-heading clearfix">
                            <div class="col-sm-12">
                                <h3 class="panel-title">Datos Documento Solicitud de Gasto</h3>
                            </div>
                            <div class="col-sm-6" id="Datos_SG">
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="row col-md-6 col-sm-6">
                                    <table class="table table-hover">
                                        <tbody>
                                            <tr class="table-light">
                                                <th scope="row">Departamento</th>
                                                <td>
                                                    
                                                    <div name="VerCodDepartamentoSGdiv" id="VerCodDepartamentoSGdiv"></div>
                                                </td>
                                            </tr>
                                            <tr class="table-light">
                                                <th scope="row">Municipio</th>
                                                <td>
                                                    <div name="CodMunicipio" id="CodMunicipio" style="display:none"></div>
                                                    <div name="VerCodMunicipioSGdiv" id="VerCodMunicipioSGdiv"></div>
                                                </td>
                                            </tr>
                                            <!-- <tr class="table-light">
                                                <th scope="row">Vereda</th>
                                                <td>
                                                    <div name="VerCodVeredaSGdiv" id="VerCodVeredaSGdiv"></div>
                                                </td>
                                            </tr> -->
                                            <tr class="table-light">
                                                <th scope="row">Proyecto</th>
                                                <td>
                                                    <div name="VerCodProyectoSGdiv" id="VerCodProyectoSGdiv"></div>
                                                </td>
                                            </tr>
                                            <tr class="table-light">
                                                <th scope="row">Proceso</th>
                                                <td>
                                                    <div name="VerCodProcesoSGdiv" id="VerCodProcesoSGdiv"></div>
                                                </td>
                                            </tr>
                                            <tr class="table-light">
                                                <th scope="row">Actividad</th>
                                                <td>
                                                    <div name="VerCodActividadSGdiv" id="VerCodActividadSGdiv"></div>
                                                </td>
                                            </tr>
                                            
                                        </tbody>
                                    </table>
                                </div>
                                <div class="row col-md-6 col-sm-6">
                                    <table class="table table-hover">
                                        <tbody>
                                            
                                            <tr class="table-light">
                                                <th scope="row">Fecha Salida</th>
                                                <td>
                                                    <div name="VerFechaHoraSalidaSGdiv" id="VerFechaHoraSalidaSGdiv"></div>
                                                </td>
                                            </tr>
                                            <tr class="table-light">
                                                <th scope="row">Fecha Regreso</th>
                                                <td>
                                                    <div name="VerFechaHoraRegresoSGdiv" id="VerFechaHoraRegresoSGdiv"></div>
                                                </td>
                                            </tr>
                                            <tr class="table-light">
                                                <th scope="row">Valor Aprobado</th>
                                                <td>
                                                    <div name="VerValorAprobadoSGdiv" id="VerValorAprobadoSGdiv"></div>
                                                </td>
                                            </tr>
                                            <tr class="table-light">
                                                <th scope="row">Anticipo</th>
                                                <td>
                                                    <div name="VerDocAnticipoSGdiv" id="VerDocAnticipoSGdiv"></div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row col-sm-6">
                                    <label>Responsables</label>
                                    <label>
                                        <select class="js-example-basic-multiple" name="VerresponsableSGdiv" id="VerresponsableSGdiv" multiple="multiple" style="width:100%">
                                            <?php
                                                    mysqli_data_seek($listaEmpleados, 0);
                                                    while ($rowEM=$listaEmpleados->fetch_array(MYSQLI_BOTH)) { 
                                                        echo "<option value='".$rowEM[0]."'>".$rowEM[2]."</option>";
                                                    }
                                                ?>
                                        </select>
                                    </label>
                                </div>
                                <div class="row col-sm-6">
                                    <label>Entidades</label>
                                    <label>
                                        <select class="js-example-basic-multiple" name="VerentidadesSGdiv" id="VerentidadesSGdiv" multiple="multiple" style="width:100%">
                                            <?php
                                                mysqli_data_seek($listaEntidades, 0);																		
                                                while ($rowEnt=$listaEntidades->fetch_array(MYSQLI_BOTH)) { 
                                                    echo "<option value='".$rowEnt[0]."'>".$rowEnt[2]."</option>";
                                                    }
                                            ?>
                                        </select>
                                    </label>
                                </div>
                            </div>                     
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!-- Row end -->