    <div class="modal fade" id="VerSolicitudGasto" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 class="modal-title" id="myModalLabel">Solicitud de Gastos - # <label id="VerIdSolicitudGastoSG"></Label></h3>
                    Fecha Elaboracion: <label align="right" id="VerFechaSolicitudGastoSG"></label>
                    <div id="msgEstado"></div>
                </div>

                <div class="panel-body center-block">
                    <div id="Cab_IdSG"><?php include('CabSG.php');?></div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-12">
                            <div id="ListaDetalleSG"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div id="VerAprobarizacionSG"></div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <div style="width:70%; float:left;" id="msgBotonAccionSG" class="btn-group" role="group"></div>
                    <div style="width:30%; float:right;" class="btn-group" role="group">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>