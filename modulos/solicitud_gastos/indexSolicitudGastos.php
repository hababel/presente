<?php 

    /*
         Forma de colocar permisos en cada modulo.
 
         Funcion: colocar_permiso($IdUsuario,$accion,$IdRecurso)
 
         $IdUsuario: $IdUsuariologin=$_SESSION['id'];
         $accion: 1-Consultar, 2-Editar,3-Insertar,4-Eliminar,5-Listar - > Este nuemro verifica la columna de la tabal que relaciona el recurso_perfil
         $IdRecurso: Cod del recurso - Columna: CodRecurso de la tabla recurso.
         Retorna el valor que hay en l aBD (1-SI tiene permiso,0-NO tiene permiso)    
     */
    session_start();
    require_once("../../modulos/funciones.php");
	
    $IdUsuariologin=$_SESSION['id']; //Id Usuario logeado
    $useradmon=$_SESSION['administrador']; // valor: true - si es superadministrador
    
    // Permisos para Solicitudes de Gasto (SG)
        $IdRecurso=50; // Recurso #50 - Solicitudes de Gasto
        $Permisos_Consultar_SG=colocar_permiso($IdUsuariologin,1,$IdRecurso); //Permisos para Consultar/Ver Registro
        $Permisos_Editar_SG=colocar_permiso($IdUsuariologin,2,$IdRecurso); //Permisos para Editar o modificar Registro
        $Permisos_Insertar_SG=colocar_permiso($IdUsuariologin,3,$IdRecurso); //Permiso para Insertar o Crear Registro
        $Permisos_Listar_SG=colocar_permiso($IdUsuariologin,5,$IdRecurso); //Permiso para Listar en menu ppal o de ingreso a registro

    // Permisos para Aprobacion de Gastos (APG)
        $IdRecurso=44; //Recurso #44 - Aprobacion de gastos
        $Permisos_Consultar_APG=colocar_permiso($IdUsuariologin,1,$IdRecurso); //Permisos para Consultar/Ver Registro
        $Permisos_Editar_APG=colocar_permiso($IdUsuariologin,2,$IdRecurso); //Permisos para Editar o modificar Registro
        $Permisos_Insertar_APG=colocar_permiso($IdUsuariologin,3,$IdRecurso); //Permiso para Insertar o Crear Registro
        $Permisos_Listar_APG=colocar_permiso($IdUsuariologin,5,$IdRecurso); //Permiso para Listar en menu ppal o de ingreso a registro
    
    // Permisos para Anticipos (ATG)
        $IdRecurso=39; //Recurso #39 - Anticipos de gastos
        $Permisos_Consultar_ATG=colocar_permiso($IdUsuariologin,1,$IdRecurso); //Permisos para Consultar/Ver Registro
        $Permisos_Editar_ATG=colocar_permiso($IdUsuariologin,2,$IdRecurso); //Permisos para Editar o modificar Registro
        $Permisos_Insertar_ATG=colocar_permiso($IdUsuariologin,3,$IdRecurso); //Permiso para Insertar o Crear Registro
        $Permisos_Listar_ATG=colocar_permiso($IdUsuariologin,5,$IdRecurso); //Permiso para Listar en menu ppal o de ingreso a registro
    
    // Permisos para Legalizacion de Gastos (LG)
        $IdRecurso=46; //Recurso #46 - Legalizacion de gastos
        $Permisos_Consultar_LG=colocar_permiso($IdUsuariologin,1,$IdRecurso); //Permisos para Consultar/Ver Registro
        $Permisos_Editar_LG=colocar_permiso($IdUsuariologin,2,$IdRecurso); //Permisos para Editar o modificar Registro
        $Permisos_Insertar_LG=colocar_permiso($IdUsuariologin,3,$IdRecurso); //Permiso para Insertar o Crear Registro
        $Permisos_Listar_LG=colocar_permiso($IdUsuariologin,5,$IdRecurso); //Permiso para Listar en menu ppal o de ingreso a registro


	if($_SESSION['autentic']){
        $IdUser=$_SESSION['id'];
        
            require_once("../conn_BD.php");
            require_once("../departamentos/class/ClassDepartamentos.php");
            require_once("../plan_cuentas/class/ClassPlanCuentas.php");
            require_once("../tipo_gastos/class/ClassTipodeGasto.php");
            require_once("../conceptos_gastos/class/classConceptoGastos.php");
            require_once("class/ClassSolicitudGastos.php");
            require_once("../empleados/class/classEmpleados.php");
            require_once("../proyectos/class/classProyecto.php");
            require_once("../municipios/class/ClassMunicipios.php");
            require_once("../veredas/class/classVeredas.php");
            require_once("../procesos/class/ClassProcesos.php");
            require_once("../entidades/class/classEntidades.php");
            require_once("../actividades/class/ClassActividades.php");
            require_once("../funciones.php");
            $InstanciaDB=new Conexion();
            $InstDepartamento=new Proceso_Departamento($InstanciaDB);
            $InstConceptoGastos=new Proceso_ConceptoGastos($InstanciaDB);
            $InstSolicitudGasto=new Proceso_SolicitudGastos($InstanciaDB);
            $InstEmpleados=new Proceso_Empleados($InstanciaDB);
            $InstProyecto=new Proceso_Proyecto($InstanciaDB);
            $InstMunicipio=new Proceso_Municipios($InstanciaDB);
            $InstProcesos=new Proceso_Procesos($InstanciaDB);
            $InstEntidades=new Proceso_Entidades($InstanciaDB);
            $InstActividades=new Proceso_Actividad($InstanciaDB);
            $ListaDepartamentos=$InstDepartamento->ListaDepartamento();
            $listaConceptoGasto=$InstConceptoGastos->ListarConceptoGastos();
            $listaEmpleados=$InstEmpleados->ListarEmpleados();
            $listaProyecto=$InstProyecto->ListarProyecto();
            $listaMunicipios=$InstMunicipio->ListaMunicipio();
            $listaProcesos=$InstProcesos->ListaProcesos();
            $listaEntidades=$InstEntidades->ListarEntidades();
            $listaActividades=$InstActividades->ListaActividad();
            
            $ListaSGxAprobar=$InstSolicitudGasto->ListarSolicitudGastosxEstado(0); // SG Creadas, pendientes por aprobacion
            $ListaSGxAnticipo=$InstSolicitudGasto->ListarSolicitudGastosxEstado(1); // SG Creadas,Aprobadas, pendientes por Anticipo
            $ListaSGxLegalizar=$InstSolicitudGasto->ListarSolicitudGastosxEstado(2); //SG Creadas,Aprobadas, Con Anticipo, pendientes por Legalizar
            $ListaSGxCumplidas=$InstSolicitudGasto->ListarSolicitudGastosxEstado(3); // SG Creadas,Aprobadas, Anticipo, Legalizadas pendietes por dar por cumplidas (con o sin saldo)
            $ListaSGCerradas=$InstSolicitudGasto->ListarSolicitudGastosxEstado(4); // SG Ya cumplidas, estado cerrado !!!
            $ListaRelacionSGconsaldo=$InstSolicitudGasto->RelacionSGSaldo();
    
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- <head> -->
<?php include_once('../headScript.php'); ?>

<!-- </head> -->

<body>
    <?php 
		include_once('../headWeb.php');
		include_once("../../menu/m_principal.php");
	?>
    <div id="wrapper">
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="panel-body" align="right">
                    <?php if( $Permisos_Insertar_SG==1 || $useradmon ){ //Permiso para Insertar o Crear SG?>
                        <button type="button" class="btn btn-success btn-circle" data-toggle="modal" data-target="#NuevaSolicitudGasto" onclick="SolicitudNueva();">
                            <i class="fa fa-plus fa-2x"></i>
                        </button>
                        <div id="fechadatetimepicker" style="display:none;"><?php echo $fechadatetimepicker; ?></div>
                        <div id="UsuarioCreacion" style="display:none;"><?php echo $IdUser; ?></div>
                        <!-- <div id="UsuarioCreacion2" style="display:none;"><?php echo $IdUser; ?></div> -->
                    <?php } ?>
                </div>
                <!-- Lista de Solicitudes creadas y pendientes por Aprobar -->
                    <?php if( $Permisos_Consultar_SG==1 || $useradmon ){ // Permisos para Consultar/Ver Registro ?>
                        <div class="panel panel-primary">
                            <div class="panel-heading" style="height:55px;">
                                Solicitudes de Gastos -<b>pendientes por Aprobar</b>-
                                <div style="float:right;"><button type="button" class="btn btn-primary"><span class="badge">
                                    <?php echo $ListaSGxAprobar->num_rows;?></span></button>
                                </div>
                            </div>
                            <div class="panel-body">
                                <?php 
                                    if ($ListaSGxAprobar->num_rows > 0) { ?>
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                                <thead>
                                                    <th><input type="checkbox" id="AprobarSG_Todas" name="AprobarSG_Todas"></th>
                                                    <th>#</th>
                                                    <th>Fecha Solicitud</th>
                                                    <th>Municipio</th>
                                                    <th>Valor Solicitado</th>
                                                    <th>Usuario</th>
                                                    <th><span class='glyphicon glyphicon-cog' title='Config'></span></th>
                                                </thead>
                                                <tbody>
                                                <form id="formid" action="#" method="post">
                                                    <?php
                                                    while($row=$ListaSGxAprobar->fetch_array()){
                                                        $datos=json_encode($row);                                                                          
                                                        
                                                        $ListaResponsables=$InstSolicitudGasto->ListaResponsablesxIDsolicitud($row[0]);
                                                        $datosResponsables='';
                                                        while ($rowR=$ListaResponsables->fetch_array()) {
                                                            $datosResponsables=$datosResponsables."||".$rowR[2];
                                                        }
                                                ?>
                                                    <tr class="odd gradeX">
                                                        <td><input type="checkbox" id="AprobarSG_<?php echo $row[0];?>" name="AprobarSG"  value="<?php echo $row[0];?>"></td>
                                                        <td><?php echo $row[0]; ?></td>
                                                        <td><?php echo $row[1]; ?></td>
                                                        <td><?php echo $row[13]; ?> <input type="text" class="form-control" name="MunicipioSG" id="MunicipioSG<?php echo $row[0];?>" style="display:none;" value="<?php echo $row[13]; ?>"></td>
                                                        <td><?php echo '$'.number_format($row[9]); ?><input type="text" class="form-control" name="ValorSG" id="ValorSG<?php echo $row[0];?>" style="display:none;" value="<?php echo $row[9]; ?>"></td>
                                                        <td><?php echo $row[24]; ?><input type="text" class="form-control" name="UsuarioSG" id="UsuarioSG<?php echo $row[0];?>" style="display:none;" value="<?php echo $row[24]; ?>"></td>
                                                        <td>
                                                        
                                                            <?php if($Permisos_Consultar_SG==1 || $useradmon){ //Permisos para Consultar/Ver Registro ?> 
                                                                <button title="Ver" onclick="VerSolicitudGasto('<?php echo $row[0];?>','<?php echo $datosResponsables;?>')"
                                                                class="btn btn-default btn-sm" data-toggle="modal" data-target="#VerSolicitudGasto"><span
                                                                    class="glyphicon glyphicon-info-sign" style="color:blue;"></span></button>
                                                            <?php } ?>

                                                            <?php if($Permisos_Editar_SG==1 || $useradmon){ //Permisos para Editar o modificar Registro ?>
                                                                <button title="Editar" onclick='formeditSolicitudGasto(<?php echo  $datos;?>)'
                                                                class="btn btn-default btn-sm" data-toggle="modal" data-target="#NuevaSolicitudGasto"><span
                                                                    class="glyphicon glyphicon-pencil"></span></button>
                                                            <?php } ?>
                                                            <?php if($Permisos_Insertar_APG==1 || $useradmon){ //Permisos para Insertar o Crear Aprobacion de Gastos ?>
                                                                <button title="Aprobar Gasto" onclick="AprobarSolicitudGastodesdeSolicitudes('<?php echo  $row[0];?>')" class="btn btn-default btn-sm" data-toggle="modal" data-target="#NuevaAprobarSolicitudGasto">                                                      
                                                                <span class="glyphicon glyphicon-arrow-right" style="color:green;"></span><span style="color:green;"> $</span></button>
                                                            <?php } ?>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                    </form>
                                                </tbody>
                                            </table>
                                        </div>
                                            <?php if($Permisos_Insertar_APG==1 || $useradmon){ //Permisos para Insertar o Crear Aprobacion de Gastos ?>
                                                <div id="BotonMultiSeleccion"><button type="button" id="AprobacionMultiple" class="btn btn-primary">Aprobación Múltiple</button></div>
                                            <?php } ?>
                                        <?php } else { ?>
                                        <div class="alert alert-dismissible alert-info">
                                            <strong>Buen trabajo!</strong> <b class="alert-link"> - No hay </b> Solicitudes de Gatos pendientes por Aprobar.
                                        </div>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>                        
                <!-- Fin Lista de Solicitudes creadas y pendientes por Aprobar -->
                    <hr>
                <!-- INICIO Lista de Solicitudes Aprobadas y Pendientes por Anticipos -->
                    <?php if( $Permisos_Consultar_SG==1 || $useradmon ){ // Permisos para Consultar/Ver Registro ?>
                            <div class="panel panel-primary">
                                <div class="panel-heading" style="height:55px;">
                                    Solicitudes de Gastos <b>X Generarle Anticipo</b>
                                    <div style="float:right;"><button type="button" class="btn btn-primary"><span class="badge">
                                                <?php echo $ListaSGxAnticipo->num_rows;?></span></button></div>
                                </div>
                                <div class="panel-body">
                                <?php 
                                    if ($ListaSGxAnticipo->num_rows > 0) { ?>
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                            <thead>
                                                <th>#</th>
                                                <th>Fecha Solicitud</th>
                                                <th>Municipio</th>
                                                <th>Valor Aprobado</th>
                                                <th>Usuario</th>
                                                <th><span class='glyphicon glyphicon-cog' title='Config'></span></th>
                                            </thead>
                                            <tbody>
                                                <?php
                                                
                                                    while($row=$ListaSGxAnticipo->fetch_array()){
                                                    
                                                        $ListaResponsables=$InstSolicitudGasto->ListaResponsablesxIDsolicitud($row[0]);
                                                        $datosResponsables='';
                                                        while ($rowR=$ListaResponsables->fetch_array()) {
                                                            $datosResponsables=$datosResponsables."||".$rowR[2];
                                                        }
                                                ?>
                                                <tr class="odd gradeX">
                                                    <td><?php echo $row[0]; ?></td>
                                                    <td><?php echo $row[1]; ?></td>
                                                    <td><?php echo $row[13]; ?></td>

                                                    <td><?php echo '$'.number_format($row[18]); ?></td>
                                                    <td><?php echo $row[24]; ?></td>
                                                    <td>
                                                        <?php if($Permisos_Consultar_SG==1 || $useradmon){ //Permisos para Consultar/Ver Registro ?> 
                                                            <button title="Ver" onclick="VerSolicitudGasto('<?php echo $row[0];?>','<?php echo $datosResponsables;?>')"
                                                            class="btn btn-default btn-sm" data-toggle="modal" data-target="#VerSolicitudGasto"><span
                                                                class="glyphicon glyphicon-info-sign" style="color:blue;"></span></button>
                                                        <?php } ?>
                                                        
                                                        <?php if($Permisos_Insertar_ATG==1 || $useradmon){ //Permisos para Consultar/Ver Registro ?> 
                                                            <button type="button" data-toggle="modal" data-target="#ModalNuevoAnticipo" class="btn btn-default btn-sm" onclick="AnticipodesdeIdSG(<?php echo $row[0];?>,<?php echo $row[18];?>)">
                                                            <span class="glyphicon glyphicon-arrow-right" style="color:rgb(255, 128, 0);"></span>
                                                            <span class="glyphicon glyphicon-new-window" style="color:rgb(255, 128, 0);"></span></button>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <?php } else { ?>
                                        <div class="alert alert-dismissible alert-info">
                                            <strong>Buen trabajo!</strong> <b class="alert-link"> - No hay </b> Solicitudes de Gatos pendientes para Anticipos.
                                        </div>
                                <?php } ?>
                                </div>
                            </div>
                    <?php } ?>
                <!-- FIN Lista de Solicitudes Aprobadas y Pendientes por Anticipos -->
                    <hr>
                <!-- Inicio Lista de Solicitudes con Anticipo y pendientes por Legalizar -->
                    <?php if( $Permisos_Consultar_SG==1 || $useradmon ){ // Permisos para Consultar/Ver Registro ?>                    
                        <div class="panel panel-primary">
                            <div class="panel-heading" style="height:55px;">
                                Solicitudes de Gastos <b>X Legalizar</b>
                                <div style="float:right;"><button type="button" class="btn btn-primary"><span class="badge">
                                    <?php echo $ListaSGxLegalizar->num_rows;?></span></button></div>
                            </div>
                            <div class="panel-body">
                                <?php  if ($ListaSGxLegalizar->num_rows > 0) { ?>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                        <thead>
                                            <th>#</th>
                                            <th>Fecha Solicitud</th>
                                            <th>Municipio</th>
                                            <th>Valor x Legalizar</th>
                                            <th>Usuario</th>
                                            <th><span class='glyphicon glyphicon-cog' title='Config'></span></th>
                                        </thead>
                                        <tbody>
                                            <?php
                                            
                                            while($row=$ListaSGxLegalizar->fetch_array()){
                                            
                                                $ListaResponsables=$InstSolicitudGasto->ListaResponsablesxIDsolicitud($row[0]);
                                                $datosResponsables='';
                                                while ($rowR=$ListaResponsables->fetch_array()) {
                                                    $datosResponsables=$datosResponsables."||".$rowR[2];
                                                }
                                        ?>
                                            <tr class="odd gradeX">
                                                <td><?php echo $row[0]; ?></td>
                                                <td><?php echo $row[1]; ?></td>
                                                <td><?php echo $row[13]; ?></td>

                                                <td><?php echo '$'.number_format($row[18]); ?></td>
                                                <td><?php echo $row[24]; ?></td>
                                                <td>
                                                    <?php if($Permisos_Consultar_SG==1 || $useradmon){ //Permisos para Consultar/Ver Registro ?> 
                                                        <button title="Ver" onclick="VerSolicitudGasto('<?php echo $row[0];?>','<?php echo $datosResponsables;?>')"
                                                            class="btn btn-default btn-sm" data-toggle="modal" data-target="#VerSolicitudGasto"><span
                                                                class="glyphicon glyphicon-info-sign" style="color:blue;"></span></button>
                                                    <?php } ?>   
                                                        <!-- <button title="Editar" onclick=""
                                                            class="btn btn-default btn-sm" data-toggle="modal" data-target="#modaleditMcpio"><span
                                                                class="glyphicon glyphicon-pencil"></span></button> -->
                                                    <?php if($Permisos_Insertar_LG==1 || $useradmon){ //Permisos para Crear/Insertar legalizacion de gastos ?>
                                                                <a href="../legalizaciongastos/legalizardetallegasto.php?IdSG=<?php echo $row[0];?>&IdLG=0" class="btn btn-default btn-sm">
                                                        <span class="glyphicon glyphicon-arrow-right" style="color:rgb(255, 128, 0);"></span>
                                                            <span class="glyphicon glyphicon-new-window" style="color:rgb(255, 128, 0);"></span></button>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <?php } else { ?>
                                        <div class="alert alert-dismissible alert-info">
                                            <strong>Buen trabajo!</strong> <b class="alert-link"> - No hay </b> Solicitudes de Gatos pendientes pendientes por Legalizar.
                                        </div>
                                    <?php } ?>
                            </div>
                        </div>
                    <?php } ?> 
                <!-- fin Lista de Solicitudes pendientes por Legalizar -->
                    <hr>
                <!-- Inicio  Lista de Solicitudes con Saldo -->
                    <?php if( $Permisos_Consultar_SG==1 || $useradmon ){ // Permisos para Consultar/Ver Registro ?>     
                        <div class="panel panel-primary">
                            <div class="panel-heading" style="height:55px;">
                                Solicitudes de Gastos <b>con Saldo Pendiente en legalización</b>
                                <div style="float:right;"><button type="button" class="btn btn-primary"><span class="badge">
                                    <?php echo $ListaRelacionSGconsaldo->num_rows;?></span></button>
                                </div>
                            </div>
                            <div class="panel-body">
                            <?php  if ($ListaRelacionSGconsaldo->num_rows > 0) { ?>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="dataTables-ConSaldo">
                                        <thead>
                                            <th>#</th>
                                            <th>Fecha Solicitud</th>
                                            <th>Municipio</th>
                                            <th>Valor Solicitud</th>
                                            <th>Valor Aprobado (A)</th>
                                            <th>Valor Legalizado (L)</th>
                                            <th>Saldo A-L</th>
                                            <th>Usuario</th>
                                            <th><span class='glyphicon glyphicon-cog' title='Config'></span></th>
                                        </thead>
                                        <tbody>
                                            <?php
                                                while($rowSaldo=$ListaRelacionSGconsaldo->fetch_array()){
                                                    // $datos=$row[0]."||".$row[1]."||".$row[2]."||".$row[3]."||".$row[4]."||".$row[5]."||".$row[6]."||".$row[7]."||".$row[8]."||".$row[9]."||".$row[10]."||".$row[11]."||".$row[12];

                                                    $ListaResponsables=$InstSolicitudGasto->ListaResponsablesxIDsolicitud($rowSaldo[0]);
                                                    $datosResponsables='';
                                                    while ($rowR=$ListaResponsables->fetch_array()) {
                                                        $datosResponsables=$datosResponsables."||".$rowR[2];
                                                    }
                                            ?>
                                            <tr class="odd gradeX">
                                                <td><?php echo $rowSaldo[0]; ?></td>
                                                <td><?php echo $rowSaldo[1]; ?></td>
                                                <td><?php echo $rowSaldo[13]; ?></td>
                                                <td><?php echo '$'.number_format($rowSaldo[9]); ?></td>
                                                <td><?php echo '$'.number_format($rowSaldo[15]); ?></td>
                                                <td><?php echo '$'.number_format($rowSaldo[14]); ?></td>
                                                
                                                    <?php 
                                                        $saldo=($rowSaldo[15]-$rowSaldo[14]);
                                                        if ($saldo < 0) {
                                                            echo '<td style="color:red;">';
                                                        } else {
                                                            echo '<td style="color:black;">';
                                                        }
                                                        echo '$'.number_format($saldo);
                                                    ?>
                                                </td>
                                                <td><?php echo $rowSaldo[17]; ?></td>
                                                <td>
                                                    <?php if($Permisos_Consultar_SG==1 || $useradmon){ //Permisos para Consultar/Ver Registro ?> 
                                                        <button title="Ver" onclick="VerSolicitudGasto('<?php echo $rowSaldo[0];?>','<?php echo $datosResponsables;?>')"
                                                            class="btn btn-default btn-sm" data-toggle="modal" data-target="#VerSolicitudGasto"><span
                                                                class="glyphicon glyphicon-info-sign" style="color:blue;"></span></button>
                                                        <!-- <button title="Editar" onclick="SolicitudGasto('<?php echo $datos;?>')"
                                                            class="btn btn-default btn-sm" data-toggle="modal" data-target="#modaleditMcpio"><span
                                                                class="glyphicon glyphicon-pencil"></span></button> -->
                                                    <?php } ?> 

                                                    <?php if($Permisos_Insertar_LG==1 || $useradmon){ //Permisos para Crear/Insertar Legalizacion de gastos ?> 
                                                        <a href="../legalizaciongastos/legalizardetallegasto.php?IdSG=<?php echo $rowSaldo[0];?>&IdLG=<?php echo $rowSaldo[16];?>" title="Completar Legalizacion"
                                                            class="btn btn-default btn-sm"><span class="glyphicon glyphicon-arrow-right" style="color:rgb(255, 128, 0);"></span>
                                                            <span class="glyphicon glyphicon-new-window" style="color:rgb(255, 128, 0);"></span></a>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <?php } else { ?>
                                        <div class="alert alert-dismissible alert-info">
                                            <strong>Buen trabajo!</strong> <b class="alert-link"> - No hay </b> Solicitudes de Gatos con algun saldo pendiente.
                                        </div>
                                    <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
                <!-- Fin Lista de Solicitudes con Saldo --> 
            </div>
        </div>
    </div>

    <!-- Inicio Modal Nueva Solicitud de gastos -->
        <div class="modal fade" id="NuevaSolicitudGasto" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow:hidden;overflow-y:auto;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">

                    <div class="modal-header" id="Titulo_modal"> </div>
                    <div id="msgInstitucionNuevo"></div>
                    <div class="panel-body">
                        <div class="row col-sm-5">
                            <div class="form-group">
                                <div class="col-lg-12" id="Mostrar_dpo_editSG" style="padding-left: 0px;">
                                    <div class="col-lg-6 text-left" style="padding-left: 0px;"><label>Departamento: </label></div>
                                    <div class="col-lg-6 text-left" id="Nombre_dpto_editSG"></div>
                                </div>
                                <div id="Mostrar_dpo_InsertSG">
                                    <select class="js-example-basic-single" name="CodDepartamentoSG" id="CodDepartamentoSG" style="width:280px">
                                        <option value="0"> -- Seleccione un Departamento -- </option>
                                        <?php
                                            mysqli_data_seek($ListaDepartamentos,0);																		
                                            while ($rowMun=$ListaDepartamentos->fetch_array(MYSQLI_BOTH)) { 
                                                echo "<option value='".$rowMun[0]."'>".$rowMun[2]."</option>";
                                                }
                                                ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-12" id="Mostrar_mcpo_editSG" style="padding-left: 0px;">
                                    <input type="hidden" name="" id="input_mcpo_editSG">
                                    <div class="col-lg-6 text-left" style="padding-left: 0px;"><label for="">Municipio: </label></div>
                                    <div class="col-lg-6 text-left" id="Nombre_mcpo_editSG"></div>
                                </div>
                                <div id="Mostrar_mcpo_insertSG">
                                    <select class="js-example-basic-single" name="CodMunicipioSG" id="CodMunicipioSG" style="width:280px">
                                    </select>
                                </div>
                                
                            </div>
                            <div class="form-group">
                                
                            </div>
                            <div class="form-group">
                                
                                <select class="form-control" name="CodProyectoSG" id="CodProyectoSG" style="width:280px">
                                    <option value="00"> -- Seleccione un Proyecto -- </option>
                                    <?php
                                        mysqli_data_seek($listaProyecto, 0);																		
                                        while ($rowProy=$listaProyecto->fetch_array(MYSQLI_BOTH)) { 
                                            echo "<option value='".$rowProy[0]."'>".$rowProy[1]."</option>";
                                            }
                                            ?>
                                </select>
                            </div>
                            <div class="form-group">
                                
                                <select class="form-control" name="CodProcesoSG" id="CodProcesoSG" style="width:280px">
                                    <option value="00"> -- Seleccione un Proceso -- </option>
                                    <?php
                                        mysqli_data_seek($listaProcesos, 0);																			
                                        while ($rowProc=$listaProcesos->fetch_array(MYSQLI_BOTH)) { 
                                            echo "<option value='".$rowProc[0]."'>".$rowProc[1]."</option>";
                                            }
                                            ?>
                                </select>
                            </div>
                            <div class="form-group">
                                                             
                                <select class="js-example-basic-single" name="CodActividadSG" id="CodActividadSG" style="width:280px">
                                    <option value="00"> -- Seleccione una Actividad --</option>
                                    <?php
                                        mysqli_data_seek($listaActividades, 0);																			
                                        while ($rowAct=$listaActividades->fetch_array(MYSQLI_BOTH)) { 
                                            echo "<option value='".$rowAct[0]."'>".$rowAct[1]."</option>";
                                            }
                                            ?>
                                </select>
                            </div>                                               
                        </div>
                        <div class="row col-sm-2"></div>
                        <div class="row col-sm-5">
                            
                            <div class="form-group">
                                
                                <select class="form-control" name="CodEntidadSG" id="CodEntidadSG" multiple="multiple" style="width:280px">
                                    <?php
                                        mysqli_data_seek($listaEntidades,0);																		
                                        while ($rowEnt=$listaEntidades->fetch_array(MYSQLI_BOTH)) { 
                                            echo "<option value='".$rowEnt[0]."'>".$rowEnt[2]."</option>";
                                            }
                                            ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <div class='input-group date datetime' id='datetimepicker1' style="width:280px">
                                    <input id="FechaHoraSalidaSG" size="16" type="text" class="form-control" autocomplete="off" 
                                          placeholder="Fecha salida">
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                </div>
                                <?php
                                    $TipoCalendario='Fecha';
                                    include('../../Insertfecha.php');
                                ?>
                            </div>
                            <div class="form-group">
                                <div class='input-group date datetime' id='datetimepicker12' style="width:280px" >
                                    <input id="FechaHoraRegresoSG" size="16" type="text" class="form-control" autocomplete="off" 
                                         placeholder="Fecha regreso">
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                </div>
                                <?php
                                    $TipoCalendario='Fecha';
                                    include('../../Insertfecha.php');
                                ?>
                            </div>
                            <div class="form-group">
                                <select class="js-example-basic-multiple" name="responsableSG" id="responsableSG" multiple="multiple" style="width:280px">
                                    <?php
                                        mysqli_data_seek($listaEmpleados, 0);
                                        while ($rowEM=$listaEmpleados->fetch_array(MYSQLI_BOTH)) { 
                                            echo "<option value='".$rowEM[0]."'>".$rowEM[2]."</option>";
                                        }
                                    ?>
                                </select>
                            </div>                                                     
                        </div>
                        <div class="col-lg-10">
                            <div class="form-group">
                            <label for="">Observaciones Solicitud Gasto - (Maximo 100 caracteres)</label>
                            <textarea class="form-control" name="ObservacionesSG" id="ObservacionesSG" rows="2" cols="50" maxlength="100" style="resize: none;"></textarea>
                            </div>
                        </div>


                        <!-- Row start -->
                        <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading clearfix">
                                                <div class="col-sm-4">
                                                    <h3 class="panel-title">Insertar Concepto de Gasto</h3>
                                                </div>
                                                <div class="col-sm-8">
                                                    <div class="col-md-2" style="border:0px; padding:0px;">Vereda: </div>
                                                    <div class="col-lg-9"><select class="form-control" name="CodVeredaSG" id="CodVeredaSG" style="width:280px;"></select></div>
                                                </div>
                                            </div>
                                            
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-4 col-sm-4">
                                                            <div class="input-group">
                                                                <select  class="form-control" name="ConceptoGastoSG" id="ConceptoGastoSG"  style="width:280px;">
                                                                    <option value=0> --- Seleccione un concepto de Gasto --- </option>
                                                                    
                                                                        <?php 
                                                                            while ($rowCG=$listaConceptoGasto->fetch_array()) {
                                                                                echo "<option value='".$rowCG[0]."'>".$rowCG[2]."</option>";
                                                                            }                
                                                                       ?>

                                                                </select>
                                                            </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                        <div class="input-group" lang="en-EN">
                                                            <input class="form-control input-number NumeroDias" type="number" step="0.1" id="NumDiasSG" placeholder="Cantidad">
                                                            <span class="input-group-addon">00.00</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 col-sm-4">
                                                        <div class="input-group">
                                                            <span class="input-group-addon">$</span>
                                                            <input  class="form-control input-number ValorGasto" type="number" id="ValorConceptoSG" min="1" max="1500000" placeholder="Valor unidad">
                                                            <span class="input-group-addon">00</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1 col-sm-1">
                                                        <div class="input-group" id="BotonInsertarDetalle">
                                                            <button type="button" id="AgrgarGastoTMP" onclick="InsertarDetalleTMP();"><span class="glyphicon glyphicon-arrow-down"></span></button> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <!-- Row end -->
                        
                                
                         <div class="row" id="detall_modal"> </div>
                    </div>
                    <div class="modal-footer" id="Conjunto_botone_Footmodal">                       
                    </div>
                </div>
            </div>
        </div>
    <!-- Final Modal Nuevo Solicitud de gastos -->

   

    <!-- Inicio modal visualizacion Solicitud Gasto -->

        <div class="modal fade" id="VerSolicitudGasto" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title" id="myModalLabel">Solicitud de Gastos - # <label id="VerIdSolicitudGastoSG"></Label></h3>
                        Fecha Elaboracion: <label align="right" id="VerFechaSolicitudGastoSG"></label> 
                        <div id="msgEstado"></div>
                    </div>

                    <div class="panel-body center-block">
                        <div id="Cab_IdSG"><?php include('CabSG.php');?></div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-12">
                                <div id="ListaDetalleSG"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div id="VerAprobarizacionSG"></div>
                              
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <div style="width:70%; float:left;" id="msgBotonAccionSG" class="btn-group" role="group"></div>
                        <div style="width:30%; float:right;" class="btn-group" role="group">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!-- fin modal visualizacion Solicitud Gasto -->

    <!-- Inicio Modal de Aprobarizacion de Gastos -->
        <?php
            include('../aprobacionSolicGasto/modalnuevaaprobacion.php'); 
        ?>
    <!-- Fin Modal de leglizacion de Gastos -->
    
    <!-- Inicio Modal para Aprobacion Multiple -->
        <div class="modal fade" id="ModalAprobacionMultiple" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="modal-title">Aprobacion Multiple Solicitudes de Gasto</h3>
                    </div>
                    <div class="modal-body">
                        
                        <table class="table table-striped table-inverse table-responsive">
                            <thead class="thead-inverse">
                                <tr>
                                    <th>#</th>
                                    <th>Municipio</th>
                                    <th>Usuario</th>
                                    <th class="text-right">Valor</th>
                                </tr>
                                </thead>
                                <tbody id="TbodyAprobacionMasiva"></tbody>
                        </table>

                        <div class="row form-group">
                            <div class="col-md-5"><label>Empleado que aprueba</label></div>
                            <div class="col-md-6">
                                <select class="js-example-basic-single form-control" name="responsableSGAprobacionMultiple" id="responsableSGAprobacionMultiple" style="width:250px">
                                    <option value='0'>-- Seleccione Empleado --</option>
                                    <?php
                                        mysqli_data_seek($listaEmpleados, 0);
                                        while ($rowEM=$listaEmpleados->fetch_array(MYSQLI_BOTH)) { 
                                            echo "<option value='".$rowEM[0]."'>".$rowEM[2]."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                        <label for="">Observaciones</label>
                            <textarea class="form-control" name="ObservacionSGMultiple" id="ObservacionSGMultiple" rows="3"  style="resize: none;" placeholder="Se aprueba valor total de la solicitud de gastos"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer" id="modal-footerSG">
                        
                    </div>
                </div>
            </div>
        </div>
    <!-- Fin Modal para Aprobacion Multiple -->  

    <!-- INICIO Modal muestra Error al ingresar Datos ConceptoSG -->
            <div class="modal fade" id="ErrorDatosConceptoSG" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="container-fluid alert alert-dismissible alert-warning">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
                                <b>Error</b> - Datos de detalle no pueden estar vacios o deben tener el formato solicitado.  Por favor Verifique.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    <!-- FIN Modal muestra Error al ingresar Datos ConceptoSG -->

    <div id="MsgModalSG"></div>

<?php include_once("../anticipos/modalinsanticipo.php"); ?>


    <script src="../../assets/js/jquery-1.10.2.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script src="../../assets/js/jquery.metisMenu.js"></script>
    <script src="../../assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="../../assets/js/dataTables/dataTables.bootstrap.js"></script>
    <script src="../../assets/js/jasny-bootstrap.min.js"></script>

    <style>
        table {
            width: 70%;
            margin: 0 auto;
        }

        .modal-body {
            max-height: calc(100vh - 210px);
            overflow-y: auto;
        }
    </style>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

    <script>
        $(document).ready(function() {

                        $('#Mostrar_dpo_editSG').hide();
                        $('#Mostrar_mcpo_editSG').hide();

                        $('#Mostrar_dpo_InsertSG').show();
                        $('#Mostrar_mcpo_insertSG').show();

                        $('.input-number').on('input', function () { 
                            this.value = this.value.replace(/[^0-9]/g,'');
                        });

                        $('#dataTables-example').dataTable({
                            "language": {
                                "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Spanish.json"
                            }
                        } );

                        $('#dataTables-ConSaldo').dataTable({
                            "columns": [
                                { "width": "2%" },
                                { "width": "9%" },
                                null,
                                null,
                                null,
                                null,
                                null,
                                { "width": "15%" },
                                { "width": "11%" }
                            ],
                            "language": {
                                "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Spanish.json"
                            }
                            });

                        $('#AprobacionMultiple').click(function(){
                            var SGSeleccionadas = [];
                            var SGValoresSeleccion=[]; 
                            var MunicipioSG=[];
                            var UsuarioSG=[];
                            
                            $(":checkbox[name=AprobarSG]").each(function(){
                                if (this.checked) {
                                    SGSeleccionadas.push($(this).val());
                                    SGValoresSeleccion.push($('#ValorSG'+$(this).val()).val());
                                    MunicipioSG.push($('#MunicipioSG'+$(this).val()).val());
                                    UsuarioSG.push($('#UsuarioSG'+$(this).val()).val());
                                }
                            });
                            if (SGSeleccionadas.length > 1) {                               
                                $("#ModalAprobacionMultiple").modal("show");
                                var texto=null;
                                var total=null;
                                for (let r = 0; r < SGSeleccionadas.length; r++) {
                                    texto+=
                                    `<tr>
                                        <td>` +SGSeleccionadas[r]+`</td>
                                        <td>` +MunicipioSG[r]+`</td>
                                        <td>`+UsuarioSG[r]+`</td>
                                        <td class="text-right">$ ` +formatNumber.new(SGValoresSeleccion[r])+`</td>
                                    </tr>`;
                                    total+=Number(SGValoresSeleccion[r]);
                                }
                                
                                texto+=`<tr><td colspan="3">Total a aprobar</td><td class="text-right"><b>$ `+ formatNumber.new(total)+`</b></td></tr>`;
                                $('#TbodyAprobacionMasiva').html(texto);
                                $('#modal-footerSG').html(
                                    '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>'+
                                    '<button type="button" class="btn btn-primary" onclick="AprobacionSGMultiple(['+SGSeleccionadas+'],['+SGValoresSeleccion+']);">Aprobar</button>'
                                );
                            }else{
                                alert('Debe seleccionar minimo dos (2) Solicitudes para aprobacion múltiple.');
                            }
                            return false;
                        });

                        $( '#AprobarSG_Todas' ).on( 'change', function() {
                            if( $(this).is(':checked') ){
                                //  si el checkbox ha sido seleccionado
                                $(":checkbox[name=AprobarSG]").prop('checked', true);
                            } else {
                                //  si el checkbox ha sido deseleccionado
                                $(":checkbox[name=AprobarSG]").prop('checked', false);
                            }
                        });

                        $('#responsableSG').select2({
                            placeholder:"Responsables",
                            allowClear: true
                        });         
                        
                        $('#CodEntidadSG').select2({
                            placeholder:"Entidades",
                            allowClear: true
                        });        

                        $('#VerentidadesSGdiv2').select2();
                        $('#VerresponsableSGdiv2').select2();

                        $('#CodVeredaSG').select2();   

                        $('#responsableSGAprobacionMultiple').select2({
                            dropdownParent: $("#ModalAprobacionMultiple"),
                        });

                        $('#responsableSG2').select2({
                            dropdownParent: $("#NuevaAprobarSolicitudGasto"),
                        });
                        
                        $('#VerentidadesSGdiv').select2({
                            dropdownParent: $("#VerSolicitudGasto"),
                        });        

                        $('#CodMunicipioSG').select2({
                            dropdownParent: $("#NuevaSolicitudGasto"),
                        });
                    
                        $('#IdSolicitudGastoSG2').select2({
                            dropdownParent: $("#NuevaAprobarSolicitudGasto"),
                        });

                        $('#CodActividadSG').select2({
                            dropdownParent: $("#NuevaSolicitudGasto"),
                        });          
                        
                        // $('#ConceptoGastoSG').select2({
                        //     dropdownParent: $("#NuevaSolicitudGasto"),
                        // });

                        $('#CodDepartamentoSG').select2({
                            dropdownParent: $("#NuevaSolicitudGasto"),
                        });

                        $('.NumeroDias').on("keypress keyup blur", function(event) {
                            $(this).val($(this).val().replace(/[^\d].+/, ""));
                            if ((event.which < 48 || event.which > 57)) {
                                if (event.which == 8) {

                                } else {
                                    event.preventDefault();
                                }
                            }
                        });

                        $(".ValorGasto").on("keypress keyup", function(event) {
                            $(this).val($(this).val().replace(/[^0-9\.]/g, ','));
                            if (($(this).val().indexOf(',') != -1) && (event.which < 48 || event.which > 57)) {
                                if (event.which == 8 || event.which == 44) {

                                } else {
                                    event.preventDefault();
                                }
                            }
                        });

                        $('#CodDepartamentoSG').change(function () { 
                            var CodDepartamentoSG=$('#CodDepartamentoSG').val();
                            if (CodDepartamentoSG != 0) {
                                var parametros={CodDepartamentoSG};
                                $.ajax({
                                    type: "POST",
                                    url: "../../logica/logica.php?accion=LlenaSelectMunicipioSegunDepartamento",
                                    data: parametros,
                                    cache: false,
                                    dataType: "json",
                                    success: function (data) {
                                        $('#CodMunicipioSG').html(data);
                                    }
                                });   
                            }  
                        });

                        $('#CodMunicipioSG').change(function () {
                            var CodMunicipioSelect=$('#CodMunicipioSG').val();
                            if (CodMunicipioSelect != 0) {
                                var parametros={CodMunicipioSelect};
                                $.ajax({
                                    type: "POST",
                                    url: "../../logica/logica.php?accion=LlenaSelectVeredasSegunMunicipio",
                                    data: parametros,
                                    dataType: "json",
                                    cache: false,
                                    success: function (response) {
                                        $('#CodVeredaSG').html(response).change();
                                    }
                                });
                                }  
                        });

                $('#responsableSGFM').select2({
                        placeholder:"Responsables2",
                        allowClear: true
                });

                $('#CodEntidadSGFM').select2({
                        placeholder:"Entidades2",
                        allowClear: true
                });

                $('#VerentidadesSGdiv2').select2({
                        dropdownParent: $("#NuevaAprobarSolicitudGasto"),
                });

                $('#VerresponsableSGdiv2').select2({
                        dropdownParent: $("#NuevaAprobarSolicitudGasto"),
                });
        });              
                    function ShowVerAprobarizacionSolicitudGasto() {
                        $("#VerAprobarizacionSG").show();
                    }

                    function HideVerAprobarizacionSolicitudGasto() {
                        $("#VerAprobarizacionSG").hide();
                    }

                    function mostrar_items() {
                        var UsuarioCreacion=$('#UsuarioCreacion').text();
                        var parametros = {UsuarioCreacion};
                        $.ajax({
                            url: '../../logica/logica.php?accion=ListarDetalleTMP',
                            type: "POST",
                            data: parametros,
                            beforeSend: function(objeto) {
                                $('#msgDetalleSolicitudGastoLista').html('Cargando...');
                            },
                            success: function(data) {
                                $("#msgDetalleSolicitudGastoLista").html(data).fadeIn('slow');
                            }
                        });
                    }

                    function InsertarDetalleTMP() {
                        var UsuarioCreacion=$('#UsuarioCreacion').text();
                        var ConceptoGastoSG = $('#ConceptoGastoSG').val();
                        var NumDiasSG = $('#NumDiasSG').val();
                        var ValorConceptoSG = $('#ValorConceptoSG').val();
                        var CodVereda=$('#CodVeredaSG').val();
                        if (NumDiasSG == "" || NumDiasSG === '0'|| CodVereda === '0' || CodVereda==null|| isNaN(NumDiasSG) || ValorConceptoSG=="" || isNaN(ValorConceptoSG)) {
                            var $miModal = $('#ErrorDatosConceptoSG');
                                $miModal.modal('show');
                                setTimeout(function() {
                                    $miModal.modal('toggle');
                                    $('.modal-backdrop').remove();
                                }, 4000);
                        } else {
                            var parametros = {ConceptoGastoSG,NumDiasSG,ValorConceptoSG,UsuarioCreacion,CodVereda};
                            $.ajax({
                                type: "POST",
                                url: '../../logica/logica.php?accion=InsertarDetalleTMP',
                                data: parametros,
                                success: function(data) {
                                    mostrar_items();
                                }
                            })
                        }
                    }

                    function eliminar_item(id_detalle_TMP) {
                        var parametros = {id_detalle_TMP};
                        $.ajax({
                            url: '../../logica/logica.php?accion=eliminar_item',
                            type: 'POST',
                            data: parametros,
                            success: function(data) {
                                mostrar_items();
                            }
                        });
                    }

                    function eliminar_item_DetalleSG(id_detalle_SG,IdSG){
                        var parametros = {id_detalle_SG,IdSG};
                        $.ajax({
                            url: '../../logica/logica.php?accion=eliminar_item_DetalleSG',
                            type: 'POST',
                            data: parametros,
                            success: function() {
                                mostrarDetalleSG(IdSG,'detall_modal');
                            }
                        });
                    }
                    
                    
                    function eliminar_Todos_TMP(UsuarioCreacion) {
                        var parametros={UsuarioCreacion}
                        $.ajax({
                            type: "POST",
                            url: '../../logica/logica.php?accion=eliminar_Todos_TMP',
                            data: parametros,
                            success: function (response) {
                                mostrar_items();
                            }
                        });
                        
                    }

                    function InsertarDetalle() {                         
                        var idSolicGastoFM= $('#idSolicitudgastoFM').text();           
                        var ConceptoGastoSGFM=$('#ConceptoGastoSG').val();
                        var NumDiasSG=$('#NumDiasSG').val();
                        var ValorConceptoSGFM=$('#ValorConceptoSG').val();
                        var IdVereda=$('#CodVeredaSG').val();
                                           
                        if (NumDiasSG == "" || NumDiasSG === '0'|| IdVereda === '0' || IdVereda == "" || IdVereda==null || isNaN(NumDiasSG) || ValorConceptoSGFM=="" || isNaN(ValorConceptoSGFM)) {
                            
                            var $miModal = $('#ErrorDatosConceptoSG');
                                $miModal.modal('show');
                                setTimeout(function() {
                                    $miModal.modal('toggle');
                                    $('.modal-backdrop').remove();
                                }, 4000);
                        } else {
                            var parametros = {idSolicGastoFM,ConceptoGastoSGFM,NumDiasSG,ValorConceptoSGFM,IdVereda};
                            $.ajax({
                                type: "POST",
                                url: '../../logica/logica.php?accion=llenarDetalleSGasto',                                
                                data: parametros,
                                success: function(data) { 
                                    mostrarDetalleSG(idSolicGastoFM,'detall_modal');    
                                }
                            })                         
                        }                                  
                    }
                    
                         

        function SolicitudNueva(){
            var UserLogin=$('#UsuarioCreacion').text();
            eliminar_Todos_TMP(UserLogin);
            $('#Mostrar_dpo_editSG').hide();
            $('#Mostrar_mcpo_editSG').hide();

            $('#Mostrar_dpo_InsertSG').show();
            $('#Mostrar_mcpo_insertSG').show();
            
            $('#CodDepartamentoSG').val(0);
            $('#CodMunicipioSG').val(null);
            $('#CodProyectoSG').val('00');
            $('#CodProcesoSG').val('00');
            $('#CodActividadSG').val(0);
            $('#CodEntidadSG').val(null).trigger("change");
            $('#CodEntidadSG').select2({
                placeholder: "Entidades"
            });
         
            $('#FechaHoraSalidaSG').val(null).trigger('change');
            $('#FechaHoraRegresoSG').val(null).trigger('change');

            $('#responsableSG').val(null).trigger("change");
            $('#responsableSG').select2({
                placeholder: "Responsables"
            });
            $('#ObservacionesSG').val(null);
            $('#CodVeredaSG').val(0);
            $('#ConceptoGastoSG').val(0);
            $('#NumDiasSG').val(null);
            $('#ValorConceptoSG').val(null);
            $('#Conjunto_botone_Footmodal').html(`           
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" onclick="guardarSolicitudGasto();" class="btn btn-primary">Guardar</button>`);
          
            $('#Titulo_modal').html(`<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 align="center" class="modal-title" id="myModalLabel">Nueva Solicitud de Gastos</h3>
                <div id="msgSolicitudGasto"></div>`);
                
                mostrar_items();                
            $('#detall_modal').html(`<div class="col-sm-12">
                <div id="msgDetalleSolicitudGastoLista"></div>`);

            $('#AgrgarGastoTMP').attr('onclick','InsertarDetalleTMP()');

            
        }       
  
    </script>

</body>
</html>
<?php 

        
	}else{
		header('Location:../../php_cerrar.php');
	}
?>