<?php 

Class Proceso_SolicitudGastos{

    function __construct($InstanciaDB){
        $this->ConnxClass=$InstanciaDB;
     }

    
     function ListarSolicitudGastos(){
        $sql="SELECT 
        solicitudgastos.idConsecutivoSolicitudGastos, 
        solicitudgastos.FechaSolicitud, 
        solicitudgastos.idProyectoSolicitudGastos, 
        solicitudgastos.idProcesoSolicitudGastos, 
        solicitudgastos.idActividadSolicitudGastos, 
        solicitudgastos.idMunicipioSolicitudGastos, 
        solicitudgastos.FechaSalidaSolicitudGastos, 
        solicitudgastos.FechaRegresoSolicitudGastos, 
        solicitudgastos.EstadoSolicitudGastos, 
        solicitudgastos.ValorTotalSolicitudGastos, 
        solicitudgastos.ObservacionesSolicitudGastos, 
        solicitudgastos.IdDepartamentoSolicitudGastos, 
        solicitudgastos.IdUsuarioCreacion,
        municipio.NombreMunicipio,
        departamento.NombreDepartamento,
        vereda.NombreVereda
        FROM solicitudgastos 
        INNER JOIN municipio ON municipio.idMunicipio=solicitudgastos.idMunicipioSolicitudGastos
        INNER JOIN departamento on departamento.idDepartamento=solicitudgastos.IdDepartamentoSolicitudGastos
        LEFT JOIN vereda ON vereda.idVereda=solicitudgastos.IdVeredaSolicitudGasto";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function ListarSolicitudGastosxEstado($EstadoSG){
        $sql="SELECT
                solicitudgastos.idConsecutivoSolicitudGastos, 
        solicitudgastos.FechaSolicitud, 
        solicitudgastos.idProyectoSolicitudGastos, 
        solicitudgastos.idProcesoSolicitudGastos, 
        solicitudgastos.idActividadSolicitudGastos, 
        solicitudgastos.idMunicipioSolicitudGastos, 
        solicitudgastos.FechaSalidaSolicitudGastos, 
        solicitudgastos.FechaRegresoSolicitudGastos, 
        solicitudgastos.EstadoSolicitudGastos, 
        solicitudgastos.ValorTotalSolicitudGastos, 
        solicitudgastos.ObservacionesSolicitudGastos, 
        solicitudgastos.IdDepartamentoSolicitudGastos, 
        solicitudgastos.IdUsuarioCreacion,
                municipio.NombreMunicipio,
                departamento.NombreDepartamento,
                proyecto.DescProyecto,
                actividad.DescripcionActividad,
                proceso.DescProceso,
                aprobacionsolictudgasto.ValorAprobacion,
                anticipos.IdAnticipos,
                anticipos.DocEquivalenteAnticipo,
                anticipos.ValorAnticipo,
                legalizaciongastos.idLegalizacionGastos,
                legalizaciongastos.TotalLegalizacionGastos,
                usuario.NombreUsuario
            FROM solicitudgastos 
                INNER JOIN municipio ON municipio.idMunicipio=solicitudgastos.idMunicipioSolicitudGastos
                LEFT JOIN aprobacionsolictudgasto on solicitudgastos.idConsecutivoSolicitudGastos=aprobacionsolictudgasto.idSolicitudGasto
                INNER JOIN departamento on departamento.idDepartamento=solicitudgastos.IdDepartamentoSolicitudGastos
                LEFT JOIN anticipos ON anticipos.IdSGAnticipo=solicitudgastos.idConsecutivoSolicitudGastos
                INNER JOIN proyecto ON proyecto.idProyecto=solicitudgastos.idProyectoSolicitudGastos
                INNER JOIN actividad ON actividad.idActividad=solicitudgastos.idActividadSolicitudGastos
                INNER JOIN proceso ON proceso.idProceso=solicitudgastos.idProcesoSolicitudGastos
                INNER JOIN usuario ON usuario.idUsuario=solicitudgastos.IdUsuarioCreacion
                LEFT JOIN legalizaciongastos ON legalizaciongastos.idSolicitudGasto=solicitudgastos.idConsecutivoSolicitudGastos
                WHERE EstadoSolicitudGastos=".$EstadoSG;
                // echo $sql;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function BuscarSolicitudGastosxid($idSolicitudGastos){
        $sql="SELECT
        solicitudgastos.idConsecutivoSolicitudGastos, 
        solicitudgastos.FechaSolicitud, 
        solicitudgastos.idProyectoSolicitudGastos, 
        solicitudgastos.idProcesoSolicitudGastos, 
        solicitudgastos.idActividadSolicitudGastos, 
        solicitudgastos.idMunicipioSolicitudGastos, 
        solicitudgastos.FechaSalidaSolicitudGastos, 
        solicitudgastos.FechaRegresoSolicitudGastos, 
        solicitudgastos.EstadoSolicitudGastos, 
        solicitudgastos.ValorTotalSolicitudGastos, 
        solicitudgastos.ObservacionesSolicitudGastos, 
        solicitudgastos.IdDepartamentoSolicitudGastos, 
        solicitudgastos.IdUsuarioCreacion,
        municipio.NombreMunicipio,
        departamento.NombreDepartamento,
        
        proyecto.DescProyecto,
        actividad.DescripcionActividad,
        proceso.DescProceso,
        aprobacionsolictudgasto.ValorAprobacion,
        anticipos.IdAnticipos,
        anticipos.DocEquivalenteAnticipo,
        anticipos.ValorAnticipo,
        legalizaciongastos.idLegalizacionGastos,
        legalizaciongastos.TotalLegalizacionGastos
     FROM solicitudgastos 
        INNER JOIN municipio ON municipio.idMunicipio=solicitudgastos.idMunicipioSolicitudGastos
        LEFT JOIN aprobacionsolictudgasto on solicitudgastos.idConsecutivoSolicitudGastos=aprobacionsolictudgasto.idSolicitudGasto
        INNER JOIN departamento on departamento.idDepartamento=solicitudgastos.IdDepartamentoSolicitudGastos
        LEFT JOIN anticipos ON anticipos.IdSGAnticipo=solicitudgastos.idConsecutivoSolicitudGastos
        
        INNER JOIN proyecto ON proyecto.idProyecto=solicitudgastos.idProyectoSolicitudGastos
        INNER JOIN actividad ON actividad.idActividad=solicitudgastos.idActividadSolicitudGastos
        INNER JOIN proceso ON proceso.idProceso=solicitudgastos.idProcesoSolicitudGastos
        LEFT JOIN legalizaciongastos ON legalizaciongastos.idSolicitudGasto=solicitudgastos.idConsecutivoSolicitudGastos
        where solicitudgastos.idConsecutivoSolicitudGastos=".$idSolicitudGastos;
        // echo $sql;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function InsertarSolicitudGastos($fecha,$CodProyectoSG,
        $CodProcesoSG,$CodActividadSG,$CodMunicipioSG,$FechaHoraSalidaSG,$FechaHoraRegresoSG,
        $TotalSolicitudGastoSG,$ObservacionesSG,$CodDepartamentoSG,$UsuarioCreacion){
            $sql="INSERT INTO solicitudgastos 
            (FechaSolicitud,idProyectoSolicitudGastos,idProcesoSolicitudGastos,idActividadSolicitudGastos,idMunicipioSolicitudGastos,FechaSalidaSolicitudGastos,FechaRegresoSolicitudGastos,EstadoSolicitudGastos,ValorTotalSolicitudGastos,ObservacionesSolicitudGastos,IdDepartamentoSolicitudGastos,IdUsuarioCreacion) VALUES 
            ('".$fecha."',".$CodProyectoSG.",".$CodProcesoSG.",".$CodActividadSG.",".$CodMunicipioSG.",'".$FechaHoraSalidaSG."','".$FechaHoraRegresoSG."',0,".$TotalSolicitudGastoSG.",'".$ObservacionesSG."',".$CodDepartamentoSG.",".$UsuarioCreacion.")";
            
            $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
            return $this->ConnxClass->link->insert_id;
    }

    // modulo editar solicitud de gasto

    function EditarSolicitudGastos($IdSolicitudGastoSG,$CodProyectoSG,$CodActividadSG,$CodMunicipioSG,$FechaHoraSalidaSG,
        $FechaHoraRegresoSG,$TotalSolicitudGastoSG,$ObservacionesSG,$CodDepartamentoSG){
            $sql="UPDATE solicitudgastos SET idProyectoSolicitudGastos='".$CodProyectoSG."', idActividadSolicitudGastos='".$CodActividadSG."',
            idMunicipioSolicitudGastos=".$CodMunicipioSG.",FechaSalidaSolicitudGastos='".$FechaHoraSalidaSG."',FechaRegresoSolicitudGastos='".$FechaHoraRegresoSG."',
            ValorTotalSolicitudGastos=".$TotalSolicitudGastoSG.", ObservacionesSolicitudGastos='".$ObservacionesSG."', IdDepartamentoSolicitudGastos=".$CodDepartamentoSG."
            WHERE idConsecutivoSolicitudGastos=".$IdSolicitudGastoSG;         
            // echo $sql;
            $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
            return $this->resultado;
    }

    function actualizarValorSG($IdsG){
        $sql="UPDATE solicitudgastos SET ValorTotalSolicitudGastos = 
        (SELECT SUM(detallesolicitudgasto.NumdiasTrayectoDetalleSolicitud * detallesolicitudgasto.ValorUnitarioDetalleSolicitud)
		   FROM detallesolicitudgasto
			WHERE detallesolicitudgasto.idConsecutivoSolicitudGasto=$IdsG)   
			WHERE solicitudgastos.idConsecutivoSolicitudGastos=".$IdsG;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function EditarResponsablesSG($IdSolicitudGastoSG,$idResponsableSG){
        $sql="UPDATE solicitudgastos_has_empleado SET Empleado_DocumentoEmpleado = ".$idResponsableSG."
         WHERE solicitudgastos_idConsecutivoSolicitudGastos=".$IdSolicitudGastoSG;
        //  echo $sql;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function EditarDetalleEntidadSG($IdSolicitudGastoSG,$CodEntidadSG){
        $sql="UPDATE detallesolicitudgastos_entidades SET IdEntidad = ".$CodEntidadSG."
         WHERE IdSolicitudGasto=".$IdSolicitudGastoSG;
        //  echo $sql;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function borrarEntidadSG($idSG){ 
        $sql="DELETE FROM detallesolicitudgastos_entidades WHERE IdSolicitudGasto=".$idSG;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function borrarResponsableSG($idSG){ 
        $sql="DELETE FROM solicitudgastos_has_empleado WHERE solicitudgastos_idConsecutivoSolicitudGastos=".$idSG;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

        // fin de la modal editar



    function ListarDetallexidSolicitud($IdSolilcitudGasto){

        $sql="SELECT detallesolicitudgasto.*,
        vereda.NombreVereda
        from detallesolicitudgasto
        LEFT JOIN vereda ON vereda.idVereda=detallesolicitudgasto.idVereda
        where idConsecutivoSolicitudGasto=".$IdSolilcitudGasto;
        // echo $sql;

        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function getnombreconceptoGasto($IdConceptoGasto){
        $sql="SELECT * FROM conceptodegasto where idConceptodeGasto=".$IdConceptoGasto;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        if ($this->resultado->num_rows > 0) {
            $this->rowCG=$this->resultado->fetch_array(MYSQLI_BOTH);
            $this->nombreCG=$this->rowCG[2];
        }
        return $this->nombreCG;
    }

    function InsertarResponsablesSG($ConsecutivoSolicitud,$idResponsableSG){
        $sql="INSERT INTO solicitudgastos_has_empleado (solicitudgastos_idConsecutivoSolicitudGastos,Empleado_DocumentoEmpleado) VALUES (".$ConsecutivoSolicitud.",".$idResponsableSG.")";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function ListaResponsablesxIDsolicitud($idSolcitudGastos){
        $sql="SELECT
            solicitudgastos_has_empleado.*
            from solicitudgastos_has_empleado
            where solicitudgastos_idConsecutivoSolicitudGastos=".$idSolcitudGastos;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function ListaEntidadesxIDsolicitud($idSolicitudGastos){
        $sql="SELECT
            detallesolicitudgastos_entidades.*
            from detallesolicitudgastos_entidades
            where detallesolicitudgastos_entidades.IdSolicitudGasto=".$idSolicitudGastos;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function InsertarTPMaDetalle($idConsecutivoSolicitudGasto, $idConceptoGastoDetalleSolicitud, $NumdiasTrayectoDetalleSolicitud,$ValorUnitarioDetalleSolicitud,$idVereda){
        $sql="INSERT INTO
        detallesolicitudgasto (idConsecutivoSolicitudGasto, idConceptoGastoDetalleSolicitud, NumdiasTrayectoDetalleSolicitud,ValorUnitarioDetalleSolicitud,EstadoRelacionDetalleSolicitud,idVereda)
        VALUES (".$idConsecutivoSolicitudGasto.",".$idConceptoGastoDetalleSolicitud.",".$NumdiasTrayectoDetalleSolicitud.",".$ValorUnitarioDetalleSolicitud.",0,".$idVereda.")";
        // echo $sql;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function CambiarEstadoSG($IdSolicitudGasto,$nuevoEstadoSG){
        $sql="UPDATE solicitudgastos SET EstadoSolicitudGastos=".$nuevoEstadoSG." where idConsecutivoSolicitudGastos=".$IdSolicitudGasto;
        // echo $sql;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function ArchivarSG($IdSolicitudGasto,$usuarioArchivo,$fechaarchivo){
        $sql="UPDATE solicitudgastos SET 
            EstadoSolicitudGastos=4,
            UsuarioArchivo=".$usuarioArchivo.",
            FechaArchivo='".$fechaarchivo."'
            where idConsecutivoSolicitudGastos=".$IdSolicitudGasto;
        // echo $sql;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function CambiarEstadoDetalleSG($IdRegDetalleSolicitud,$NuevoEstadoDetalleSG){
        $sql="UPDATE detallesolicitudgasto SET EstadoRelacionDetalleSolicitud=".$NuevoEstadoDetalleSG." where IdRegDetalleSolicitud=".$IdRegDetalleSolicitud;
        // echo $sql;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function RelacionSGSaldo(){
        $sql="SELECT
        solicitudgastos.idConsecutivoSolicitudGastos, 
        solicitudgastos.FechaSolicitud, 
        solicitudgastos.idProyectoSolicitudGastos, 
        solicitudgastos.idProcesoSolicitudGastos, 
        solicitudgastos.idActividadSolicitudGastos, 
        solicitudgastos.idMunicipioSolicitudGastos, 
        solicitudgastos.FechaSalidaSolicitudGastos, 
        solicitudgastos.FechaRegresoSolicitudGastos, 
        solicitudgastos.EstadoSolicitudGastos, 
        solicitudgastos.ValorTotalSolicitudGastos, 
        solicitudgastos.ObservacionesSolicitudGastos, 
        solicitudgastos.IdDepartamentoSolicitudGastos, 
        solicitudgastos.IdUsuarioCreacion,
        municipio.NombreMunicipio,
        legalizaciongastos.TotalLegalizacionGastos,
        aprobacionsolictudgasto.ValorAprobacion,
        legalizaciongastos.idLegalizacionGastos,
        usuario.NombreUsuario
        from solicitudgastos
        inner join legalizaciongastos on legalizaciongastos.idSolicitudGasto=solicitudgastos.idConsecutivoSolicitudGastos
        INNER JOIN municipio on municipio.idMunicipio=solicitudgastos.idMunicipioSolicitudGastos
        INNER JOIN aprobacionsolictudgasto on aprobacionsolictudgasto.idSolicitudGasto=solicitudgastos.idConsecutivoSolicitudGastos
        INNER JOIN usuario ON usuario.idUsuario=solicitudgastos.IdUsuarioCreacion
        where 
        -- (legalizaciongastos.TotalLegalizacionGastos > 0) and 
        (legalizaciongastos.TotalLegalizacionGastos < solicitudgastos.ValorTotalSolicitudGastos) and
        (solicitudgastos.EstadoSolicitudGastos = 3)";
        // echo $sql;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function ListaSGsinSaldo(){
        $sql="SELECT 
        solicitudgastos.idConsecutivoSolicitudGastos, 
        solicitudgastos.FechaSolicitud, 
        solicitudgastos.idProyectoSolicitudGastos, 
        solicitudgastos.idProcesoSolicitudGastos, 
        solicitudgastos.idActividadSolicitudGastos, 
        solicitudgastos.idMunicipioSolicitudGastos, 
        solicitudgastos.FechaSalidaSolicitudGastos, 
        solicitudgastos.FechaRegresoSolicitudGastos, 
        solicitudgastos.EstadoSolicitudGastos, 
        solicitudgastos.ValorTotalSolicitudGastos, 
        solicitudgastos.ObservacionesSolicitudGastos, 
        solicitudgastos.IdDepartamentoSolicitudGastos, 
        solicitudgastos.IdUsuarioCreacion,
        municipio.NombreMunicipio,
        legalizaciongastos.TotalLegalizacionGastos,
        aprobacionsolictudgasto.ValorAprobacion,
        legalizaciongastos.idLegalizacionGastos,
        usuario.NombreUsuario
        from solicitudgastos
        inner join legalizaciongastos on legalizaciongastos.idSolicitudGasto=solicitudgastos.idConsecutivoSolicitudGastos
        INNER JOIN municipio on municipio.idMunicipio=solicitudgastos.idMunicipioSolicitudGastos
        INNER JOIN aprobacionsolictudgasto on aprobacionsolictudgasto.idSolicitudGasto=solicitudgastos.idConsecutivoSolicitudGastos
        INNER JOIN usuario ON usuario.idUsuario=solicitudgastos.IdUsuarioCreacion
        where 
        (legalizaciongastos.TotalLegalizacionGastos >0) and 
        (legalizaciongastos.TotalLegalizacionGastos = solicitudgastos.ValorTotalSolicitudGastos) and
        (solicitudgastos.EstadoSolicitudGastos = 3)";
        // echo $sql;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function ObtenerCABdeIdSG($IdSG){
        $sql="SELECT 
                solicitudgastos.idConsecutivoSolicitudGastos, 
                solicitudgastos.FechaSolicitud, 
                solicitudgastos.idProyectoSolicitudGastos, 
                solicitudgastos.idProcesoSolicitudGastos, 
                solicitudgastos.idActividadSolicitudGastos, 
                solicitudgastos.idMunicipioSolicitudGastos, 
                solicitudgastos.FechaSalidaSolicitudGastos, 
                solicitudgastos.FechaRegresoSolicitudGastos, 
                solicitudgastos.EstadoSolicitudGastos, 
                solicitudgastos.ValorTotalSolicitudGastos, 
                solicitudgastos.ObservacionesSolicitudGastos, 
                solicitudgastos.IdDepartamentoSolicitudGastos, 
                solicitudgastos.IdUsuarioCreacion,
                municipio.NombreMunicipio,
                proyecto.DescProyecto,
                proceso.DescProceso,
                actividad.DescripcionActividad,
                departamento.NombreDepartamento,
                aprobacionsolictudgasto.ValorAprobacion,
                anticipos.IdAnticipos,
                anticipos.DocEquivalenteAnticipo,
                anticipos.ValorAnticipo,
                legalizaciongastos.idLegalizacionGastos,
                legalizaciongastos.TotalLegalizacionGastos
            FROM solicitudgastos 
                INNER JOIN municipio on municipio.idMunicipio=solicitudgastos.idMunicipioSolicitudGastos
                INNER JOIN proyecto on proyecto.idProyecto=solicitudgastos.idProyectoSolicitudGastos
                INNER JOIN proceso on proceso.idProceso=solicitudgastos.idProcesoSolicitudGastos
                INNER JOIN actividad on actividad.idActividad=solicitudgastos.idActividadSolicitudGastos
                INNER JOIN departamento on departamento.idDepartamento=solicitudgastos.IdDepartamentoSolicitudGastos
                LEFT JOIN aprobacionsolictudgasto on aprobacionsolictudgasto.idSolicitudGasto=solicitudgastos.idConsecutivoSolicitudGastos
                LEFT JOIN anticipos ON anticipos.IdSGAnticipo=solicitudgastos.idConsecutivoSolicitudGastos
                LEFT JOIN legalizaciongastos ON legalizaciongastos.idSolicitudGasto=solicitudgastos.idConsecutivoSolicitudGastos
                WHERE solicitudgastos.idConsecutivoSolicitudGastos=".$IdSG;
        
         $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
         return $this->resultado;
    }

    function ObtenerCABdeIdSGLeg($IdSG){
        $sql="SELECT solicitudgastos.idConsecutivoSolicitudGastos, 
        solicitudgastos.FechaSolicitud, 
        solicitudgastos.idProyectoSolicitudGastos, 
        solicitudgastos.idProcesoSolicitudGastos, 
        solicitudgastos.idActividadSolicitudGastos, 
        solicitudgastos.idMunicipioSolicitudGastos, 
        solicitudgastos.FechaSalidaSolicitudGastos, 
        solicitudgastos.FechaRegresoSolicitudGastos, 
        solicitudgastos.EstadoSolicitudGastos, 
        solicitudgastos.ValorTotalSolicitudGastos, 
        solicitudgastos.ObservacionesSolicitudGastos, 
        solicitudgastos.IdDepartamentoSolicitudGastos, 
        solicitudgastos.IdUsuarioCreacion,
        municipio.NombreMunicipio,
        proyecto.DescProyecto,
        proceso.DescProceso,
        actividad.DescripcionActividad,
        departamento.NombreDepartamento,
        anticipos.IdAnticipos,
        anticipos.DocEquivalenteAnticipo,
        legalizaciongastos.idLegalizacionGastos,
        legalizaciongastos.TotalLegalizacionGastos
        FROM solicitudgastos
        INNER JOIN municipio on municipio.idMunicipio=solicitudgastos.idMunicipioSolicitudGastos
        INNER JOIN proyecto on proyecto.idProyecto=solicitudgastos.idProyectoSolicitudGastos
        INNER JOIN proceso on proceso.idProceso=solicitudgastos.idProcesoSolicitudGastos
        INNER JOIN actividad on actividad.idActividad=solicitudgastos.idActividadSolicitudGastos
        INNER JOIN departamento on departamento.idDepartamento=solicitudgastos.IdDepartamentoSolicitudGastos
        INNER JOIN anticipos ON anticipos.IdSGAnticipo=solicitudgastos.idConsecutivoSolicitudGastos
        LEFT JOIN legalizaciongastos ON legalizaciongastos.idSolicitudGasto=solicitudgastos.idConsecutivoSolicitudGastos
        WHERE solicitudgastos.idConsecutivoSolicitudGastos=".$IdSG;
         $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
         return $this->resultado;
    }

    function InsDetalleEntidadSG($IdsG,$IdEntidad){
        $sql="INSERT INTO detallesolicitudgastos_entidades (IdSolicitudGasto,IdEntidad) VALUES (".$IdsG.",".$IdEntidad.")";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
         return $this->resultado;
    }

    function borrarlineadetalleSG($id_detalle_SG){ 
        $sql="DELETE FROM
        detallesolicitudgasto
        WHERE detallesolicitudgasto.IdRegDetalleSolicitud=".$id_detalle_SG;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    // function GetTarifa($idConceptodeGasto,$idMunicipio){
    //     $sql="SELECT
	// 	detalletarifagastosgnr.idTarifaGastosGnr,
	// 	municipio.idMunicipio,
	// 	municipio.NombreMunicipio,
	// 	cabtarifagastognr.idTipoGasto,
	// 	conceptodegasto.DesConceptodeGasto,
	// 	conceptodegasto.idConceptodeGasto,
	// 	detalletarifagastosgnr.idTipoTarifa,
	// 	tipotarifa.DescTipoTarifa,
	// 	detalletarifagastosgnr.ValorGastosGnr
    //  FROM cabtarifagastognr
	// 	INNER JOIN detalletarifagastosgnr ON cabtarifagastognr.idCabTarifaGastoGNR=detalletarifagastosgnr.CabTarifaGastoGNR
	// 	INNER JOIN tipodegasto ON tipodegasto.idTipodeGasto=cabtarifagastognr.idTipoGasto
	// 	INNER JOIN tipotarifa ON tipotarifa.idTipoTarifa=detalletarifagastosgnr.idTipoTarifa
	// 	INNER JOIN municipio ON municipio.idTipoTarifa=tipotarifa.idTipoTarifa
	// 	INNER JOIN conceptodegasto ON conceptodegasto.TipodeGasto_idTipodeGasto=tipodegasto.idTipodeGasto
    //     WHERE conceptodegasto.TarifaSN=1 AND conceptodegasto.idConceptodeGasto=".$idConceptodeGasto." AND municipio.idMunicipio=".$idMunicipio;
    //     // echo $sql;
    //     $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
    //      return $this->resultado;
    // }


    //  **************** Movimientos de TMP SG


    function listardetalleTMP($UsuarioCreacion){
        $sql="SELECT tmp_detallesolicitudgastos.*,
        vereda.NombreVereda
        FROM tmp_detallesolicitudgastos
        INNER JOIN vereda ON vereda.idVereda=tmp_detallesolicitudgastos.TMP_CodVereda
        WHERE TMP_UsuarioCreacionSolicitudGastos=".$UsuarioCreacion;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function borrarlineadetalleTMP($idTMP_DetalleSolicitudGastos){ 
        $sql="DELETE FROM tmp_detallesolicitudgastos WHERE idTMP_DetalleSolicitudGastos=".$idTMP_DetalleSolicitudGastos;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function vaciarTMPDetalle($UsuarioCreacion){
        $sql="DELETE FROM tmp_detallesolicitudgastos where TMP_UsuarioCreacionSolicitudGastos=".$UsuarioCreacion;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function InsertarDetalleTMP($ConceptoSolicitud,$Numerodias,$ValorSolicitud,$UsuarioCreacion,$CodVereda){
        $sql="INSERT INTO tmp_detallesolicitudgastos (TMP_IdConceptoSolicitudGastos,TMP_NumDisSolicitudGastos,TMP_ValorundSolicitudGastos,TMP_UsuarioCreacionSolicitudGastos,TMP_CodVereda) VALUES (".$ConceptoSolicitud.",".$Numerodias.",".$ValorSolicitud.",".$UsuarioCreacion.",".$CodVereda.")";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        echo $sql;
        return $this->resultado;
    }
}
?>