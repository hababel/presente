<?php 

Class Proceso_Donantes{

    function __construct($InstanciaDB){
        $this->ConnxClass=$InstanciaDB;
     }

    function InsertDonante($idProyectoAsignacionDonante, $FechaInicioAsignacionDonante, $FechaFinalizacionAsignacionDonante, $idEntidadVinculadaAsignacionDonante,$CuantiaAsignacionDonante,$TipoVinculacionAsignacionDonante){
        $sql="INSERT INTO asignaciondonante
            (idProyectoAsignacionDonante, FechaInicioAsignacionDonante, FechaFinalizacionAsignacionDonante, idEntidadVinculadaAsignacionDonante,CuantiaAsignacionDonante,TipoVinculacionAsignacionDonante)
            VALUES (".$idProyectoAsignacionDonante.", '".$FechaInicioAsignacionDonante."', '".$FechaFinalizacionAsignacionDonante."', ".$idEntidadVinculadaAsignacionDonante.",".$CuantiaAsignacionDonante.",'".$TipoVinculacionAsignacionDonante."')";
       $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->ConnxClass->link->insert_id;
    }

    function ListaDonantes(){
        $sql="SELECT asignaciondonante.*,
        proyecto.DescProyecto,
        entidadvinculada.NombreEntidadVinculada,
        entidadvinculada.NitEntidadVinculada
        FROM asignaciondonante
        INNER JOIN proyecto ON  proyecto.idProyecto=asignaciondonante.idProyectoAsignacionDonante
        INNER JOIN entidadvinculada ON entidadvinculada.idEntidadVinculada=asignaciondonante.idEntidadVinculadaAsignacionDonante ORDER BY idAsignacionDonante DESC LIMIT 1000";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function ListaDonantesxID($idAsignacionDonante){
        $sql="SELECT asignaciondonante.*,
        proyecto.DescProyecto,
        entidadvinculada.NombreEntidadVinculada,
        entidadvinculada.NitEntidadVinculada
        FROM asignaciondonante
        INNER JOIN proyecto ON  proyecto.idProyecto=asignaciondonante.idProyectoAsignacionDonante
        INNER JOIN entidadvinculada ON entidadvinculada.idEntidadVinculada=asignaciondonante.idEntidadVinculadaAsignacionDonante
        WHERE idAsignacionDonante=".$idAsignacionDonante;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function BuscarDonantesxNIT($NitDonante){
        $sql="SELECT asignaciondonante.*,
        proyecto.DescProyecto,
        entidadvinculada.NombreEntidadVinculada,
        entidadvinculada.NitEntidadVinculada
        FROM asignaciondonante
        INNER JOIN proyecto ON  proyecto.idProyecto=asignaciondonante.idProyectoAsignacionDonante
        INNER JOIN entidadvinculada ON entidadvinculada.idEntidadVinculada=asignaciondonante.idEntidadVinculadaAsignacionDonante
        WHERE entidadvinculada.NitEntidadVinculada=".$NitDonante;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function EditarAsignacionDonante($idAsignacionDonante,$idProyectoAsignacionDonante, $FechaInicioAsignacionDonante, $FechaFinalizacionAsignacionDonante, $idEntidadVinculadaAsignacionDonante,$CuantiaAsignacionDonante,$TipoVinculacionAsignacionDonante){
        $sql="UPDATE asignaciondonante 
        SET idProyectoAsignacionDonante = ".$idProyectoAsignacionDonante.",
        FechaInicioAsignacionDonante = '".$FechaInicioAsignacionDonante."',
        FechaFinalizacionAsignacionDonante = '".$FechaFinalizacionAsignacionDonante."',
        idEntidadVinculadaAsignacionDonante = ".$idEntidadVinculadaAsignacionDonante.",
        CuantiaAsignacionDonante = ".$CuantiaAsignacionDonante.",
        TipoVinculacionAsignacionDonante = '".$TipoVinculacionAsignacionDonante."'
        WHERE idAsignacionDonante=".$idAsignacionDonante;
        // echo $sql;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function InstdetalleMunicipio($idAsignacionDonante, $idMunicipio){
        $sql="INSERT INTO detalleasignaciondonante (idAsignacionDonante, idMunicipio)
        VALUES (".$idAsignacionDonante.", ".$idMunicipio.")";
        // echo $sql;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function listadetallemunicipioxidasignaciondonante($idAsignacionDonante){
        $sql="SELECT * from detalleasignaciondonante where detalleasignaciondonante.idAsignacionDonante=".$idAsignacionDonante;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function BorrraregistrosAsignacionDonante($idAsignacionDonante){
        $sql="DELETE from detalleasignaciondonante where detalleasignaciondonante.idAsignacionDonante=".$idAsignacionDonante;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function BuscarMunicipioAsignacionDonantes($IdMunicipio,$IdAsignacionDonante){
        $sql="SELECT * from detalleasignaciondonante where detalleasignaciondonante.idMunicipio=".$IdMunicipio." and detalleasignaciondonante.idAsignacionDonante=".$IdAsignacionDonante;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }
}


?>