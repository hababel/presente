<?php 
	 $IdRecurso=42;
     include_once('../principal/CABparapermisos.php');
     
     /*
         Forma de colocar permisos en cada modulo.
 
         Funcion: colocar_permiso($IdUsuario,$accion,$IdRecurso)
 
         $IdUsuario: $IdUsuariologin=$_SESSION['id'];
         $accion: 1-Consultar, 2-Editar,3-Insertar,4-Eliminar,5-Listar - > Este nuemro verifica la columna de la tabal que relaciona el recurso_perfil
         $IdRecurso: Cod del recurso - Columna: CodRecurso de la tabla recurso.
         Retorna el valor que hay en l aBD (1-SI tiene permiso,0-NO tiene permiso)    
     */
	 if($_SESSION['autentic']){
        if($Permisos_Listar==1 || $useradmon){
            require_once("../conn_BD.php");
            require_once('class/ClassDonantes.php');
            require_once('../proyectos/class/classProyecto.php');
            require_once('../entidades/class/classEntidades.php');
            require_once('../municipios/class/ClassMunicipios.php');
            $InstanciaDB=new Conexion();
            $insDonantes=new Proceso_Donantes($InstanciaDB);
            $InstProyectos=new Proceso_Proyecto($InstanciaDB);
            $InstEntidades=new Proceso_Entidades($InstanciaDB);
            $InstMunicipio=new Proceso_Municipios($InstanciaDB);
            $ListaDonantes=$insDonantes->ListaDonantes();
            $ListaProyectos=$InstProyectos->ListarProyecto();
            $ListaEntidades=$InstEntidades->ListarEntidades();
            $listaMunicipios=$InstMunicipio->ListaMunicipio();
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- <head> -->
    <?php include_once('../headScript.php'); ?>
<!-- </head> -->        
<body>
    <div id="wrapper">
        <?php 
            include_once('../headWeb.php');
            include_once("../../menu/m_principal.php");
        ?>
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="panel-body" align="right">
                    <?php if( $Permisos_Insertar==1 || $useradmon ){ //Permiso para Insertar o Crear ?>
                        <button type="button" class="btn btn-success btn-circle" data-toggle="modal" onclick="NuevoRegDonantes();" data-target="#modalinsertDonantes">
                            <i class="fa fa-plus fa-2x"></i>
                        </button>
                    <?php } ?> 
                </div>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                            Asignacion de Donantes
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">                                            	                               
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>ID</i></th>
                                        <th>Nit</th>
                                        <th>Nombre Donante</th>
                                        <th>Cuantia</th>
                                        
                                        <th><span class='glyphicon glyphicon-cog' title='Config'></span></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        while($row=$ListaDonantes->fetch_array()){
                                        $datos=$row[0]."||".$row[1]."||".$row[2]."||".$row[3]."||".$row[4]."||".$row[5]."||".$row[6];
                                        
                                    ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $row[0]; ?></td>
                                        <td><?php echo $row[10]; ?></td>
                                        <td><?php echo $row[9]; ?></td>
                                        <td><?php echo '$'.formato($row[5]); ?></td>
                                        <td>
                                        <?php if($Permisos_Consultar==1 || $useradmon) { //Permisos para Consultar/Ver Registro ?>
                                            <button title="Ver" onclick="VerDonantes('<?php echo $datos;?>')" class="btn btn-default btn-sm" data-toggle="modal" data-target="#modalinsertDonantes"><span class="glyphicon glyphicon-info-sign" style="color:blue;"></span></button>
                                            <?php if($Permisos_Editar==1 || $useradmon) { //Permisos para Consultar/Ver Registro ?>	
                                                <button title="Edit" onclick="formeditdonante('<?php echo $datos;?>')" class="btn btn-default btn-sm" data-toggle="modal" data-target="#modalinsertDonantes"><span class="glyphicon glyphicon-pencil"></span></button>
                                            <?php } ?>
                                        <?php } ?>
                                        </td>
                                    </tr> 
                                    <?php } ?>
                                </tbody>									
                            </table>							
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="modal fade" id="modalinsertDonantes" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">			
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>															
                    <h3 align="center" class="modal-title" id="TituloAsignacionDonante"></h3>
                </div>
                <div id="msgInsertDonantes"></div>
                <div class="panel-body">
                    <div class="row">                                       
                        <div class="col-md-10">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Proyecto</label>
                                    <div class="col-sm-8">
                                        <select class="js-example-basic-single form-control" name="IdProyecto" id="IdProyecto" style="width:350px">
                                            <option value="0"> -- Seleccione un proyecto -- </option>  
                                            <?php
                                                mysqli_data_seek($ListaProyectos, 0);
                                                while ($rowEM=$ListaProyectos->fetch_array(MYSQLI_BOTH)) { 
                                                    echo "<option value='".$rowEM[0]."'>".$rowEM[1]."</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Fecha Inicio</label>
                                    <div class="col-sm-8">
                                        <div class='input-group date datetime' id='datetimepickerN'>
                                            <input type='text' class="form-control" id="fechaInicioDonante" autocomplete="off">
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <?php
                                        $TipoCalendario='Fecha';
                                        include('../../Insertfecha.php');?> 
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Fecha Finalizacion</label>
                                    <div class="col-sm-8">
                                        <div class='input-group date datetime' id='datetimepicker1'>
                                            <input type='text' class="form-control" id="fechaFinalDonante" autocomplete="off">
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <?php
                                        $TipoCalendario='Fecha';
                                        include('../../Insertfecha.php');?>
                                    </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Entidad Vinculada</label>
                                    <div class="col-sm-8">
                                        <select class="js-example-basic-single form-control" name="idEntidad" id="idEntidad"  style="width:350px">
                                            <option value="0"> -- Seleccione una entidad -- </option>  
                                            <?php
                                                mysqli_data_seek($ListaEntidades, 0);
                                                while ($rowEN=$ListaEntidades->fetch_array(MYSQLI_BOTH)) { 
                                                    echo "<option value='".$rowEN[0]."'>".$rowEN[2]."</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Cuantia</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-usd"></span>
                                            </span>
                                            <input name="Cuantia" id="Cuantia" type="text" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Tipo Vinculacion</label>
                                    <div class="col-sm-8">
                                        <input name="TipoVinculacion" id="TipoVinculacion" type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Municipios</label>
                                    <div class="col-sm-8">
                                        <select class="js-example-basic-multiple form-control"  name="VerCodMunicipio" id="VerCodMunicipio" multiple="multiple" style="width:350px">
                                            <?php
                                                mysqli_data_seek($listaMunicipios,0);																		
                                                while ($rowMun=$listaMunicipios->fetch_array(MYSQLI_BOTH)) { 
                                                    echo "<option value='".$rowMun[0]."'>".$rowMun[2]."</option>";
                                                    }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>                                                                 
                    </div> 
                </div> 
                <div class="modal-footer" id="FootAsignacionDonantes">
                    
                </div>										 
            </div>
        </div>
    </div>


       <!-- /. WRAPPER  -->
       <script src="../../assets/js/jquery-1.10.2.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script src="../../assets/js/jquery.metisMenu.js"></script>
    <script src="../../assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="../../assets/js/dataTables/dataTables.bootstrap.js"></script>
	<script src="../../assets/js/jasny-bootstrap.min.js"></script>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
                
                $('.js-example-basic-multiple').select2({
                    placeholder:"Municipios",
                    allowClear: true,
                });

                $("#Cuantia").on({
                    "focus": function(event) {
                        $(event.target).select();
                    },
                    "keyup": function(event) {
                        $(event.target).val(function(index, value) {
                        return value.replace(/\D/g, "")
                            .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
                        });
                    }
                });
                
                $("#Cuantiae").on({
                    "focus": function(event) {
                        $(event.target).select();
                    },
                    "keyup": function(event) {
                        $(event.target).val(function(index, value) {
                        return value.replace(/\D/g, "")
                            .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
                        });
                    }
                });
            });


            function NuevoRegDonantes(){
                $('#IdProyecto').attr("disabled",false);
                $('#fechaInicioDonante').attr("disabled",false);
                $('#fechaFinalDonante').attr("disabled",false);
                $('#idEntidad').attr("disabled",false);
                $('#Cuantia').attr("disabled",false);
                $('#TipoVinculacion').attr("disabled",false);
                $('#VerCodMunicipio').attr("disabled",false);
                $('#IdProyecto').val(null);
                $('#fechaInicioDonante').val(null);
                $('#fechaFinalDonante').val(null);
                $('#idEntidad').val(null);
                $('#Cuantia').val(null);
                $('#TipoVinculacion').val(null);
                $('#VerCodMunicipio').val(null).trigger('change');
                $('#TituloAsignacionDonante').html(`Asignar Nuevo Donante`);
                $('#FootAsignacionDonantes').html(`
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="button" onclick="InsertAsignacionDonntes();" class="btn btn-primary">Guardar</button>
                `);
            }

            
             
    </script>
         <!-- CUSTOM SCRIPTS -->
    <script src="../../assets/js/custom.js"></script>
</body>
</html>
<?php 
        }else{
            include_once('../principal/sinpermisolistar.php');         
        }
	}else{
		header('Location:../../php_cerrar.php');
	}
?>