<?php 
    session_start();
    $private_id=session_id();
	if($_SESSION['autentic']){
		require_once("../conn_BD.php");
		require_once("../funciones.php");
        require_once("../proyectos/class/classProyecto.php");
        require_once("../requisicion_materiales/class/ClassRequisicionMateriales.php");
        require_once("../empleados/class/classEmpleados.php");
        
		$InstanciaDB=new Conexion();     
        $InstProyecto=new Proceso_Proyecto($InstanciaDB);
        $InstRequisiciones=new Proceso_RequisicionMateriales($InstanciaDB);
        $InstEmpleados=New Proceso_Empleados($InstanciaDB);
        
        $ListaProyecto=$InstProyecto->ListarProyecto();
        $ListaEmpleados=$InstEmpleados->ListarEmpleados();
        $ListaRequisicionesPendientes=$InstRequisiciones->ListarRequisicionesxEstado(0);

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- <head> -->
<?php include_once('../headScript.php'); ?>

<!-- </head> -->

<body>
    <div id="private_id" style="display:none;"><?php echo $private_id;?></div>
    <?php 
		include_once('../headWeb.php');
		include_once("../../menu/m_principal.php");
	?>

    <div id="wrapper">
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="panel-body" align="right">
                    <button type="button" class="btn btn-success btn-circle" data-toggle="modal" onclick="NuevaRequisicionMateriales();" data-target="#NuevaRequisicionMateriales">
                        <i class="fa fa-plus fa-2x"></i>
                    </button>
                </div>
                    <div class="panel panel-primary">
                        <div class="panel-heading" style="height:55px;">
                            Requisicion de Materiales -<b>pendientes por entregar</b>-
                            <div style="float:right;">
                                <button type="button" class="btn btn-primary">
                                    <span class="badge"><?php echo $ListaRequisicionesPendientes->num_rows;?></span>
                                </button>
                            </div>
                         </div>   
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <th>#</th>
                                        <th>Fecha Requisicion</th>
                                        <th>Proyecto</th>
                                        <th>Solicitante</th>
                                        <th>Estado</th>
                                        <th><span class='glyphicon glyphicon-cog' title='Config'></span></th>
                                    </thead>
                                    <tbody>
                                        <?php
                                        while($row=$ListaRequisicionesPendientes->fetch_array()){
                                            $datos=$row[0]."||".$row[1]."||".$row[2]."||".$row[3]."||".$row[4]."||".$row[5]."||".$row[6]."||".$row[7];
                                        ?>
                                        <tr class="odd gradeX">
                                            <td><?php echo $row[0]; ?></td>
                                            <td><?php echo $row[1]; ?></td>
                                            <td><?php echo $row[8]; ?></td>
                                            <td><?php echo $row[7]; ?></td>
                                            <td><?php echo $row[6]; ?></td>
                                            <td>
                                                <!-- <button title="Ver" onclick="EditarRequisicionMateriales('<?php echo $datos; ?>',0)" class="btn btn-default btn-sm" data-toggle="modal" data-target="#NuevaRequisicionMateriales">
                                                    <span class="glyphicon glyphicon-info-sign" style="color:blue;"></span>
                                                </button>
                                                <button title="Ver" onclick="EditarRequisicionMateriales('<?php echo $datos; ?>',1)" class="btn btn-default btn-sm" data-toggle="modal" data-target="#NuevaRequisicionMateriales">
                                                    <span class="glyphicon glyphicon glyphicon-pencil" style="color:blue;"></span>
                                                </button>
                                                <button title="Aprobar Gasto" class="btn btn-default btn-sm" data-toggle="modal" data-target="#NuevaAprobarSolicitudGasto">
                                                    <span class="glyphicon glyphicon-arrow-right" style="color:green;"></span><span style="color:green;"> $</span>
                                                </button> -->
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>

    <!-- Inicio Modal Nueva Requisicion de Materiales -->
        <div class="modal fade" id="NuevaRequisicionMateriales" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 align="center" class="modal-title" id="TituloModalRequisicionM"></h3>
                        <div id="msgRequisicionMateriales"></div>
                    </div>
                    <div class="panel-body">
                        <div class="row col-sm-4">
                            <div class="form-group">
                                <label for="">Fecha </label>
                                <div class="form-group">
                                    <div class='input-group date datetime' id='datetimepicker1'>
                                        <input type='text' class="form-control" id="FechaRequisicionM" value="<?php echo $fechadatetimepicker;?>" autocomplete="off">
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <?php
                                    $TipoCalendario='Fecha';
                                    include('../../Insertfecha.php');
                                ?> 
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="">Proyecto</label>
                                <select class="form-control Select2" name="ProyectoRequisicionM" id="ProyectoRequisicionM" style="width:100%">
                                    <option value=0> -- Seleccione un Proyecto -- </option>
                                    <?php
                                        mysqli_data_seek($ListaProyecto, 0);																		
                                        while ($rowProy=$ListaProyecto->fetch_array(MYSQLI_BOTH)) { 
                                            echo "<option value='".$rowProy[0]."'>".$rowProy[1]."</option>";
                                            }
                                    ?>
                                </select>
                            </div>
                        </div> 
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="">Empledo</label>
                                <select class="form-control Select2" name="EmpleadosRequisicionM" id="EmpleadosRequisicionM" style="width:100%">
                                <option value=0> -- Seleccione un Empleado -- </option>
                                    <?php
                                        mysqli_data_seek($ListaEmpleados, 0);
                                        while ($rowEM=$ListaEmpleados->fetch_array(MYSQLI_BOTH)) { 
                                            echo "<option value='".$rowEM[0]."'>".$rowEM[2]."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>              
                        <div class="row">
                            <div class="col-sm-12">
                            <hr>
                                <div id="DetalleRequisicionM"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" id="FooterBotonesModal">
                        
                    </div>
                </div>
            </div>
        </div>
    <!-- Final Modal Nuevo Solicitud de gastos -->
    
    <script src="../../assets/js/jquery-1.10.2.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script src="../../assets/js/jquery.metisMenu.js"></script>
    <script src="../../assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="../../assets/js/dataTables/dataTables.bootstrap.js"></script>
    <script src="../../assets/js/jasny-bootstrap.min.js"></script>

    <style>
        table {
            width: 70%;
            margin: 0 auto;
        }

        .modal-body {
            max-height: calc(100vh - 210px);
            overflow-y: auto;
        }
    </style>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

    <script>
        $(document).ready(function() {

            $('#ProyectoRequisicionM').select2();
            $('#EmpleadosRequisicionM').select2();

            $('#ProyectoRequisicionM').select2({
				    dropdownParent: $("#NuevaRequisicionMateriales")
                });
            $('#EmpleadosRequisicionM').select2({
                dropdownParent: $("#NuevaRequisicionMateriales")
            });

        });

        function mostrardetalleRequisicionMTMP() {
            var private_id=$('#private_id').text();
            var parametros = {private_id};
            $.ajax({
                url: '../../logica/logica.php?accion=ListarDetalleRequisicionMTMP',
                type: "POST",
                data: parametros,
                beforeSend: function(objeto) {
                    $('#DetalleRequisicionM').html('Cargando...');
                },
                success: function(data) {
                    $("#DetalleRequisicionM").html(data).fadeIn('slow');
                }
            });
        }

        function InsertarDetalleRequisicionMTMP(){

            var MaterialRequisicion=$('#MaterialRequisicion').val();
            var CantiRequeridaM=$('#CantiRequeridaM').val();
            // var UndxCajaRequisicionM=$('#UndxCajaRequisicionM').val();
            var private_id=$('#private_id').text();
            if (MaterialRequisicion==''|| CantiRequeridaM=='') {
                $("#msgDetalleRTMP").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
            } else {
                var parametros={MaterialRequisicion,CantiRequeridaM,private_id};
                var url = "../../logica/logica.php?accion=InsertarDetalleRequisicionTMP";
                $.ajax({
                    type: "POST",
                    url: url,
                    data: parametros,
                    // dataType: "json",
                    cache: false,
                    success: function (response) {
                        if (response==0) {
                            $("#msgDetalleRTMP").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Por fAvor Comuniquese con Soporte</div>");
                        } else {
                            mostrardetalleRequisicionMTMP();
                        }
                        
                    }
                });
            }
        }

        function BorrarDetalleRequisicionMTMP(idDetalleRequisicionMTMP,private_id) {
            var parametros = {idDetalleRequisicionMTMP,private_id};
            $.ajax({
                url: '../../logica/logica.php?accion=EliminaridDetalleRequisicionMTMP',
                type: 'POST',
                data: parametros,
                success: function(data) {
                    mostrardetalleRequisicionMTMP();
                }
            });
        }

        
    </script>

</body>

</html>
<?php 
	}else{
		header('Location:../../php_cerrar.php');
	}
?>