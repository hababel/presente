<?php 

//********** Requisision Materiales **************
Class Proceso_RequisicionMateriales{

    function __construct($InstanciaDB){
        $this->ConnxClass=$InstanciaDB;
     }

    function ListarRequisicionMateriales(){
        $sql="SELECT 
        requisicionmaterial.*,
        empleado.NombreEmpleado,
        proyecto.DescProyecto
        FROM requisicionmaterial 
        INNER JOIN empleado ON empleado.idEmpleado=requisicionmaterial.idSolicitanteRequisicionMaterial
        INNER JOIN proyecto ON proyecto.idProyecto=requisicionmaterial.idProyectoRequisicionMaterial";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function BuscarRequisicion($IdRequisicionMaterial){
        $sql="SELECT 
        requisicionmaterial.*,
        empleado.NombreEmpleado,
        proyecto.DescProyecto
        FROM requisicionmaterial 
        INNER JOIN empleado ON empleado.idEmpleado=requisicionmaterial.idSolicitanteRequisicionMaterial
        INNER JOIN proyecto ON proyecto.idProyecto=requisicionmaterial.idProyectoRequisicionMaterial
        WHERE  requisicionmaterial.idRequisicionMaterial=".$IdRequisicionMaterial;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function BuscarRequisicionMXIdMaterial($IdMaterial,$IdRequisicionMaterial){
        $sql="SELECT * FROM detallerequisicionmaterial WHERE detallerequisicionmaterial.idMaterial=".$IdMaterial." and detallerequisicionmaterial.idRequisicionMaterial=".$IdRequisicionMaterial;

        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function UpdateCantMaterialM($IdRequisicionM,$idMaterial,$CantRequeridaMat){
        $sql="UPDATE detallerequisicionmaterial SET detallerequisicionmaterial.CantRequeridaMaterial=".$CantRequeridaMat."
         WHERE detallerequisicionmaterial.idMaterial=".$idMaterial." and detallerequisicionmaterial.idRequisicionMaterial=".$IdRequisicionM;
    
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function UpdateEstadoRequisicion($IdRequisicionM,$Estado){
        $sql="UPDATE requisicionmaterial SET requisicionmaterial.EstadoRequisicionMaterial=".$Estado." WHERE requisicionmaterial.idRequisicionMaterial=".$IdRequisicionM;
        echo $sql;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function InsertarRequisicionMateriales($FechaRequisicionMaterial,$idProyectoRequisicionMaterial,$idSolicitanteRequisicionMaterial,$TotalCantRequisicionMaterial){
        $sql="INSERT INTO `requisicionmaterial`
        (`FechaRequisicionMaterial`, `idProyectoRequisicionMaterial`, `idSolicitanteRequisicionMaterial`, `EstadoRequisicionMaterial`, `TotalCantRequisicionMaterial`)
        VALUES ('".$FechaRequisicionMaterial."', ".$idProyectoRequisicionMaterial.", ".$idSolicitanteRequisicionMaterial.", 0, ".$TotalCantRequisicionMaterial.");";
        // echo $sql;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->ConnxClass->link->insert_id;
    }
    
    function InsertarDetalleRequisicionM($idRequisicionMaterial,$idMaterial,$CantRequeridaMaterial){
        $sql="INSERT INTO `detallerequisicionmaterial` (`idRequisicionMaterial`, `idMaterial`, `CantRequeridaMaterial`,  `ValidacionOrigenCAB`)
        VALUES (".$idRequisicionMaterial.", ".$idMaterial.", ".$CantRequeridaMaterial.", 0);";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function ListarDetalleRMxIdRequisicionM($IdRequisicionM){
        $sql="SELECT 
        detallerequisicionmaterial.*,
        archivodematerial.DescArchivodeMaterial
        FROM detallerequisicionmaterial
        INNER JOIN archivodematerial ON archivodematerial.idArchivodeMaterial=detallerequisicionmaterial.idMaterial
        WHERE detallerequisicionmaterial.idRequisicionMaterial=".$IdRequisicionM;
        // echo $sql;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function BorrarDetalleRequisicionM($idDetalleRequisicionM){ 
        $sql="DELETE FROM detallerequisicionmaterial WHERE idDetalleMaterial=".$idDetalleRequisicionM;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

     //  ********* Listar Requisiciones x Estado
    /*
    
    0 - Requisiciones Pendientes
    1 - Requisiciones Entregadas Satisfactorias (Completas)
    2 - Requisiciones Entregadas Incompletas
    
    
    */

    function ListarRequisicionesxEstado($EstadoRequisicionM){
        $sql="SELECT 
        requisicionmaterial.*,
        CASE requisicionmaterial.EstadoRequisicionMaterial
	     when 0 then 'Abierta'
	     when 1 then 'Cerrada'
	     END AS	'Estado requisicion',
        empleado.NombreEmpleado,
        proyecto.DescProyecto
        FROM requisicionmaterial 
        INNER JOIN empleado ON empleado.idEmpleado=requisicionmaterial.idSolicitanteRequisicionMaterial
        INNER JOIN proyecto ON proyecto.idProyecto=requisicionmaterial.idProyectoRequisicionMaterial
        WHERE requisicionmaterial.EstadoRequisicionMaterial=".$EstadoRequisicionM;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }


    // ********************** Tabla TEMPORAL (tmp_detallematerial) ************************

    // Insertar Detalle TMP Requisicion Materiales
    function InsertarDetalleRequisicionTMP($idMaterialTMP,$CantRequeridaMatTMP,$private_id){
        $sql="INSERT INTO `tmp_detallematerial` (`idMaterialTMP`, `CantRequeridaMatTMP`, `IdSessionMatTMP`) 
        VALUES (".$idMaterialTMP.",".$CantRequeridaMatTMP.", '".$private_id."');";      
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function ListarDetalleRequisicionesTMP($private_id){
        $sql="SELECT 
        tmp_detallematerial.*,
        archivodematerial.DescArchivodeMaterial
        FROM tmp_detallematerial
        INNER JOIN archivodematerial ON archivodematerial.idArchivodeMaterial=tmp_detallematerial.idMaterialTMP
        WHERE IdSessionMatTMP='".$private_id."'";
        // echo $sql;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function BuscarRequisicionMXIdMaterialTPM($IdMaterial,$private_id){
        $sql="SELECT * FROM tmp_detallematerial WHERE tmp_detallematerial.idMaterialTMP=".$IdMaterial." and IdSessionMatTMP='".$private_id."'";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function UpdateCantMaterialRMTMP($idMaterialTMP,$CantRequeridaMatTMP, $private_id){
        $sql="UPDATE tmp_detallematerial SET tmp_detallematerial.CantRequeridaMatTMP=".$CantRequeridaMatTMP."  WHERE tmp_detallematerial.idMaterialTMP=".$idMaterialTMP." and IdSessionMatTMP='".$private_id."'";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function BorrarDetalleRequisicionMTMP($idDetalleRequisicionMTMP,$private_id){ 
        $sql="DELETE FROM tmp_detallematerial WHERE idDetalleMaterialTMP=".$idDetalleRequisicionMTMP." and IdSessionMatTMP='".$private_id."'";
        // echo $sql;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function vaciarTMPDetalleRequisicionMaterial($Id_Session){
        $sql="DELETE FROM tmp_detallematerial WHERE tmp_detallematerial.IdSessionMatTMP='".$Id_Session."'";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

// ********** Recibo Materiales ************** 

    function ListaReciboMaterial(){
        $sql="SELECT recibomaterial.*,
        empleado.NombreEmpleado,
        proyecto.DescProyecto
        from recibomaterial
        inner join proyecto on proyecto.idProyecto=recibomaterial.idProyectoReciboMaterial
        inner join empleado on empleado.idEmpleado=recibomaterial.IdEmpleadoRecibo";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function InsertarDetalleAdicionalRecibo($idRequisicionMaterial,$idMaterial,$CantRecibidaMaterial){
        $sql="INSERT INTO `detallerequisicionmaterial`
        (idRequisicionMaterial,idMaterial,CantRecibidaMaterial,ValidacionOrigenCAB)
        VALUES (".$idRequisicionMaterial.",".$idMaterial.",".$CantRecibidaMaterial.",1);";       
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function UpdateCantMaterialenRecibo($IdRequisicionM,$idMaterial,$CantRecibidaMaterial){
        $sql="UPDATE detallerequisicionmaterial SET detallerequisicionmaterial.CantRecibidaMaterial=".$CantRecibidaMaterial."
        WHERE detallerequisicionmaterial.idMaterial=".$idMaterial." and detallerequisicionmaterial.idRequisicionMaterial=".$IdRequisicionM;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function InsertarCabReciboM($idRequisicionMaterial,$IdEmpleadoRecibo,$FechaReciboMaterial,$idProyectoReciboMaterial,$EstadoReciboMaterial){

        $sql="INSERT INTO recibomaterial (idRequisicionMaterial,IdEmpleadoRecibo,FechaReciboMaterial,idProyectoReciboMaterial,EstadoReciboMaterial)
        VALUES (".$idRequisicionMaterial.",".$IdEmpleadoRecibo.",'".$FechaReciboMaterial."',".$idProyectoReciboMaterial.",".$EstadoReciboMaterial.");";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->ConnxClass->link->insert_id;
    }

    function UpdateDetalleGuardarRecibo($IdDetalleMaterial,$IdReciboMaterial,$CantRecibidaM,$IdTipoNovedad,$CantTipoNovedad){
        $sql="UPDATE detallerequisicionmaterial SET
        detallerequisicionmaterial.IdReciboMaterial=".$IdReciboMaterial.",
        detallerequisicionmaterial.CantRecibidaMaterial=".$CantRecibidaM.", 
        detallerequisicionmaterial.IdTipoNovedad=".$IdTipoNovedad.",
        detallerequisicionmaterial.CantTipoNovedad=".$CantTipoNovedad."
        WHERE detallerequisicionmaterial.idDetalleMaterial=".$IdDetalleMaterial;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
            return $this->resultado;
        }
    }





?>