<?php 
	$IdRecurso=13;
	include_once('../principal/CABparapermisos.php');
	
	/*
		Forma de colocar permisos en cada modulo.

		Funcion: colocar_permiso($IdUsuario,$accion,$IdRecurso)

		$IdUsuario: $IdUsuariologin=$_SESSION['id'];
		$accion: 1-Consultar, 2-Editar,3-Insertar,4-Eliminar,5-Listar - > Este nuemro verifica la columna de la tabal que relaciona el recurso_perfil
		$IdRecurso: Cod del recurso - Columna: CodRecurso de la tabla recurso.
		Retorna el valor que hay en l aBD (1-SI tiene permiso,0-NO tiene permiso)    
	*/
	if($_SESSION['autentic']){
		if($Permisos_Listar==1 || $useradmon){
			require_once("../conn_BD.php");
			require_once('../centro_costos/class/classCentroCostosH.php');
			require_once('../centro_costos/class/classCentrosCostosP.php');
			$InstanciaDB=new Conexion();
			$InstCentroCostosH=new Proceso_CentroCostosH($InstanciaDB);
			$InstCentroCostosP=new Proceso_CentroCostosP($InstanciaDB);
			$ListaCentroCostosH=$InstCentroCostosH->ListarCentroCostosH();
			$ListaCentroCostosP=$InstCentroCostosP->ListarCentroCostosP();
			$ListaCentroCostosPE=$InstCentroCostosP->ListarCentroCostosP();
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- <head> -->
    <?php include_once('../headScript.php'); ?>
<!-- </head> -->        
<body>
    <div id="wrapper">
	<?php 
		include_once('../headWeb.php');
		include_once("../../menu/m_principal.php");
	?>
        <div id="page-wrapper" >
            <div id="page-inner">						                
                 <!-- /. ROW  -->               
                <div class="panel-body" align="right">
					<?php if( $Permisos_Insertar==1 || $useradmon ){ //Permiso para Insertar o Crear ?>                                                                                 
						<button type="button" class="btn btn-success btn-circle" data-toggle="modal" data-target="#ModalNuevoCentroCostosH">
							<i class="fa fa-plus fa-2x"></i>
						</button>
					<?php } ?>                                                                                
                </div>
            <div class="row">
                <div class="col-md-12">
                	<!-- Advanced Tables -->
<div class="panel panel-primary">
<div class="panel-heading">
		Centro de Costos Hijo
</div>
<div class="panel-body">
	<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover" id="dataTables-example">
			<thead>
				<tr>
					<th>Id</th>
					<th>Codigo</th>
					<th>Descripcion</th>                                                                         
					<th>Centro de Costos Padre</th>
					<th><span class='glyphicon glyphicon-cog' title='Config'></span></th>
				</tr>
			</thead>
			<tbody>
				<?php
					while ($row=$ListaCentroCostosH->fetch_array()) {
						$datosCentroCostosH=$row[0]."||".$row[1]."||".$row[2]."||".$row[3];
					?>
				<tr class="odd gradeX">
					<td><?php echo $row[0]; ?></td>
					<td><?php echo $row[1]; ?></td>
					<td><?php echo $row[2]; ?></td>
					<td><?php echo $row[4]; ?></td>                                                                                  
					<td class="center">
						<?php if($Permisos_Consultar==1 || $useradmon) { //Permisos para Consultar/Ver Registro ?>
							<div class="btn-group">
								<button type="button" onclick="formeditCentroCostosH('<?php echo $datosCentroCostosH;?>')" class="btn btn-default btn-sm" data-toggle="modal" data-target="#ModalEditarCentroCostosHFM"><span class="glyphicon glyphicon-pencil"></span></button>
							</div>
						<?php } ?>
					</td>
				</tr>
					<?php }?>       	

		<!--  Modal Nuevo CentroCostosH-->
			<div class="modal fade" id="ModalNuevoCentroCostosH" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<form name="form2" method="post" enctype="multipart/form-data" action="">											
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>															
								<h3 align="center" class="modal-title" id="myModalLabel">Nuevo Centro de Costos Hijo</h3>
							</div>
							<div id="msgCentroCostosHNuevo"></div>
							<div class="panel-body">
							<div class="row">                                       
								<div class="col-md-6">
									<div class="form-group">
										<label for="">Codigo Centro de Costos Hijo</label>														
										<input class="form-control" name="CodCentroCostosH" id="CodCentroCostosH" autocomplete="off" required>
									</div>
									<div class="form-group">
										<label for="">Descripcion Centro de Costos Hijo</label>
										<input class="form-control" name="DescCentroCostosH" id="DescCentroCostosH" aria-describedby="emailHelpId" autocomplete="off" required>
									</div>
									<div>
									<div class="form-group">
										<label for="">Centro de costos Padre</label>
										<select class="form-control" name="CodCentroCostosP" id="CodCentroCostosP">
										<?php																			
											while ($rowCCP=$ListaCentroCostosP->fetch_array(MYSQLI_BOTH)) { 
												echo "<option value='".$rowCCP[0]."'>".$rowCCP[1]."	- ".$rowCCP[2]."</option>";
												}
										?>
										</select>
									</div>
								</div> 
								</div>                                                          
							</div> 
							</div> 
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
								<button type="button" onclick="InsertCentroCostosH();" class="btn btn-primary">Guardar</button>
							</div>										 
						</div>
					</div>
				</form>
			</div>
		<!-- End Modal Nuevo CentroCostosH-->

			<!--  Modal Editar CentroCostosH-->
			<div class="modal fade" id="ModalEditarCentroCostosHFM" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>															
								<h3 align="center" class="modal-title" id="myModalLabel">Actualizar Centro de Costos Hijo</h3>
								<div id="msgEditCentroCostosHFM"></div>
							</div>
							<div class="panel-body">
								<div class="row">                                       
									<div class="col-md-6">																												
										<label>ID</label>
										<input class="form-control" id="IdCentroCostosHFM" name="IdCentroCostosHFM" disabled>
										<label>Codigo Centro de Costos Hijo</label>
										<input class="form-control" id="CodigoCentroCostosHFM" name="CodigoCentroCostosHFM" autocomplete="off" required>
										<label>Descripcion Centro de Costos Hijo</label>
										<input class="form-control" id="descCentroCostosHFM" name="descCentroCostosHFM" autocomplete="off" required>
										<label for="">Centro de costos Padre</label>
										<select class="form-control" name="CodCentroCostosPFM" id="CodCentroCostosPFM">
										<?php																			
											while ($rowCCP=$ListaCentroCostosPE->fetch_array(MYSQLI_BOTH)) { 
												echo "<option value='".$rowCCP[0]."'>".$rowCCP[1]."	- ".$rowCCP[2]."</option>";
												}
										?>
										</select>
									</div>                                                                       
								</div> 
							</div> 
							<div class="modal-footer">
								<div align="right">
									<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
									<?php if($Permisos_Editar == 1 || $useradmon) { //Permisos para Editar Registro ?>
										<button type="submit" class="btn btn-primary" onclick="EditarCentroCostosHFM();">Guardar</button>
									<?php } ?> 	
								</div> 
							</div>										 
						</div>
					</div>
				</div>
			<!-- End Modal Editar Centro de Costos Hijo-->
						</tbody>									
					</table>							
				</div>                            
			</div>
		</div>
		<!--End Advanced Tables -->
	</div>
            </div>                                
        </div>               
    </div>
  </div>
         <!-- /. PAGE WRAPPER  -->
     <!-- /. WRAPPER  -->
    <script src="../../assets/js/jquery-1.10.2.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script src="../../assets/js/jquery.metisMenu.js"></script>
    <script src="../../assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="../../assets/js/dataTables/dataTables.bootstrap.js"></script>
	<script src="../../assets/js/jasny-bootstrap.min.js"></script>
	<script src="../../assets/js/custom.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });

		 $('#ModalNuevoCentroCostosH').on('shown.bs.modal', function () {
			   $('#CodCentroCostosH').trigger('focus'); })	

		$('#ModalEditarCentroCostosHFM').on('shown.bs.modal', function () {
		$('#CodigoCentroCostosHFM').trigger('focus'); })

    </script>
    
</body>
</html>
<?php 
		}else{
			include_once('../principal/sinpermisolistar.php');        
		}
	}else{
		header('Location:../../php_cerrar.php');
	}
?>