<?php

    /*
         Forma de colocar permisos en cada modulo.
 
         Funcion: colocar_permiso($IdUsuario,$accion,$IdRecurso)
 
         $IdUsuario: $IdUsuariologin=$_SESSION['id'];
         $accion: 1-Consultar, 2-Editar,3-Insertar,4-Eliminar,5-Listar - > Este numero verifica la columna de la tabla que relaciona el recurso_perfil
         $IdRecurso: Cod del recurso - Columna: CodRecurso de la tabla recurso.
         Retorna el valor que hay en la BD (1-SI tiene permiso,0-NO tiene permiso)    
     */

    session_start();
    require_once("../../modulos/funciones.php");
	
    $IdUsuariologin=$_SESSION['id']; //Id Usuario logeado
    $useradmon=$_SESSION['administrador']; // valor: true - si es superadministrador

    // Permisos para Archivar SG (ArchivarSG)
    $IdRecurso=52;
    $Permisos_Insertar_ArchivarSG=colocar_permiso($IdUsuariologin,3,$IdRecurso); //Permiso para Insertar o Crear Registro


	if($_SESSION['autentic']){
		require_once("../conn_BD.php");
        require_once("../solicitud_gastos/class/ClassSolicitudGastos.php");
        require_once("../empleados/class/classEmpleados.php");
        require_once("../entidades/class/classEntidades.php");
        require_once("../aprobacionSolicGasto/class/ClassaprobacionSolicGasto.php");
        require_once("class/ClassLegalizacionGastos.php");
        require_once("../funciones.php");
		$InstanciaDB=new Conexion(); 
        $InstSolicitudGasto=new Proceso_SolicitudGastos($InstanciaDB);
        $InstEmpleados=new Proceso_Empleados($InstanciaDB);
        $InstEntidades=new Proceso_Entidades($InstanciaDB);
        $InstAprobacionSolicGastos=new Proceso_AprobacionSolicitudGastos($InstanciaDB);
        $InsLegalizacionGastos=new Proceso_LegalizacionGastos($InstanciaDB);
        $ListaSGxLegalizar=$InstSolicitudGasto->ListarSolicitudGastosxEstado(2); //SG Creadas,Aprobadas, Con Anticipo, pendientes por Legalizar
        $ListaSGxCerrar=$InstSolicitudGasto->ListarSolicitudGastosxEstado(3); //SG Creadas,Aprobadas, Con Anticipo, Con Legalizar, Pendientes por cerrar
        $listaEmpleados=$InstEmpleados->ListarEmpleados();
        $listaEntidades=$InstEntidades->ListarEntidades();
        $ListaAprSolicGastos=$InstAprobacionSolicGastos->ListarAprobarSolicitudGastos();
        $ListaLegalizacionGastos=$InsLegalizacionGastos->ListarLegalizacionGasto();
?>
                                                    
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- <head> -->
    <?php include_once('../headScript.php'); ?>
<!-- </head> --> 

<body>
	<?php 
		include_once('../headWeb.php');
		include_once("../../menu/m_principal.php");
	?>
            <input type="hidden" name="IdUsuariologin" id="IdUsuariologin" value="<?php echo $IdUsuariologin; ?>">
            <div id="wrapper">
                <div id="page-wrapper">
                    <div id="page-inner">
                        <div class="panel panel-primary"> 
                            <div class="panel-heading">
                                <div class="col-lg-1">
                                    <a href="javascript:window.history.back();"><span class="glyphicon glyphicon-chevron-left"></span> atrás</a>
                                </div>
                                <div >
                                    <b> Plantilla - Legalizacion de Gastos</b>
                                </div>
                            </div>
                            <?php 
                            
                            if ($ListaSGxLegalizar->num_rows>0 || $ListaSGxCerrar->num_rows  >0) { 
                                $rowdf=$ListaSGxLegalizar->fetch_array(MYSQLI_BOTH);
                                ?>      
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="table-responsive form-horizontal">
                                                <div>
                                                    <div id="IdLG" style="display:none;"><?php echo $_GET['IdLG'];?></div>
                                                    <div id="IdSG" style="display:none;"><?php echo $_GET['IdSG'];?></div>
                                                    <div class="container col-md-12" id="ParaInsNuevaLG" style="display:none;"> 
                                                        <div class="form-group"> 
                                                            <div class="col-md-4"><label>Solicitud de Gasto</label></div>
                                                            <div class="col-md-8">
                                                                <select class="js-example-basic-single form-control" name="IdSolicitudGastoSG" id="IdSolicitudGastoSG" style="width:350px" <?php if($_GET['IdSG'] != 0){ echo "disabled"; } ?>>
                                                                <option value="0"> -- Seleccione una Solicitud de Gasto -- </option>                 
                                                                    <?php
                                                                        mysqli_data_seek($ListaSGxLegalizar, 0);											
                                                                        while ($rowMun=$ListaSGxLegalizar->fetch_array(MYSQLI_BOTH)) { 
                                                                            
                                                                            echo "<option value='".$rowMun[0]."'";

                                                                            if ($_GET) {
                                                                                if($rowMun[0]==$_GET['IdSG']){
                                                                                    echo "selected='selected'";
                                                                                }
                                                                            }
                                                                            echo "'> #".$rowMun[0]." - ".$rowMun[1]." - ".$rowMun[14]." - $".number_format($rowMun[18])."</option>";
                                                                        }
                                                                    ?> 
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-md-4"><label>Empleado</label></div>
                                                            <div class="col-md-8">
                                                                <select class="js-example-basic-single form-control" name="VerresponsableSGdivLeg" id="VerresponsableSGdivLeg" style="width:350px">
                                                                <option value=0> </option>
                                                                    <?php
                                                                        mysqli_data_seek($listaEmpleados, 0);
                                                                        while ($rowEM=$listaEmpleados->fetch_array(MYSQLI_BOTH)) { 
                                                                            echo "<option value='".$rowEM[0]."'>".$rowEM[2]."</option>";
                                                                        }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-md-11">
                                                                <label>Observaciones</label>
                                                                <textarea class="form-control"  name="ObservacioneLegalizacionSGdiv" id="ObservacioneLegalizacionSGdiv" rows="2" maxlength="100" style="resize: none;"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="col-md-11">
                                                                <div id="BotonesGuardarLegalizacionSG" class="col-md-7 text-left">
                                                                    <?php if ($_GET) {?>
                                                                        <button type="button" onclick="guardarNuevaLegalizacionSG(<?php echo $rowdf[0]; ?>);" class="btn btn-primary" id="NuevaLegalizacionG">Legalizar Gasto</button>
                                                                    <?php }?> 
                                                                </div>
                                                                <div class="col-md-5 text-right"><b>Ir a:  </b>
                                                                        <button type="button" class="btn btn-default" onClick="window.location.href='../solicitud_gastos/indexSolicitudGastos.php'">Solicitudes Gasto</button>
                                                                        <button type="button" class="btn btn-default" onClick="window.location.href='indexLegalizacionGastos.php'">Legalizacion Gasto</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                          
                                        </div>
                                    </div>

                                    <div class="row" id="DocumentoSG">
                                        <div id="CABRG"></div>
                                        <div id="CabIdSG_LG"><?php include('../solicitud_gastos/CabSG.php');?></div>
                                        <br>
                                       
                                        <div id="ListaDetalleSGLegalizacion"></div> <!-- detalle de la Solicitud de Gasto para Legalizacionar -->
                                    </div>
                                    
                                    <div> <!-- Boton archivar -->
                                        <?php
                                            if ($Permisos_Insertar_ArchivarSG == 1 || $useradmon) { // Permisos para Archivar SG (Segun columna "insertar" en la tabla)
                                                if ($_GET['IdLG'] !=0 && $_GET['IdSG'] !=0) {
                                                    echo '
                                                    <div class="form-group col-sm-6 text-left">
                                                        <button class="btn-default" style="font-size:17px;color:black" onclick="ArchivarSG('.$_GET['IdSG'].')">Archivar SG <i class="fas fa-flag-checkered"></i></button>
                                                    </div>';
                                                    // <button style="font-size:15px;color:green">Archivar <i class="fas fa-archive"></i></button>
                                                }
                                            }
                                        ?>
                                    </div>
                                    <div class="col-lg-12">
                                        <a href="javascript:window.history.back();" style="color:black;"><span class="glyphicon glyphicon-chevron-left"></span> atrás</a>
                                    </div>
                                </div>
                            
                                <?php 
                            } else { ?>
                                    <div class="alert alert-dismissible alert-info">
                                        <strong>Buen trabajo! </strong> <b class="alert-link"> - No hay </b> Solicitudes de Gatos pendientes por Legalizar.
                                    </div>
                                    <div class="col-md-12 text-right">
                                        <button type="button" class="btn btn-default" onClick="window.location.href='../solicitud_gastos/indexSolicitudGastos.php'">Solicitudes Gasto</button>
                                        <button type="button" class="btn btn-default" onClick="window.location.href='indexLegalizacionGastos.php'">Legalizacion Gasto</button>
                                    </div>
                            <?php 
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>

                    <!-- INICIO ModalUpLoadLegalizacion Modal -->
                        <div id="ModalUpLoadLegalizacion" class="modal fade" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h3 align="center" class="modal-title" id="myModalLabel">Legalizacion de Gasto</h3> 
                                    </div>
                                    <form class="form-horizontal" method="POST" role="form" name="miFormulario" enctype="multipart/form-data" id="miFormulario">
                                        <div id="AvisoForm"></div>
                                        <div class="modal-body" id="body_form">
                                            <div class="form-group">
                                                <label for="ConceptoGastoLegalizacion" class="col-sm-12 control-label" id="ResumenSG"></label>
                                            </div>
                                            <div class="form-group">
                                                <label for="NitBeneficiario" class="col-sm-4 control-label">Nit Beneficiario</label>
                                                <div class="col-sm-8">
                                                    <input type="text" id="NitBeneficiario" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="NombreBeneficiario" class="col-sm-4 control-label">Nombre Beneficiario</label>
                                                <div class="col-sm-8">
                                                    <input type="text" id="NombreBeneficiario" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="DireccionBeneficiario" class="col-sm-4 control-label">Direccion Beneficiario</label>
                                                <div class="col-sm-8">
                                                    <input type="text" id="DireccionBeneficiario" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="TelefonoBeneficiario" class="col-sm-4 control-label">Telefono Beneficiario</label>
                                                <div class="col-sm-8">
                                                    <input type="text" id="TelefonoBeneficiario" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="NumeroFactura" class="col-sm-4 control-label">Numero Factura</label>
                                                <div class="col-sm-8">
                                                    <input type="text" id="NumeroFactura" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="ValorFactura" class="col-sm-4 control-label">Valor Factura</label>
                                                <div class="col-sm-8">
                                                    <input type="text" id="ValorFactura" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Soporte Fisico</label>
                                                <div class="col-sm-8">
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input form-control" name="Soporte_Legalizacion" id="Soporte_Legalizacion" accept=".jpg,.jpeg,.png" placeholder='Seleccione archivo (jpg,jpeg,png) Para soporte Fisico' onchange="validarImagen(this);" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-2"></div>
                                                <div class="col-sm-8">
                                                    <img src="" id="ImagenSoporteFisico" class="img-responsive" width="450px" height="450px">
                                                </div>
                                                <div class="col-sm-2"></div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-4">
                                                    <div class="text-center"><b>Pago Tarjeta Crédito Corporativa</b></div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="text-center"><b>Entrega RUT</b></div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="text-center"><b>Entrega Copia Cédula</b></div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-4">
                                                    <div class="col-sm-8">
                                                        <select class="form-control" name="PagoTCD" id="PagoTCD">
                                                            <option value=0>NO</option>
                                                            <option value=1>SI</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="col-sm-8">
                                                        <select class="form-control" name="EntregaRUT" id="EntregaRUT">
                                                            <option value=0>NO</option>
                                                            <option value=1>SI</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="col-sm-8">
                                                        <select class="form-control" name="EntregaCedula" id="EntregaCedula">
                                                            <option value=0>NO</option>
                                                            <option value=1>SI</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Observaciones</label>
                                                <textarea class="form-control" type="text" cols="40" rows="2" id="Observaciones" maxlength="250" autocomplete="off" style="resize: none;width:90%;" required></textarea>
                                            </div>
                                        </div>
                                        <div class="modal-footer" id="modal_footer_DetalleLG">
                                            
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    <!-- Final Modal ModalUpLoadLegalizacion -->

                    <!-- INICIO Modal mensaje correcto para la imagen -->
                        <div class="modal fade" id="ModalMensajewarningImagen" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <div id="MensajeaMostrar">
                                            <div class="alert alert-dismissible alert-warning">
                                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                <b>Hubo un error !!</b>: El tamaño de la imagen no puede superar los <strong>4 MB</strong>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!-- FIN Modal mensaje correcto para la imagen -->

                    <!-- INICIO Modal mensaje de campos vacios -->
                        <div class="modal fade" id="ModalMensajeCamposVacios" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <div id="MensajeaMostrar">
                                            <div class="alert alert-dismissible alert-warning">
                                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                <b>Atencion !!</b>: El Campo imagen no puede estar <strong>Vacio</strong>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                      
                    <!-- FINAL Modal mensaje de campos vacios -->

                <!-- Inicio Modal de aviso de imagen obligatoria          -->
                    <div class="modal fade" id="ModalMensajeImagenObligatoria" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <div id="MensajeaMostrar">
                                        <div class="alert alert-dismissible alert-warning">
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                            <b>Atencion !!</b>: La imagen es <strong>obligatoria</strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <!-- Fin modal imagen obligatoria -->

                <!-- Inicio Modal mensaje archivar SG -->
                    <div class="modal fade" id="ModalMensajeArchivarSG" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <div id="MensajeaMostrar">
                                        <div class="alert alert-dismissible alert-success">
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                            <b>Buen trabajo !!</b>: Solicitud de Gasto <strong>Archivada</strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <!-- Fin Mensaje Archivas SG -->
                <div id="MsgLG"></div>

                <script src="../../assets/js/jquery-1.10.2.js"></script>
                <script src="../../assets/js/bootstrap.min.js"></script>
                <script src="../../assets/js/jquery.metisMenu.js"></script>
                <script src="../../assets/js/dataTables/jquery.dataTables.js"></script>
                <script src="../../assets/js/dataTables/dataTables.bootstrap.js"></script>
                <script src="../../assets/js/jasny-bootstrap.min.js"></script>
                
                <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
                <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script> 

                <meta name='viewport' content='width=device-width, initial-scale=1'>
                <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.6.3/css/all.css' integrity='sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/' crossorigin='anonymous'>

            <script>

                $(document).ready(function() {
                    if ($('#IdLG').text()==0) {
                        $('#DocumentoSG').hide();
                    }

                    if ($('#IdLG').text() !=0 && $('#IdSG').text() !=0) {
                        $('#ParaInsNuevaLG').hide();
                        $('#DocumentoSG').show();
                        
                        mostrarCABRG($('#IdLG').text());
                        mostrarCABSG($('#IdSG').text());
                        mostrarDetalleSGLegalizacion($('#IdSG').text(),$('#IdLG').text());
                        $('#ConceptoGastoLegalizacion').html($('#IdLG').text());
                        $('#Datos_SG').html("<strong> - # "+$('#IdSG').text()+"</strong>");
                    }else{

                        $('#ParaInsNuevaLG').show();
                         $('#DocumentoSG').show();

                    }

                    $('#VerresponsableSGdiv').select2();
                    $('#VerresponsableSGdivLeg').select2();
                    $('#VerentidadesSGdiv').select2();
                    
                    $('.js-example-basic-multiple').select2({
                        allowClear: true,
                        // disabled: true
                    });

                    $('#IdSolicitudGastoSG').select2();
                
                    $('#IdSolicitudGastoSG').change(function () {
                        var IdSG=$(this).val();
                        var texto=`
                                    <button type="button" onclick="guardarNuevaLegalizacionSG(`+IdSG+`);" class="btn btn-primary" id="NuevaLegalizacionG">Legalizar Gasto</button>`;
                                    $('#NuevaLegalizacionG').attr("disabled", false);
                                    $('#BotonesGuardarLegalizacionSG').html(texto);
                    });
                    
                    
                    var altura_arr = [];//CREAMOS UN ARREGLO VACIO
                    $('.grid_1_4').each(function(){//RECORREMOS TODOS LOS CONTENEDORES DE LAS IMAGENES, DEBEN TENER LA MISMA CLASE
                        var altura = $(this).height(); //LES SACAMOS LA ALTURA
                        altura_arr.push(altura);//METEMOS LA ALTURA AL ARREGLO
                    });
                    altura_arr.sort(function(a, b){return b-a}); //ACOMODAMOS EL ARREGLO EN ORDEN DESCENDENTE
                    $('.grid_1_4').each(function(){//RECORREMOS DE NUEVO LOS CONTENEDORES
                        $(this).css('height',altura_arr[0]);//LES PONEMOS A TODOS LOS CONTENEDORES EL PRIMERO ELEMENTO DE ALTURA DEL ARREGLO, QUE ES EL MAS GRANDE.
                    });

                });

            </script>

            <style type="text/css">
                a:link
                {
                    text-decoration:none;
                    color:white;
                }

                h4{
                    margin-top: 0px;padding-top: 0px;margin-bottom: 0px;
                }
            </style>
    </body>
</html>
<?php 
	}else{
		header('Location:../../php_cerrar.php');
	}
?>