<?php 
	$IdRecurso=46;
    include_once('../principal/CABparapermisos.php');
   
	
	/*
        Forma de colocar permisos en cada modulo.

        Funcion: colocar_permiso($IdUsuario,$accion,$IdRecurso)

        $IdUsuario: $IdUsuariologin=$_SESSION['id'];
        $accion: 1-Consultar, 2-Editar,3-Insertar,4-Eliminar,5-Listar - > Este nuemro verifica la columna de la tabal que relaciona el recurso_perfil
		$IdRecurso: Cod del recurso - Columna: CodRecurso de la tabla recurso.
		Retorna el valor que hay en l aBD (1-SI tiene permiso,0-NO tiene permiso)    
	*/
	if($_SESSION['autentic']){
        if($Permisos_Listar==1 || $useradmon){
            require_once("../conn_BD.php");
            require_once("../solicitud_gastos/class/ClassSolicitudGastos.php");
            require_once("../empleados/class/classEmpleados.php");
            require_once("../aprobacionSolicGasto/class/ClassaprobacionSolicGasto.php");
            require_once("class/ClassLegalizacionGastos.php");
            require_once("../entidades/class/classEntidades.php");
            require_once("../funciones.php");
            $InstanciaDB=new Conexion();
            $InstSolicGastos=new Proceso_SolicitudGastos($InstanciaDB);
            $InstEmpleados=new Proceso_Empleados($InstanciaDB);
            $InstAprobacionSolicGastos=new Proceso_AprobacionSolicitudGastos($InstanciaDB);
            $InsLegalizacionGastos=new Proceso_LegalizacionGastos($InstanciaDB);
            $InstEntidades=new Proceso_Entidades($InstanciaDB);
            
            $listaEntidades=$InstEntidades->ListarEntidades();

            $ListaSGxLegalizar=$InstSolicGastos->ListarSolicitudGastosxEstado(2); //SG Creadas,Aprobadas, Con Anticipo, pendientes por Legalizar

            $listaEmpleados=$InstEmpleados->ListarEmpleados();
            $ListaaproSolicGastos=$InstAprobacionSolicGastos->ListarAprobarSolicitudGastos();
            $ListaLegalizacionGastos=$InsLegalizacionGastos->ListarLegalizacionGasto();
            $ListaRelacionSGconsaldo=$InstSolicGastos->RelacionSGSaldo();
            $ListaSGsinsaldo=$InstSolicGastos->ListaSGsinSaldo();
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- <head> -->
    <?php include_once('../headScript.php'); ?>
    
<!-- </head> -->        
<body>
	<?php 
		include_once('../headWeb.php');
		include_once("../../menu/m_principal.php");
	?>
             <div id="wrapper">
                <div id="page-wrapper">
                    <div id="page-inner">
                        <div class="panel-body" align="right">
                            <?php if( $Permisos_Insertar==1 || $useradmon ){ //Permiso para Insertar o Crear ?> 
                                <button type="button" class="btn btn-success btn-circle" onclick = "location='legalizardetallegasto.php?IdSG=0&IdLG=0'"/>
                                    <i class="fa fa-plus fa-2x"></i>
                                </button>
                            <?php } ?>
                        </div>

                        <!-- inicio Solicitudes pendientes x legalizar -->
                            <div class="panel panel-primary">
                                <div class="panel-heading" style="height:55px;">
                                    Solicitudes de Gastos <b>X Legalizar</b>
                                    <div style="float:right;"><button type="button" class="btn btn-primary"><span class="badge">
                                        <?php echo $ListaSGxLegalizar->num_rows;?></span></button></div>
                                </div>

                                <div class="panel-body">
                                    <?php  if ($ListaSGxLegalizar->num_rows > 0) { ?>
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover" id="dataTables-xlegalizar">
                                            <thead>
                                                <th><input type="checkbox" id="ArchivartodaspendientesLG" name="ArchivartodaspendientesLG"></th>
                                                <th>#</th>
                                                <th>Fecha Solicitud</th>
                                                <th>Municipio</th>
                                                <th>Valor x Legalizar</th>
                                                <th>Usuario</th>
                                                <th><span class='glyphicon glyphicon-cog' title='Config'></span></th>
                                            </thead>
                                            <tbody>
                                                <?php
                                                
                                                while($row=$ListaSGxLegalizar->fetch_array()){
                                                    
                                                    $ListaResponsables=$InstSolicGastos->ListaResponsablesxIDsolicitud($row[0]);
                                                    $datosResponsables='';
                                                    while ($rowR=$ListaResponsables->fetch_array()) {
                                                        $datosResponsables=$datosResponsables."||".$rowR[2];
                                                    }
                                            ?>
                                                <tr class="odd gradeX">
                                                    <td><input type="checkbox" id="ArchivarSGpendientesLG_<?php echo $row[0];?>" name="ArchivarSGpendientesLG"  value="<?php echo $row[0];?>"></td>
                                                    <td><?php echo $row[0]; ?></td>
                                                    <td><?php echo $row[1]; ?></td>
                                                    <td><?php echo $row[13]; ?></td>
                                                    <td><?php echo '$'.number_format($row[18]); ?></td>
                                                    <td><?php echo $row[24]; ?></td>
                                                    <td>
                                                        <?php if($Permisos_Consultar==1 || $useradmon) { //Permisos para Consultar/Ver Registro ?>
                                                            <button title="Ver" onclick="VerSolicitudGasto('<?php echo $row[0];?>','<?php echo $datosResponsables;?>')"
                                                                class="btn btn-default btn-sm" data-toggle="modal" data-target="#VerSolicitudGasto"><span
                                                                    class="glyphicon glyphicon-info-sign" style="color:blue;"></span></button>
                                                        <?php } ?>
                                                        <?php if( $Permisos_Insertar==1 || $useradmon ){ //Permiso para Insertar o Crear ?> 
                                                            <a href="../legalizaciongastos/legalizardetallegasto.php?IdSG=<?php echo $row[0];?>&IdLG=0" class="btn btn-default btn-sm">
                                                            <span class="glyphicon glyphicon-arrow-right" style="color:rgb(255, 128, 0);"></span>
                                                                <span class="glyphicon glyphicon-new-window" style="color:rgb(255, 128, 0);"></span></button>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                        <button type="button" class="btn btn-primary" id="selectmultiplesSGpendienteLG">Archivar Multiples SG</button>
                                    </div>
                                    <?php } else { ?>
                                            <div class="alert alert-dismissible alert-info">
                                                <strong>Buen trabajo!</strong> <b class="alert-link"> - No hay </b> Solicitudes de Gatos pendientes pendientes por Legalizar.
                                            </div>
                                        <?php } ?>
                                </div>
                            </div>
                        <!-- Fin Solicitudes pendientes x legalizar -->

                        <!-- Inicio  Lista de Solicitudes con Saldo -->
                                 
                            <div class="panel panel-primary">
                                <div class="panel-heading" style="height:55px;">
                                    Legalizacion de Gastos <b>con algun Saldo Pendiente</b>
                                    <div style="float:right;"><button type="button" class="btn btn-primary"><span class="badge">
                                        <?php echo $ListaRelacionSGconsaldo->num_rows;?></span></button>
                                    </div>
                                </div>
                                <div class="panel-body">
                                <?php  if ($ListaRelacionSGconsaldo->num_rows > 0) { ?>
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover dataTables-ConSaldo" id="dataTables-ConSaldo">
                                            <thead>
                                                <th><input type="checkbox" id="ArchivartodasconsaldoSG" name="ArchivartodasconsaldoSG"></th>
                                                <th>#</th>
                                                <th>Fecha Solicitud</th>
                                                <th>Municipio</th>
                                                <th>Valor Solicitud</th>
                                                <th>Valor Aprobado (A)</th>
                                                <th>Valor Legalizado (L)</th>
                                                <th>Saldo (A-L)</th>
                                                <th>Usuario</th>
                                                <th><span class='glyphicon glyphicon-cog' title='Config'></span></th>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    while($rowSaldo=$ListaRelacionSGconsaldo->fetch_array()){
                                                        
                                                        $ListaResponsables=$InstSolicGastos->ListaResponsablesxIDsolicitud($rowSaldo[0]);
                                                        $datosResponsables='';
                                                        while ($rowR=$ListaResponsables->fetch_array()) {
                                                            $datosResponsables=$datosResponsables."||".$rowR[2];
                                                        }
                                                ?>
                                                <tr class="odd gradeX">
                                                    <td><input type="checkbox" id="ArchivarSGconsaldo_<?php echo $rowSaldo[0];?>" name="ArchivarSGconsaldo"  value="<?php echo $rowSaldo[0];?>"></td>
                                                    <td><?php echo $rowSaldo[0]; ?></td>
                                                    <td><?php echo $rowSaldo[1]; ?></td>
                                                    <td><?php echo $rowSaldo[13]; ?></td>
                                                    <td><?php echo '$'.number_format($rowSaldo[9]); ?></td>
                                                    <td><?php echo '$'.number_format($rowSaldo[15]); ?></td>
                                                    <td><?php echo '$'.number_format($rowSaldo[14]); ?></td>
                                                    
                                                        <?php 
                                                            $saldo=($rowSaldo[15]-$rowSaldo[14]);
                                                            if ($saldo < 0) {
                                                                echo '<td style="color:red;">';
                                                            } else {
                                                                echo '<td style="color:black;">';
                                                            }
                                                            echo '$'.number_format($saldo);
                                                        ?>
                                                    </td>
                                                    <td><?php echo $rowSaldo[17]; ?></td>
                                                    <td>
                                                        <?php if($Permisos_Consultar==1 || $useradmon){ //Permisos para Consultar/Ver Registro ?> 
                                                            <button title="Ver" onclick="VerSolicitudGasto('<?php echo $rowSaldo[0];?>','<?php echo $datosResponsables;?>')"
                                                                class="btn btn-default btn-sm" data-toggle="modal" data-target="#VerSolicitudGasto"><span
                                                                    class="glyphicon glyphicon-info-sign" style="color:blue;"></span></button>
                                                        <?php } ?> 

                                                        <?php if($Permisos_Insertar==1 || $useradmon){ //Permisos para Crear/Insertar Legalizacion de gastos ?> 
                                                            <a href="../legalizaciongastos/legalizardetallegasto.php?IdSG=<?php echo $rowSaldo[0];?>&IdLG=<?php echo $rowSaldo[16];?>" title="Completar Legalizacion"
                                                                class="btn btn-default btn-sm"><span class="glyphicon glyphicon-arrow-right" style="color:rgb(255, 128, 0);"></span>
                                                                <span class="glyphicon glyphicon-new-window" style="color:rgb(255, 128, 0);"></span></a>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                        <button type="button" class="btn btn-primary" id="selectmultiplesSGconsaldo">Archivar Multiples SG</button>                     
                                    </div>
                                    <?php } else { ?>
                                        <div class="alert alert-dismissible alert-info">
                                            <strong>Buen trabajo!</strong> <b class="alert-link"> - No hay </b> Solicitudes de Gatos con algun saldo pendiente.
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            
                        <!-- Fin Lista de Solicitudes con Saldo --> 

                        <!-- Inicio Lista de Solicitudes Saldo=0 pendientes por cerrar -->
                                 
                            <div class="panel panel-primary">
                                <div class="panel-heading" style="height:55px;">
                                    Legalizacion de Gastos <b>con Saldo=0 pendientes por Archivar</b>
                                    <div style="float:right;"><button type="button" class="btn btn-primary"><span class="badge">
                                        <?php echo $ListaSGsinsaldo->num_rows;?></span></button>
                                    </div>
                                </div>
                                <div class="panel-body">
                                <?php  if ($ListaSGsinsaldo->num_rows > 0) { ?>
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover dataTables-ConSaldo" id="dataTables-SinSaldo">
                                            <thead>
                                                <th><input type="checkbox" id="ArchivartodasSGsinsaldo" name="ArchivartodasSGsinsaldo" ></th>
                                                <th>#</th>
                                                <th>Fecha Solicitud</th>
                                                <th>Municipio</th>
                                                <th>Valor Solicitud</th>
                                                <th>Valor Aprobado (A)</th>
                                                <th>Valor Legalizado (L)</th>
                                                <th>Saldo (A-L)</th>
                                                <th>Usuario</th>
                                                <th><span class='glyphicon glyphicon-cog' title='Config'></span></th>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    while($rowSaldo=$ListaSGsinsaldo->fetch_array()){
                                                        
                                                        $ListaResponsables=$InstSolicGastos->ListaResponsablesxIDsolicitud($rowSaldo[0]);
                                                        $datosResponsables='';
                                                        while ($rowR=$ListaResponsables->fetch_array()) {
                                                            $datosResponsables=$datosResponsables."||".$rowR[2];
                                                        }
                                                ?>
                                                <tr class="odd gradeX">
                                                    <td><input type="checkbox" id="ArchivarsinsaldoSG_<?php echo $rowSaldo[0];?>" name="ArchivarsinsaldoSG"  value="<?php echo $rowSaldo[0];?>"></td>
                                                    <td><?php echo $rowSaldo[0]; ?></td>
                                                    <td><?php echo $rowSaldo[1]; ?></td>
                                                    <td><?php echo $rowSaldo[13]; ?></td>
                                                    <td><?php echo '$'.number_format($rowSaldo[9]); ?></td>
                                                    <td><?php echo '$'.number_format($rowSaldo[15]); ?></td>
                                                    <td><?php echo '$'.number_format($rowSaldo[14]); ?></td>
                                                    
                                                        <?php 
                                                            $saldo=($rowSaldo[15]-$rowSaldo[14]);
                                                            if ($saldo < 0) {
                                                                echo '<td style="color:red;">';
                                                            } else {
                                                                echo '<td style="color:black;">';
                                                            }
                                                            echo '$'.number_format($saldo);
                                                        ?>
                                                    </td>
                                                    <td><?php echo $rowSaldo[17]; ?></td>
                                                    <td>
                                                        <?php if($Permisos_Consultar==1 || $useradmon){ //Permisos para Consultar/Ver Registro ?> 
                                                            <button title="Ver" onclick="VerSolicitudGasto('<?php echo $rowSaldo[0];?>','<?php echo $datosResponsables;?>')"
                                                                class="btn btn-default btn-sm" data-toggle="modal" data-target="#VerSolicitudGasto"><span
                                                                    class="glyphicon glyphicon-info-sign" style="color:blue;"></span></button>
                                                        <?php } ?> 

                                                        <?php if($Permisos_Insertar==1 || $useradmon){ //Permisos para Crear/Insertar Legalizacion de gastos ?> 
                                                            <a href="../legalizaciongastos/legalizardetallegasto.php?IdSG=<?php echo $rowSaldo[0];?>&IdLG=<?php echo $rowSaldo[16];?>" title="Completar Legalizacion"
                                                                class="btn btn-default btn-sm"><span class="glyphicon glyphicon-arrow-right" style="color:rgb(255, 128, 0);"></span>
                                                                <span class="glyphicon glyphicon-new-window" style="color:rgb(255, 128, 0);"></span></a>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                        <button type="button" class="btn btn-primary" id="selectmultiplesSGsinsaldo">Archivar Multiples SG</button>
                                    </div>
                                    <?php } else { ?>
                                            <div class="alert alert-dismissible alert-info">
                                                <strong>Buen trabajo!</strong> <b class="alert-link"> - No hay </b> Solicitudes de Gatos con Saldo=0, pendientes por archivar.
                                            </div>
                                        <?php } ?>
                                </div>
                            </div>
                            
                        <!-- Fin Lista de Solicitudes Saldo=0 pendientes por cerrar --> 
                        
                    </div>
                </div>
                    
               <?php  include_once('../solicitud_gastos/VerSG.php'); ?>

               <div id="Mensajeconfimacionarchivar"></div>
               <div id="ModalMensajeArchivarSG"></div>

    <script src="../../assets/js/jquery-1.10.2.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script src="../../assets/js/jquery.metisMenu.js"></script>
    <script src="../../assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="../../assets/js/dataTables/dataTables.bootstrap.js"></script>
    <script src="../../assets/js/jasny-bootstrap.min.js"></script>
    
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.6.3/css/all.css' integrity='sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/' crossorigin='anonymous'>
    
    
    

    <style>
        #preview {
            border:1px solid #ddd;
            padding:5px;
            border-radius:2px;
            background:#fff;
            max-width:200px;
            
        }

        #preview img {width:100%;display:block;}
    
    </style>
    <script>
       $(document).ready(function() {
        $('#DocumentoSG').hide();
        $('.js-example-basic-multiple').select2({
                    placeholder:"Responsables",
                    allowClear: true,
                    disabled: true
                });


            $('.js-example-basic-single').select2({
                dropdownParent: $("#NuevaLegalizaciondeGasto")
            });

            
            $('#dataTables-xlegalizar').dataTable({
                "columns": [
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    { "width": "11%" }
                ],
                "language": {
                                "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Spanish.json"
                            }
            });
            $('.dataTables-ConSaldo').dataTable({
                "columns": [
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    { "width": "11%" }
                ],
                "language": {
                                "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Spanish.json"
                            }
            });

            // Check todas las SG de tabla con saldo=0
            $('#ArchivartodasSGsinsaldo').on('change', function() {
                if( $(this).is(':checked') ){
                    $(":checkbox[name=ArchivarsinsaldoSG]").prop('checked', true);
                } else {
                    $(":checkbox[name=ArchivarsinsaldoSG]").prop('checked', false);
                }
            });

            // Check todas las SG de tabla con saldo pendiente
            $('#ArchivartodasconsaldoSG').on('change', function() {
                if( $(this).is(':checked') ){
                    $(":checkbox[name=ArchivarSGconsaldo]").prop('checked', true);
                } else {
                    $(":checkbox[name=ArchivarSGconsaldo]").prop('checked', false);
                }
            });

            // Check todas las SG de tabla pendientes por LG
            $('#ArchivartodaspendientesLG').on('change', function() {
                if( $(this).is(':checked') ){
                    $(":checkbox[name=ArchivarSGpendientesLG]").prop('checked', true);
                } else {
                    $(":checkbox[name=ArchivarSGpendientesLG]").prop('checked', false);
                }
            });
            
            // Modal de confirmacion para archivar multiples SG en la tabla saldo=0     
                $("#selectmultiplesSGsinsaldo").click(function (e) { 
                    e.preventDefault();
                    var archivarmultiplesSG=[];
                    $('input[name="ArchivarsinsaldoSG"]').each(function() {
                        if (this.checked) { archivarmultiplesSG.push($(this).val()); }
                    });

                    if (archivarmultiplesSG.length > 1) {
                        var botonaceptar="<button type='button btn' class='btn btn-success' onclick='archivarmultiplessinsaldoSG(["+archivarmultiplesSG+"])'> Archivar SG <i class='fas fa-flag-checkered'></i></button>";                               
                        AlertConfirmacion("Archivar Solicitudes de Gasto","Esta seguro que desea archivar las "+archivarmultiplesSG.length+" solicitudes seleccionadas?",botonaceptar,"Mensajeconfimacionarchivar");
                    }else{
                        alert('Debe seleccionar minimo dos (2) Solicitudes para aprobacion múltiple.');
                    }
                });

                // Modal de confirmacion para archivar multiples SG en la tabla con saldo
                $("#selectmultiplesSGconsaldo").click(function (e) { 
                    e.preventDefault();
                    var archivarmultiplesSG=[];
                    $('input[name="ArchivarSGconsaldo"]').each(function() {
                        if (this.checked) { archivarmultiplesSG.push($(this).val()); }
                    });

                    if (archivarmultiplesSG.length > 1) {
                            var botonaceptar="<button type='button btn' class='btn btn-success' onclick='archivarmultiplessinsaldoSG(["+archivarmultiplesSG+"])'> Archivar SG <i class='fas fa-flag-checkered'></i></button>";                               
                        AlertConfirmacion("Archivar Solicitudes de Gasto","Esta seguro que desea archivar las "+archivarmultiplesSG.length+" solicitudes seleccionadas?",botonaceptar,"Mensajeconfimacionarchivar");
                    }else{
                        alert('Debe seleccionar minimo dos (2) Solicitudes para aprobacion múltiple.');
                    }
                });


                // Modal de confirmacion para archivar multiples SG en la tabla SG pendientes por LG
                $("#selectmultiplesSGpendienteLG").click(function (e) { 
                    e.preventDefault();
                    var archivarmultiplesSG=[];
                    $('input[name="ArchivarSGpendientesLG"]').each(function() {
                        if (this.checked) { archivarmultiplesSG.push($(this).val()); }
                    });

                    if (archivarmultiplesSG.length > 1) {
                            var botonaceptar="<button type='button btn' class='btn btn-success' onclick='archivarmultiplessinsaldoSG(["+archivarmultiplesSG+"])'> Archivar SG <i class='fas fa-flag-checkered'></i></button>";                               
                        AlertConfirmacion("Archivar Solicitudes de Gasto","Esta seguro que desea archivar las "+archivarmultiplesSG.length+" solicitudes seleccionadas?",botonaceptar,"Mensajeconfimacionarchivar");
                    }else{
                        alert('Debe seleccionar minimo dos (2) Solicitudes para aprobacion múltiple.');
                    }
                });
        });

        function archivarmultiplessinsaldoSG(SGmultiplesaarchivar) {
            var fecha=$("#fechadatetimepicker").html();
            var jsonString = JSON.stringify(SGmultiplesaarchivar);
            $.ajax({
                    type: "POST",
                    url: '../../logica/logica.php?accion=ArchivarmultiplesSG',
                    data: { SGS : jsonString,
                            fechaarchivo:fecha,
                            usuarioarchivo:<?php echo $IdUsuariologin; ?>},
                    // dataType: "dataType",
                    success: function (response) {
                    
                        if (response == 1) {
                            AlertModalmsg("verde","Acción exitosa","Solicitudes seleccionadas se archivaron con éxito !!","ModalMensajeArchivarSG");
                            location.reload();
                        } else {
                            AlertModalmsg("rojo","Error","Ocurrio un error, por favor comuniquese con soporte","ModalMensajeArchivarSG");
                        }
                    }
                });
                $("#ModalMensajeArchivarSG").delay(5000).fadeOut(500);
            }

       
        function limpiarmodal() {
            $('#DocumentoSG').hide();
            $('#IdSolicitudGastoSG').find('option:first').attr('selected', 'selected').parent('select');
            $('#ListaDetalleSGLegalizacion').html('');
            $('#BotonesGuardarLegalizacionSG').html('');
            $('#VerresponsableSGdiv2 option[value="0"]').attr('selected',true);
            $('#ObservacioneLegalizacionSGdiv').val('');
        }

    </script>
   
</body>
</html>
<?php 
        }else{
            include_once('../principal/sinpermisolistar.php');         
            }
	}else{
		header('Location:../../php_cerrar.php');
	}
?>
