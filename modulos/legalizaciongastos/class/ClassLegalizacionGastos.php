<?php 

Class Proceso_LegalizacionGastos{ 

    function __construct($InstanciaDB){ 
        $this->ConnxClass=$InstanciaDB;
     }

    function ListarLegalizacionGasto(){
        
        $sql="SELECT legalizaciongastos.*,
        aprobacionsolictudgasto.ValorAprobacion
        FROM legalizaciongastos 
        inner join aprobacionsolictudgasto on  aprobacionsolictudgasto.idSolicitudGasto=legalizaciongastos.idSolicitudGasto
        order by idLegalizacionGastos";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }
    
    function InsertarCabLegalizacionGasto($idSolicitudGasto,$FechaLegalizacionGastos,$ObservacionesLegalizacionGasto,$UsuarioLegalizacionGastos){
        $sql="INSERT INTO legalizaciongastos
        (idSolicitudGasto,FechaLegalizacionGastos,ObservacionesLegalizacionGastos,UsuarioLegalizacionGastos)
        VALUES (".$idSolicitudGasto.",'".$FechaLegalizacionGastos."','".$ObservacionesLegalizacionGasto."',".$UsuarioLegalizacionGastos.")";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->ConnxClass->link->insert_id;
    }
    
    function BuscarLegalizacionGastoxIdDetalleSG($IdDetalleRG){
        $sql="SELECT
                detallelegalizaciongasto.*,
                conceptodegasto.DesConceptodeGasto
            from detallelegalizaciongasto
                INNER JOIN conceptodegasto ON detallelegalizaciongasto.IdConceptoGastoDetalleLegalizacion=conceptodegasto.idConceptodeGasto
            where detallelegalizaciongasto.idDetalleLegalizacionGasto=".$IdDetalleRG;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado; 
    }

    function ListaCABLegalizacionGastoxIdRG($IdRG){
        $sql="SELECT legalizaciongastos.* from legalizaciongastos where legalizaciongastos.idLegalizacionGastos=".$IdRG;
        // echo $sql;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function ListaDetalleLegalizacionGastoxIddetalleSG($idDetalleSG){
        $sql="SELECT detallelegalizaciongasto.*
        from detallelegalizaciongasto
        where detallelegalizaciongasto.iddetallesolicitudGasto=".$idDetalleSG;
        // echo $sql;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function mostrarLegalizaciondegastosxDetalleIdRG($idDetalleRG){
        $sql="SELECT detallelegalizaciongasto.*,
        vereda.NombreVereda
        from detallelegalizaciongasto
         left JOIN vereda ON detallelegalizaciongasto.IdVereda=vereda.idVereda
        where detallelegalizaciongasto.idDetalleLegalizacionGasto=".$idDetalleRG;
        // echo $sql;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function ListaDetalleConceptosAdicionalesRGxIdRG($IdRG){
        $sql="SELECT detallelegalizaciongasto.*,
                vereda.NombreVereda
                from detallelegalizaciongasto
                left JOIN vereda ON detallelegalizaciongasto.IdVereda=vereda.idVereda
        where ISNULL(detallelegalizaciongasto.IdDetalleSolicitudGasto) and detallelegalizaciongasto.idLegalizacionGastos=".$IdRG;
        // echo $sql;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }
    
    function InsLineaDetalleReacionSG($idLegalizacionGastos,$IdConcepto,$NitBeneficiarioDetalleLegalizacion,$NombreBeneficiarioDetalleLegalizacion,$NumFacturaDetalleLegalizacion,$ValorFacturaDetalleLegalizacion,$PagoTCEDetalleLegalizacion,$ObservacionDetalleLegalizacion,$IddetalleSG,$EntregaRUT,$EntregaCedula,$DireccionBeneficiario,$TelefonoBeneficiario,$IdVereda){
        if ($IddetalleSG==0) {
            $IddetalleSG='null';
        } 
        
        $sql="INSERT INTO detallelegalizaciongasto
        (idLegalizacionGastos,IdConceptoGastoDetalleLegalizacion,NitBeneficiarioDetalleLegalizacion,NombreBeneficiarioDetalleLegalizacion,NumFacturaDetalleLegalizacion,ValorFacturaDetalleLegalizacion,PagoTCEDetalleLegalizacion,ObservacionDetalleLegalizacion,IdDetalleSolicitudGasto,DireccionDetalleLegalizacion,TelefonoDetalleLegalizacion,EntregaRUTDetalleLegalizacion,EntregaCopiaCedulaDetalleLegalizacion,IdVereda)
        VALUES (".$idLegalizacionGastos.",".$IdConcepto.", '".$NitBeneficiarioDetalleLegalizacion."', '".$NombreBeneficiarioDetalleLegalizacion."','".$NumFacturaDetalleLegalizacion."',".$ValorFacturaDetalleLegalizacion.", ".$PagoTCEDetalleLegalizacion.",'".$ObservacionDetalleLegalizacion."',".$IddetalleSG.",'".$DireccionBeneficiario."','".$TelefonoBeneficiario."',".$EntregaRUT.",".$EntregaCedula.",".$IdVereda.")";
        //  echo $sql;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->ConnxClass->link->insert_id;
    }

    function UpdateValorCABRG($IdRG){
        $sql="UPDATE 
                legalizaciongastos
            SET legalizaciongastos.TotalLegalizacionGastos = (
                SELECT 
                SUM(detallelegalizaciongasto.ValorFacturaDetalleLegalizacion) 
                FROM detallelegalizaciongasto 
                WHERE detallelegalizaciongasto.idLegalizacionGastos=".$IdRG."
                )
            WHERE legalizaciongastos.idLegalizacionGastos=".$IdRG;
           
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function UpdateDetalleLG($IdDetalleLG,$NitBeneficiario,$NombreBeneficiario,$NumeroFactura,$ValorFactura,$PagoTCD,$Observaciones,$EntregaRUT,$EntregaCedula,$DireccionBeneficiario,$TelefonoBeneficiario){
        $sql="UPDATE 
                detallelegalizaciongasto
            SET
                NitBeneficiarioDetalleLegalizacion='".$NitBeneficiario."',
                NombreBeneficiarioDetalleLegalizacion='".$NombreBeneficiario."',
                NumFacturaDetalleLegalizacion='".$NumeroFactura."',
                ValorFacturaDetalleLegalizacion=".$ValorFactura.",
                PagoTCEDetalleLegalizacion=".$PagoTCD.",
                ObservacionDetalleLegalizacion='".$Observaciones."',
                DireccionDetalleLegalizacion='".$DireccionBeneficiario."',
                TelefonoDetalleLegalizacion='".$TelefonoBeneficiario."',
                EntregaRUTDetalleLegalizacion=".$EntregaRUT.",
                EntregaCopiaCedulaDetalleLegalizacion=".$EntregaCedula."
            WHERE 
                detallelegalizaciongasto.idDetalleLegalizacionGasto=".$IdDetalleLG;
                // echo $sql;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function UpdateRutaImagen($IdDetalleRG,$RutaImagen){
        $sql="UPDATE
                detallelegalizaciongasto 
              SET detallelegalizaciongasto.ImagenDetalleLegalizacion = '".$RutaImagen."'
              where detallelegalizaciongasto.idDetalleLegalizacionGasto=".$IdDetalleRG;
            //   echo $sql;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

}

?>