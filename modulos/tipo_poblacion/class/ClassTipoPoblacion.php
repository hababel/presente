<?php 

Class Proceso_TipoPoblacion{

    function __construct($InstanciaDB){
        $this->ConnxClass=$InstanciaDB;
     }

    function getlastidTipoPoblacion(){
        $lastid=0;
        $sql="SELECT MAX(idTipoTarifa) AS id FROM  tipopoblacion";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        if ($this->resultado->num_rows > 0) {
            $this->lastid=$this->resultado->fetch_array(MYSQLI_BOTH);
        }
        return $this->lastid[0];
    }

    function InserttipoPoblacion($descTipoPoblacion){
        $sql="INSERT INTO  tipopoblacion (descTipoPoblacion) VALUES ('".$descTipoPoblacion."')";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function ListatipoPoblacion(){
        $sql="SELECT * FROM  tipopoblacion";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function Buscardesctipotarifa($descTipoPoblacion){
        $sql="SELECT * FROM tipopoblacion where descTipoPoblacion ='".$descTipoPoblacion."'";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function Buscartipotarifa($idTipoTarifa){
        $sql="SELECT * FROM tipotarifa where idTipoTarifa =".$idTipoTarifa;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;

    }

    function EditartipoPoblacion($idtipopoblacion, $descTipoPoblacion){
        $sql="UPDATE  tipopoblacion SET descTipoPoblacion = '".$descTipoPoblacion."' WHERE idtipopoblacion=".$idtipopoblacion;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function BuscarTipoPoblacion($idtipopoblacion){
        $sql="SELECT * from tipopoblacion WHERE tipopoblacion.idtipopoblacion= ".$idtipopoblacion;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }
    
}


?>