<?php 
	 $IdRecurso=35;
     include_once('../principal/CABparapermisos.php');
     
     /*
         Forma de colocar permisos en cada modulo.
     
         Funcion: colocar_permiso($IdUsuario,$accion,$IdRecurso)
     
         $IdUsuario: $IdUsuariologin=$_SESSION['id'];
         $accion: 1-Consultar, 2-Editar,3-Insertar,4-Eliminar,5-Listar - > Este nuemro verifica la columna de la tabal que relaciona el recurso_perfil
         $IdRecurso: Cod del recurso - Columna: CodRecurso de la tabla recurso.
         Retorna el valor que hay en l aBD (1-SI tiene permiso,0-NO tiene permiso)    
     */
     if($_SESSION['autentic']){
         if($Permisos_Listar==1 || $useradmon){
		 require_once("../conn_BD.php");
         require_once('class/ClassTipoPoblacion.php');
		 $InstanciaDB=new Conexion();
		 $InstTipoPoblacion=new Proceso_TipoPoblacion($InstanciaDB);
		 $ListaTipoPoblacion=$InstTipoPoblacion->ListatipoPoblacion();
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- <head> -->
    <?php include_once('../headScript.php'); ?>
<!-- </head> -->        
<body>
    <div id="wrapper">
        <?php 
            include_once('../headWeb.php');
            include_once("../../menu/m_principal.php");
        ?>
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="panel-body" align="right">  
                    <?php if( $Permisos_Insertar==1 || $useradmon ){ //Permiso para Insertar o Crear ?>  
                        <button type="button" class="btn btn-success btn-circle" data-toggle="modal" data-target="#modalinsertTipoPoblacion">
                            <i class="fa fa-plus fa-2x"></i>
                        </button>
                    <?php } ?>
                </div>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Tipo Poblacion
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">                                            	                               
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>ID</i></th>
                                        <th>Decripcion tipo Poblacion</th>
                                        <th><span class='glyphicon glyphicon-cog' title='Config'></span></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        while($row=$ListaTipoPoblacion->fetch_array()){
                                        $datos=$row[0]."||".$row[1];
                                    ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $row[0]; ?></td>
                                        <td><?php echo $row[1]; ?></td>
                                        <td class="center">
                                            <?php if($Permisos_Consultar==1 || $useradmon) { //Permisos para Consultar/Ver Registro ?>
                                                <button title="Edit" onclick="formeditTipoPoblacion('<?php echo $datos;?>')" class="btn btn-default btn-sm" data-toggle="modal" data-target="#modaleditTipoPoblacion"><span class="glyphicon glyphicon-pencil"></span></button>
                                            <?php } ?>
                                        </td>
                                    </tr> 
                                    <?php } ?>
                                </tbody>									
                            </table>							
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!-- Modal de insertar Tipo Poblacion nueva -->
        <div class="modal fade" id="modalinsertTipoPoblacion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title" id="exampleModalLabel">Nuevo Tipo Poblacion</h3>
                        <div class="col-md-10">
                            <div id="msgTipoPoblacion"></div>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">Descripcion Tipo Poblacion</label>
                            <input type="text" class="form-control form-control-sm" name="descTipoPoblacion" id="descTipoPoblacion" aria-describedby="tipo Poblacion" autocomplete="off" autofocus require>
                        </div>                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-primary" onclick="InsertTipoPoblacion();">Guardar</button>
                    </div>
                </div>
            </div>  
        </div>
<!-- Fin modal de insertar Tipo Poblacion nueva -->
                    
<!-- Modal para Editar Tipo Poblacion-->
        <div class="modal fade" id="modaleditTipoPoblacion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel">Editar Tipo de Poblacion</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" action="" method="post">
                        <fieldset>
                            <div id="msgEditTipoPoblac"></div>
                            <div class="form-group">
                                <label>Codigo tipo Poblacion</label>
                                <input id="IdTipoPoblacFM" name="IdTipoPoblacFM" type="text" placeholder="formeditCod" class="form-control" autocomplete="off" disabled>
                            </div>                           
                            <div class="form-group">
                                <label>Descripcion tipo Poblacion</label>
                                <input id="desTipoPoblacFM" name="desTipoPoblacFM" type="text" placeholder="formeditDesc" class="form-control" autocomplete="off" required>
                            </div>
                        </fieldset>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <?php if($Permisos_Editar==1 || $useradmon) { //Permisos para Editar/Modificar Registro ?>
                        <button class="btn btn-primary" onclick="EditarTipoPoblacion();">Grabar</button>
                    <?php } ?>
                </div>
                </div>
            </div>
        </div>
 <!-- /Modal para Editar Tipo Poblacion -->

    <script src="../../assets/js/jquery-1.10.2.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script src="../../assets/js/jquery.metisMenu.js"></script>
    <script src="../../assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="../../assets/js/dataTables/dataTables.bootstrap.js"></script>
	<script src="../../assets/js/jasny-bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });

            $('#modalinsertTipoPoblacion').on('shown.bs.modal', function(){
            $('#descTipoPoblacion').trigger('focus'); })

            $('#modaleditTipoPoblacion').on('shown.bs.modal', function(){
            $('#desTipoPoblacFM').trigger('focus'); })
            
    </script>
         <!-- CUSTOM SCRIPTS -->
    <script src="../../assets/js/custom.js"></script>
</body>
</html>
<?php }else{
            include_once('../principal/sinpermisolistar.php');         
        }
    }else{
		 header('Location:../../php_cerrar.php');
     }
     ?>