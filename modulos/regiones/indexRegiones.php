<?php 
	 $IdRecurso=27;
     include_once('../principal/CABparapermisos.php');
     
     /*
         Forma de colocar permisos en cada modulo.
 
         Funcion: colocar_permiso($IdUsuario,$accion,$IdRecurso)
 
         $IdUsuario: $IdUsuariologin=$_SESSION['id'];
         $accion: 1-Consultar, 2-Editar,3-Insertar,4-Eliminar,5-Listar - > Este nuemro verifica la columna de la tabal que relaciona el recurso_perfil
         $IdRecurso: Cod del recurso - Columna: CodRecurso de la tabla recurso.
         Retorna el valor que hay en l aBD (1-SI tiene permiso,0-NO tiene permiso)    
     */
     if($_SESSION['autentic']){
         if($Permisos_Listar==1 || $useradmon){
		 require_once("../conn_BD.php");
         require_once('class/classRegiones.php');
		 $InstanciaDB=new Conexion();
		 $insRegion=new Proceso_Region($InstanciaDB);
		 $LastidRegion=$insRegion->getlastidRegion();
		 $ListaRegion=$insRegion->ListaRegion();
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- <head> -->
    <?php include_once('../headScript.php'); ?>
<!-- </head> -->        
<body>
    <div id="wrapper">
        <?php 
            include_once('../headWeb.php');
            include_once("../../menu/m_principal.php");
        ?>
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="panel-body" align="right">  
                    <?php if( $Permisos_Insertar==1 || $useradmon ){ //Permiso para Insertar o Crear ?> 
                        <button type="button" class="btn btn-success btn-circle" data-toggle="modal" data-target="#modalinsertPrograma">
                            <i class="fa fa-plus fa-2x"></i>
                        </button>
                    <?php } ?> 
                </div>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Regiones
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">                                            	                               
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>ID</i></th>
                                        <th>Decripcion Region</th>
                                        <th><span class='glyphicon glyphicon-cog' title='Config'></span></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        while($row=$ListaRegion->fetch_array()){
                                        $datos=$row[0]."||".$row[1];
                                    ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $row[0]; ?></td>
                                        <td><?php echo $row[1]; ?></td>
                                        <td>
                                            <?php if($Permisos_Consultar==1 || $useradmon) { //Permisos para Consultar/Ver Registro ?>
                                                <button title="Edit" onclick="formeditRegion('<?php echo $datos;?>')" class="btn btn-default btn-sm" data-toggle="modal" data-target="#modaleditRegion"><span class="glyphicon glyphicon-pencil"></span></button>
                                            <?php } ?>
                                        </td>	
                                    </tr> 
                                    <?php } ?>
                                </tbody>									
                            </table>							
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal de insertar Region nueva -->
        <div class="modal fade" id="modalinsertPrograma" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title" id="exampleModalLabel">Nueva Region</h3>
                        <div class="col-md-10">
                            <div id='idRegionFM'></div>
                            <div id="msgRegion"></div>
                        </div>
                    </div>
                    <div class="modal-body">
                       
                        <div class="form-group">
                            <label for="">Descripcion Region</label>
                            <input type="text" class="form-control  form-control-sm" name="descripcionRegion" id="descripcionRegion" aria-describedby="Descripcion region"  autocomplete="off" require>
                        </div>
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-primary" onclick="InsertRegion();">Guardar</button>
                    </div>
                </div>
            </div>  
        </div>
    <!-- Fin modal de insertar Region nuevo -->
                    

    <!-- Modal para Editar Region-->
        <div class="modal fade" id="modaleditRegion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel">Editar Region</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" action="" method="post">
                        <fieldset>
                            <div id="msgEditRegion"></div>
                                                    
                            <div class="form-group">
                                <label>Descripcion Region</label>
                                <input id="descRegionFM" name="descRegionFM" type="text" placeholder="formeditDescRegion" class="form-control" autocomplete="off" required>
                            </div>
                        </fieldset>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <?php if($Permisos_Editar==1 || $useradmon) { //Permisos para Editar/Modificar Registro ?>
                        <button class="btn btn-primary" onclick="EditarRegion();">Grabar</button>
                    <?php } ?>	
                </div>
                </div>
            </div>
        </div>
        <!-- /Modal para Editar Region -->

    <script src="../../assets/js/jquery-1.10.2.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script src="../../assets/js/jquery.metisMenu.js"></script>
    <script src="../../assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="../../assets/js/dataTables/dataTables.bootstrap.js"></script>
	<script src="../../assets/js/jasny-bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });

            $('#modalinsertPrograma').on('shown.bs.modal', function(){
            $('#descripcionRegion').trigger('focus'); })

            $('#modaleditRegion').on('shown.bs.modal', function(){
            $('#descRegionFM').trigger('focus'); })


        </script>
         <!-- CUSTOM SCRIPTS -->
    <script src="../../assets/js/custom.js"></script>
</body>
</html>
<?php  }else{
            include_once('../principal/sinpermisolistar.php');         
        }
    }else{
		 header('Location:../../php_cerrar.php');
     }
     ?>