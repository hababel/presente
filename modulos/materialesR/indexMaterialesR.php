<?php 
	$IdRecurso=24;
	include_once('../principal/CABparapermisos.php');
	
	/*
		Forma de colocar permisos en cada modulo.

		Funcion: colocar_permiso($IdUsuario,$accion,$IdRecurso)

		$IdUsuario: $IdUsuariologin=$_SESSION['id'];
		$accion: 1-Consultar, 2-Editar,3-Insertar,4-Eliminar,5-Listar - > Este nuemro verifica la columna de la tabal que relaciona el recurso_perfil
		$IdRecurso: Cod del recurso - Columna: CodRecurso de la tabla recurso.
		Retorna el valor que hay en l aBD (1-SI tiene permiso,0-NO tiene permiso)    
	*/
	if($_SESSION['autentic']){
        if($Permisos_Listar==1 || $useradmon){
            require_once("../conn_BD.php");
            require_once("../materialesR/class/ClassMaterialesR.php");
            require_once("../tipo_materiales/class/ClassTipoMaterial.php");
            $InstanciaDB=new Conexion();
            $InstMaterialesR=new Proceso_MaterialesR($InstanciaDB);
            $InstTipoMateriales=new Proceso_TipoMaterial($InstanciaDB);
            $ListaMateriales=$InstMaterialesR->listarMaterialesR();
            $ListaTipoMateriales=$InstTipoMateriales->listatipomaterial();
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- <head> -->
<?php include_once('../headScript.php'); ?>

<!-- </head> -->

<body>

    <?php 
		include_once('../headWeb.php');
		include_once("../../menu/m_principal.php");
	?>


    <div id="wrapper">
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="panel-body" align="right">
                    <?php if( $Permisos_Insertar==1 || $useradmon ){ //Permiso para Insertar o Crear ?>
                        <button type="button" onclick="NuevoMaterialR();" class="btn btn-success btn-circle" data-toggle="modal" data-target="#NuevoMaterialR">
                            <i class="fa fa-plus fa-2x"></i>
                        </button>
                    <?php } ?>
                </div>
                <div class="panel panel-primary">
                    <div class="panel-heading" style="height:55px;">
                        Materiales
                    </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <th>Codigo</th>
                                        <th>Descripcion</th>
                                        
                                        <th>Costo</th>
                                        <th>Tipo de Material</th>
                                        <th><span class='glyphicon glyphicon-cog' title='Config'></span></th>
                                    </thead>
                                    <tbody>
                                        <?php
                                            while($row=$ListaMateriales->fetch_array()){
                                            $datos=$row[0]."||".$row[1]."||".$row[2]."||".$row[3]."||".$row[4]."||".$row[5]."||".$row[6]."||".$row[7]."||".$row[8]."||".$row[9];
                                        ?>
                                        <tr class="odd gradeX">
                                            <td><?php echo $row[1]; ?></td>
                                            <td><?php echo $row[2]; ?></td>
                                            
                                            <td><?php echo '$'.number_format($row[7]*$row[3]); ?></td>
                                            <td><?php echo $row[9]; ?></td>
                                            <td>
                                                <?php if($Permisos_Consultar==1 || $useradmon) { //Permisos para Consultar/Ver Registro ?>
                                                    <button title="Ver" onclick="VerMaterialesR('<?php echo $datos;?>')"
                                                        class="btn btn-default btn-sm" data-toggle="modal" data-target="#NuevoMaterialR">
                                                        <span class="glyphicon glyphicon-info-sign" style="color:blue;"></span>
                                                    </button>
                                                    <?php if($Permisos_Editar==1 || $useradmon) { //Permisos para Editar/Modificar Registro ?>
                                                        <button title="Ver" onclick="paraeditarMaterialR('<?php echo $datos;?>')"
                                                            class="btn btn-default btn-sm" data-toggle="modal" data-target="#NuevoMaterialR">
                                                            <span class="glyphicon glyphicon-pencil"></span>
                                                        </button>
                                                    <?php } ?>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>                                   
                </div>
            </div>
        </div>
    </div>


    <!-- Inicio Modal Insertar Nuevo Material -->

        <div class="modal fade" id="NuevoMaterialR" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
            style="overflow:hidden;overflow-y:auto;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 align="center" class="modal-title" id="TituloModalMaterialesR"></h3>
                    </div>
                    <div id="msgMaterialR"></div>
                    <div class="panel-body">
                        <div class="row col-sm-12">
                            <div class="form-group">
                                <label for="">Codigo Material</label>
                                <input type="text" class="form-control" name="CodMaterialR" id="CodMaterialR" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="">Descripcion Material</label>
                                <input type="text" class="form-control" name="DescripcionMaterialR" id="DescripcionMaterialR" autocomplete="off">
                            </div>
                            
                                <div class="col-sm-4 form-group">
                                    <label for="">Cantidad</label>
                                    <input type="number" class="form-control" name="CantidadMaterialR" id="CantidadMaterialR" autocomplete="off" value="1" min="1"  max="1000" step="1">
                                </div>
                                <div class="col-sm-8 form-group">
                                    <label for="">Costo Unitario Material</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" class="form-control" name="CostounitMaterialR" id="CostounitMaterialR" autocomplete="off" value="1" min="1">
                                    </div>
                                </div>
                           

                            <!-- <div class="form-group VerMaterialesR">
                                <label for="">Saldo</label>
                                <input type="text" class="form-control form-control-sm" name="SaldoMaterialesR" id="SaldoMaterialesR" aria-describedby="helpId" placeholder="">
                            </div> -->
                            <div class="form-group VerMaterialesR">
                                <label for="">Costo Total</label>
                                <input type="text" class="form-control" name="CostoTotalMaterialesR" id="CostoTotalMaterialesR" aria-describedby="helpId" placeholder="">
                            </div>

                            <div class="form-group">
                                <label for="">Tipo de Material </label>                 
                                <select class="form-control" name="CodTipoMaterialR" id="CodTipoMaterialR" style="width:100%;">
                                    <option value="00"> -- Seleccione Tipo de Material -- </option>
                                    <?php
                                        															
                                        while ($rowTMat=$ListaTipoMateriales->fetch_array(MYSQLI_BOTH)) { 
                                            echo "<option value='".$rowTMat[0]."'>".$rowTMat[1]."</option>";
                                            }
                                        ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div id="BotonAccionMaterialesR"></div>
                    </div>
                </div>
            </div>
        </div>
    <!-- Final Modal Nuevo Material R-->

    <script src="../../assets/js/jquery-1.10.2.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script src="../../assets/js/jquery.metisMenu.js"></script>
    <script src="../../assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="../../assets/js/dataTables/dataTables.bootstrap.js"></script>
    <script src="../../assets/js/jasny-bootstrap.min.js"></script>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

    <style>
        table {
            width: 70%;
            margin: 0 auto;
        }

        .modal-body {
            max-height: calc(100vh - 210px);
            overflow-y: auto;
        }
    </style>

    <script>
        $(document).ready(function () {
            
                $('#dataTables-example').dataTable();
                $('#CodTipoMaterialR').select2();
                
        });
    
        $('#NuevoMaterialR').on('shown.bs.modal', function(){
            $('#CodMaterialR').trigger('focus');
        })
        
    </script>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

   
</body>

</html>
<?php }else{
            include_once('../principal/sinpermisolistar.php');         
        }
	}else{
		header('Location:../../php_cerrar.php');
	}
?>