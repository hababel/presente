<?php 


$IdRecurso=40;
include_once('../principal/CABparapermisos.php');

/*
    Forma de colocar permisos en cada modulo.

    Funcion: colocar_permiso($IdUsuario,$accion,$IdRecurso)

    $IdUsuario: $IdUsuariologin=$_SESSION['id'];
    $accion: 1-Consultar, 2-Editar,3-Insertar,4-Eliminar,5-Listar - > Este nuemro verifica la columna de la tabal que relaciona el recurso_perfil
    $IdRecurso: Cod del recurso - Columna: CodRecurso de la tabla recurso.
    Retorna el valor que hay en l aBD (1-SI tiene permiso,0-NO tiene permiso)    
*/
if($_SESSION['autentic']){
    if($Permisos_Listar==1 || $useradmon){
		 require_once("../conn_BD.php");
         require_once('class/classEncuestraIfra.php');
         require_once("../../modulos/funciones.php");
         require_once("../municipios/class/ClassMunicipios.php");
         require_once("../veredas/class/classVeredas.php");
         require_once("../tipo_encuestas/class/ClassTipoEncuestasInf.php");
         require_once("../fuentes/class/classFuentes.php");
         require_once("../dotaciones/class/classDotaciones.php");
         require_once("../contacto/class/classContacto.php");
         require_once("../departamentos/class/ClassDepartamentos.php");

         require_once("../filemodulos/class/classFileModulos.php"); // Clase para adjuntar archivos

		 $InstanciaDB=new Conexion();
         $InstMunicipios=new Proceso_Municipios($InstanciaDB);
         $InstVeredas=new Proceso_Vereda($InstanciaDB);
         $InstTipoMaterialesEncuesta=new Proceso_TipoEncInfr($InstanciaDB);
         $InstFuentesAbastecimientoAgua=new Proceso_Fuentes($InstanciaDB);
         $InstDotacion=new Proceso_Dotaciones($InstanciaDB);
         $InstContactos=New Proceso_Contacto($InstanciaDB);
         $InstEncuestaInfra=new Proceso_EncuestaInfra($InstanciaDB);
         $InstDepartamento=new Proceso_Departamento($InstanciaDB);
         $InstFileModulos= new Proceso_FileModulos($InstanciaDB); // Instancia para adjuntar archivos
         
         
         $ListaMaterialesEinfra=$InstTipoMaterialesEncuesta->ListaTipoEncInfraes();
         $ListaAbastecimientoAgua=$InstFuentesAbastecimientoAgua->listaFuentes();
         $ListaElemetosDotacion=$InstDotacion->listaDotaciones();
         $ListadeDocentes=$InstContactos->listaContactoxClasificacion(1);
         $ListaEncuestraInfra=$InstEncuestaInfra->ListarEncuestasInfra();
         $ListaDepartamentos=$InstDepartamento->ListaDepartamento();

         $rutaActual = getcwd();	//Obtener ruta actual													
		 $DirActual=basename($rutaActual); //Obtener ruta absoluta actual
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- <head> -->
    <?php include_once('../headScript.php'); ?>
<!-- </head> -->        
<body>
    <div id="wrapper">
        <?php 
            include_once('../headWeb.php');
            include_once("../../menu/m_principal.php");
        ?>
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="panel-body" align="right">
                    <?php if( $Permisos_Insertar==1 || $useradmon ){ //Permiso para Insertar o Crear ?> 
                        <button type="button" class="btn btn-success btn-circle" onclick="NuevaEInfra();" data-toggle="modal" data-target="#modalinsertEInfra">
                            <i class="fa fa-plus fa-2x"></i>
                        </button>
                    <?php } ?>
                </div>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Encuestra Infraestructura
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">                                            	                               
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Fecha</th>
                                    <th>Municipio</th>
                                    <th>Vereda</th>
                                    <th>IER_CER</th>
                                    <th>Docente</th>
                                    <th><span class='glyphicon glyphicon-cog' title='Config'></span>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        while ($row=$ListaEncuestraInfra->fetch_array()) {
                                            $datosEncuestaInfra=$row[0]."||".$row[1]."||".$row[2]."||".$row[3]."||".$row[4]."||".$row[5]."||".$row[6]."||".$row[7]."||".$row[8]."||".$row[9]."||".$row[10]."||".$row[11]."||".$row[12]."||".$row[13]."||".$row[14]."||".$row[15]."||".$row[16]."||".$row[17]."||".$row[18]."||".$row[19]."||".$row[20]."||".$row[21]."||".$row[22]."||".$row[23]."||".$row[24]."||".$row[25]."||".$row[26]."||".$row[27]."||".$row[28]."||".$row[29]."||".$row[30];
                                        ?>
                                        <tr class="odd gradeX">
                                            <td><?php echo $row[0]; ?></td>
                                            <td><?php echo $row[1]; ?></td>
                                            <td><?php echo $row[32]; ?></td>
                                            <td><?php echo $row[33]; ?></td>
                                            <td><?php echo $row[4]; ?></td>
                                            <td><?php echo $row[6]; ?></td> 								             
                                            <td class="center">
                                            <?php if($Permisos_Consultar==1 || $useradmon) { //Permisos para Consultar/Ver Registro ?>	
                                                <div class="btn-group">
                                                    <button type="button" onclick='formeditEncuestaInfra(<?php echo  json_encode($row);?>);' class="btn btn-default btn-sm" data-toggle="modal" data-target="#modalinsertEInfra"><span class="glyphicon glyphicon-pencil"></span></button>


                                                    <!-- Boton para subir archivos -->
                                                    <?php 
                                                            $ListaFilexDoc=$InstFileModulos->ListFilexOrigen_idDoc($DirActual,$row[0]);
                                                            $CantFileDoc=$ListaFilexDoc->num_rows;
                                                            
                                                    ?>
                                                    
                                                    <button type="button"
                                                        onclick="AbrilModalUploadFile('<?php echo $DirActual; ?>','<?php echo $row[0];?>')"
                                                        class="btn btn-default btn-sm" data-toggle="modal"
                                                        data-target="#UploadFile" >
                                                        
                                                        <span id="ImagenFiles<?php echo $row[0];?>" <?php if($CantFileDoc>0){ echo 'style="color:DodgerBlue;"';}else{echo 'style="color:Gray;"';}?> class="glyphicon glyphicon-paperclip"></span>
                                                    </button>
                                                    <!-- Fin Boton adjuntar archivos -->
                                                    
                                                </div>
                                            <?php } ?>
                                            </td>
                                        </tr>
                                    <?php }?> 
                                </tbody>									
                            </table>							
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal de insertar Encuesta de Infraestructura nueva -->
        <div class="modal fade" id="modalinsertEInfra" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 align="center" class="modal-title" id="TituloEncInfra"></h3>
                        <div class="col-md-10">
                            <div id="msgEinfra"></div>
                        </div>
                    </div>
                    <form role="form" action="" id="Q_A" method="POST">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label for="">Fecha </label>
                                        <div class="form-group">
                                            <div class='input-group date datetime' id='datetimepicker1'>
                                                <input type='text' class="form-control" id="fechaEInfra" value="<?php echo $fechadatetimepicker;?>" autocomplete="off">
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                        <?php
                                            $TipoCalendario='Fecha';
                                            include('../../Insertfecha.php');
                                        ?>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Departamento</label>
                                        <select class="form-control Select2" id="CodDepartamentoEInfra" style="width:100%;" autocomplete="off" required>
                                            <option value="0">--- Selecione Departamento ---</option>
                                            <?php 
                                                while ($rowDept=$ListaDepartamentos->fetch_array()) {
                                                    echo "<option value='".$rowDept[0]."'>".$rowDept[2]."</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Municipio</label>
                                        <select class="form-control Select2" id="CodMunicipioEInfra" style="width:100%;" autocomplete="off" required>
                                        </select>
                                    </div> 
                                    <div class="form-group">
                                        <label for="">Vereda</label>
                                        <select class="form-control Select2" id="CodVeredaEInfra" style="width:100%;" required>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <div style="width:65%;display: inline-block;margin-right:10px;">
                                            <label for="">IER_CER</label>
                                            <input type="text" class="form-control" name="IER_CEREInfra" id="IER_CEREInfra" placeholder="">
                                        </div>
                                        <div style="width:30%;display: inline-block;">Tiene Escritura Legalizada? 
                                            <!-- <input type="checkbox" id="" checked data-toggle="toggle" data-on="SI" data-off="NO" data-onstyle="success" data-offstyle="danger"> -->
                                                <select class="form-control" name="IER_CERLegalEInfra" id="IER_CERLegalEInfra">
                                                    <option value="0">No</option>
                                                    <option value="1">SI</option>
                                                </select>
                                        </div>
                                    </div>
                                    <div class="form-group">    
                                            <label for="">Número estudiantes</label>
                                            <input type="text" class="form-control" name="NumEEInfra" id="NumEEInfra">
                                    </div>
                                </div>
                                <div></div>
                                <div class="col-sm-5">          
                                    <div class="form-group">
                                            <label for="">Modo de acceso</label>
                                            <input type="text" class="form-control" name="ModoAccesoEInfra" id="ModoAccesoEInfra">
                                    </div>
                                    <div class="form-group">
                                            <div style="width:25%;display: inline-block;">
                                                Hay Huerta Escolar? 
                                                <select class="form-control" name="HayHuertaEscolarEInfra" id="HayHuertaEscolarEInfra">
                                                    <option value="0">No</option>
                                                    <option value="1">SI</option>
                                                </select>
                                            </div>                                           
                                            <div style="width:70%;display: inline-block;">
                                                <!-- <input type="checkbox" id="HayHuertaEscolarEInfra" checked data-toggle="toggle" data-on="SI" data-off="NO" data-onstyle="success" data-offstyle="danger"> -->
                                                <label>Usos de la Huerta Escolar</label>
                                                <input type="text" class="form-control" name="UsoHuertaEInfra" id="UsoHuertaEInfra" placeholder="" style="width:100%;">
                                            </div>
                                    </div>
                                    <div class="panel panel-default">
                                                <div class="panel-heading clearfix">
                                                    <h3 class="panel-title">Datos Docente</h3>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="form-group">
                                                        <label class="control-label">Nombre</label> <!-- Nombre docente -->
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
                                                            <input type="text" class="form-control" name="nombre_docente" id="nombre_docente">
                                                        </div>
                                                        <label for="">Telefono</label> <!-- Telefono docente -->
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><span class="glyphicon glyphicon-earphone"></span></div>
                                                            <input type="tel" class="form-control" name="telefono_docente" id="telefono_docente">
                                                        </div>
                                                        <label for="">Correo</label> <!-- Correo docente -->
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></div>
                                                            <input type="email" class="form-control" name="correo_docente" id="correo_docente" placeholder="Ej.: usuario@servidor.com">
                                                        </div>
                                                    </div>
                                                </div>
                                    </div>    
                                </div> 
                             </div>
                            <div><hr></div>
                            <div class="row"> 
                                <div class="col-sm-12"> <!-- ************ Agua Potable ************ --> 
                                    <div class="text-center">
                                        <label>Agua Potable</label>
                                    </div>
                                    <div class="container-fluid">
                                        <div style="width:20%;display: inline-block;">
                                            Hay Agua Potable?
                                            <select class="form-control" name="HayAguaPotableEInfra" id="HayAguaPotableEInfra" style="width:40%;">
                                                <option value="0">No</option>
                                                <option value="1">SI</option>
                                            </select>
                                            <!-- <input type="checkbox" id="HayAguaPotableEInfra" checked data-toggle="toggle" data-on="SI" data-off="NO" data-onstyle="success" data-offstyle="danger"> -->
                                        </div>
                                        <div style="width:20%;display: inline-block;">
                                            Agua de Baños es la misma que Toman ?
                                            <select class="form-control" name="FuenteAguaBanosEInfra" id="FuenteAguaBanosEInfra" style="width:40%;">
                                                <option value="0">No</option>
                                                <option value="1">SI</option>
                                            </select>
                                            <!-- <input type="checkbox" id="FuenteAguaBanosEInfra" checked data-toggle="toggle" data-on="SI" data-off="NO" data-onstyle="success" data-offstyle="danger"> -->
                                        </div>
                                        <div style="width:25%;display: inline-block;">
                                            Usos del Agua potable
                                            <input type="text" class="form-control" name="UsoAbastecimientoAguaEInfra" id="UsoAbastecimientoAguaEInfra" style="width:100%;">
                                        </div>
                                        <div style="width:25%;display: inline-block;">
                                            Fuente Agua Potable
                                            <select class="form-control" id="FuenteAbasAguaEInfra" multiple style="width:100%;">
                                                <?php 
                                                    while ($rowLAAgua=$ListaAbastecimientoAgua->fetch_array()) {
                                                        echo "<option value='".$rowLAAgua[0]."'>".$rowLAAgua[1]."</option>";
                                                    }
                                                ?>
                                            </select> 
                                        </div>      
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <label for="">Observaciones de acueducto y baños</label>
                                        <textarea class="form-control" name="ObservacionesAcueductoybanosEInfra" id="ObservacionesAcueductoybanosEInfra" maxlength="200" autocomplete="off" style="resize: none;width:100%;" required rows="3"></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-12"><hr></div>
                                <div class="col-sm-12"> <!-- ************** Matriz de Estados **************** -->
                                    <div class="row form-group">
                                        <div class="col-sm-2 radioEInfra text-center"><label class="form-label">Concepto</label></div>
                                        <div class="col-sm-4 radioEInfra text-center"><label class="form-label">Tipo/Material</label></div>
                                        <div class="col-sm-1 radioEInfra text-center"><label class="form-label">Bueno</label></div>
                                        <div class="col-sm-1 radioEInfra text-center"><label class="form-label">Regular</label></div>
                                        <div class="col-sm-1 radioEInfra text-center"><label class="form-label">Malo</label></div>
                                        <div class="col-sm-1 radioEInfra text-center"><label class="form-label">No tiene</label></div>     
                                    </div>
                                    <!----- *********** Techo Material/Estado ************* ---->
                                    <div class="row form-group"> 
                                        <div class="col-sm-2 radioEInfra text-left"><label class="form-label">Material Techo</label></div>        
                                        <div class="col-sm-4 radioEInfra text-right">
                                            <select class="form-control" id="TipoMaterialTechoEinfra" style="width:100%;" required>
                                                <option value="0">--- Selecione material Techo ---</option>
                                                <?php 
                                                    while ($rowTMTEinfra=$ListaMaterialesEinfra->fetch_array()) {
                                                        echo "<option value='".$rowTMTEinfra[0]."'>".$rowTMTEinfra[1]."</option>";
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-sm-1 radioEInfra text-center custom-control custom-radio"><label class="custom-control custom-radio">                                        
                                            <input type="radio" name="EstadoMaterialTechoEinfra" id="EstadoMaterialTechoEinfra3" value="3" class="custom-control-input form-radio">
                                            </label></div>
                                        <div class="col-sm-1 radioEInfra text-center"><label class="custom-control custom-radio">
                                            <input type="radio" name="EstadoMaterialTechoEinfra" id="EstadoMaterialTechoEinfra2" value="2" class="custom-control-input form-radio">
                                            </label></div>
                                        <div class="col-sm-1 radioEInfra text-center"><label class="custom-control custom-radio">
                                            <input type="radio" name="EstadoMaterialTechoEinfra" id="EstadoMaterialTechoEinfra1" value="1" class="custom-control-input form-radio">
                                            </label></div>
                                        <div class="col-sm-1 radioEInfra text-center"><label class="custom-control custom-radio">
                                            <input type="radio" name="EstadoMaterialTechoEinfra" id="EstadoMaterialTechoEinfra0" value="0" class="custom-control-input form-radio">
                                            </label>                                            
                                        </div>     
                                    </div>
                                    <!-- *********** Paredes Material/Estado *************  -->
                                    <div class="row form-group">
                                        <div class="col-sm-2 radioEInfra text-left"><label class="form-label">Material Paredes</label></div>        
                                        <div class="col-sm-4 radioEInfra text-right">
                                            <select class="form-control" id="TipoMaterialParedesEinfra" style="width:100%;" required>
                                                <option value="0">--- Selecione material Paredes ---</option>
                                                <?php 
                                                    mysqli_data_seek($ListaMaterialesEinfra, 0);
                                                    while ($rowTMTEinfra=$ListaMaterialesEinfra->fetch_array()) {
                                                        echo "<option value='".$rowTMTEinfra[0]."'>".$rowTMTEinfra[1]."</option>";
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-sm-1 radioEInfra text-center"><label class="custom-control custom-radio">
                                            <input type="radio" name="EstadoMaterialParedesEinfra" id="EstadoMaterialParedesEinfra3" value="3" class="custom-control-input form-radio">
                                            </label>
                                        </div>
                                        <div class="col-sm-1 radioEInfra text-center"><label class="custom-control custom-radio">
                                            <input type="radio" name="EstadoMaterialParedesEinfra" id="EstadoMaterialParedesEinfra2" value="2" class="custom-control-input form-radio">
                                            </label></div>
                                        <div class="col-sm-1 radioEInfra text-center" ><label class="custom-control custom-radio">
                                            <input type="radio" name="EstadoMaterialParedesEinfra" id="EstadoMaterialParedesEinfra1" value="1" class="custom-control-input form-radio">
                                            </label></div>
                                        <div class="col-sm-1 radioEInfra text-center"><label class="custom-control custom-radio">
                                            <input type="radio" name="EstadoMaterialParedesEinfra" id="EstadoMaterialParedesEinfra0" value="0" class="custom-control-input form-radio">
                                            </label>
                                        </div>     
                                    </div>
                                     <!-- *********** Electricidad Material/Estado *************  -->
                                    
                                     <div class="row form-group">
                                        <div class="col-sm-2 radioEInfra text-left"><label class="form-label">Electricidad</label></div>        
                                        <div class="col-sm-4 radioEInfra text-left">
                                            <div class="text-left" style="display: inline-block;width:70%;">
                                                Tiene Electricidad?
                                            </div>
                                            <div class="text-right" style="display: inline-block;">
                                                <select class="form-control" name="HayElecEInfra" id="HayElecEInfra" onchange="validatecheck(this)">
                                                    <option value=0>NO</option>
                                                    <option value=1 >SI</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-1 radioEInfra text-center"><label class="custom-control custom-radio">
                                            <input type="radio" name="EstadoElecEInfra" id="EstadoElecEInfra3" value="3" class="custom-control-input form-radio">
                                            </label>
                                        </div>
                                        <div class="col-sm-1 radioEInfra text-center"><label class="custom-control custom-radio">
                                            <input type="radio" name="EstadoElecEInfra" id="EstadoElecEInfra2" value="2" class="custom-control-input form-radio">
                                            </label></div>
                                        <div class="col-sm-1 radioEInfra text-center" ><label class="custom-control custom-radio">
                                            <input type="radio" name="EstadoElecEInfra" id="EstadoElecEInfra1" value="1" class="custom-control-input form-radio">
                                            </label></div>
                                        <div class="col-sm-1 radioEInfra text-center"><label class="custom-control custom-radio glyp">
                                            <input type="radio" name="EstadoElecEInfra" id="EstadoElecEInfra0" value="0" class="custom-control-input form-radio">
                                            </label>
                                        </div>     
                                    </div>



                                    <!-- *********** Alcantarrilado Material/Estado *************  -->
                                    <div class="row form-group">
                                        <div class="col-sm-2 radioEInfra text-left"><label class="form-label">Alcantarillado</label></div>        
                                        <div class="col-sm-4 radioEInfra text-left">
                                            <div class="text-left" style="display: inline-block;width:70%;">
                                                Tiene Alcantarrilado?
                                            </div>
                                            <div class="text-right" style="display: inline-block;">
                                                <select class="form-control" name="HayAlcantarilladoEInfra" id="HayAlcantarilladoEInfra" onchange="validatecheck(this)">
                                                    <option value=0 >NO</option>
                                                    <option value=1 >SI</option>
                                                </select> 
                                                <!-- <input type="checkbox" id="HayAlcantarilladoEInfra" checked data-toggle="toggle" data-on="SI" data-off="NO" data-onstyle="success" data-offstyle="danger"> -->
                                            </div>
                                        </div>
                                        <div class="col-sm-1 text-center"><label class="control-label">
                                            <input type="radio" name="EstadoAlcantarilladoEInfra" id="EstadoAlcantarilladoEInfra3" value="3">
                                            </label>
                                        </div>
                                        <div class="col-sm-1 text-center"><label class="control-label">
                                            <input type="radio" name="EstadoAlcantarilladoEInfra" id="EstadoAlcantarilladoEInfra2" value="2">
                                            </label></div>
                                        <div class="col-sm-1 text-center" ><label class="control-label">
                                            <input type="radio" name="EstadoAlcantarilladoEInfra" id="EstadoAlcantarilladoEInfra1" value="1">
                                            </label></div>
                                        <div class="col-sm-1 text-center"><label class="control-label">
                                            <input type="radio" name="EstadoAlcantarilladoEInfra" id="EstadoAlcantarilladoEInfra0" value="0">
                                            </label>
                                        </div>     
                                    </div>
                                    <!-- *********** Pozo Septico Material/Estado *************  -->
                                    <div class="row form-group">
                                        <div class="col-sm-2 radioEInfra text-left"><label class="form-label">Pozo Septico</label></div>        
                                        <div class="col-sm-4 radioEInfra">
                                            <div class="text-left" style="display: inline-block;width:70%">
                                                Tiene Pozo Septico?
                                            </div>
                                            <div class="text-right" style="display: inline-block;">
                                                <select class="form-control" name="HayPozoSepticoEInfra" id="HayPozoSepticoEInfra" onchange="validatecheck(this)">
                                                    <option value=0 >NO</option>
                                                    <option value=1 >SI</option>
                                                </select> 
                                                <!-- <input type="checkbox" id="HayPozoSepticoEInfra" checked data-toggle="toggle" data-on="SI" data-off="NO" data-onstyle="success" data-offstyle="danger"> -->
                                            </div>
                                        </div>
                                        <div class="col-sm-1 radioEInfra text-center"><label class="custom-control custom-radio">
                                            <input type="radio" name="EstadoPozoSepticoEinfra" id="EstadoPozoSepticoEinfra3" value="3" class="custom-control-input form-radio">
                                            </label>
                                        </div>
                                        <div class="col-sm-1 radioEInfra text-center"><label class="custom-control custom-radio">
                                            <input type="radio" name="EstadoPozoSepticoEinfra" id="EstadoPozoSepticoEinfra2" value="2" class="custom-control-input form-radio">
                                            </label></div>
                                        <div class="col-sm-1 radioEInfra text-center" ><label class="custom-control custom-radio">
                                            <input type="radio" name="EstadoPozoSepticoEinfra" id="EstadoPozoSepticoEinfra1" value="1" class="custom-control-input form-radio">
                                            </label></div>
                                        <div class="col-sm-1 radioEInfra text-center"><label class="control-label">
                                            <input type="radio" name="EstadoPozoSepticoEinfra" id="EstadoPozoSepticoEinfra0" value="0" class="custom-control-input form-radio">
                                            </label>
                                        </div>     
                                    </div>
                                    <!-- *********** Elementos de Dotacion Estado *************  -->
                                    <div class="row form-group">
                                        <div class="col-sm-2 radioEInfra text-left"><label class="form-label">Elementos de dotación</label></div>        
                                        <div class="col-sm-4 radioEInfra text-right">
                                            <select class="form-control" id="EleDotacionEInfra" multiple style="width:100%;">
                                            <?php 
                                                while ($rowEDEInfra=$ListaElemetosDotacion->fetch_array()) {
                                                    echo "<option value='".$rowEDEInfra[0]."'>".$rowEDEInfra[1]."</option>";
                                                }
                                            ?>
                                            </select>
                                        </div>
                                        <div class="col-sm-1 radioEInfra text-center"><label class="custom-control custom-radio">
                                            <input type="radio" name="EstadoEleDotacionEInfra" id="EstadoEleDotacionEInfra3" value="3" class="custom-control-input form-radio">
                                            </label>
                                        </div>
                                        <div class="col-sm-1 radioEInfra text-center"><label class="custom-control custom-radio">
                                            <input type="radio" name="EstadoEleDotacionEInfra" id="EstadoEleDotacionEInfra2" value="2" class="custom-control-input form-radio">
                                            </label></div>
                                        <div class="col-sm-1 radioEInfra text-center" ><label class="custom-control custom-radio">
                                            <input type="radio" name="EstadoEleDotacionEInfra" id="EstadoEleDotacionEInfra1" value="1" class="custom-control-input form-radio">
                                            </label></div>
                                        <div class="col-sm-1 radioEInfra text-center"><label class="custom-control custom-radio">
                                            <input type="radio" name="EstadoEleDotacionEInfra" id="EstadoEleDotacionEInfra0" value="0" class="custom-control-input form-radio">
                                            </label>
                                        </div>     
                                    </div>
                                </div>
                                <div class="col-sm-12"><hr></div>
                                <div class="col-sm-12"> <!-- ************* Observaciones ********* -->
                                    <div class="form-group">
                                        <label for="">Observaciones de Tecnologia</label>
                                        <textarea class="form-control" name="ObservacionesTecnologiaEInfra" id="ObservacionesTecnologiaEInfra" maxlength="200" autocomplete="off" style="resize: none;width:100%;" rows="3"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Otras Observaciones</label>
                                        <textarea class="form-control" name="ObservacionesOtrosEInfra" id="ObservacionesOtrosEInfra" maxlength="200" autocomplete="off" style="resize: none;width:100%;" rows="3"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>               
                        <div class="modal-footer" id="footer_EnInfra">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                <?php if($Permisos_Editar==1 || $useradmon) { //Permisos para Editar/Modificar Registro ?>
                                    <button type="button" class="btn btn-primary" onclick="InsertarEncuestraInfra();" id="BotonGuardarEInfra">Guardar</button>
                                <?php } ?>
                        </div>
                    </form>
                </div>
            </div>  
        </div>
    <!-- Fin modal de insertar Encuesta de Infraestructura nuevo -->

    
    <!--Mensajes de error-->
    <div id="Mensajes"></div>
    
    <?php include_once ('../filemodulos/indexFilemodulos.php'); //Codigo modal de adjuntar archivos ?>

    <script src="../../assets/js/jquery-1.10.2.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script src="../../assets/js/jquery.metisMenu.js"></script>
    <script src="../../assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="../../assets/js/dataTables/dataTables.bootstrap.js"></script>
	<script src="../../assets/js/jasny-bootstrap.min.js"></script>
    <!-- Select 2 -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    
    <!-- Iconos  -->
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.6.3/css/all.css' integrity='sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/' crossorigin='anonymous'>
    
    <!-- Boton toggle -->
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

        <script>
            $(document).ready(function () {
                
                 $('#dataTables-example').dataTable({
                    "order": [[ 1, "asc" ]]
                } );

                $('#FuenteAbasAguaEInfra').select2();
                $('#EleDotacionEInfra').select2();

                 $('.Select2').select2({
				    dropdownParent: $("#modalinsertEInfra")
                });

                $('#CodDepartamentoEInfra').change(function () { 
                    var CodDepartamentoSG=$('#CodDepartamentoEInfra').val();
                    if (CodDepartamentoSG != 0) {
						var parametros={CodDepartamentoSG};
						$.ajax({
							type: "POST",
							url: "../../logica/logica.php?accion=LlenaSelectMunicipioSegunDepartamento",
							data: parametros,
							cache: false,
							dataType: "json",
							success: function (data) {
								$('#CodMunicipioEInfra').html(data);
							}
						});   
                	} 
                    
                });

                $("#CodMunicipioEInfra").change(function () { 
                    var CodMunicipioSelect=$('#CodMunicipioEInfra').val();
                    // alert(CodMunicipioSelect);
                    var parametros={CodMunicipioSelect};
                    $.ajax({
                        type: "POST",
                        url: "../../logica/logica.php?accion=LlenaSelectVeredasSegunMunicipio",
                        data: parametros,
                        cache: false,
                        dataType: "json",
                        success: function (data) {
                            $('#CodVeredaEInfra').html(data);
                        }
                    });   
                });
            });

            $('#modalinsertEInfra').on('show.bs.modal', event => {
                var button = $(event.relatedTarget);
                if($('#HayElecEInfra').val()==0){
                    $('input[name="EstadoElecEInfra"][id="EstadoElecEInfra0"]').prop('checked',true);
                    $('input[name="EstadoElecEInfra"]').prop('disabled',true);
                }
                if($('#HayAlcantarilladoEInfra').val()==0){
                    $('input[name="EstadoAlcantarilladoEInfra"][id="EstadoAlcantarilladoEInfra0"]').prop('checked',true);
                    $('input[name="EstadoAlcantarilladoEInfra"]').prop('disabled',true);
                }
                if($('#HayPozoSepticoEInfra').val()==0){
                    $('input[name="EstadoPozoSepticoEinfra"][id="EstadoPozoSepticoEinfra0"]').prop('checked',true);
                    $('input[name="EstadoPozoSepticoEinfra"]').prop('disabled',true);
                }
            
            });

            function validatecheck(option){
                switch (option.id) {

                    case 'HayElecEInfra':
                        if (option.value==0) {
                            $('input[name="EstadoElecEInfra"][id="EstadoElecEInfra0"]').prop('checked',true);
                            $('input[name="EstadoElecEInfra"]').prop('disabled',true);
                        } else {
                            $('input[name="EstadoElecEInfra"][id="EstadoElecEInfra0"]').prop('checked',false);
                            $('input[name="EstadoElecEInfra"]').prop('disabled',false);
                            $('input[name="EstadoElecEInfra"][id="EstadoElecEInfra0"]').prop('disabled',true); 
                        }
                    break;

                    case 'HayAlcantarilladoEInfra':
                        if (option.value==0) {
                            $('input[name="EstadoAlcantarilladoEInfra"][id="EstadoAlcantarilladoEInfra0"]').prop('checked',true);
                            $('input[name="EstadoAlcantarilladoEInfra"]').prop('disabled',true);
                        } else {
                            $('input[name="EstadoAlcantarilladoEInfra"][id="EstadoAlcantarilladoEInfra0"]').prop('checked',false);
                            $('input[name="EstadoAlcantarilladoEInfra"]').prop('disabled',false);
                            $('input[name="EstadoAlcantarilladoEInfra"][id="EstadoAlcantarilladoEInfra0"]').prop('disabled',true); 
                        }
                    break;

                    case 'HayPozoSepticoEInfra':
                        if (option.value==0) {
                            $('input[name="EstadoPozoSepticoEinfra"][id="EstadoPozoSepticoEinfra0"]').prop('checked',true);
                            $('input[name="EstadoPozoSepticoEinfra"]').prop('disabled',true); 
                        } else {
                            $('input[name="EstadoPozoSepticoEinfra"][id="EstadoPozoSepticoEinfra0"]').prop('checked',false);
                            $('input[name="EstadoPozoSepticoEinfra"]').prop('disabled',false);
                            $('input[name="EstadoPozoSepticoEinfra"][id="EstadoPozoSepticoEinfra0"]').prop('disabled',true);
                        }
                    break;

                    default:

                        break;
                }
            }
    </script>

    
         <!-- CUSTOM SCRIPTS -->
    <script src="../../assets/js/custom.js"></script>
</body>
</html>
<?php  }else{
            include_once('../principal/sinpermisolistar.php');         
        }
    }else{
		 header('Location:../../php_cerrar.php');
     }
     ?>