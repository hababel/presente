<?php

Class Proceso_EncuestaInfra{


    function __construct($InstanciaDB){
        $this->ConnxClass=$InstanciaDB;
     }

        function ListarEncuestasInfra(){
            $sql="SELECT encuestainfraestructura.*,
            municipio.NombreMunicipio,
            vereda.NombreVereda
            FROM encuestainfraestructura           
            INNER JOIN municipio on municipio.idMunicipio=encuestainfraestructura.idMunicipio
            INNER JOIN vereda on vereda.idVereda=encuestainfraestructura.idVereda
            ORDER BY encuestainfraestructura.FechaEncuestaInfraestructura ASC";
            $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
            return $this->resultado;
        }

        function InsertarEncuentraInfra(
            $FechaEncuestaInfraestructura,$idMunicipio,$idVereda,$IER_CER,
            $CERLegalizadoEncuestaInfraestructura,$Nombre_DocenteEncuestaInfra,$Email_DocenteEncuestaInfra,$Telefono_DocenteEncuestaInfra,
            $NumEstudiantesEncuestaInfraestructura,$ModoAccesoEncuestaInfraestructura,
            $TipoMaterialTecho,$EstadoMaterialTecho,$TipoMaterialParedes,$EstadoMaterialParedes,
            $AguaPotable,$FuenteAguaBanos,$UsoAbastecimientodeAgua,$ObservacionesAcueductoybanos,
            $AlcantarilladoEncuestaInfraestructura,$EstadoAlcantarilladoEncuestaInfraestructura,
            $PozoSeptico,$EstadoPozoSeptico,$electricidadEncInf,$estadoElectricidadEncInf,
            $EstadoElementodeDotacion,$HuertaEscolar,$UsosHuertaEscolar,
            $ObservacionesTecnologia,$ObservacionesOtros,$idDepartamento){

            $sql="INSERT INTO encuestainfraestructura 
            (FechaEncuestaInfraestructura,idMunicipio,idVereda,IER_CER,
            CERLegalizadoEncuestaInfraestructura,Nombre_DocenteEncuestaInfra,Email_DocenteEncuestaInfra,Telefono_DocenteEncuestaInfra,
            NumEstudiantesEncuestaInfraestructura,ModoAccesoEncuestaInfraestructura,
            TipoMaterialTecho,EstadoMaterialTecho,TipoMaterialParedes,EstadoMaterialParedes,
            AguaPotable,FuenteAguaBanos,UsoAbastecimientodeAgua,ObservacionesAcueductoybanos,
            AlcantarilladoEncuestaInfraestructura,EstadoAlcantarilladoEncuestaInfraestructura,
            PozoSeptico,EstadoPozoSeptico,electricidadEncInf,estadoElectricidadEncInf,
            EstadoElementodeDotacion,HuertaEscolar,UsosHuertaEscolar,
            ObservacionesTecnologia,ObservacionesOtros,idDepartamento)
            VALUES ('".$FechaEncuestaInfraestructura."',".$idMunicipio.",".$idVereda.",
            '".$IER_CER."',".$CERLegalizadoEncuestaInfraestructura.",
            '".$Nombre_DocenteEncuestaInfra."','".$Email_DocenteEncuestaInfra."','".$Telefono_DocenteEncuestaInfra."',
            ".$NumEstudiantesEncuestaInfraestructura.",'".$ModoAccesoEncuestaInfraestructura."',
            ".$TipoMaterialTecho.",".$EstadoMaterialTecho.",
            ".$TipoMaterialParedes.",".$EstadoMaterialParedes.",
            ".$AguaPotable.",".$FuenteAguaBanos.",'".$UsoAbastecimientodeAgua."',
            '".$ObservacionesAcueductoybanos."',
            ".$AlcantarilladoEncuestaInfraestructura.",".$EstadoAlcantarilladoEncuestaInfraestructura.",          
            ".$PozoSeptico.",".$EstadoPozoSeptico.",
            ".$electricidadEncInf.",".$estadoElectricidadEncInf.",
            ".$EstadoElementodeDotacion.",".$HuertaEscolar.",
            '".$UsosHuertaEscolar."','".$ObservacionesTecnologia."','".$ObservacionesOtros."',".$idDepartamento.")";
            $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
            return $this->ConnxClass->link->insert_id;
        }
        
        //IdPerteneceaCampoEI=0 --> El detalle viene de Fuentes de Abastecimiento
        // IdPerteneceaCampoEI=1 -- > El detalle viene de elementos de dotacion.
        Function InseratrDetalleEncuestaInfra($idEncuestraInfra,$IdDatoDetalleEncuestaInfra,$IdPerteneceaCampoEI){
            $sql="INSERT INTO detalleencuestrainfra(IdEncuestaInfra,IdDatoDetalleEncuestaInfra,IdPerteneceaCampoEI) VALUES(".$idEncuestraInfra.",".$IdDatoDetalleEncuestaInfra.",".$IdPerteneceaCampoEI.")";
            $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
            return $this->resultado;
        } 

        //ValorfiltroCampo=0 --> El detalle viene de Fuentes de Abastecimiento
        //ValorfiltroCampo=1 -- > El detalle viene de elementos de dotacion.
        function ListarDettaleEInfraxIdPerteneceaCampoEI($IdEInfra,$ValorfiltroCampo){
            $sql="SELECT * FROM detalleencuestrainfra where IdPerteneceaCampoEI=".$ValorfiltroCampo." and IdEncuestaInfra=".$IdEInfra;
            $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
            return $this->resultado;
        }

        function ListarEncuestaInfraxIdReg($idEncuestaInfraestructura){
            $sql='SELECT * FROM encuestainfraestructura where idEncuestaInfraestructura='.$idEncuestaInfraestructura;
            $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
            return $this->resultado;
        }

        function UpdateEncuestrInfra(
            $idEncuestaInfraestructura,
            $fechaEInfra,
            $CodMunicipioEInfra,
            $CodVeredaEInfra,
            $IER_CEREInfra,
            $IER_CERLegalEInfra,
            $nombre_docente,
            $telefono_docente,
            $correo_docente,
            $NumEEInfra,
            $ModoAccesoEInfra,
            $TipoMaterialTechoEinfra,
            $EstadoMaterialTechoEinfra,
            $TipoMaterialParedesEinfra,
            $EstadoMaterialParedesEinfra,
            $HayAguaPotableEInfra,
            $FuenteAguaBanosEInfra,
            $UsoAbastecimientoAguaEInfra,
            $ObservacionesAcueductoybanosEInfra,
            $HayAlcantarilladoEInfra,
            $EstadoAlcantarilladoEInfra,
            $HayPozoSepticoEInfra,
            $EstadoPozoSepticoEinfra,
            $hayElectricidadEncInf,
            $estadoElectricidadEncInf,
            $EstadoEleDotacionEInfra,
            $HayHuertaEscolarEInfra,
            $UsoHuertaEInfra,
            $ObservacionesTecnologiaEInfra,
            $ObservacionesOtrosEInfra,
            $CodDepartamentoEInfra
          ){
            $sql="UPDATE encuestainfraestructura SET 
            FechaEncuestaInfraestructura='".$fechaEInfra."',
            idMunicipio=".$CodMunicipioEInfra.",
            idVereda=".$CodVeredaEInfra.",
            IER_CER='".$IER_CEREInfra."',
            CERLegalizadoEncuestaInfraestructura=".$IER_CERLegalEInfra.",
            Nombre_DocenteEncuestaInfra='".$nombre_docente."',
            Email_DocenteEncuestaInfra='".$correo_docente."',
            Telefono_DocenteEncuestaInfra='".$telefono_docente."',
            NumEstudiantesEncuestaInfraestructura=".$NumEEInfra.",
            ModoAccesoEncuestaInfraestructura='".$ModoAccesoEInfra."',
            TipoMaterialTecho=".$TipoMaterialTechoEinfra.",
            EstadoMaterialTecho=".$EstadoMaterialTechoEinfra.",
            TipoMaterialParedes=".$TipoMaterialParedesEinfra.",
            EstadoMaterialParedes=".$EstadoMaterialParedesEinfra.",
            AguaPotable=".$HayAguaPotableEInfra.",
            FuenteAguaBanos=".$FuenteAguaBanosEInfra.",
            UsoAbastecimientodeAgua='".$UsoAbastecimientoAguaEInfra."',
            ObservacionesAcueductoybanos='".$ObservacionesAcueductoybanosEInfra."',
            AlcantarilladoEncuestaInfraestructura=".$HayAlcantarilladoEInfra.",
            EstadoAlcantarilladoEncuestaInfraestructura=".$EstadoAlcantarilladoEInfra.",
            PozoSeptico=".$HayPozoSepticoEInfra.",
            EstadoPozoSeptico=".$EstadoPozoSepticoEinfra.", 
            electricidadEncInf=".$hayElectricidadEncInf.",
            estadoElectricidadEncInf=".$estadoElectricidadEncInf.",
            EstadoElementodeDotacion=".$EstadoEleDotacionEInfra.",
            HuertaEscolar=".$HayHuertaEscolarEInfra.",
            UsosHuertaEscolar='".$UsoHuertaEInfra."',          
            ObservacionesTecnologia='".$ObservacionesTecnologiaEInfra."',
            ObservacionesOtros='".$ObservacionesOtrosEInfra."',
            idDepartamento=".$CodDepartamentoEInfra." 
            where idEncuestaInfraestructura=".$idEncuestaInfraestructura;
            $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
            return $this->resultado;

        }
    }
    ?>