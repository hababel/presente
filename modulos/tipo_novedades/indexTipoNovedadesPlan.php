<?php 
	$IdRecurso=34;
	include_once('../principal/CABparapermisos.php');
	
	/*
		Forma de colocar permisos en cada modulo.
	
		Funcion: colocar_permiso($IdUsuario,$accion,$IdRecurso)
	
		$IdUsuario: $IdUsuariologin=$_SESSION['id'];
		$accion: 1-Consultar, 2-Editar,3-Insertar,4-Eliminar,5-Listar - > Este nuemro verifica la columna de la tabal que relaciona el recurso_perfil
		$IdRecurso: Cod del recurso - Columna: CodRecurso de la tabla recurso.
		Retorna el valor que hay en l aBD (1-SI tiene permiso,0-NO tiene permiso)    
	*/
	if($_SESSION['autentic']){
		if($Permisos_Listar==1 || $useradmon){
		require_once("../conn_BD.php");
		require_once('../tipo_novedades/class/classTipoNovedadesPlan.php');
		$InstanciaDB=new Conexion();
		$InstTipoNovedadesPlan=new Proceso_TipoNovedadesPla($InstanciaDB);
		$ListaTipoNovedadesPlan=$InstTipoNovedadesPlan->listatipoNovedadesPlan();
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- <head> -->
    <?php include_once('../headScript.php'); ?>
<!-- </head> -->        
<body>
    <div id="wrapper">
	<?php 
		include_once('../headWeb.php');
		include_once("../../menu/m_principal.php");
	?>
        <div id="page-wrapper" >
            <div id="page-inner">						                
                 <!-- /. ROW  -->               
                <div class="panel-body" align="right">                                                                                 
					<?php if( $Permisos_Insertar==1 || $useradmon ){ //Permiso para Insertar o Crear ?> 
						<button type="button" class="btn btn-success btn-circle" data-toggle="modal" data-target="#ModalNuevoTipoNovedadesPlan">
							<i class="fa fa-plus fa-2x"></i>
						</button>
					<?php } ?>
                </div>
            <div class="row">
                <div class="col-md-12">
	<!-- Advanced Tables -->
	<div class="panel panel-primary">
		<div class="panel-heading">
				Tipo de NovedadesPlan
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover" id="dataTables-example">
					<thead>
						<tr>
							<th>Id</th>
							<th>Descripcion</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
							while ($row=$ListaTipoNovedadesPlan->fetch_array()) {
								$datosTipoNovedadesPlan=$row[0]."||".$row[1];?>
							
								<tr class="odd gradeX">
									<td><?php echo $row[0]; ?></td>
									<td><?php echo $row[1]; ?></td>                                                                                  
									<td class="center">
										<div class="btn-group">
											<?php if($Permisos_Consultar==1 || $useradmon) { //Permisos para Consultar/Ver Registro ?>
												<button type="button" onclick="formeditTipoNovedadesPlan('<?php echo $datosTipoNovedadesPlan;?>')" class="btn btn-success" data-toggle="modal" data-target="#ModalEditarTipoNovedadesPlanFM"><i class="fa fa-edit"></i></button>
											<?php } ?>
										</div>
									</td>
								</tr>
						<?php }?>       	
						
		<!--  Modal Nuevo Tipo de NovedadesPlan-->
			<div class="modal fade" id="ModalNuevoTipoNovedadesPlan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<form name="form2" method="post" enctype="multipart/form-data" action="">											
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>															
								<h3 align="center" class="modal-title" id="myModalLabel">Nuevo Tipo de NovedadesPlan</h3>
							</div>
							<div id="msgTipoNovedadesPlanNuevo"></div>
							<div class="panel-body">
							<div class="row">                                       
								<div class="col-md-6">
									
									<div class="form-group">
										<label for="">Descripcion Tipo de NovedadesPlan</label>
										<input class="form-control" name="DescTipoNovedadesPlan" id="DescTipoNovedadesPlan" aria-describedby="emailHelpId" autocomplete="off" required>
									</div>
									
								</div>
																										
							</div> 
							</div> 
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
								<button type="button" onclick="InsertTipoNovedadesPlan();" class="btn btn-primary">Guardar</button>
							</div>										 
						</div>
					</div>
				</form>
			</div>
		<!-- End Modal Nuevo Tipo de NovedadesPlan-->

			<!--  Modal Editar Tipo de NovedadesPlan-->
			<div class="modal fade" id="ModalEditarTipoNovedadesPlanFM" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>															
								<h3 align="center" class="modal-title" id="myModalLabel">Actualizar Tipo de NovedadesPlan</h3>
								<div id="msgEditTipoNovedadesPlanFM"></div>
							</div>
							<div class="panel-body">
								<div class="row">                                       
									<div class="col-md-6">																												
										<label>ID</label>
										<input class="form-control" id="IdTipoNovedadesPlanFM" name="IdTipoNovedadesPlanFM" disabled>
										<label>Descripcion Tipo de NovedadesPlan</label>
										<input class="form-control" id="descTipoNovedadesPlanFM" name="descTipoNovedadesPlanFM" autocomplete="off" required>
										
										
									</div>                                                                       
								</div> 
							</div> 
							<div class="modal-footer">
								<div align="right">
									<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
									<?php if($Permisos_Editar==1 || $useradmon) { //Permisos para Editar/Modificar Registro ?>
										<button type="submit" class="btn btn-primary" onclick="EditarTipoNovedadesPlanFM();">Guardar</button>
									<?php } ?>
								</div> 
							</div>										 
						</div>
					</div>
				</div>
			<!-- End Modal Editar TipoNovedadesPlan-->
                                    </tbody>									
                                </table>							
                            </div>                            
                        </div>
                    </div>
                    <!--End Advanced Tables -->
                </div>
            </div>                                
        </div>               
    </div>
  </div>
         <!-- /. PAGE WRAPPER  -->
     <!-- /. WRAPPER  -->
    <script src="../../assets/js/jquery-1.10.2.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script src="../../assets/js/jquery.metisMenu.js"></script>
    <script src="../../assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="../../assets/js/dataTables/dataTables.bootstrap.js"></script>
	<script src="../../assets/js/jasny-bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });

			$('#ModalNuevoTipoNovedadesPlan').on('shown.bs.modal', function(){
			$('#DescTipoNovedadesPlan').trigger('focus'); })

			$('#ModalEditarTipoNovedadesPlanFM').on('shown.bs.modal', function(){
			$('#descTipoNovedadesPlanFM').trigger('focus'); })

    </script>
         <!-- CUSTOM SCRIPTS -->
    <script src="../../assets/js/custom.js"></script>
</body>
</html>
<?php }else{
            include_once('../principal/sinpermisolistar.php');         
        }
	}else{
		header('Location:../../php_cerrar.php');
	}
?>