<?php 

Class Proceso_InformeInstalacion{

    function __construct($InstanciaDB){
        $this->ConnxClass=$InstanciaDB;
     }

    function ListarInformeInstalacion(){
        $sql="SELECT informeinstalacion.*,
        municipio.NombreMunicipio,
        entidadvinculada.NombreEntidadVinculada,
        vereda.NombreVereda,
        institucion.NombreInstitucion
        FROM informeinstalacion
        INNER JOIN municipio ON informeinstalacion.municipio=municipio.idMunicipio
        INNER JOIN entidadvinculada ON informeinstalacion.patrocinador=entidadvinculada.idEntidadVinculada 
        INNER JOIN vereda ON informeinstalacion.vereda=vereda.idVereda
        INNER JOIN institucion ON informeinstalacion.instEducativas=institucion.idInstitucion
        order by informeinstalacion.idInfInstalac DESC";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function BuscarInformeInstalacionxId($idInfInstalac){
        $sql="SELECT informeinstalacion.*,
        municipio.NombreMunicipio,
        entidadvinculada.NombreEntidadVinculada,
        vereda.NombreVereda,
        institucion.NombreInstitucion
        FROM informeinstalacion
        INNER JOIN municipio ON informeinstalacion.municipio=municipio.idMunicipio
        INNER JOIN entidadvinculada ON informeinstalacion.patrocinador=entidadvinculada.idEntidadVinculada 
        INNER JOIN vereda ON informeinstalacion.vereda=vereda.idVereda
        INNER JOIN institucion ON informeinstalacion.instEducativas=institucion.idInstitucion
        WHERE idInfInstalac=".$idInfInstalac;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function InsertarInformeInstalacion($dpto,$mcpio,$patrocinador,$fecha,$vereda,$centroEducativoRural,
      $NrofamiliasProgramadas,$NrofamiliasEntregadas,$instEducativas,$nombre_docente,$telefono_docente,$correo_docente,$observaciones,$IdUsuarioCreacion){
        $sql="INSERT INTO informeinstalacion (municipio,patrocinador,fecha,vereda,centroEducativoRural,
        NrofamiliasProgramadas,NrofamiliasEntregadas,instEducativas,nombre_docente,telefono_docente,correo_docente,observaciones,departamento,IdUsuarioCreacion)
         VALUES (".$mcpio.",".$patrocinador.",'".$fecha."',".$vereda.",'".$centroEducativoRural."',
         ".$NrofamiliasProgramadas.",".$NrofamiliasEntregadas.",".$instEducativas.",'".$nombre_docente."','".$telefono_docente."','".$correo_docente."','".$observaciones."',".$dpto.",".$IdUsuarioCreacion.")";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->ConnxClass->link->insert_id;
    }

    function EditarInformeInstalacion($idInfInstalac,$municipio,$patrocinador,$fecha,$vereda,$centroEducRural,$NroFamilProgram,$NroFamilEntreg,
       $InstEducativas,$nombre_docente,$telefono_docente,$correo_docente,$observaciones,$departamento){        
        $sql="UPDATE informeInstalacion SET 
        municipio=".$municipio.",
        patrocinador=".$patrocinador.",
        fecha='".$fecha."',
        vereda=".$vereda.",
        centroEducativoRural=".$centroEducRural.",
        NrofamiliasProgramadas=".$NroFamilProgram.",
        NrofamiliasEntregadas=".$NroFamilEntreg.",
        InstEducativas=".$InstEducativas.",
        nombre_docente='".$nombre_docente."',
        telefono_docente='".$telefono_docente."',
        correo_docente='".$correo_docente."',
        observaciones='".$observaciones."',
        departamento=".$departamento."
       
        WHERE idInfInstalac=".$idInfInstalac;
        
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function DonantesxMunicipio($IdMunicipio){
        $sql="SELECT *
        FROM asignaciondonante
        INNER JOIN detalleasignaciondonante ON asignaciondonante.idAsignacionDonante=detalleasignaciondonante.idAsignacionDonante
        INNER JOIN entidadvinculada on asignaciondonante.idEntidadVinculadaAsignacionDonante=entidadvinculada.idEntidadVinculada
        WHERE detalleasignaciondonante.idMunicipio=".$IdMunicipio;
        // echo $sql;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function AgregarDetalleInfoInstalacion($idCabInformeInst,
        $idMaterial_EmpleadoInformeInst,
        $idTipoPoblacionInformeInst,$CantEntregadaInformeInst,$FuenteInfoInformeInst){
            $sql="INSERT INTO detalleinformeinstalacion (idCabInformeInst,idMaterial_EmpleadoInformeInst,
            idTipoPoblacionInformeInst,CantEntregadaInformeInst,FuenteInfoInformeInst) VALUES (".$idCabInformeInst.",".$idMaterial_EmpleadoInformeInst.",
        ".$idTipoPoblacionInformeInst.",".$CantEntregadaInformeInst.",".$FuenteInfoInformeInst.")";
            $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
            return $this->resultado;
    }

    function ListarDetalleInfoInstalacionxCABxFuente($idCabInformeInst,$FuenteInfoInformeInst){ //Muestra el detalle del documento segun el orgien 0 - si es responsable, 1 - si es materiales
        if ($FuenteInfoInformeInst==0) {
            $sql="SELECT detalleinformeinstalacion.idDetalleInformeInst,
            detalleinformeinstalacion.idCabInformeInst,
            detalleinformeinstalacion.idMaterial_EmpleadoInformeInst,
            empleado.NombreEmpleado
            FROM detalleinformeinstalacion
            INNER JOIN empleado ON empleado.idEmpleado=detalleinformeinstalacion.idMaterial_EmpleadoInformeInst
            WHERE detalleinformeinstalacion.idCabInformeInst=".$idCabInformeInst." and detalleinformeinstalacion.FuenteInfoInformeInst=0 ORDER BY empleado.NombreEmpleado";
        } else {
            $sql="SELECT detalleinformeinstalacion.*,
            archivodematerial.DescArchivodeMaterial,
            tipopoblacion.descTipoPoblacion
            FROM detalleinformeinstalacion 
            INNER join archivodematerial ON archivodematerial.idArchivodeMaterial=detalleinformeinstalacion.idMaterial_EmpleadoInformeInst
            INNER JOIN tipopoblacion ON tipopoblacion.idtipopoblacion=detalleinformeinstalacion.idTipoPoblacionInformeInst
            WHERE idCabInformeInst=".$idCabInformeInst." and FuenteInfoInformeInst=1";
        }
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function EliminarDetalleInformeInst($idRegDetalleInformeInst){
        $sql="DELETE FROM detalleinformeinstalacion WHERE idDetalleInformeInst=".$idRegDetalleInformeInst;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function UpdateDetalleInfoInstalacion($idCabInformeInst,$idMaterial_EmpleadoInformeInst,
      $TipoPidTipoPoblacionInformeInstoblacion,$CantEntregadaInformeInst,$FuenteInfoInformeInst){
        $sql="UPDATE detalleinformeinstalacion SET idMaterial_EmpleadoInformeInst=".$idMaterial_EmpleadoInformeInst.",
        TipoPidTipoPoblacionInformeInstoblacion=".$TipoPidTipoPoblacionInformeInstoblacion.",CantEntregadaInformeInst=".$CantEntregadaInformeInst;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function EliminarDetalleInformeInfraResponsables($idCabInformeInst){
        $sql="DELETE FROM detalleinformeinstalacion WHERE detalleinformeinstalacion.FuenteInfoInformeInst=0 and detalleinformeinstalacion.idCabInformeInst=".$idCabInformeInst;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }



// **************  Tabla Detalle Temporal *************
 
    function InsertarDetalleInformeInstTMP($idMaterialDetalleInformeInstTMP,$idTipoPoblacionDetalleInformeInstTMP,$cantDetalleInformeInstTMP,$idSesionInformeInstTMP){
        $sql="INSERT INTO tmp_detalleinformeinstalacion (idMaterialDetalleInformeInstTMP,idTipoPoblacionDetalleInformeInstTMP,cantDetalleInformeInstTMP,idSesionInformeInstTMP)
        VALUES (".$idMaterialDetalleInformeInstTMP.",".$idTipoPoblacionDetalleInformeInstTMP.",".$cantDetalleInformeInstTMP.",'".$idSesionInformeInstTMP."')";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function EliminarDetalleInformeInstTMP($idRegDetalleInformeInstTMP,$idSesionInformeInstTMP){
        $sql="DELETE FROM tmp_detalleinformeinstalacion WHERE idRegDetalleInformeInstTMP=".$idRegDetalleInformeInstTMP." and idSesionInformeInstTMP='".$idSesionInformeInstTMP."'";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function ListarDetalleInformeInstTMP($idSesionInformeInstTMP){
        $sql="SELECT tmp_detalleinformeinstalacion.*,
        archivodematerial.DescArchivodeMaterial,
        tipopoblacion.descTipoPoblacion
        FROM tmp_detalleinformeinstalacion
        INNER JOIN archivodematerial ON archivodematerial.idArchivodeMaterial=tmp_detalleinformeinstalacion.idMaterialDetalleInformeInstTMP
        INNER JOIN tipopoblacion ON tipopoblacion.idtipopoblacion=tmp_detalleinformeinstalacion.idTipoPoblacionDetalleInformeInstTMP
        WHERE tmp_detalleinformeinstalacion.idSesionInformeInstTMP='".$idSesionInformeInstTMP."'";
        
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function UpdateCantInformeInstTMP($idMaterialDetalleInformeInstTMP,$cantDetalleInformeInstTMP,$idTipoPoblacionInformeInst,$idSesionInformeInstTMP){
        $sql="UPDATE tmp_detalleinformeinstalacion SET tmp_detalleinformeinstalacion.cantDetalleInformeInstTMP=".$cantDetalleInformeInstTMP." WHERE tmp_detalleinformeinstalacion.idMaterialDetalleInformeInstTMP=".$idMaterialDetalleInformeInstTMP." and tmp_detalleinformeinstalacion.idTipoPoblacionDetalleInformeInstTMP=".$idTipoPoblacionInformeInst." and tmp_detalleinformeinstalacion.idSesionInformeInstTMP='".$idSesionInformeInstTMP."'";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function BuscarxIdMaterial($idMaterialDetalleInformeInstTMP,$idTipoPoblacionInformeInst,$idSesionInformeInstTMP){
        $sql="SELECT * FROM tmp_detalleinformeinstalacion WHERE tmp_detalleinformeinstalacion.idMaterialDetalleInformeInstTMP=".$idMaterialDetalleInformeInstTMP." and tmp_detalleinformeinstalacion.idTipoPoblacionDetalleInformeInstTMP=".$idTipoPoblacionInformeInst." and tmp_detalleinformeinstalacion.idSesionInformeInstTMP='".$idSesionInformeInstTMP."'";
        
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;

    }

    function vaciarTMPDetalleInformeInfra($Id_Session){
        $sql="DELETE FROM tmp_detalleinformeinstalacion WHERE tmp_detalleinformeinstalacion.idSesionInformeInstTMP='".$Id_Session."'";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }
}
?>