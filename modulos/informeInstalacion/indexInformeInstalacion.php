<?php 
	$IdRecurso=45;
	include_once('../principal/CABparapermisos.php');
	
	/*
		Forma de colocar permisos en cada modulo.

		Funcion: colocar_permiso($IdUsuario,$accion,$IdRecurso)

		$IdUsuario: $IdUsuariologin=$_SESSION['id'];
		$accion: 1-Consultar, 2-Editar,3-Insertar,4-Eliminar,5-Listar - > Este nuemro verifica la columna de la tabal que relaciona el recurso_perfil
		$IdRecurso: Cod del recurso - Columna: CodRecurso de la tabla recurso.
		Retorna el valor que hay en l aBD (1-SI tiene permiso,0-NO tiene permiso)    
	*/
	if($_SESSION['autentic']){
		$private_id=$_SESSION['id'];
		if($Permisos_Listar==1 || $useradmon){
		require_once("../conn_BD.php");
		require_once("../veredas/class/classVeredas.php");
		require_once("../empleados/class/classEmpleados.php");
		require_once("../entidades/class/classEntidades.php");
		require_once("../tipo_instituciones/class/ClassTipoInstitucion.php");
		require_once("../institucion/class/classInstitucion.php");
		require_once("class/classInformeInstalacion.php");
		require_once("../municipios/class/ClassMunicipios.php");
		require_once("../departamentos/class/ClassDepartamentos.php");
		require_once("../funciones.php");
		require_once("../contacto/class/classContacto.php");
		require_once("../materialesR/class/ClassMaterialesR.php");
		require_once("../tipo_poblacion/class/ClassTipoPoblacion.php");

		require_once("../filemodulos/class/classFileModulos.php"); // Clase para adjuntar archivos

		$InstanciaDB=new Conexion();

		$InstVereda=new Proceso_Vereda($InstanciaDB);
		$InstEmpleados=new Proceso_Empleados($InstanciaDB);
		$InstEntid=new Proceso_Entidades($InstanciaDB);
		$InstTipoInst=new Proceso_TipoInstitucion($InstanciaDB);
		$InstInstitucion=new Proceso_Institucion($InstanciaDB);
		$InstInformeInstalacion=new Proceso_InformeInstalacion($InstanciaDB);
		$InstMcpios=new Proceso_Municipios($InstanciaDB);
		$InstDepartamentos= new Proceso_Departamento($InstanciaDB);
		$InstContactos=New Proceso_Contacto($InstanciaDB);
		$InsMateriales= new Proceso_MaterialesR($InstanciaDB);
		$InstTipoPoblacion= new Proceso_TipoPoblacion($InstanciaDB);
		$InstFileModulos= new Proceso_FileModulos($InstanciaDB); // Instancia para adjuntar archivos
		
		$ListadeDocentes=$InstContactos->listaContactoxClasificacion(1);
		$ListaVeredas=$InstVereda->listaVereda();
		$listaEmpleados=$InstEmpleados->ListarEmpleados();
		$ListaMcpios=$InstMcpios->ListaMunicipio();
		$ListaEntidades=$InstEntid->ListarEntidades();
		$ListaTipoInst=$InstTipoInst->listatipoInstitucion();
		$ListaInstitucion=$InstInstitucion->listaInstitucion();
		$ListaInfInstalac=$InstInformeInstalacion->ListarInformeInstalacion();
		$ListaDepartamentos=$InstDepartamentos->ListaDepartamento();
		$ListaMateriales=$InsMateriales->listarMaterialesR();
		$ListaTipoPoblacion=$InstTipoPoblacion->ListatipoPoblacion();

		$rutaActual = getcwd();	//Obtener ruta actual													
		 $DirActual=basename($rutaActual); //Obtener ruta absoluta actual
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- <head> -->
    <?php include_once('../headScript.php'); ?>	
<!-- </head> -->        
<body>
		<?php 
			include_once('../headWeb.php');
			include_once("../../menu/m_principal.php");
		?>
    <div id="wrapper">
		<div id="page-wrapper" >
			<div id="page-inner">						                
					<!-- /. ROW  -->
				<div class="panel-body" align="right"> 
					<?php if( $Permisos_Insertar==1 || $useradmon ){ //Permiso para Insertar o Crear ?>                                                               
						<button type="button" class="btn btn-success btn-circle" onclick="NuevoInformeInstalacion('<?php echo $private_id;?>');" data-toggle="modal" data-target="#ModalNuevoInfInstalacion">
							<i class="fa fa-plus fa-2x"></i>
						</button>
					<?php } ?> 																				
				</div> 
				<div class="row">
					<div class="col-md-12">
						<!-- Advanced Tables -->
						<div class="panel panel-primary">
							<div class="panel-heading">
									Informe de Instalacion
							</div>
							<div class="panel-body">
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover" id="dataTables-example">
										<thead>
											<tr>
												<th>Id</th>
												<th>Fecha</th>
												<th>Municipio</th>
												<th>Patrocinador</th>
												<th>Vereda</th>
												<th>Institucion educativa</th>
												<th><span class='glyphicon glyphicon-cog' title='Config'></span>
											</tr>
										</thead>
										<tbody>
											<?php
												
												while ($row=$ListaInfInstalac->fetch_array()) {													
												?>
											<tr class="odd gradeX">
												<td><?php echo $row[0]; ?></td>
												<td><?php echo $row[3]; ?></td>
												<td><?php echo $row[15]; ?></td>
												<td><?php echo $row[16]; ?></td>
												<td><?php echo $row[17]; ?></td>
												<td><?php echo $row[18]; ?></td>	             
												<td class="center">
													<div class="btn-group">
													<?php if($Permisos_Consultar==1 || $useradmon) { //Permisos para Consultar/Ver Registro ?>
														<button type="button" onclick='formatEditInfInstalacion(<?php echo  json_encode($row);?>)' class="btn btn-default btn-sm" data-toggle="modal" data-target="#ModalNuevoInfInstalacion"><span class="glyphicon glyphicon-pencil"></span></button>
														<!-- Boton para subir archivos -->
														<?php 
                                                            $ListaFilexDoc=$InstFileModulos->ListFilexOrigen_idDoc($DirActual,$row[0]);
                                                            $CantFileDoc=$ListaFilexDoc->num_rows;                                                            
														?>														
														<button type="button"
															onclick="AbrilModalUploadFile('<?php echo $DirActual; ?>','<?php echo $row[0];?>')"
															class="btn btn-default btn-sm" data-toggle="modal"
															data-target="#UploadFile" >															
															<span id="ImagenFiles<?php echo $row[0];?>" <?php if($CantFileDoc>0){ echo 'style="color:DodgerBlue;"';}else{echo 'style="color:Gray;"';}?> class="glyphicon glyphicon-paperclip"></span>
														</button>
														<!-- Fin Boton adjuntar archivos -->
													<?php } ?>
													</div>
												</td>
											</tr>
												<?php }?>       	
										</tbody>									
									</table>				
								</div>                            
							</div>
						</div>
								<!--End Advanced Tables -->                             
					</div>               
				</div>
			</div>
		</div>
	</div>

	<!--  Modal Nuevo Informe Instalacion-->
			<div class="modal fade" id="ModalNuevoInfInstalacion" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow:hidden;overflow-y: auto;">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>															
							<h3 align="center" class="modal-title" id="TituloModalInformeInst"></h3> 
						</div>
						<div id="msginformeInstalc"></div>
						<div class="panel-body">
							<div class="row col-sm-5">
								<div class="form-group">
									<label class="control-label">Fecha</label> <!-- fechaIns -->
									<div class="form-group">
										<div class='input-group date datetime' id='datetimepicker1'>
											<input type='text' class="form-control" id="fechaIns" value="" autocomplete="off">
											<span class="input-group-addon">
												<span class="glyphicon glyphicon-calendar"></span>
											</span>
										</div>								
									</div>
									<?php
										$TipoCalendario='Fecha';
										include('../../Insertfecha.php');
									?> 
								</div>
								<div class="form-group">
									<label class="control-label" for="exampleFormControlSelect1">Departamento</label>
									<select class="form-control js-example-basic-single" id="CodDepartamentoSelect" onChange="MunicipiosegunDepartamento(this.id,'municipio');" name="CodDepartamentoSelect" style="width:100%;">
										<option value="0">-- Selecione Departamento --</option>
										<?php 
											while ($rowDpto=$ListaDepartamentos->fetch_array()) {
												echo "<option value='".$rowDpto[0]."'>".$rowDpto[2]."</option>";
											}
										?>
									</select>
								</div>
								<div class="form-group">
									<label class="control-label" for="exampleFormControlSelect1">Municipio</label>
									<select class="form-control js-example-basic-single" id="municipio" onChange="VeredaSegunMunicipio(this.id,'idVereda');" name="municipio" style="width:100%;">										
									</select>
								</div>
								<div class="form-group">
									<label class="control-label" for="exampleFormControlSelect1">Vereda</label>
									<select class="form-control js-example-basic-single" id="idVereda"  onChange="ListaInstitucionesxVereda(this.id)" style="width:100%;">										
									</select>		
								</div>							
								<div class="form-group">
									<label class="control-label" for="exampleFormControlSelect1">Entidades vinculadas</label>
									<select class="form-control js-example-basic-single" id="patrocinador" style="width:100%;">
										<option value="0">--Seleccione una Entidad--</option>
										<?php 
											while ($rowInfInfra=$ListaEntidades->fetch_array(MYSQLI_NUM)) {
												echo "<option value='".$rowInfInfra[0]."'>".$rowInfInfra[2]."</option>";
											}
										?>									
									</select>
								</div>
								<div class="form-group">
									<label class="control-label" for="exampleFormControlSelect1">Centro Educativo Rural</label>
									<input type="text" class="form-control" name="centroEducRural" id="centroEducRural">
								</div>
							</div>
							<div class="row col-sm-2"></div>							
							<div class="row col-sm-5">
								<div class="form-group">
									<label class="control-label" for="exampleFormControlSelect1">Instituciones Educativas</label>
									<select class="form-control js-example-basic-single" id="InstEducativas" style="width:100%;">										
									</select>
								</div>
								<div class="form-group">
									<label class="control-label" for="exampleFormControlSelect1">Reponsables</label>
									<select class="form-control js-example-basic-multiple" id="responsables" multiple style="width:100%;">
										<?php 
											while ($rowEmp=$listaEmpleados->fetch_array()) {
												echo "<option value='".$rowEmp[0]."'>".$rowEmp[2]."</option>";
											}
										?>
									</select>
								</div>
								<div class="row col-sm-12">
									<div class="row">
										<div class="col-sm-8" style="padding-right: 2px;"><label class="control-label"for="">Nro. Familias Programadas</label></div>
										<div class="col-sm-4" style="padding-left: 5px;padding-right: 5px;"><input  class="form-control" name="NroFamilProgram" id="NroFamilProgram" autocomplete="off" required></div>
									</div>
									<div class="row">
										<div class="col-sm-8" style="padding-right: 2px;"><label class="control-label" for="">Nro. Familias Entregada</label></div>
										<div class="col-sm-4" style="padding-left: 5px;padding-right: 5px;"><input  class="form-control" name="NroFamilEntreg" id="NroFamilEntreg" autocomplete="off" required></div>
									</div>
								</div>
								<div class="row col-sm-12">
									<br>
									<!-- Panel de datos de docente -->
									<div class="panel panel-default">
										<div class="panel-heading clearfix">
											<h3 class="panel-title">Datos Docente</h3>
										</div>
										<div class="panel-body">
											<div class="form-group">
												<label class="control-label">Nombre</label> <!-- Nombre docente -->
												<div class="input-group">
													<div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
													<input type="text" class="form-control" name="nombre_docente" id="nombre_docente">
												</div>
												<label for="">Telefono</label> <!-- Telefono docente -->
												<div class="input-group">
													<div class="input-group-addon"><span class="glyphicon glyphicon-earphone"></span></div>
													<input type="tel" class="form-control" name="telefono_docente" id="telefono_docente">
												</div>
												<label for="">Correo</label> <!-- Correo docente -->
												<div class="input-group">
													<div class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></div>
													<input type="email" class="form-control" name="correo_docente" id="correo_docente" placeholder="Ej.: usuario@servidor.com">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row col-sm-12">
								<div class="form-group">
									<label class="control-label" for="">Observaciones</label>
									  <textarea class="form-control" name="observaciones" id="observaciones" rows="2" placeholder="Observaciones" autocomplete="off" style="resize: none;width:100%;" required></textarea>
								</div>
							</div>
							<div class="row col-sm-12">								
								<!-- *************************************************************** Insertar Detalle Informe Instalacion -->
											<!-- Row start -->
												<div class="row">
													<div class="col-md-12 col-sm-12 col-xs-12">
														<div class="panel panel-default">
															<div class="panel-heading clearfix">
																<h3 class="panel-title">Detalle Materiales Informe Instalacion</h3>
															</div>
															<div class="panel-body">
																<div class="row">
																	<table class="table table-striped  table-hover">
																		<thead>
																			<tr>
																				<th class="text-center">Item</th>
																				<th>Materiales</th>
																				<th>Tipo Poblacion</th>
																				<th class="text-center">Cantidad</th>
																				<th class="text-center">
																					<span class="glyphicon glyphicon-cog" title="Config"></span>
																					
																				</th>
																			</tr>
																		</thead>
																		<tbody id="items">																		
																		</tbody>
																	</table>
																</div>
															</div>
														</div>
													</div>
												</div>
											<!-- Row end -->

											<!-- Row start -->
												<div class="row">
													<div class="col-md-12 col-sm-12 col-xs-12">
														<div class="panel panel-default">
															<div class="panel-heading clearfix">
																<h3 class="panel-title">Insertar Material de Informe Instalación</h3>
															</div>
															<div class="panel-body">
																<div class="row">
																	<div class="col-sm-5">
																		<div class="input-group" style="width:100%;">
																			<select  class="form-control" name="idMaterialInformeInst" id="idMaterialInformeInst" style="width:100%;">
																				<option value=0>-- Seleccione un Material --</option>
																				<?php
																				while ($rowCG=$ListaMateriales->fetch_array()) {
																					echo "<option value='".$rowCG[0]."'>".$rowCG[2]."</option>";
																				} ?>
																			</select>
																		</div>
																	</div>
																	<div class="col-sm-4">
																		<div class="input-group" style="width:100%;">
																			<select  class="form-control" name="idTipoPoblacionInformeInst" id="idTipoPoblacionInformeInst" style="width:100%;">
																				<option value=0>-- Seleccione Tipo Población --</option>
																				<?php
																				while ($rowCG=$ListaTipoPoblacion->fetch_array()) {
																					echo "<option value='".$rowCG[0]."'>".$rowCG[1]."</option>";
																				} ?>
																			</select>
																		</div>
																	</div>
																	<div class="col-sm-2">
																		<div class="form-group">
																			<input type="text" class="form-control" name="CantInformeInst" id="CantInformeInst" placeholder="Cant Materiales" style="width:100%;">
																		</div>
																	</div>
																	<div class="col-md-1 col-sm-1">
																		<div class="form-group" id="agregarDetalle">
																			 
																		</div>
																	</div>

																</div>
																<div class="col-md-12" id="msgInsertDetalleInformeInstTPM"></div>
															</div>
														</div>
													</div>
												</div>
											<!-- Row end -->
	
								<!-- *************************************************************** -->

							</div>
						</div>
						
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
							<div class="col-sm-11" id="modal-footerInfInsNuevo"></div>
							<?php if($Permisos_Editar==1 || $useradmon) { //Permisos para Consultar/Ver Registro ?>	
								<div class="col-sm-12" id="modal-footerInfInsEditar"></div>
							<?php } ?>
						</div>										 
					</div>
				</div>
			</div>
			
	<!-- End Modal Nuevo Informe Instalacion-->

	
	
	<div id="ModalError_InformeInst"></div>

	<?php include_once ('../filemodulos/indexFilemodulos.php'); //Codigo modal de adjuntar archivos ?> 

    <script src="../../assets/js/jquery-1.10.2.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script src="../../assets/js/jquery.metisMenu.js"></script>
    <script src="../../assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="../../assets/js/dataTables/dataTables.bootstrap.js"></script>
	<script src="../../assets/js/jasny-bootstrap.min.js"></script>
	
    <!-- CUSTOM SCRIPTS -->
    <!-- <script src="../../assets/js/custom.js"></script> -->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
	<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.6.3/css/all.css' integrity='sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/' crossorigin='anonymous'>

	<script>
		$(document).ready(function () {

			$('#dataTables-example').dataTable({
					"order": []
				} );
				
			$('#municipio').select2();
			


			$('.js-example-basic-single').select2({
				dropdownParent: $("#ModalNuevoInfInstalacion")
				
			});

			$('#idMaterialInformeInst').select2();
			$('#idTipoPoblacionInformeInst').select2();			

			$('.js-example-basic-multiple').select2({
				dropdownParent: $("#ModalNuevoInfInstalacion")
			});

		});

		

	</script>
	
<style>
	
#div_carga{
    position:absolute;
    top:0;
    left:0;
    width:100%;
    height:100%;
	background: url(images/gris.png) repeat;
	display:none;
	z-index:1;
}
 
#cargador{
    position:absolute;
    top:50%;
    left: 50%;
    margin-top: -25px;
    margin-left: -25px;
}
</style>			
		
</body>
</html>
<?php 
		}else{
			include_once('../principal/sinpermisolistar.php');         
		}
	}else{
		header('Location:../../php_cerrar.php');
	}
?>