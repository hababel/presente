<?php 

    Class Proceso_Empleados{
    
        function __construct($InstanciaDB){
            $this->ConnxClass=$InstanciaDB;
         }
    
        function BuscarEmpleadoAjax($search){
            $sql="SELECT * FROM empleado where NombreEmpleado LIKE '%$search%'";
            $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
            return $this->resultado;
        }
        
        function listarArea(){
             $sql="SELECT * FROM area";
             $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
             return $this->resultado;
        }

        function ListarEmpleados(){
            $sql="SELECT
            empleado.idEmpleado,
            empleado.DocumentoEmpleado,
            empleado.NombreEmpleado,
            empleado.TelefonoEmpleado,
            empleado.CargoEmpleado,
            empleado.idAreaEmpleado,
            empleado.EstadoEmpleado,
            area.idArea,
            area.DescArea
            FROM empleado
            INNER JOIN area
            ON area.idArea=empleado.idAreaEmpleado
            order by NombreEmpleado ASC";
            $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
            return $this->resultado;
        }

        function InsertEmpleado($DocumentoEmpleado,$NombreEmpleado,$TelefonoEmpleado,$CargoEmpleado,$idAreaEmpleado,
        $EstadoEmpleado){
            $sql="INSERT INTO empleado (DocumentoEmpleado,NombreEmpleado,TelefonoEmpleado,CargoEmpleado,
            idAreaEmpleado,EstadoEmpleado) 
            VALUES (".$DocumentoEmpleado.",'".$NombreEmpleado."','".$TelefonoEmpleado."','".$CargoEmpleado."',".$idAreaEmpleado.",
            ".$EstadoEmpleado.")"; 
            $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
            return $this->resultado;
        }


        function BuscarEmpleado($DocumentoEmpleado){
            $sql="SELECT * FROM empleado where DocumentoEmpleado ='".$DocumentoEmpleado."'";
            $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
            return $this->resultado;
        }

        function EditarEmpleado($idEmpleado,$DocumentoEmpleado,$NombreEmpleado,$TelefonoEmpleado,$CargoEmpleado,$idAreaEmpleado,
        $EstadoEmpleado){
            $sql="UPDATE empleado SET 
            DocumentoEmpleado=".$DocumentoEmpleado.",
            NombreEmpleado='".$NombreEmpleado."',
            TelefonoEmpleado='".$TelefonoEmpleado."',
            CargoEmpleado='".$CargoEmpleado."',
            idAreaEmpleado=".$idAreaEmpleado.",
            EstadoEmpleado=".$EstadoEmpleado.",
            WHERE idEmpleado=".$idEmpleado;
            // echo $sql;
            $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
            return $this->resultado;
        }
    }

?>