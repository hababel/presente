<?php
	header('Content-Type: text/html; charset=UTF-8');
	session_start();
	require_once("../../modulos/funciones.php");
    $IdUsuariologin=$_SESSION['id']; //Id Usuario logeado
    $useradmon=$_SESSION['administrador']; // valor: true - si es superadministrador
	$Permisos_Consultar=colocar_permiso($IdUsuariologin,1,$IdRecurso); //Permisos para Consultar/Ver Registro
	$Permisos_Editar=colocar_permiso($IdUsuariologin,2,$IdRecurso); //Permisos para Editar o modificar Registro
	$Permisos_Insertar=colocar_permiso($IdUsuariologin,3,$IdRecurso); //Permiso para Insertar o Crear Registro
	$Permisos_Listar=colocar_permiso($IdUsuariologin,5,$IdRecurso); //Permiso para Listar en menu ppal o de ingreso a registro
?>