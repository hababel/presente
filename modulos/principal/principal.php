<?php 
    session_start();
    if($_SESSION['autentic']){
        $IdUsuario=$_SESSION['id'];
        require_once("../conn_BD.php");
        require_once("../../modulos/funciones.php");
        require_once("../usuarios/class/ClassUsuario.php");
        $InstanciaDB=new Conexion();
        $InstUsuario=new Proceso_Usuario($InstanciaDB);
        $DatosUsuarioLogin=$InstUsuario->usuario_empleado($IdUsuario);
        $PerfilesxUsuario=$InstUsuario->ListaPerfilesxUsuario($IdUsuario);
        $rowUsuarioEmp=$DatosUsuarioLogin->fetch_array();
                                    
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- <head> -->
<?php include_once('../headScript.php'); ?>
<!-- </head> -->  
<body>
    <div id="wrapper">
    <?php 
		include_once('../headWeb.php');
		include_once("../../menu/m_principal.php");
	?>

        <div id="page-wrapper" >
            <div id="page-inner">
                <div id="Perfil_User" class="col-sm-5">
                    <div class="panel panel-default">
                        <div class="panel-heading clearfix">
                            <h3 class="panel-title">Datos Usuario - Bienvenido </h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="control-label">Nombre: </label> <!-- Nombre Usuario -->
                                <div class="input-group">
                                    <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
                                    <input type="text" class="form-control" name="nombreuser" id="nombreuser" value="<?php echo $rowUsuarioEmp[1]; ?>">
                                </div>
                                <br>
                                <label for="">Correo:</label> <!-- Correo Usuario -->
                                <div class="input-group">
                                    <div class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></div>
                                    <input type="email" class="form-control" name="correouser" id="correouser" placeholder="Ej.: usuario@servidor.com" value="<?php echo $rowUsuarioEmp[2]; ?>" disabled>
                                </div>
                                <br>
                                <label for="">Perfiles asignados:</label> <!-- Perfiles del Usuario -->
                                <div class="input-group">
                                    <select class="js-example-basic-multiple" name="perfilesusuario" id="perfilesusuario" multiple="multiple" style="width:100%">
                                        <?php
                                            while ($rowP=$PerfilesxUsuario->fetch_array()) {
                                                echo "<option value='.$rowP[0].' selected>".$rowP[3]."</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                                <br>
                                <label>Cambiar Contraseña:</label> <!-- Cambiar Clave -->
                                <div class="input-group">
                                    <span class="input-group-addon">***</span>
                                    <input type="password" name="NuevaClave_cambioclave" id="NuevaClave_cambioclave" class="form-control">
                                    
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary" type="button" onclick="CambioClaveUsuario(<?php echo $rowUsuarioEmp[0]; ?>)">
                                            Cambiar
                                        </button>
                                    </span>
                                </div>
                                <strong><small id="helpId" class="form-text text-muted">Clave mínimo de 6 caracteres.</small></strong>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <center>
                        <img aling = center src="logo.jpg" class="img-fluid"  width="300">
                    </center>
                </div>
            </div>
        </div>



        <!-- Modal cambio de clave -->
        <div id="CambioClave"></div>


    <script src="../../assets/js/jquery-1.10.2.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script src="../../assets/js/jquery.metisMenu.js"></script>
     <script src="../../assets/js/morris/raphael-2.1.0.min.js"></script>
    <script src="../../assets/js/morris/morris.js"></script>
    <script src="../../assets/js/custom.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
    
    <script>
        $('#perfilesusuario').select2();
        $("#perfilesusuario").prop("disabled", true);
    </script>
</body>
</html>
<?php 
	}else{
		header('Location:../../php_cerrar.php');
	}
?>
