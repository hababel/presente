    <!-- Inicio Modal Archivos Adjuntos -->
        <div class="modal fade" id="UploadFile" role="dialog" style="z-index: 1400;" aria-labelledby="modelTitleId"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="modal-title">Gestor de Archivos - # <label id="idDoc_"></label></h3>
                        <input type="text" class="form-control" name="idDoc" id="idDoc" style="display:none;">
                        <input type="text" class="form-control" name="moduloorigen" id="moduloorigen" style="display:none;">
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div>
                                <div class="panel panel-default ">
                                    <div class="panel-heading" style="height:45px;">
                                        <div style="float:right;">
                                            <span class="badge badge-primary badge-pill">
                                                <div id="CantArchivos"></div>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <div id="TablaArchivos"></div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <form class="form-horizontal" method="POST" role="form" name="miFormulario"
                                    enctype="multipart/form-data" id="miFormulario">
                                    <div class="input-group">
                                        <div id="BotonesUpload"></div>
                                        <div class="input-group-btn">
                                            <!-- <button type="button" id="ButtonUploadFile" class="btn btn-secondary">Guardar <span class="glyphicon glyphicon-arrow-up"></span></button> -->
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    <!-- Fin Modal Archivos Adjuntos -->


    <!-- INICIO modal visualiza el archivo seleccionado -->
        <div id="myModal" class="modal fade" role="dialog" style="z-index: 1600;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div id="MostrarArchivo"></div>
                    </div>
                </div>
            </div>
        </div>
    <!-- FIN modal visualiza el archivo seleccionado -->

    <!-- INICIO Modal de confirmacion de eliminacion archivo -->
        <div class="modal fade" id="ConfirmacionDel" style="z-index: 1600;" role="dialog" aria-labelledby="modelTitleId"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-color-red">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" style="color:white;">&times;</span>
                        </button>
                        <h4 class="modal-title"><b>Eliminacion de Archivos</b></h4>
                    </div>
                    <div class="modal-body">
                        <input type="text" class="form-control" name="" id="IdArchivoDel" aria-describedby="helpId" placeholder="" style="display:none;">                                       
                        <div id="msgEliminacion"></div>
                    </div>
                    <div class="modal-footer">
                        <a class="btn btn-danger text-white" id="dataComfirmOK" onclick="EliminarArchivo();">Borrar Archivo</a>
                        <button type="button" id="datacancelar" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>
    <!-- FIN Modal de confirmacion de eliminacion archivo -->

    <!-- INICIO Modal de confirmacion de UPLOAD CORRECTO archivo -->
        <div class="modal fade" id="ConfirmacionUPLOADFile" style="z-index: 1600;" role="dialog" aria-labelledby="modelTitleId"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-color-green">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" style="color:white;">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">                                       
                        <div id="msgUploadFile"></div>
                    </div>
                </div>
            </div>
        </div>
    <!-- FIN Modal de confirmacion de UPLOAD CORRECTO archivo -->
