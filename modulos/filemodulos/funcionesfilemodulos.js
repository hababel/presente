// Funcion que evalua el tipo y el tamaño del archivo permitido para adjuntar.

function uploadFile(obj, CarpetaOrigen) {
    var idDoc = $('#idDoc').val();
    var moduloorigen=$('#moduloorigen').val();
    var uploadFile = obj.files[0];
    if (!window.FileReader) {
        alert('El navegador no soporta la lectura de archivos');
        return;
    }
    if (!(/\.(jpg|png|jpeg|pdf)$/i).test(uploadFile.name)) {
        alert(`El archivo a adjuntar no es un formato permitido.
                Los formatos permitidos son: jpg,png,jpeg,pdf (Imágenes y archivos PDF)`);
        $('#FileUpload').val(null);
        $('#ButtonUploadFile').attr('disabled', true);
    } else {
        if (uploadFile.size > 4194304) { // 4194304 bytes o 4096 Kb o 4 MB
            alert(`El archivo a adjuntar es mas grande que lo permitido.
                    El tamaño permitido es menor o igual a 4 MB`);
            $('#ButtonUploadFile').attr('disabled', true);
        } else {
            $('#ButtonUploadFile').attr('disabled', false);
            var formData = new FormData();
            formData.append('FileUpload', uploadFile);
            formData.append('CarpetaOrigen', CarpetaOrigen);
            formData.append('idDoc', idDoc);

            $.ajax({
                type: "POST",
                url: '../../logica/logica.php?accion=UploadFiles',
                data: formData,
                contentType: false,
                cache: false,
                processData: false,
                // dataType: "dataType",
                success: function(response) {
                    var $miModal = $('#ConfirmacionUPLOADFile');
                    $miModal.modal('show');
                    $('#msgUploadFile').html('<h4><span class="glyphicon glyphicon-check" style="color:green"> </span> Guardado Con exito !!!</h4>');
                    setTimeout(function() {
                    $miModal.modal('toggle');
                    $('.modal-backdrop').remove();
                    }, 3000);
                    AbrilModalUploadFile(moduloorigen,idDoc);
                    $("#ImagenFiles"+idDoc).css({"color":"DodgerBlue"});
                }
            });
        }
    }
}


//Funcion que muestra los archivos que tiene asociado el documento
function AbrilModalUploadFile(moduloorigen,idDoc) {
    $('#TablaArchivos').html('');
    $('#FileUpload').val(null);
    $('#idDoc').val(idDoc);
    $('#idDoc_').html(idDoc);
    $('#moduloorigen').val(moduloorigen);
    $('#BotonesUpload').html(`
        <input type='file' class="form-control" name='FileUpload' id='FileUpload'
        accept=".jpg,.jpeg,.png,.pdf"
        onchange="uploadFile(this,'`+moduloorigen+`');">
    `);
    var parametros={moduloorigen,idDoc};
    $.ajax({
        type: "POST",
        url: "../../logica/logica.php?accion=ListFiles",
        data: parametros,
        dataType: "json",
        success: function (response) {
            $('#CantArchivos').html(response.length);
            var text= '<table class="table tab-pane">';
            for (let x = 0; x < response.length; x++) {
                text+=`
                <tr>
                    <td>
                        <div>
                            <a href='#'
                                onclick="AbrirArchivoModal('`+response[x][3]+`','`+response[x][5]+`')">`;
                                    if (response[x][5]=="pdf") {
                                text +=`<img src='../../img/iconos/pdf.png' style='max-width:80px;'
                                    class='img-thumbnail'>`;
                                    } else {
                                text+=`<img src='`+response[x][3]+`' class='img-fluid img-thumbnail'>`;
                                    } 
                            text+=`</a>
                        </div>
                    </td>
                    <td style="vertical-align:middle;"><a href='#'
                            onclick="AbrirArchivoModal('`+response[x][3]+`','`+response[x][5]+`')">
                            `+response[x][4]+` </a>
                    </td>
                    <td>
                        <a name="" id="" onclick="ModalConfirmacionEleiminacionFila('`+response[x][0]+`')" class="btn" href="#" role="button">
                            <span class='glyphicon glyphicon-trash' style='color:red;'></span>
                        </a>
                    </td>
                </tr>`;
            }
            $('#TablaArchivos').html(text);
        }
    });
}

// Funcion que muestra el archivo adjunto en una modal aparte
function AbrirArchivoModal(file, ext) {
    var $miModal = $('#myModal')
    $miModal.modal('show');
    if (ext == "pdf") {
        $('#MostrarArchivo').html('<iframe src="' + file + '" width="800" height="600"></iframe>');
    } else {
        $('#MostrarArchivo').html('<img src="' + file + '" style="max-width: 850px; max-height: 100%">');
    }
}

// Funcion que activa la modal de veficiacion de eliminacion de archivos
function ModalConfirmacionEleiminacionFila(idFile){
    $('#dataComfirmOK').attr('disabled', false);
    $('#datacancelar').attr('disabled', false);

    var $miModal = $('#ConfirmacionDel');
    $miModal.modal('show');
    $('#msgEliminacion').html('¿Está seguro de que desea eliminar el elemento seleccionado?');
    $('#IdArchivoDel').val(idFile);
}

//funcion que ejecuta la eliminacion del archivo solicitado luego de la cnfirmacion positiva
function EliminarArchivo() {
    var idFileDel=$('#IdArchivoDel').val();
    var idDoc=$('#idDoc').val();
    var moduloorigen=$('#moduloorigen').val();
    var data={idFileDel};
    $.ajax({
        type: "POST",
        url: '../../logica/logica.php?accion=DelFiles',
        data: data,
        dataType: "json",
        success: function (response) {
            if(response[0]){
                $('#dataComfirmOK').attr('disabled', true);
                $('#datacancelar').attr('disabled', true);

                var $miModal = $('#ConfirmacionDel')
                $miModal.modal('show');
                $('#msgEliminacion').html('');
                $('#msgEliminacion').html('<span class="glyphicon glyphicon-check" style="color:green"> </span> Eliminado con Exito !! ');
                setTimeout(function() {
                    $miModal.modal('toggle');
                    $('.modal-backdrop').remove();
                    }, 3000);
                    AbrilModalUploadFile(moduloorigen,idDoc);
                    if(response[1]==0){
                        $("#ImagenFiles"+idDoc).css({"color":"Gray"});
                    }
            }else{
                var $miModal = $('#ConfirmacionDel')
                $miModal.modal('show');
                $('#msgEliminacion').html('');
                $('#msgEliminacion').html('No fue posible realizar la Eliminación, Comuniquese con soporte');
                setTimeout(function() {
                    $miModal.modal('toggle');
                    $('.modal-backdrop').remove();
                    }, 4000);     
            }
        }
    });
}