<?php 
   $IdRecurso=11;
   include_once('../principal/CABparapermisos.php');
   
   /*
       Forma de colocar permisos en cada modulo.
   
       Funcion: colocar_permiso($IdUsuario,$accion,$IdRecurso)
   
       $IdUsuario: $IdUsuariologin=$_SESSION['id'];
       $accion: 1-Consultar, 2-Editar,3-Insertar,4-Eliminar,5-Listar - > Este nuemro verifica la columna de la tabal que relaciona el recurso_perfil
       $IdRecurso: Cod del recurso - Columna: CodRecurso de la tabla recurso.
       Retorna el valor que hay en la columna de la BD (1-SI tiene permiso,0-NO tiene permiso)    
   */
   if($_SESSION['autentic']){
       if($Permisos_Listar==1 || $useradmon){
            require_once("../conn_BD.php");
            require_once("class/ClassUsuario.php");
            $InstanciaDB=new Conexion();
            $InstUsuarios=new Proceso_Usuario($InstanciaDB);
            $ListarUsuarios=$InstUsuarios->ListaUsuario(1);
            $ListarPerfiles=$InstUsuarios->ListarPerfiles('todos'); //Lista de perfiles activos (1).
            $ListaRecursos=$InstUsuarios->Listarrecursos(); //Lista de recursos
       }
	}else{
		header('Location:../../php_cerrar.php');
	}
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- <head> -->
    <?php include_once('../headScript.php'); ?>
<!-- </head> -->        
<body>
    <div id="wrapper">
		<?php 
			include_once('../headWeb.php');
			include_once("../../menu/m_principal.php");
		?>
		<div id="page-wrapper" >
			<div id="page-inner">						                
				<div class="panel-body" align="right">                                                                                 
                    <?php if( $Permisos_Insertar==1 || $useradmon ){ //Permiso para Insertar o Crear ?>
                        <button type="button" class="btn btn-success btn-circle" onclick="ParaCrearPerfil();" data-toggle="modal" data-target="#ModalPerfilNuevo">
                            <i class="fa fa-plus fa-2x"></i>
                        </button>
                    <?php } ?>
				</div> 
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-primary">
							<div class="panel-heading">
								Permisos de perfiles
							</div>
							<div class="panel-body">
                            <div class="table-responsive">
									<table class="table table-striped table-bordered table-hover dataTables-example" id="dataTables-example">
										<thead>
											<tr>
												<th>Id</th>
												<th>Perfil</th>
                                                <th>Estado</th>																			
												<th><span class='glyphicon glyphicon-cog' title='Config'></span></th>
											</tr>
										</thead>
										<tbody>
                                        <?php
                                            
                                        while ($row=$ListarPerfiles->fetch_array()) { 
                                             if ($row[3] != 1) { ?>
                                                <tr class="odd gradeX">
                                                    <td><?php echo $row[0];?></td>
                                                    <td><?php echo $row[1];?></td>
                                                    <td class="center">
                                                        <?php 
                                                            if ($row[2]==1) {
                                                                echo "<span class='glyphicon glyphicon-ok-sign text-success' title='Activo'></span>"; 
                                                            } else {
                                                                echo "<span class='glyphicon glyphicon-minus-sign text-danger' title='Desactivado'></span>";
                                                            }
                                                        ?>
                                                    </td>      								             
                                                    <td class="center">
                                                        <?php if($Permisos_Consultar==1 || $useradmon) { //Permisos para Consultar/Ver Registro ?>
                                                            <div class="btn-group">
                                                                <button title="Editar Perfil" type="button" onclick='ParaEditarPerfil(<?php echo json_encode($row); ?>);' class="btn btn-default btn-sm" data-toggle="modal" data-target="#ModalPerfilNuevo"><span class="glyphicon glyphicon-pencil"></span></button>
                                                                <button title="Asignar Usuarios" onclick='usuariosaperfiles(<?php echo json_encode($row); ?>);' class="btn btn-default btn-sm" data-toggle="modal" data-target="#UsuariosaPerfiles"><span class="glyphicon glyphicon-user" style="color:blue;"></span></button>
                                                                <button title="Asignar Permisos" type="button" onclick='Asignarpermisosxperfil(<?php echo json_encode($row); ?>);' class="btn btn-default btn-sm" data-toggle="modal" data-target="#PermisosxPerfil"><span class="glyphicon glyphicon-tasks" style="color:Green;"></span></button>
                                                            </div>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                            <?php 
                                                }
                                            }
                                        ?>
										</tbody>									
									</table>							
								</div>                            
							</div>
						</div>
			    	</div>    
		    	</div>               
	    	</div> 
     	</div> 
    </div>


    <!-- Modal Nuevo Perfil -->

        <div class="modal fade" id="ModalPerfilNuevo"  role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
                            <h3 class="modal-title" id="CAB_Perfil"></h3>            
                        </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="form-group">
                              <label for="">Descripción Perfil</label>
                              <input type="text" class="form-control" name="DescripcionPerfil" id="DescripcionPerfil" aria-describedby="helpId" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="">Estado</label>
                                <select class="form-control" name="EstadoPerfil" id="EstadoPerfil" autocomplete="off" required>
                                    <option value="1">ACTIVO</option>
                                    <option value="0">NO ACTIVO</option>													
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" id="Botones_Modal_Perfil">
                        
                    </div>
                </div>
            </div>
        </div>

    <!-- Fin Modal Nuevo Perfil -->


    <!-- Modal POPUP mensaje -->
        <div class="modal fade" id="Mensaje_Perfil" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body" id="modal-body_msg">
                        
                    </div>
                </div>
            </div>
        </div>
    <!-- FIN Modal POPUP mensaje -->


    <!-- Modal Asignar Permisos a perfiles-->
        <div class="modal fade" id="PermisosxPerfil" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h3 class="modal-title">Asignar permisos a: <b><label id="PerfilSelect"></label></b></h3>            
                        </div>
                    <div class="modal-body modal-body_A">
                        
                        <h5>Grupo permisos </h5> 
                        <input name="" id="IdPerfil" class="btn btn-primary" type="button" style="display:none">
                         
                        <!-- Inicio pestañas  -->
                            <div class="row">
                                <!-- Navegacion Botones -->
                                    <div class="col-sm-3">
                                        <ul class="nav nav-pills nav-stacked" id="myTabs">
                                            <li class="active"><a href="#home" data-toggle="pill">Aspectos Iniciales</a></li>
                                            <li><a href="#profile" data-toggle="pill">Procesos</a></li>
                                            <li><a href="#adminuser" data-toggle="pill">Admin Usuarios</a></li>
                                            <li><a href="#control" data-toggle="pill">Otros Controles</a></li>
                                        </ul>
                                    </div>
                                <!-- FIN Navegacion Botones -->
                                <!-- Contenido -->
                                    <div class="col-sm-9">
                                        <div class="tab-content">

                                            <!-- Panel de aspectos inciales -->
                                            <div class="tab-pane active" id="home">
                                                <table class="table table-striped table-inverse table-responsive">
                                                    <thead class="thead-inverse">
                                                        <tr>
                                                            <th class="col-sm-4">Aspectos Iniciales</th>
                                                            <th style="padding-right: 2px;padding-left: 2px;"><div class="col-sm-4"><input name="Todos_Ver" id="Todos_Ver_1" class="form-check-input" type="checkbox" value="1" onclick="Check_Todos(this);"></div><i class="fa fa-eye" aria-hidden="true"></i> Ver</th>
                                                            <th style="padding-right: 2px;padding-left: 2px;"><div class="col-sm-4"><input name="Todos_Crear" id="Todos_Crear_1" class="form-check-input" type="checkbox" value="1" onclick="Check_Todos(this);"></div><i class="fa fa-plus-square-o" aria-hidden="true"></i> Crear</th>
                                                            <th style="padding-right: 2px;padding-left: 2px;"><div class="col-sm-4"><input name="Todos_Update" id="Todos_Update_1" class="form-check-input" type="checkbox" value="1" onclick="Check_Todos(this);"></div><i class="fa fa-refresh" aria-hidden="true"></i> Editar</th>
                                                            <th style="padding-right: 2px;padding-left: 2px;"><div class="col-sm-4"><input name="Todos_Listar" id="Todos_Listar_1" class="form-check-input" type="checkbox" value="1" onclick="Check_Todos(this);"></div><i class="fa fa-list" aria-hidden="true"></i> Listar</th>
                                                        </tr>
                                                    </thead>
                                                    
                                                </table>

                                                <div style="height:350px; overflow:auto;">
                                                    <table class="table dataTables-example" id="myTable">
                                                        <tbody>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                            <!-- Panel de Recursos de procesos -->
                                            <div class="tab-pane" id="profile">
                                                <table class="table table-striped table-inverse table-responsive">
                                                    <thead class="thead-inverse">
                                                        <tr>
                                                            <th class="col-sm-4">Procesos</th>
                                                            <th style="padding-right: 2px;padding-left: 2px;"><div class="col-sm-4"><input name="Todos_Ver" id="Todos_Ver_2" class="form-check-input" type="checkbox" value="2" onclick="Check_Todos(this);"></div><i class="fa fa-eye" aria-hidden="true"></i> Ver</th>
                                                            <th style="padding-right: 2px;padding-left: 2px;"><div class="col-sm-4"><input name="Todos_Crear" id="Todos_Crear_2" class="form-check-input" type="checkbox" value="2" onclick="Check_Todos(this);"></div><i class="fa fa-plus-square-o" aria-hidden="true"></i> Crear</th>
                                                            <th style="padding-right: 2px;padding-left: 2px;"><div class="col-sm-4"><input name="Todos_Update" id="Todos_Update_2" class="form-check-input" type="checkbox" value="2" onclick="Check_Todos(this);"></div><i class="fa fa-refresh" aria-hidden="true"></i> Editar</th>
                                                            <th style="padding-right: 2px;padding-left: 2px;"><div class="col-sm-4"><input name="Todos_Listar" id="Todos_Listar_2" class="form-check-input" type="checkbox" value="2" onclick="Check_Todos(this);"></div><i class="fa fa-list" aria-hidden="true"></i> Listar</th>
                                                        </tr>
                                                    </thead>
                                                </table>

                                                <div style="height:350px; overflow:auto;">
                                                    <table class="table dataTables-example" id="myTableProcesos">
                                                        <tbody>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            
                                            <!-- Panel de administracion de usuario -->
                                            <div class="tab-pane" id="adminuser">
                                                <table class="table table-striped table-inverse table-responsive">
                                                    <thead class="thead-inverse">
                                                        <tr>
                                                            <th class="col-sm-3">Usuarios</th>
                                                            <th style="padding-right: 2px;padding-left: 2px;"><div class="col-sm-4"><input name="Todos_Ver" id="Todos_Ver_3" class="form-check-input" type="checkbox" value="3" onclick="Check_Todos(this);"></div><i class="fa fa-eye" aria-hidden="true"></i> Ver</th>
                                                            <th style="padding-right: 2px;padding-left: 2px;"><div class="col-sm-4"><input name="Todos_Crear" id="Todos_Crear_3" class="form-check-input" type="checkbox" value="3" onclick="Check_Todos(this);"></div><i class="fa fa-plus-square-o" aria-hidden="true"></i> Crear</th>
                                                            <th style="padding-right: 2px;padding-left: 2px;"><div class="col-sm-4"><input name="Todos_Update" id="Todos_Update_3" class="form-check-input" type="checkbox" value="3" onclick="Check_Todos(this);"></div><i class="fa fa-refresh" aria-hidden="true"></i> Editar</th>
                                                            <th style="padding-right: 2px;padding-left: 2px;"><div class="col-sm-4"><input name="Todos_Listar" id="Todos_Listar_3" class="form-check-input" type="checkbox" value="3" onclick="Check_Todos(this);"></div><i class="fa fa-list" aria-hidden="true"></i> Listar</th>
                                                        </tr>
                                                    </thead>
                                                </table>

                                                <div style="height:350px; overflow:auto;">
                                                    <table class="table dataTables-example" id="myTableAdminuser">
                                                        <tbody>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                            <!-- Panel de permisos de control -->
                                            <div class="tab-pane" id="control">
                                                <table class="table table-striped table-inverse table-responsive">
                                                    <thead class="thead-inverse">
                                                        <tr>
                                                            <th class="col-sm-4">Permisos de Control</th>
                                                            <th class="col-sm-3" style="padding-right: 2px;padding-left: 2px;">
                                                                <div class="col-sm-4" style="padding-right: 1px;">
                                                                    <input name="Todos_Crear" id="Todos_Crear_4" class="form-check-input" type="checkbox" value="4" onclick="Check_Todos(this);">
                                                                    
                                                                </div>
                                                                <div class="col-sm-8 text-left" style="padding-left: 1px;">
                                                                    <i class="fa fa-plus-square-o" aria-hidden="true"></i> Crear
                                                                </div>
                                                            </th>
                                                            <th class="col-sm-5"></th>
                                                            
                                                        </tr>
                                                    </thead>
                                                </table>

                                                <div style="height:350px; overflow:auto;">
                                                    <table class="table dataTables-example" id="myTableControl">
                                                        <tbody>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                <!-- final contenido -->
                            </div>
                        <!-- final pestañas  -->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    <!-- Fin Modal Asignar Permisos a perfiles -->

    
    <!-- INICIO Modal Usuarios a perfiles -->
        <div class="modal fade" id="UsuariosaPerfiles" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="modal-title">Asignar usuarios al perfil: <label id="nombreperfilModal"></label></h3>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="row">
                                <form action="" method="post" id="formulario">
                                    <div class="col-sm-4">
                                        <label class="control-label">Lista de Usuarios</label>
                                        <select name="origen[]" id="origen" multiple="multiple" size="8" style="width:100%">
                                            
                                        </select>
                                    </div>
                                    <div class="col-sm-4" style="padding-top: 55px;">
                                        <div class="input-group">
                                            <a name="" id="" class="btn-sm btn-default" style="text-decoration:none;" href="#" role="button" onclick="pasarunuser();">
                                                Pasar <span class="glyphicon glyphicon-step-forward"></span>
                                            </a>   
                                            
                                            <a name="" id="" class="btn-sm btn-default" style="text-decoration:none;"href="#" role="button" onclick="quitarunuser();">
                                            <span class="glyphicon glyphicon-step-backward"></span> Quitar 
                                            </a>
                                        </div>
                                        <br>
                                        <div class="input-group">
                                            <a name="" id="" class="btn-sm btn-default" style="text-decoration:none;" href="#" role="button" onclick="pasartodos();" >
                                                Todos <span class="glyphicon glyphicon-fast-forward"></span>
                                            </a>
                                            
                                            <a name="" id="" class="btn-sm btn-default" style="text-decoration:none;" href="#" role="button" onclick="quitartodos();">
                                            <span class="glyphicon glyphicon-fast-backward"></span> Todos 
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <label>A cargar en el perfil</label>
                                        <select name="destino[]" id="destino" multiple="multiple" size="8" style="width:100%">
                                            
                                        </select>
                                    </div>
                                    <div class="col-sm-12">
                                        <p class="clear" id="guardarusuariosperfiles"></p>
                                    </div>
                                    
                                </form>
                            </div>
                            <div class="row" id="TablaUsuariosAsignados">
                                <table class="table table-striped table-bordered table-hover dataTables-example" id="UsuariosAsignadosPerfil">
                                    <caption id="captiontabla"></caption>
                                        <thead>
                                            <th>Id</th>
                                            <th>Nombre Usuario</th>
                                            <th>
                                                <div id="eliminartodosusuariosdeperfiles"></div>
                                            </th>
                                        </thead>
                                        
                                        <tbody id="usuarioxperfil"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    <!-- FIN Modal Usuarios a Perfiles -->

    <!-- INICIO Modal de confirmacion de eliminacion archivo -->
        <div class="modal fade" id="ConfirmacionDelusuarioperfil" style="z-index: 1600;" role="dialog" aria-labelledby="modelTitleId"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header" id="cabmodalmsg">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span style="color:white;">&times;</span>
                        </button>
                        <h4 class="modal-title" id="itulo_modal"><b>Quitar Usuarios</b></h4>
                    </div>
                    <div class="modal-body">
                        <input type="text" class="form-control" name="" id="IdRegDel" aria-describedby="helpId" placeholder="" style="display:none;">                                       
                        <div id="msgEliminacion"></div>
                    </div>
                    <div class="modal-footer" id="fotter_modal">
                        
                    </div>
                </div>
            </div>
        </div>
    <!-- FIN Modal de confirmacion de eliminacion archivo -->

    
    <script src="../../assets/js/jquery-1.10.2.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script src="../../assets/js/jquery.metisMenu.js"></script>
    <script src="../../assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="../../assets/js/dataTables/dataTables.bootstrap.js"></script>
	<script src="../../assets/js/jasny-bootstrap.min.js"></script>
    <script src="../../assets/js/custom.js"></script>
    
    <!-- Select 2 -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

    <script>
            $(document).ready(function () {

                $('.dataTables-example').dataTable();
                $('#origen').empty();
                $('#destino').empty();
                
            });

            // $('#UsuariosAsignadosPerfil').dataTable();
            
            
    </script>
    
</body>
</html>