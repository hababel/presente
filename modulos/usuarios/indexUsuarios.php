<?php 
	$IdRecurso=1;
	include_once('../principal/CABparapermisos.php');
	
	/*
		Forma de colocar permisos en cada modulo.
	
		Funcion: colocar_permiso($IdUsuario,$accion,$IdRecurso)
	
		$IdUsuario: $IdUsuariologin=$_SESSION['id'];
		$accion: 1-Consultar, 2-Editar,3-Insertar,4-Eliminar,5-Listar - > Este nuemro verifica la columna de la tabal que relaciona el recurso_perfil
		$IdRecurso: Cod del recurso - Columna: CodRecurso de la tabla recurso.
		Retorna el valor que hay en la columna de la BD (1-SI tiene permiso,0-NO tiene permiso)    
	*/
	if($_SESSION['autentic']){
		if($Permisos_Listar==1 || $useradmon){
		require_once("../conn_BD.php");
		require_once('../usuarios/class/ClassUsuario.php');
		require_once('../empleados/class/classEmpleados.php');
		$InstanciaDB=new Conexion();
		$InstUsuario=new Proceso_Usuario($InstanciaDB);
		$InstEmpleados=new Proceso_Empleados($InstanciaDB);
		$ListaUsuarios=$InstUsuario->ListaUsuario('todos');
		$ListaPerfiles=$InstUsuario->ListarPerfiles(1);
		$ListaEmpleados=$InstEmpleados->ListarEmpleados();
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- <head> -->
    <?php include_once('../headScript.php'); ?>
<!-- </head> -->        
<body>
    <div id="wrapper">
	<?php 
		include_once('../headWeb.php');
		include_once("../../menu/m_principal.php");
	?>
        <div id="page-wrapper" >
            <div id="page-inner">						                
                 <!-- /. ROW  -->               
                <div class="panel-body" align="right">
					<?php if( $Permisos_Insertar==1 || $useradmon ){ //Permiso para Insertar o Crear ?>
						<button type="button" class="btn btn-success btn-circle" data-toggle="modal" data-target="#ModalNuevoUsuario" onclick="ParaUsuarioNuevo()">
							<i class="fa fa-plus fa-2x"></i>
						</button>
					<?php } ?>                                                                                
                </div>
            <div class="row">
                <div class="col-md-12">
                	<!-- Advanced Tables -->
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                             USUARIOS
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
									<thead>
                                        <tr>
                                            <th>Codigo</th>
                                            <th>Nombre</th>
                                            <th>Correo</th>
                                            <th>Estado</th>                                                                                      
                                            <th><span class='glyphicon glyphicon-cog' title='Config'></span>
                                        </tr>
                                    </thead>
                                    <tbody>
										<?php
											while ($row=$ListaUsuarios->fetch_array()) {

												
													$datosUsuario=array($row[0],$row[1],$row[2],$row[4],$row[5]); ?>

													<tr class="odd gradeX">
													
														<td><?php echo $row[0]; ?></td>
														<td><?php echo $row[1]; ?></td>
														<td><?php echo $row[2]; ?></td>
														<td class="center">
															<?php 
																if ($row[4]==1) {
																	echo "<span class='glyphicon glyphicon-ok-sign text-success' title='Activo'></span>"; 
																} else {
																	echo "<span class='glyphicon glyphicon-minus-sign text-danger' title='Desactivado'></span>";
																}
															?>
														</td>                                                                                   
														<td class="center">
															<div class="btn-group">
																<?php if($Permisos_Consultar==1 || $useradmon) { //Permisos para Consultar/Ver Registro ?>
																	<button type="button" onclick='ParaEditarUsuario(<?php echo json_encode($datosUsuario);?>)' class="btn btn-default btn-sm" data-toggle="modal" data-target="#ModalNuevoUsuario"><span class="glyphicon glyphicon-pencil"></span></button>
																	<button type="button" onclick="Paramodal_cambio_clave(<?php echo $row[0]; ?>,'<?php echo $row[1]; ?>');" class="btn btn-default btn-sm" data-target="#modelCambioClave" data-toggle="modal" aria-pressed="false" autocomplete="off"><i class="fa fa-cogs" aria-hidden="true" style="color:black;"></i></button>
																<?php } ?>
															</div>
														</td>
													</tr>
											<?php 
											
										}?>     	
			
                                    </tbody>									
                                </table>							
                            </div>                            
                        </div>
                    </div>
                    <!--End Advanced Tables -->
                </div>
            </div>                                
        </div>               
    </div>
  </div>

 
  		<!--  Modal Nuevo Usuario-->
  			<div class="modal fade" id="ModalNuevoUsuario" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<form name="form2" method="post" enctype="multipart/form-data" action="">											
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>															
								<h3 align="center" class="modal-title" id="TituloModalUsuario"></h3>
							</div>
							<div id="msgUsuarioNuevo"></div>
							<div class="panel-body">
								<div class="row">                                       
									<div class="col-md-6">
										<div class="form-group">
											<label for="">Nombre Usuario</label>														
											<input class="form-control" name="NombreUsuario" id="NombreUsuario" placeholder="Nombre Completo" autocomplete="off" required>
										</div>
										<div class="form-group">
											<label for="">Correo Electronico</label>
											<input type="email" class="form-control" name="EmailUsuario" id="EmailUsuario" aria-describedby="emailHelpId" placeholder="usuario@dominio.com" autocomplete="off" required>
										</div>
										<div class="form-group" id="ClaveUsuarioNuevo">
											
										</div>
										<div>
											<label for="">Estado</label>
											<select class="form-control" name="EstadoUsuario" id="EstadoUsuario" autocomplete="off" required>
												<option value="1">ACTIVO</option>
												<option value="0">NO ACTIVO</option>													
											</select>
										</div>
										<div>
											<label>Empleado</label>
											<select class="form-control" name="EmpleadoUsuario" id="EmpleadoUsuario" autocomplete="off" style="width:300px;">
												<option value="0"> -- Ninguno -- </option>
												<?php
													while ($rowEMp=$ListaEmpleados->fetch_array()) {
														echo "<option value='".$rowEMp[0]."'>".$rowEMp[1]." - ".$rowEMp[2]."</option>";
													} 
												?>
											</select>
										</div>
																																	
								</div> 
							</div>
							</div>
							<div class="modal-footer" id="modal-footer_Usuario"></div>										 
						</div>
					</div>
				</form>
			</div>
		<!-- End Modal Nuevo Usuario-->

		<?php include('modalcambioclaveusuario.php'); ?>
  
         <!-- /. PAGE WRAPPER  -->
     <!-- /. WRAPPER  -->
    <script src="../../assets/js/jquery-1.10.2.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script src="../../assets/js/jquery.metisMenu.js"></script>
    <script src="../../assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="../../assets/js/dataTables/dataTables.bootstrap.js"></script>
	<script src="../../assets/js/jasny-bootstrap.min.js"></script>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

        <script>
            $(document).ready(function () {
				$('#dataTables-example').dataTable();
				$('#EmpleadoUsuario').select2({
                	dropdownParent: $("#ModalNuevoUsuario"),
				});
            });

			$('#ModalNuevoUsuario').on('shown.bs.modal', function(){
			$('#NombreUsuario').trigger('focus'); })

			$('#ModalEditarUsuario').on('shown.bs.modal', function(){
			$('#NombreUsuariof').trigger('focus'); })
			
    </script>
         <!-- CUSTOM SCRIPTS -->
    <script src="../../assets/js/custom.js"></script>
</body>
</html>
<?php }else{
            include_once('../principal/sinpermisolistar.php');         
        }
	}else{
		header('Location:../../php_cerrar.php');
	}
?>