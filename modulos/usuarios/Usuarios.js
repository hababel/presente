//Cambia el titulo y el nombre del boton en la modal, para Usuarios Nuevos
function ParaUsuarioNuevo() {

    $('#NombreUsuario').val('').trigger('change');
    $('#EmailUsuario').val('').trigger('change');
    $("#EmailUsuario").prop('disabled', false);

    $('#ClaveUsuarioNuevo').val('').trigger('change');
    $("#EstadoUsuario option[value=1]").attr("selected",true);
    $('#PerfilUsuario').val(null).trigger('change');
    $("#EmpleadoUsuario").val(0).change();

    $('#ClaveUsuarioNuevo').html(
        `<label for="">Clave</label>
	    <input type="password" class="form-control" name="ClaveUsuario" id="ClaveUsuario" placeholder="Clave" autocomplete="off" required></input>
        `
    ).trigger('change');

    $('#modal-footer_Usuario').html(
        `<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" onclick="InsertUsuario();" class="btn btn-primary">Guardar</button>`
    ).trigger('change');

    $('#TituloModalUsuario').html('Nuevo Usuario').trigger('change');
}

//Cambia el titulo y el nombre del boton en la modal, para editar el usuario
function ParaEditarUsuario(RowUsuario){
    var IdUsuario=RowUsuario[0];

    $('#NombreUsuario').val(RowUsuario[1]).trigger('change');
    $('#EmailUsuario').val(RowUsuario[2]).trigger('change');
    $("#EstadoUsuario").val(RowUsuario[3]);
    $("#EmpleadoUsuario").val(RowUsuario[4]).change();

    $("#EmailUsuario").prop('disabled', true);
    $('#modal-footer_Usuario').html(
        `<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <?php if($Permisos_Editar==1 || $useradmon) { //Permisos para Editar/Modificar Registro ?>
            <button type="button" onclick="GuardarCambiosUsuario();" class="btn btn-primary">Guardar</button>
        <?php } ?>
        `
    ).trigger('change'); 
    $('#ClaveUsuarioNuevo').html('');
    $('#TituloModalUsuario').html(`Editar Usuario
    <input name="IdUsuario" id="IdUsuario" class="btn btn-primary" type="button" value="`+IdUsuario+`" style="display:none;">
    `).trigger('change');
}


function InsertUsuario() {
    var NombreUsuario=$('#NombreUsuario').val();
    var EmailUsuario=$('#EmailUsuario').val();
    var EstadoUsuario=$('#EstadoUsuario').val();
    var ClaveUsuario=$('#ClaveUsuario').val();
    if (NombreUsuario === "" && EmailUsuario === "" && ClaveUsuario==="") {
      $("#msgUsuarioNuevo").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos 'Nombre, Correo y Clave' son obligatorios</div>");
    } else {
      var params = {NombreUsuario,EmailUsuario,EstadoUsuario,ClaveUsuario};
      var url = "usuarioslogica.php?accion=InsertUsuario";
      $.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        data: params,
      }).done(function(result) {
        if(result == 1){
          $("#msgUsuarioNuevo").html("<div class='alert alert-dismissible alert-success'><strong>Usuario insertado Con Exito !!</strong></div>");
          location.reload(); 
        } else if (result == 3) {
          $("#msgUsuarioNuevo").html("<div class='alert alert-dismissible alert-warning'><strong>Ese correo electronico ya existe.</strong></div>");
        } else {
          $("#msgUsuarioNuevo").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong>No fue posible insertar el dato, por favorcomuniquese con soporte tecnico.</div>");
          return;
        }
      });
    }
    $("#msgUsuarioNuevo").delay(3000).fadeOut(300);
  }

//Guarda los cambios en un usuario existente (editar Usuario)
function GuardarCambiosUsuario() {
    var IdUsuario=$('#IdUsuario').val();
    var NombreUsuario=$('#NombreUsuario').val();
    var EstadoUsuario=$('#EstadoUsuario').val();
    var EmailUsuario=$('#EmailUsuario').val();
    var idEmpleado=$("#EmpleadoUsuario").val();
    if (NombreUsuario == "") {
      $("#msgEditUsuario").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Los campos no pueden estar vacios</div>");
    } else {
      var params = {IdUsuario,NombreUsuario,EstadoUsuario,EmailUsuario,idEmpleado};
        var url = "usuarioslogica.php?accion=EditUsuario";
        $.ajax({
          url: url,
          type: 'POST',
          cache: false,
          dataType: 'json',
          data: params,
        }).done(function(result) {
          if(result == 1){
            $("#msgEditUsuario").html("<div class='alert alert-dismissible alert-success'>EUREKA: <strong>Editado con Exito !!</strong></div>");
            location.reload(); 
          } else if (result == 3) {
            $("#msgEditUsuario").html("<div class='alert alert-dismissible alert-warning'><strong>Los datos quedeseas cambiar ya existen.  Intenta nuevamente</strong></div>");
          } else {
            $("#msgEditUsuario").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> No se realizo ningun cambio en los datos del usuario, no hay nada que cambiar</div>");
          }
        });
    }
    $("#msgEditUsuario").delay(3000).fadeOut(300);
  }

  //Insertar un Perfil Nuevo
function InsertarPerfil(){
    var DescripcionPerfil=$('#DescripcionPerfil').val();
    if (DescripcionPerfil=="") {
            $('#modal-body_msg').html(`
                <div class="container-fluid alert alert-dismissible alert-warning">
                    <b>Error</b> - No puede estar vacio
                </div>
            `);
            $('#ModalPerfilNuevo').modal('toggle');
            var $miModal = $('#Mensaje_Perfil');
            $miModal.modal('show');
            setTimeout(function() {
                $miModal.modal('toggle');
                $('.modal-backdrop').remove();
                }, 3000);                
    } else {
        var parametros={DescripcionPerfil};
        $.ajax({
            type: "POST",
            url: "usuarioslogica.php?accion=InsertarPerfil",
            data: parametros,
            success: function (response) {
               if (response==1) {
                $('#modal-body_msg').html(`
                    <div class="container-fluid alert alert-dismissible alert-success">
                        <b>Insertado</b> - Insertado Correcto !
                    </div>
                `);

                $('#ModalPerfilNuevo').modal('toggle');
        
                var $miModal = $('#Mensaje_Perfil');
                $miModal.modal('show');
                setTimeout(function() {
                    $miModal.modal('toggle');
                    $('.modal-backdrop').remove();
                    }, 3000);
                    location.reload();    
               } else {
                $('#modal-body_msg').html(`
                    <div class="container-fluid alert alert-dismissible alert-warning">
                        <b>Error</b> - Comunicate con el administrador
                    </div>
                `);

                $('#ModalPerfilNuevo').modal('toggle');

                var $miModal = $('#Mensaje_Perfil');
                $miModal.modal('show');
                setTimeout(function() {
                    $miModal.modal('toggle');
                    $('.modal-backdrop').remove();
                    }, 3000);
               } 
            }
        });
    }
}

function ParaCrearPerfil(){
    $('#CAB_Perfil').html('Crear Perfil');
    $('#Botones_Modal_Perfil').html(`
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="InsertarPerfil();">Grabar</button>
    `);
}

function ParaEditarPerfil(RowPerfil){

    var idperfil=RowPerfil[0];
    $('#CAB_Perfil').html('Editar Perfil - <b>'+RowPerfil[1]+'</b>');
    $('#DescripcionPerfil').val(RowPerfil[1]);
    $('#Botones_Modal_Perfil').html(`
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <?php if($Permisos_Editar==1 || $useradmon) { //Permisos para Editar/Modificar Registro ?>
            <button type="button" class="btn btn-primary" onclick="EditarPerfil(`+idperfil+`);">Grabar</button>
        <?php}?>
    `);
    $("#EstadoPerfil option[value="+ RowPerfil[2] +"]").attr("selected",true);
}

function EditarPerfil(idperfil){
    var $miModalperfil;
    var nuevadescripcion=$('#DescripcionPerfil').val();
    var nuevoestado=$('#EstadoPerfil').val();
    var parametros={idperfil,nuevadescripcion,nuevoestado};
    $.ajax({
        type: "POST",
        url: "usuarioslogica.php?accion=EditarPerfil",
        data: parametros,
        dataType: "json",
        success: function (response) {
            if (response > 0) {
                $miModalperfil = $('#ConfirmacionDelusuarioperfil');
                $('#msgEliminacion').html('');
                $('#msgEliminacion').html('<span class="glyphicon glyphicon-check" style="color:green"> </span> Perfil modificado con Exito !! ');
                $('#cabmodalmsg').addClass('modal-header bg-color-green');
                $('#cabmodalmsg').removeClass('modal-header bg-color-red');
                $('#fotter_modal').html('');
                $miModalperfil.modal('show'); 
                setTimeout(function() {
                    $miModalperfil.modal('toggle');
                    $('.modal-backdrop').remove();
                    }, 3000);
                    location.reload();

            } else {
                $miModalperfil = $('#ConfirmacionDelusuarioperfil');
                $('#msgEliminacion').html('');
                $('#msgEliminacion').html('<span class="glyphicon glyphicon-info-sign" style="color:red"> </span> Hubo un error, por favor comunicarse con soporte! ');
                $('#cabmodalmsg').removeClass('modal-header bg-color-green');
                $('#cabmodalmsg').addClass('modal-header bg-color-red');
                $('#fotter_modal').html('');
                $miModalperfil.modal('show'); 
                setTimeout(function() {
                    $miModalperfil.modal('toggle');
                    $('.modal-backdrop').remove();
                    }, 3000);
                    location.reload();
            }
        }
    });
        $("#ModalPerfilNuevo").modal('hide');//ocultamos el modal
        $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
        

}

//Cambia clave de usuario seleccionado
function CambioClaveUsuario(idusuario){
    var nuevaclave=$('#NuevaClave_cambioclave').val();
    if (nuevaclave == '' || nuevaclave.length < 6) {
        if(nuevaclave.length < 6){
            AlertModalmsg('amarillo','NO Cambio la Clave',`<b>Clave digitada NO supera los 6 caracteres</b><br>
            La nueva clave digitada debe ser mayor a 6 caracteres. <br>Por favor digitela nuevamente.`,'CambioClave');
        }else{
            AlertModalmsg('amarillo','NO Cambio la Clave','<b>Clave digitada no puede estar vacia</b><br>Debe digitar una clave mayor de 6 caracteres<br>ela nuevamente.','CambioClave');
        }
        $('#NuevaClave_cambioclave').val(null);
    } else {
        parametros={nuevaclave,idusuario};
        $.ajax({
            type: "POST",
            url: "../usuarios/usuarioslogica.php?accion=CambioClaveUsuario",
            data: parametros,
            success: function (response) {
                if (response==1) {
                    AlertModalmsg('verde','Cambio de Clave Exitosa','Clave cambiada con <b>Éxito !!!</b><br>Por favor recuerde su nueva clave.<br>Pedirá la  nueva clave en el próximo inicio de sesión.','CambioClave');
                    $('#NuevaClave_cambioclave').val(null);
                } else {
                    AlertModalmsg('rojo','No Cambio la Clave',`<b>Ocurrio un error</b><br>
            Por favor comunique este error a soporte.`,'CambioClave');
                    $('#NuevaClave_cambioclave').val(null);
                }
            }
        });       
    }
}

//Pone los botones y los titulos para la modal de cambio de contraseña Usuario
function Paramodal_cambio_clave(IdUsuario,NombreUsuario){
    $('#modal-footer_CambioClave').html(`
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" onclick="CambioClaveUsuario(`+IdUsuario+`)">Cambiar</button>
    `).trigger('change');
    $('#IdUsuario_cambioclave').html('ID: '+IdUsuario).trigger('change');
    $('#NombreUsuario_cambioclave').html('Nombre: '+NombreUsuario).trigger('change');
    $('#modelCambioClave').on('shown.bs.modal', function () {
        $('#NuevaClave_cambioclave').focus();
    });
    
}

//Muestra la modal con la grilla de permisos para asignarle a todos los recursos

function Asignarpermisosxperfil(IdPerfil){
    $('#PerfilSelect').html(IdPerfil[1]);
    $('#IdPerfil').val(IdPerfil[0]);
    var IdPerfil=IdPerfil[0];
    var parametros={IdPerfil};
    var texto='';
    var texto2='';
    var texto3='';
    var texto4='';

    var cant_recursos_totales_1=0;
    var cant_recursos_totales_2=0;
    var cant_recursos_totales_3=0;
    var cant_recursos_totales_4=0;

    var cant_chek_1_ver=0;
    var cant_chek_1_crear=0;
    var cant_chek_1_editar=0;
    var cant_chek_1_listar=0;
    var cant_chek_2_ver=0;
    var cant_chek_2_crear=0;
    var cant_chek_2_editar=0;
    var cant_chek_2_listar=0;
    var cant_chek_3_ver=0;
    var cant_chek_3_crear=0;
    var cant_chek_3_editar=0;
    var cant_chek_3_listar=0;
    
    var cant_chek_4_crear=0;

    $.ajax({
        type: "POST",
        url: "usuarioslogica.php?accion=ListarPermisos",
        data: parametros,
        dataType: "json",
        success: function (data) {
            
            $.each(data, function (indexInArray, valueOfElement) {
                 var idRegperfil_recurso=data[indexInArray].idRecurso_Perfil
                 var NomRecurso=data[indexInArray].NombreRecurso;
                 var IdRecurso=data[indexInArray].Recurso_idRecurso;
                 var grupo=data[indexInArray].gruporecurso;

                 //Permiso para Consulta de recurso
                 var valorConsulta;
                 if (data[indexInArray].ConsultarRecurso_Perfil==1) {
                     switch (grupo) {
                         case '1':
                                cant_chek_1_ver++;
                             break;

                        case '2':
                                cant_chek_2_ver++;
                            break;

                        case '3':
                                cant_chek_3_ver++;
                            break;

                         default:

                             break;
                     }
                    
                    valorConsulta="checked";
                    valoriconoConsulta='<i class="fa fa-check-square" aria-hidden="true" style="color:green;"></i>';
                 } else {
                    valorConsulta="";
                    valoriconoConsulta='<span class="glyphicon glyphicon-ban-circle" style="color:red;"></span>';
                 }
                 
                //  Permiso para Crear el recurso
                var valorCrear;
                if (data[indexInArray].InsertarRecurso_Perfil==1) {
                    switch (grupo) {
                        case '1':
                                cant_chek_1_crear++;
                            break;

                       case '2':
                               cant_chek_2_crear++;
                           break;

                       case '3':
                               cant_chek_3_crear++;
                           break;
                        case '4':
                                cant_chek_4_crear++;
                            break;

                        default:

                            break;
                    }
                    
                   valorCrear="checked";
                   valoriconoCrear='<i class="fa fa-check-square" aria-hidden="true" style="color:green;"></i>';
                } else {
                   valorCrear="";
                   valoriconoCrear='<span class="glyphicon glyphicon-ban-circle" style="color:red;"></span>';
                }

                //  Permiso Actualizar el recurso
                var valorActualizar;
                if (data[indexInArray].EditarRecurso_Perfil==1) {
                    switch (grupo) {
                        case '1':
                                cant_chek_1_editar++;
                            break;

                       case '2':
                               cant_chek_2_editar++;
                           break;

                       case '3':
                               cant_chek_3_editar++;
                           break;

                        default:

                            break;
                    }
                    
                   valorActualizar="checked";
                   valoriconoActualizar='<i class="fa fa-check-square" aria-hidden="true" style="color:green;"></i>';
                } else {
                   valorActualizar="";
                   valoriconoActualizar='<span class="glyphicon glyphicon-ban-circle" style="color:red;"></span>';
                }

                // Permisos para listar el recurso

                var valorListar;
                if (data[indexInArray].ListarRecurso_Perfil==1) {
                    switch (grupo) {
                        case '1':
                                cant_chek_1_listar++;
                            break;

                       case '2':
                               cant_chek_2_listar++;
                           break;

                       case '3':
                               cant_chek_3_listar++;
                           break;

                        default:

                            break;
                    }

                    valorListar="checked";
                    valoriconoListar='<i class="fa fa-check-square" aria-hidden="true" style="color:green;"></i>';
                } else {
                    valorListar="";
                    valoriconoListar='<span class="glyphicon glyphicon-ban-circle" style="color:red;"></span>';
                }

            switch (grupo) {
                
                case '1':
                    cant_recursos_totales_1++;
                    texto+=`
                        <tr>
                            <td>`+NomRecurso+`</td>
                            <td style="padding-top: 0px;padding-bottom: 0px;padding-right: 0px;padding-left: 0px;">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <div class="col-sm-6"><input type="checkbox" class="form-check-input Ver" name="Permisos_ver_1" id="`+idRegperfil_recurso+`_Ver" onclick = "comprobar(this)" `+valorConsulta+`></div>
                                        <div class="col-sm-6 Permisos_ver_icono_1" id="BotonPermiso`+idRegperfil_recurso+`_Ver">`+valoriconoConsulta+`</div>
                                    </label>
                                </div>
                            </td>
                            <td style="padding-top: 0px;padding-bottom: 0px;padding-right: 0px;padding-left: 0px;">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <div class="col-sm-6"><input type="checkbox" class="form-check-input Crear" name="Permisos_crear_1" id="`+idRegperfil_recurso+`_Crear" onclick = "comprobar(this)" `+valorCrear+`></div>
                                        <div class="col-sm-6 Permisos_crear_icono_1" id="BotonPermiso`+idRegperfil_recurso+`_Crear">`+valoriconoCrear+`</div>
                                    </label>
                                </div>
                            </td>
                            <td style="padding-top: 0px;padding-bottom: 0px;padding-right: 0px;padding-left: 0px;">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <div class="col-sm-6"><input type="checkbox" class="form-check-input Update" name="Permisos_actualizar_1" id="`+idRegperfil_recurso+`_Update" onclick = "comprobar(this)" `+valorActualizar+`></div>
                                        <div class="col-sm-6 Permisos_actualizar_icono_1" name="Permisos_actualizar_icono" id="BotonPermiso`+idRegperfil_recurso+`_Update">`+valoriconoActualizar+`</div>
                                    </label>
                                </div>
                            </td>
                            <td style="padding-top: 0px;padding-bottom: 0px;padding-right: 0px;padding-left: 0px;">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <div class="col-sm-6"><input type="checkbox" class="form-check-input Listar" name="Permisos_listar_1" id="`+idRegperfil_recurso+`_Listar" onclick = "comprobar(this)" `+valorListar+`></div>
                                        <div class="col-sm-6 Permisos_listar_icono_1" name="Permisos_listar_icono" id="BotonPermiso`+idRegperfil_recurso+`_Listar">`+valoriconoListar+`</div>
                                    </label>
                                </div>
                            </td>
                        </tr>
                        `;
                    break;
                case '2':
                    cant_recursos_totales_2++;
                    texto2+=`
                        <tr>
                            <td>`+NomRecurso+`</td>
                            <td style="padding-top: 0px;padding-bottom: 0px;padding-right: 0px;padding-left: 0px;">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <div class="col-sm-6"><input type="checkbox" class="form-check-input Ver" name="Permisos_ver_2" id="`+idRegperfil_recurso+`_Ver" onclick = "comprobar(this)" `+valorConsulta+`></div>
                                        <div class="col-sm-6 Permisos_ver_icono_2" id="BotonPermiso`+idRegperfil_recurso+`_Ver">`+valoriconoConsulta+`</div>
                                    </label>
                                </div>
                            </td>
                            <td style="padding-top: 0px;padding-bottom: 0px;padding-right: 0px;padding-left: 0px;">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <div class="col-sm-6"><input type="checkbox" class="form-check-input Crear" name="Permisos_crear_2" id="`+idRegperfil_recurso+`_Crear" onclick = "comprobar(this)" `+valorCrear+`></div>
                                        <div class="col-sm-6 Permisos_crear_icono_2" id="BotonPermiso`+idRegperfil_recurso+`_Crear">`+valoriconoCrear+`</div>
                                    </label>
                                </div>
                            </td>
                            <td style="padding-top: 0px;padding-bottom: 0px;padding-right: 0px;padding-left: 0px;">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <div class="col-sm-6"><input type="checkbox" class="form-check-input Update" name="Permisos_actualizar_2" id="`+idRegperfil_recurso+`_Update" onclick = "comprobar(this)" `+valorActualizar+`></div>
                                        <div class="col-sm-6 Permisos_actualizar_icono_2" name="Permisos_actualizar_icono" id="BotonPermiso`+idRegperfil_recurso+`_Update">`+valoriconoActualizar+`</div>
                                    </label>
                                </div>
                            </td>
                            <td style="padding-top: 0px;padding-bottom: 0px;padding-right: 0px;padding-left: 0px;">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <div class="col-sm-6"><input type="checkbox" class="form-check-input Listar" name="Permisos_listar_2" id="`+idRegperfil_recurso+`_Listar" onclick = "comprobar(this)" `+valorListar+`></div>
                                        <div class="col-sm-6 Permisos_listar_icono_2" name="Permisos_listar_icono" id="BotonPermiso`+idRegperfil_recurso+`_Listar">`+valoriconoListar+`</div>
                                    </label>
                                </div>
                            </td>
                        </tr>
                        `;
                    break;

                case '3':
                    cant_recursos_totales_3++;
                    texto3+=`
                        <tr>
                            <td class="col-sm-3">`+NomRecurso+`</td>
                            <td style="padding-right: 2px;padding-left: 2px;">
                                <div class="form-check" class="col-sm-4">
                                    <label class="form-check-label">
                                        <div class="col-sm-6"><input type="checkbox" class="form-check-input Ver" name="Permisos_ver_3" id="`+idRegperfil_recurso+`_Ver" onclick = "comprobar(this)" `+valorConsulta+`></div>
                                        <div class="col-sm-6 Permisos_ver_icono_3" id="BotonPermiso`+idRegperfil_recurso+`_Ver">`+valoriconoConsulta+`</div>
                                    </label>
                                </div>
                            </td>
                            <td style="padding-right: 2px;padding-left: 2px;">
                                <div class="form-check" class="col-sm-4">
                                    <label class="form-check-label">
                                        <div class="col-sm-6"><input type="checkbox" class="form-check-input Crear" name="Permisos_crear_3" id="`+idRegperfil_recurso+`_Crear" onclick = "comprobar(this)" `+valorCrear+`></div>
                                        <div class="col-sm-6 Permisos_crear_icono_3" id="BotonPermiso`+idRegperfil_recurso+`_Crear">`+valoriconoCrear+`</div>
                                    </label>
                                </div>
                            </td>
                            <td style="padding-right: 2px;padding-left: 2px;">
                                <div class="form-check" class="col-sm-4">
                                    <label class="form-check-label">
                                        <div class="col-sm-6"><input type="checkbox" class="form-check-input Update" name="Permisos_actualizar_3" id="`+idRegperfil_recurso+`_Update" onclick = "comprobar(this)" `+valorActualizar+`></div>
                                        <div class="col-sm-6 Permisos_actualizar_icono_3" name="Permisos_actualizar_icono" id="BotonPermiso`+idRegperfil_recurso+`_Update">`+valoriconoActualizar+`</div>
                                    </label>
                                </div>
                            </td>
                            <td style="padding-right: 2px;padding-left: 2px;">
                                <div class="form-check" class="col-sm-4">
                                    <label class="form-check-label">
                                        <div class="col-sm-6"><input type="checkbox" class="form-check-input Listar" name="Permisos_listar_3" id="`+idRegperfil_recurso+`_Listar" onclick = "comprobar(this)" `+valorListar+`></div>
                                        <div class="col-sm-6 Permisos_listar_icono_3" name="Permisos_listar_icono" id="BotonPermiso`+idRegperfil_recurso+`_Listar">`+valoriconoListar+`</div>
                                    </label>
                                </div>
                            </td>
                        </tr>
                        `;
                    break;

                    case '4':
                        texto4+=`
                        <tr>
                            <td class="col-sm-4">`+NomRecurso+`</td>
                            <td class="col-sm-3" style="padding-right: 2px;padding-left: 2px;">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <div class="col-sm-6"><input type="checkbox" class="form-check-input Crear" name="Permisos_crear_4" id="`+idRegperfil_recurso+`_Crear" onclick = "comprobar(this)" `+valorCrear+`></div>
                                        <div class="col-sm-6 Permisos_crear_icono_4" style="padding-right: 5px;padding-left: 5px;" id="BotonPermiso`+idRegperfil_recurso+`_Crear">`+valoriconoCrear+`</div>
                                    </label>
                                </div>
                            </td>
                            <td class="col-sm-5"></td>
                        </tr>
                        
                        `;
                        break;
            
                default:
                    break;
            }
            
        });
            $('#myTable tbody').html(texto).trigger("change");
            $('#myTableProcesos tbody').html(texto2).trigger("change");
            $('#myTableAdminuser tbody').html(texto3).trigger("change");
            $('#myTableControl tbody').html(texto4).trigger("change");

            //Cambia Check de encabezado.  Si todos los recursos tienen check, pone check en el encabezado.

                // Grupo 1 - Aspectos inciales
                if (cant_chek_1_ver == cant_recursos_totales_1) {
                    $('#Todos_Ver_1').prop('checked',true);
                }else{
                    $('#Todos_Ver_1').prop('checked',false);
                }
                if (cant_chek_1_crear == cant_recursos_totales_1) {
                    $('#Todos_Crear_1').prop('checked',true);
                }else{
                    $('#Todos_Crear_1').prop('checked',false);
                }
                if (cant_chek_1_editar == cant_recursos_totales_1) {
                    $('#Todos_Update_1').prop('checked',true);
                }else{
                    $('#Todos_Update_1').prop('checked',false);
                }
                if (cant_chek_1_listar == cant_recursos_totales_1) {
                    $('#Todos_Listar_1').prop('checked',true);
                }else{
                    $('#Todos_Listar_1').prop('checked',false);
                }

                // Grupo - 2 Procesos
                if (cant_chek_2_ver == cant_recursos_totales_2) {
                    $('#Todos_Ver_2').prop('checked',true);
                }else{
                    $('#Todos_Ver_2').prop('checked',false);
                }
                if (cant_chek_2_crear == cant_recursos_totales_2) {
                    $('#Todos_Crear_2').prop('checked',true);
                } else {
                    $('#Todos_Crear_2').prop('checked',false);
                }
                if (cant_chek_2_editar == cant_recursos_totales_2) {
                    $('#Todos_Update_2').prop('checked',true);
                }else{
                    $('#Todos_Update_2').prop('checked',false);
                }
                if (cant_chek_2_listar == cant_recursos_totales_2) {
                    $('#Todos_Listar_2').prop('checked',true);
                } else {
                    $('#Todos_Listar_2').prop('checked',false);
                }

                // Grupo 3 - Admin Usuarios
                if (cant_chek_3_ver == cant_recursos_totales_3) {
                    $('#Todos_Ver_3').prop('checked',true);
                }else{
                    $('#Todos_Ver_3').prop('checked',false);
                }
                if (cant_chek_3_crear == cant_recursos_totales_3) {
                    $('#Todos_Crear_3').prop('checked',true);
                } else {
                    $('#Todos_Crear_3').prop('checked',false);
                }
                if (cant_chek_3_editar == cant_recursos_totales_3) {
                    $('#Todos_Update_3').prop('checked',true);
                }else{
                    $('#Todos_Update_3').prop('checked',false);
                }
                if (cant_chek_3_listar == cant_recursos_totales_3) {
                    $('#Todos_Listar_3').prop('checked',true);
                } else {
                    $('#Todos_Listar_3').prop('checked',false);
                }

                // Grupo 4 - Permisos de control
                if (cant_chek_4_crear == cant_recursos_totales_4) {
                    $('#Todos_Crear_4').prop('checked',true);
                } else {
                    $('#Todos_Crear_4').prop('checked',false);
                }
        }
    });
    
}

//Hace el cambio en la base de datos de cambio en cada check
function comprobar(checkbox){
    
    var dato=checkbox.id.split('_');
    var IdReg_recurso_perfil=dato[0];
    var Columna_recurso_perfil=dato[1];
    var IdPerfil=$('#IdPerfil').val();
    
    if( (NuevoValor = $("#"+checkbox.id+":checked").val()) != null ){
        $("#BotonPermiso"+IdReg_recurso_perfil+"_"+Columna_recurso_perfil).html('').trigger('change');
        $("#BotonPermiso"+IdReg_recurso_perfil+"_"+Columna_recurso_perfil).html(`
            <i class="fa fa-check-square" aria-hidden="true" style="color:green;"></i>
        `).trigger('change');
    }else{
        $("#BotonPermiso"+IdReg_recurso_perfil+"_"+Columna_recurso_perfil).html('').trigger('change');
        $("#BotonPermiso"+IdReg_recurso_perfil+"_"+Columna_recurso_perfil).html(`
            <span class="glyphicon glyphicon-ban-circle" style="color:red;"></span>
        `).trigger('change');
    }
    
    var NuevoValorGrabar;
    if (NuevoValor=='on') {
        NuevoValorGrabar=1;
    } else {
        NuevoValorGrabar=0;
    }

    var parametros={NuevoValorGrabar,IdReg_recurso_perfil,Columna_recurso_perfil};
    $.ajax({
        type: "POST",
        url: "usuarioslogica.php?accion=CambioPermiso",
        data: parametros,
        success: function (response) {
            if (response==1) {
                
            } else {
                $('#modal-body_msg').html(`
                    <div class="container-fluid alert alert-dismissible alert-warning">
                        <b>Error</b> - Comunicate con el administrador
                    </div>
                `);

                $('#ModalPerfilNuevo').modal('toggle');
                var $miModal = $('#Mensaje_Perfil');
                $miModal.modal('show');
                setTimeout(function() {
                    $miModal.modal('toggle');
                    $('.modal-backdrop').remove();
                    }, 3000);
            }
        }
    });
}


function Check_Todos(check_todos){
     var grupo=check_todos.value;
     var IdPerfil=$('#IdPerfil').val();// ID del perfil que se le genera el cambio
     var ValorNuevo=$("#"+check_todos.id+":checked").val();//On - Off
     var Opcion_CRU=check_todos.id; // Todos_Ver - Todos_Crear - Todos_Update
     var permiso_check=null;
     var permisos_check_icono=null;
     
    $('#'+Opcion_CRU).on('change', function() {
        switch (check_todos.id) {
            case "Todos_Ver_1":
                permiso_check="Permisos_ver_1";
                permisos_check_icono="Permisos_ver_icono_1";
                Opcion_CRU='ConsultarRecurso_Perfil';
                break;
            case "Todos_Crear_1":
                permiso_check="Permisos_crear_1";
                permisos_check_icono="Permisos_crear_icono_1";
                Opcion_CRU='InsertarRecurso_Perfil';
                break;
            case "Todos_Update_1":
                permiso_check="Permisos_actualizar_1";
                permisos_check_icono="Permisos_actualizar_icono_1";
                Opcion_CRU='EditarRecurso_Perfil';
                break;
            case "Todos_Listar_1":
                permiso_check="Permisos_listar_1";
                permisos_check_icono="Permisos_listar_icono_1";
                Opcion_CRU='ListarRecurso_Perfil';
                break;
            case "Todos_Ver_2":
                permiso_check="Permisos_ver_2";
                permisos_check_icono="Permisos_ver_icono_2";
                Opcion_CRU='ConsultarRecurso_Perfil';
                break;
            case "Todos_Crear_2":
                permiso_check="Permisos_crear_2";
                permisos_check_icono="Permisos_crear_icono_2";
                Opcion_CRU='InsertarRecurso_Perfil';
                break;
            case "Todos_Update_2":
                permiso_check="Permisos_actualizar_2";
                permisos_check_icono="Permisos_actualizar_icono_2";
                Opcion_CRU='EditarRecurso_Perfil';
                break;
            case "Todos_Listar_2":
                permiso_check="Permisos_listar_2";
                permisos_check_icono="Permisos_listar_icono_2";
                Opcion_CRU='ListarRecurso_Perfil';
                break;
            case "Todos_Ver_3":
                permiso_check="Permisos_ver_3";
                permisos_check_icono="Permisos_ver_icono_3";
                Opcion_CRU='ConsultarRecurso_Perfil';
                break;
            case "Todos_Crear_3":
                permiso_check="Permisos_crear_3";
                permisos_check_icono="Permisos_crear_icono_3";
                Opcion_CRU='InsertarRecurso_Perfil';
                break;
            case "Todos_Update_3":
                permiso_check="Permisos_actualizar_3";
                permisos_check_icono="Permisos_actualizar_icono_3";
                Opcion_CRU='EditarRecurso_Perfil';
                break;
            case "Todos_Listar_3":
                permiso_check="Permisos_listar_3";
                permisos_check_icono="Permisos_listar_icono_3";
                Opcion_CRU='ListarRecurso_Perfil';
                break;
            case "Todos_Crear_4":
                permiso_check="Permisos_crear_4";
                permisos_check_icono="Permisos_crear_icono_4";
                Opcion_CRU='InsertarRecurso_Perfil';
                break;
            default:
                break;
        }

        if( $(this).is(':checked') ){
            $(":checkbox[name="+permiso_check+"]").prop('checked', true);
            $("."+permisos_check_icono).html('<i class="fa fa-check-square" aria-hidden="true" style="color:green;"></i>');
            ValorNuevo=1;
        } else {
            $(":checkbox[name="+permiso_check+"]").prop('checked', false);
            $("."+permisos_check_icono).html('<span class="glyphicon glyphicon-ban-circle" style="color:red;"></span>');
            ValorNuevo=0;
        }

        var parametros={IdPerfil,ValorNuevo,Opcion_CRU,grupo};
        $.ajax({
            type: "POST",
            url: "usuarioslogica.php?accion=CambioTodosxLinea",
            data: parametros,
            // dataType: "json",
            success: function (response) {
                
                
            }
        });
    });
}

function usuariosaperfiles(idperfil){
    $('#eliminartodosusuariosdeperfiles').html(`
        <?php if($Permisos_Editar==1 || $useradmon) { //Permisos para Editar/Modificar Registro ?>
            <span class='glyphicon glyphicon-cog' title='Config'  style="padding-right: 5px;"></span> (<a name="" id="Eliminartodosusuariodeperfiles" onclick='confirmeeliminarperfil("todos","todos",`+JSON.stringify(idperfil)+`)' class="btn" href="#" role="button" style="padding-top: 0px;padding-left: 0px;padding-bottom: 0px;padding-right: 0px;margin-left: 0px;">
                <span class='glyphicon glyphicon-trash' style='color:red;'></span>
            </a>)
        <?php } ?>
    `);
    var idperfils=idperfil[0];
    $('#nombreperfilModal').html(idperfil[1]);
    var parametros={idperfils};
    var texto=null;
    cargarListaUsuarios();
    
    $.ajax({
        type: "POST",
        url: "usuarioslogica.php?accion=listarusuariosxperfil",
        data: parametros,
        dataType: "json",
        success: function (response) {
            $('#captiontabla').html('Son <b>'+response.length+'</b> usuarios asignados al perfil: <b>'+idperfil[1]+'</b>');
            for (let x = 0; x < response.length; x++) {
                texto+=`
                <tr>
                    <td><span style="font-size: 14px;">`+response[x][2]+`</span></td>
                    <td><span style="font-size: 14px;">`+response[x][3]+`</span></td>
                    <td><span style="font-size: 10px;">
                        <?php if($Permisos_Editar==1 || $useradmon) { //Permisos para Editar/Modificar Registro ?>
                            <a name="" id="Eliminarusuariodeperfiles" onclick='confirmeeliminarperfil(\"`+response[x][0]+`\",\"`+response[x][3]+`\",`+JSON.stringify(idperfil)+`);' class="btn" href="#" role="button">
                                <span class='glyphicon glyphicon-trash' style='color:red;'></span>
                            </a>
                        <?php } ?>
                    </span></td>
                </tr>`;
                $("#origen option[value='"+response[x][2]+"']").attr("selected", true).appendTo('#destino');
                $('#destino').empty();
            }
            $('#usuarioxperfil').html(texto);
            
           
            $('#guardarusuariosperfiles').html(`
            <?php if($Permisos_Editar==1 || $useradmon) { //Permisos para Editar/Modificar Registro ?>
                <button type="button" class="btn btn-primary" onclick='guardarperfilusuario(`+JSON.stringify(idperfil)+`);' style="width:100%;"><label>Guadrar 
                    <span class="glyphicon glyphicon-save"> </span>
                    </label>
                </button>
            <?php } ?>
            `);
        }
    });
}

function cargarListaUsuarios(){
    var texto=null;
    $.ajax({
        type: "POST",
        url: "usuarioslogica.php?accion=listarusuarios",
        data: "data",
        dataType: "json",
        success: function (response) {
            for (let j = 0; j < response.length; j++) {
                    texto+=`
                        <option value="`+response[j][0]+`">`+response[j][1]+`</option>
                    `;     
            }
            $('#origen').html(texto);
        }
    });
}

function pasarunuser() { 
    return !$('#origen option:selected').remove().appendTo('#destino'); 
} 

function quitarunuser() { 
    return !$('#destino option:selected').remove().appendTo('#origen'); 
}

function pasartodos() {
$('#origen option').each(function() { $(this).remove().appendTo('#destino').prop('selected', true);});
    
}
function quitartodos () {
    return !$('#destino option').each(function() { $(this).remove().appendTo('#origen'); });
}


function guardarperfilusuario(idperfil) { 
    var useradd=[];
    var idperfils=idperfil[0];
    $("#destino option:selected").each(function() {			
        useradd.push($(this).val());			
      });
      var parametros={useradd,idperfils}
      $.ajax({
          type: "POST",
          url: "usuarioslogica.php?accion=agregarusuariosalperfil",
          data: parametros,
          dataType: "json",
          success: function (response) {
              if (response > 0) {
                var $miModal = $('#ConfirmacionDelusuarioperfil');
                $('#msgEliminacion').html('');
                $('#msgEliminacion').html('<span class="glyphicon glyphicon-check" style="color:green"> </span> Agregado con Exito !! ');
                $('#cabmodalmsg').addClass('modal-header bg-color-green');
                $('#cabmodalmsg').removeClass('modal-header bg-color-red');
                $('#fotter_modal').html('');
                $miModal.modal('show'); 
                setTimeout(function() {
                    $miModal.modal('toggle');
                    $('.modal-backdrop').remove();
                    }, 2000);
                    usuariosaperfiles(idperfil);
              } else {
                var $miModal = $('#ConfirmacionDelusuarioperfil');
                $('#msgEliminacion').html('');
                $('#msgEliminacion').html('<span class="glyphicon glyphicon-info-sign" style="color:red"> </span> Hubo un error, por favor comunicarse con soporte! ');
                $('#cabmodalmsg').addClass('modal-header bg-color-red');
                $('#cabmodalmsg').removeClass('modal-header bg-color-green');
                $('#fotter_modal').html('');
                $miModal.modal('show');
                setTimeout(function() {
                    $miModal.modal('toggle');
                    $('.modal-backdrop').remove();
                    }, 2000);
              }
          }
      }); 
}

function confirmeeliminarperfil(IdRegDel,nombreuser,idperfil){
    if (IdRegDel=="todos") {
        $('#msgEliminacion').html('');
        $('#msgEliminacion').html('¿Está seguro que desea quitar <b><label><u>TODOS</u></label></b> los usuarios asociados al perfil <b><u>'+idperfil[1]+'</u></b> ?');
        $('#cabmodalmsg').addClass('bg-color-red');
        $('#cabmodalmsg').removeClass('bg-color-green');
        $('#fotter_modal').html(`
            <a class="btn btn-danger text-white" id="dataComfirmOK" onclick=\'QuitarUsuariodePerfil( "todos", `+JSON.stringify(idperfil)+`);\'>Si, Quitar Usuario</a>
            <button type="button" id="datacancelar" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        `);
    } else {
        $('#msgEliminacion').html('');
        $('#msgEliminacion').html('¿Está seguro que desea quitar el usuario: <b><u>'+nombreuser+'</u></b> del perfil <b><u>'+idperfil[1]+'</u></b> ?');
        $('#cabmodalmsg').addClass('bg-color-red');
        $('#cabmodalmsg').removeClass('bg-color-green');
        $('#fotter_modal').html(`
            <a class="btn btn-danger text-white" id="dataComfirmOK" onclick=\'QuitarUsuariodePerfil( `+IdRegDel+`, `+JSON.stringify(idperfil)+`);\'>Si, Quitar Usuario</a>
            <button type="button" id="datacancelar" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        `);
    }
        var $miModal = $('#ConfirmacionDelusuarioperfil');
        $miModal.modal('show');
    
}

function QuitarUsuariodePerfil(IdRegDel,idperfil) {
    var parametros;
    
    if (IdRegDel == "todos") {  
        IdRegDel=idperfil;
    } 
    parametros={IdRegDel};
    $.ajax({
        type: "POST",
        url: "usuarioslogica.php?accion=eliminarusuariodeperfil",
        data: parametros,
        dataType: "json",
        success: function (response) {
            if (response > 0) {
                var $miModal = $('#ConfirmacionDelusuarioperfil');
                $('#msgEliminacion').html('');
                $('#msgEliminacion').html('<span class="glyphicon glyphicon-check" style="color:green"> </span> Usuario retirado del perfil con Exito !! ');
                $('#cabmodalmsg').addClass('bg-color-red');
                $('#cabmodalmsg').removeClass('bg-color-green');
                $('#fotter_modal').html('');
                $miModal.modal('show');
                setTimeout(function() {
                    $miModal.modal('toggle');
                    $('.modal-backdrop').remove();
                    }, 2000);
                usuariosaperfiles(idperfil);
            } else {
                var $miModal = $('#ConfirmacionDelusuarioperfil');
                $('#msgEliminacion').html('');
                $('#msgEliminacion').html('<span class="glyphicon glyphicon-info-sign" style="color:red"> </span> Hubo un error, por favor comunicarse con soporte! ');
                $('#cabmodalmsg').addClass('modal-header bg-color-red');
                $('#cabmodalmsg').removeClass('modal-header bg-color-green');
                $('#fotter_modal').html('');
                $miModal.modal('show');
                setTimeout(function() {
                    $miModal.modal('toggle');
                    $('.modal-backdrop').remove();
                    }, 2000); 
            }
        }
    }); 
}


        
    




   