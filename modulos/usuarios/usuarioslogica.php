<?php 
    // session_start();
    require_once("../conn_BD.php");
    require_once("class/ClassUsuario.php");
    require_once("../funciones.php");

    if ($_POST) {
        
        $accion=$_GET['accion'];
        $InstanciaDB=new Conexion();
        $InstUsuario=new Proceso_Usuario($InstanciaDB);
        switch ($accion) {

            case 'InsertUsuario':
                $NombreUsuario=primera_mayuscula(limpiar($_POST['NombreUsuario']));
                $EmailUsuario=limpiar($_POST['EmailUsuario']);
                $EstadoUsuario=limpiar($_POST['EstadoUsuario']);
                $ClaveUsuario=password_hash(limpiar($_POST['ClaveUsuario']), PASSWORD_DEFAULT);
                $idEmpleado=$_POST['idEmpleado'];
                
                $buscaemail=$InstUsuario->BuscarEmailUsuario($EmailUsuario);
                if ($buscaemail->num_rows>0) {
                    // Si result es 3, significa que el email del usuario ya existe.
                    echo 3;
                } else {
                    $InsertadoUsuario=$InstUsuario->InsertarUsuario($EmailUsuario,$ClaveUsuario,$EstadoUsuario,$NombreUsuario,$idEmpleado);
                    if ($InsertadoUsuario>0 ) {
                        
                        echo 1;
                    } else {
                        echo 0;
                    }
                }
                break;

            case 'CambioClaveUsuario':
                $IdUsuario=limpiar($_POST['idusuario']);
                $NuevaClave=limpiar($_POST['nuevaclave']);
                $NuevaClave_Crypt=password_hash($NuevaClave,PASSWORD_DEFAULT);
                // echo $NuevaClave_Crypt;
                $cambioClave=$InstUsuario->CambiarClave($IdUsuario,$NuevaClave_Crypt);
                if ($cambioClave) {
                    echo 1;
                } else {
                    echo 0;
                }

                break;
            case 'CambioPermiso':
                $NuevoValorGrabar=$_POST['NuevoValorGrabar'];
                $IdReg_recurso_perfil=$_POST['IdReg_recurso_perfil'];
                $Columna_recurso_perfil=$_POST['Columna_recurso_perfil'];

                $ActualizaPermiso=$InstUsuario->UpdatePerfilxRecurso($NuevoValorGrabar,$IdReg_recurso_perfil,$Columna_recurso_perfil);
                if ($ActualizaPermiso) {
                    echo 1;
                } else {
                    echo 0;
                }
            
            break;
            case 'InsertarPerfil':
                $DescripcionPerfil=$_POST['DescripcionPerfil'];
                $InsertarPerfil=$InstUsuario->InsertarPerfil($DescripcionPerfil);
                if ($InsertarPerfil != 0) {
                    $ListaRecursos=$InstUsuario->Listarrecursos();
                    while ($rowrecurso=$ListaRecursos->fetch_array()) {
                        $CrearPermisos=$InstUsuario->InsertarPermisosPerfilNuevoXRecurso($InsertarPerfil,$rowrecurso[0]);
                    }
                    echo 1 ;
                } else {
                    echo 0;
                }
                
            break;
            case 'EditarPerfil':
                $IdPerfil=$_POST['idperfil'];
                $NuevaDescPerfil=$_POST['nuevadescripcion'];
                $nuevoestado=$_POST['nuevoestado'];
                $cambioperfil=$InstUsuario->EditarPerfil($IdPerfil,$NuevaDescPerfil,$nuevoestado);
                echo $cambioperfil;
            break;
            case 'ListarPermisos':
                $IdPerfil=$_POST['IdPerfil'];
                $ListaPermisos=$InstUsuario->ListaPermisosxPerfil($IdPerfil);
                while($row = $ListaPermisos->fetch_array())
                    {
                        $rows_P[] = $row;
                    }
                echo json_encode($rows_P);
            break;

            case 'CargarPerfilesUsuario':
                    $i=0;
                    $IdUsuario=limpiar($_POST['IdUsuario']);
                    $perfilesxUsuario = array();
                    $ListaPerfilesxUsuario=$InstUsuario->ListaPerfilesxUsuario($IdUsuario);
                    while($rowPE=$ListaPerfilesxUsuario->fetch_array()){
                        $perfilesxUsuario[$i]=intval($rowPE[1]);
                        $i++;
                    }
                    echo json_encode($perfilesxUsuario);
            break;

            case 'EditUsuario':
                $IdUsuario=limpiar($_POST['IdUsuario']);
                $NombreUsuario=primera_mayuscula(limpiar($_POST['NombreUsuario']));
                $EstadoUsuario=limpiar($_POST['EstadoUsuario']);
                $EmailUsuario=limpiar($_POST['EmailUsuario']);
                $idEmpleado=$_POST['idEmpleado'];

                $DatosUsuarioDB=$InstUsuario->BuscarEmailUsuario($EmailUsuario);

                $EditarUsuario=$InstUsuario->EditarUsuario($IdUsuario,$NombreUsuario,$EstadoUsuario,$idEmpleado);
                if ($EditarUsuario>0) {
                    echo 1;
                } else {
                    echo 0;
                }
            break;
            case 'CambioTodosxLinea':

                $IdPerfil=$_POST['IdPerfil'];
                $ValorNuevo=$_POST['ValorNuevo'];
                $Opcion_CRU=$_POST['Opcion_CRU'];
                $grupo=$_POST['grupo'];
                $Cambiar_Todos_Recursos=$InstUsuario->UpdatePerfilTodosxLinea($Opcion_CRU,$ValorNuevo,$IdPerfil,$grupo);

            break;
            case 'listarusuariosxperfil':
                $IdPerfil=$_POST['idperfils'];
                $Listausuariosxperfil=$InstUsuario->ListaUsuarioxPerfil($IdPerfil);
                echo json_encode($Listausuariosxperfil->fetch_all(MYSQLI_NUM));
            break;
            case 'listarusuarios':
                $Listadusuarios=$InstUsuario->ListaUsuario(1);
                echo json_encode($Listadusuarios->fetch_all(MYSQLI_NUM));
            break;

            case 'eliminarusuariodeperfil':
                $IdRegDel=$_POST['IdRegDel'];
                $Perfilquitado=0;
                $objeto=gettype($IdRegDel);
                
                if ($objeto == "array") {
                    $Perfilquitado=$InstUsuario->quitartodosusuariosdeperfil($IdRegDel[0]);
                } else {
                $Perfilquitado=$InstUsuario->quitarusuariodeperfil($IdRegDel); 
                }

                if ($Perfilquitado > 0) {
                    echo 1;
                } else {
                    echo 0;
                }
                
            break;

            case 'agregarusuariosalperfil':
                $idperfil=$_POST['idperfils'];
                $vecusuariosadd=$_POST['useradd'];
                $insert=0;
                for ($i=0; $i < count($vecusuariosadd) ; $i++) { 
                    $addusuario=$InstUsuario->InsertarUsuario_Perfil($vecusuariosadd[$i],$idperfil);
                    if ($addusuario > 0 ) {
                        $insert=1;
                    }
                }
                echo $insert;
                
            break;

            case 'RecuperarClave':
                $emailparaclave=$_POST['emailparaclave'];
                $found_email=$InstUsuario->BuscarEmailUsuario($emailparaclave); //Busca el Email que envio el user por input
                $row=$found_email->fetch_array(MYSQLI_NUM);
                
                if($found_email->num_rows > 0){ //Si existe al menos un registro con ese correo
                    if($row[4]==1){ //El usuario esta activo
                        $Token_pass_gen = md5(uniqid(mt_rand(), false)); //Genera Token aleatorio
                        //Actualiza tabla con el token generado y con el request_pass
                        $rowupdate=$InstUsuario->updatetoken($row[2],$Token_pass_gen,$row[0]);
                        if ($rowupdate > 0) {
                            $url = 'http://www.vfonnegra.co/secretos/modulos/usuarios/recuperaclave.php?iduser='.$row[0].'&tokenpass='.$Token_pass_gen;
                            #

                            $cuerpo='<html>
                                    <head>
                                            <meta http-equiv="content-type" content="text/html; charset=UTF-8">
                                            <meta charset="utf-8">
                                            <title>LOGIN SIGLA V.1</title>
                                            <meta name="generator" content="Bootply" />
                                            <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
                                            <link href="../assets/css/bootstrap.css" rel="stylesheet" />
                                            <link href="../assets/css/font-awesome.css" rel="stylesheet" />
                                            <link href="../assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
                                            <link href="../assets/css/custom.css" rel="stylesheet" />
                                            <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css" />
                                            <script src="../assets/js/jquery-1.10.2.js"></script>   
                                            <script src="../assets/js/bootstrap.min.js"></script> 
                                            <script src="../assets/js/jquery.metisMenu.js"></script>
                                            <script src="../assets/js/morris/raphael-2.1.0.min.js"></script>
                                            <script src="../assets/js/morris/morris.js"></script>
                                        
                                        </head>

                                    <body style="margin: 0; padding: 0;">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">	
                                            <tr>
                                                <td style="padding: 10px 0 30px 0;">
                                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border: 1px solid #cccccc; border-collapse: collapse;">
                                                        <tr>
                                                            <td>
                                                                <div style="background:#D9EDF7;font-size: 16px; height: 40px" align="center"><strong>SIGLA V.1 </strong></div><br>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
                                                                            <b>Recuperaci&oacute;n de clave</b>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding: 20px 0 30px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
                                                                        <div><h3> Hola, '.$row[1].'</h3></div>   
                                                                            <p>Se ha solicitado un reinicio de clave. <br/></p><p>Para restaurar la contrase&ntilde;a, visita el siguiente enlace: <a href="'.$url.'">Click Aqui</a></p>
                                                                            <br>
                                                                            Si no puede acceder dando click, copie y pegue el siguiente texto en el barra de direccion del navegador.
                                                                            <br>
                                                                                <div><p>'.$url.'</p></div>
                                                                            <br>
                                                                            <div>
                                                                                <p>En caso que ud no haya realizado esta solicitud.  haga caso omiso de éste.</p>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td bgcolor="#ee4c50" style="padding: 30px 30px 30px 30px;">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;" width="75%">
                                                                        Equipo de soporte info@vfonnegra.co
                                                                            &reg; SIGLA V.1 - 2019<br/>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </body>
                                    </html>';
                            #

                            $asunto = 'SIGLA V.1 - Recuperación de claves de ingreso';
                            // $asunto=utf8_decode($asunto);
                            $sendmail=enviarEmail($row[2],$row[1],$asunto,$cuerpo);
                            echo $sendmail;
                        }else{
                            echo 0;
                        }
                    }
                }
            break;

            case 'guardaclave':
                $user_id=$_POST['user_id'];
                $token=$_POST['token'];
                $nuevaclave=$_POST['nuevaclave'];
                $validatoken=validatokenpass($token,$user_id);
                echo $validatoken;
                if ($validatoken) {
                    $NuevaClaveUsuario=password_hash(limpiar($nuevaclave), PASSWORD_DEFAULT);
                    $UpdateNuevaClave=$InstUsuario->cambiaclave($user_id,$NuevaClaveUsuario);
                    if ($UpdateNuevaClave > 0) {
                        echo 1;    
                    } else {
                       echo 0; 
                    }
                    
                } else {
                    echo 0;
                }
                
            break;

            default:
                
                break;
        }
    }

?>