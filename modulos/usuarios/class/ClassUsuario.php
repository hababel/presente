<?php

Class Proceso_Usuario{
    
    public $ConnxClass;
    public $EmailLogin;
    public $ClaveLogin;

    function __construct($InstanciaDB){
       $this->ConnxClass=$InstanciaDB;
    }

    //Usuario


    function BuscarLogin($EmailLogin){
        $sql="SELECT usuario.*,
        perfil.TipoPerfil
        FROM usuario 
        left JOIN usuario_perfil ON usuario_perfil.Usuario_idUsuario=usuario.idUsuario
        left JOIN perfil ON usuario_perfil.Perfil_idPerfil=perfil.idPerfil
        where EmailUsuario='".$EmailLogin."'";
        // echo $sql;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }
    
    function InsertarUsuario($EmailUsuario,$ClaveUsuario,$EstadoUsuario,$NombreUsuario,$idEmpleado){
        $sql="INSERT INTO usuario (EmailUsuario,ClaveUsuario,EstadoUsuario,NombreUsuario,idEmpleado) VALUES ('".$EmailUsuario."', '".$ClaveUsuario."', ".$EstadoUsuario.", '".$NombreUsuario."',".$idEmpleado.")";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->ConnxClass->link->insert_id;
    }

    function BuscarEmailUsuario($EmailUsuario){
        $sql="SELECT * FROM usuario where EmailUsuario='".$EmailUsuario."'";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function BuscarUsuario($IdUsuario){
        $sql="SELECT * FROM usuario where IdUsuario='".$IdUsuario."'";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function EditarEstadoUsuario($idUsuario,$EstadoUsuario){
        $sql="UPDATE usuario SET EstadoUsuario = '".$EstadoUsuario."' WHERE idUsuario=".$idUsuario;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function EditarUsuario($idUsuario,$NombreUsuario,$EstadoUsuario,$idEmpleado){
        $sql="UPDATE usuario SET EstadoUsuario = '".$EstadoUsuario."', NombreUsuario='".$NombreUsuario."', idEmpleado=".$idEmpleado." WHERE idUsuario=".$idUsuario;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function ListaUsuario($EstadoUsuario){
        $sql="SELECT usuario.*,
                    usuario_perfil.Perfil_idPerfil,
                    perfil.NombrePerfil,
                    perfil.TipoPerfil
                FROM usuario
                LEFT JOIN usuario_perfil ON usuario_perfil.Usuario_idUsuario=usuario.idUsuario
                LEFT JOIN perfil ON usuario_perfil.Perfil_idPerfil=perfil.idPerfil where usuario.EmailUsuario<>'admin'";
        if($EstadoUsuario != 'todos'){
            $sql.=" and usuario.EstadoUsuario=".$EstadoUsuario;
        }

        $sql.=" ORDER BY usuario.NombreUsuario asc";
                
        // echo $sql;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function getlastidUsuario(){
        $sql="SELECT MAX(idUsuario) AS idUsuario FROM Usuario";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        if ($this->resultado->num_rows > 0) {
            $this->lastidusuario=$this->resultado->fetch_array(MYSQLI_BOTH);
        }
        return $this->lastidusuario[0];
    }

    function CambiarClave($IdUsuario,$ClaveNueva){
        $sql="UPDATE usuario set ClaveUsuario ='".$ClaveNueva."' where idUsuario=".$IdUsuario;
        // echo $sql;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function usuario_empleado($IdUsuario){
            $sql="SELECT usuario.*, empleado.*, area.DescArea
                    FROM usuario
                    LEFT JOIN empleado ON usuario.idEmpleado=empleado.idEmpleado
                    LEFT JOIN area ON area.idArea=empleado.idAreaEmpleado
                    WHERE usuario.idUsuario=".$IdUsuario;
                    
            $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
            return $this->resultado;
    }

    // Perfiles 

    function ListarPerfiles($estadoperfil){
        
        $sql="SELECT * FROM perfil";
        if($estadoperfil != 'todos'){
            $sql.=" WHERE perfil.estadoPerfil=".$estadoperfil;
        }
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;

    }

    function Listarrecursos(){
        $sql="SELECT * from recurso ORDER BY  recurso.NombreRecurso ASC";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function UpdatePerfilTodosxLinea($Opcion_CRU,$ValorNuevo,$IdPerfil,$grupo){
        $sql="UPDATE recurso_perfil
                INNER JOIN recurso ON recurso.idRecurso=recurso_perfil.Recurso_idRecurso
                SET ".$Opcion_CRU."=".$ValorNuevo."
                WHERE Perfil_idPerfil=".$IdPerfil." AND recurso.gruporecurso=".$grupo;
                // echo $sql;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;      
    }
    
    function UpdatePerfilxRecurso($NuevoValorGrabar,$IdReg_recurso_perfil,$Columna_recurso_perfil){

        $CampoCambio=null;
        if ($Columna_recurso_perfil=='Ver') {
            $CampoCambio="ConsultarRecurso_Perfil";
        } elseif($Columna_recurso_perfil=='Crear') {
            $CampoCambio="InsertarRecurso_Perfil";
        }elseif($Columna_recurso_perfil=='Update'){
            $CampoCambio="EditarRecurso_Perfil";
        }else{
            $CampoCambio="ListarRecurso_Perfil";
        }
        
        $sql="UPDATE recurso_perfil SET recurso_perfil.".$CampoCambio."=".$NuevoValorGrabar."
         WHERE recurso_perfil.idRecurso_Perfil=".$IdReg_recurso_perfil;
        // echo $sql;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function InsertarPerfil($DescripcionPerfil){
        $sql="INSERT INTO perfil (NombrePerfil,EstadoPerfil) value('".$DescripcionPerfil."',1)";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->ConnxClass->link->insert_id;
    }

    function EditarPerfil($idperfil,$nuevadescripcion,$nuevoestado){
        $sql="UPDATE perfil
        SET
            NombrePerfil='".$nuevadescripcion."',
            EstadoPerfil='".$nuevoestado."'
        WHERE perfil.idPerfil=".$idperfil;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function InsertarPermisosPerfilNuevoXRecurso($IdPerfil,$IdRecurso){
        $sql="INSERT INTO recurso_perfil 
        (ConsultarRecurso_Perfil,EditarRecurso_Perfil,InsertarRecurso_Perfil,EliminarRecurso_Perfil,Recurso_idRecurso,Perfil_idPerfil)
        VALUE(0,0,0,0,".$IdRecurso.",".$IdPerfil.")";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function InsertarUsuario_Perfil($IdUsuario,$IdPerfil){
        $sql="INSERT INTO usuario_perfil (Perfil_idPerfil,Usuario_idUsuario) VALUES (".$IdPerfil.",".$IdUsuario.")";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function ListaPermisosxPerfil($IdPerfil){
        $sql="SELECT recurso_perfil.*,
        recurso.NombreRecurso,
        recurso.gruporecurso
         from recurso_perfil
        INNER JOIN recurso ON recurso.idRecurso=recurso_perfil.Recurso_idRecurso
        where recurso_perfil.Perfil_idPerfil=".$IdPerfil." ORDER BY  recurso.gruporecurso,recurso.NombreRecurso ASC";
        // echo $sql;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function PermisosxUsuarioxRecurso($IdUsuario,$IdRecurso){
        $sql="SELECT recurso_perfil.*,
        usuario.idUsuario,
        recurso.CodRecurso
        FROM recurso_perfil
        INNER JOIN perfil ON recurso_perfil.Perfil_idPerfil=perfil.idPerfil
        INNER JOIN recurso ON recurso.idRecurso=recurso_perfil.Recurso_idRecurso
        INNER JOIN usuario_perfil ON usuario_perfil.Perfil_idPerfil=perfil.idPerfil
        INNER JOIN usuario ON usuario_perfil.Usuario_idUsuario=usuario.idUsuario
        WHERE recurso.CodRecurso=".$IdRecurso." AND usuario.idUsuario=".$IdUsuario;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function ListaPerfilesxUsuario($IdUsuario){
        $sql="SELECT usuario_perfil.*,perfil.NombrePerfil
                FROM usuario_perfil
                INNER JOIN perfil ON perfil.idPerfil=usuario_perfil.Perfil_idPerfil
                WHERE usuario_perfil.Usuario_idUsuario=".$IdUsuario;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function ListaUsuarioxPerfil($IdPerfil){
        $sql="SELECT usuario_perfil.idUsuario_Perfil,
        usuario_perfil.Perfil_idPerfil,
        usuario_perfil.Usuario_idUsuario,
        usuario.NombreUsuario,
        usuario.EstadoUsuario
        FROM usuario_perfil
        INNER JOIN usuario ON usuario.idUsuario=usuario_perfil.Usuario_idUsuario
        WHERE usuario_perfil.Perfil_idPerfil=".$IdPerfil." order by usuario.NombreUsuario";
    
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function EliminarPerfilesdelUsuario($IdUsuario){
        $sql="DELETE FROM usuario_perfil WHERE usuario_perfil.Usuario_idUsuario=".$IdUsuario;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function quitarusuariodeperfil($IdRegDel){
        $sql="DELETE FROM usuario_perfil WHERE usuario_perfil.idUsuario_Perfil=".$IdRegDel;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function quitartodosusuariosdeperfil($idperfil){
        $sql="DELETE FROM usuario_perfil WHERE usuario_perfil.Perfil_idPerfil=".$idperfil;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function updatetoken($EmailUsuario,$Token_pass,$idUsuario){
        $sql="UPDATE usuario
        SET Token_pass='".$Token_pass."',
            Request_pass=1
        WHERE idUsuario=".$idUsuario." AND EmailUsuario='".$EmailUsuario."'";
        // echo $sql;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function validatokenpass($tokenpass,$iduser){
        $sql="SELECT usuario.idUsuario, usuario.Token_pass
        FROM usuario
        WHERE usuario.idUsuario=".$iduser." AND usuario.Token_pass='".$tokenpass."'";
        // echo $sql;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function cambiaclave($idUsuario,$nuevaclave){
        $sql="UPDATE usuario
        SET Token_pass='',
            Request_pass=0,
            ClaveUsuario='".$nuevaclave."'
        WHERE idUsuario=".$idUsuario;
        // echo $sql;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }


}
?>