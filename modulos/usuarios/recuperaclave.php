<?php

   if(isset($_GET['iduser']) && isset($_GET['tokenpass'])){
        require_once("../conn_BD.php");
        require_once("class/ClassUsuario.php");
        require_once("../funciones.php");

        $InstanciaDB=new Conexion();
        $InstUsuarios=new Proceso_Usuario($InstanciaDB);

        $iduser=$_GET['iduser'];
        $tokenpass=$_GET['tokenpass'];

        $validarToken=validatokenpass($tokenpass,$iduser);

	}else{
		header('Location:../../php_cerrar.php');
	}
?>

<html lang="en">
	<head>
		<title>Cambiar Clave SIGLA V.1</title>
		<head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>LOGIN SIGLA V.1</title>
        <meta name="generator" content="Bootply" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link href="../../assets/css/bootstrap.css" rel="stylesheet" />
        <link href="../../assets/css/font-awesome.css" rel="stylesheet" />
        <link href="../../assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
        <link href="../../assets/css/custom.css" rel="stylesheet" />
        <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css" />
        <script src="../../assets/js/jquery-1.10.2.js"></script>
        <script src="../../assets/js/bootstrap.min.js"></script>
        <script src="../../assets/js/jquery.metisMenu.js"></script>
        <script src="../../assets/js/morris/raphael-2.1.0.min.js"></script>
        <script src="../../assets/js/morris/morris.js"></script>

	</head>

	</head>

	<body>
		<div class="container">
			<div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
				<div class="panel panel-info" >
					<div class="panel-heading">
						<div class="panel-title">Cambiar Clave - SIGLA V.1</div>
						<div style="float:right; font-size: 80%; position: relative; top:-10px"><a href="../../index.php">Iniciar Sesi&oacute;n</a></div>
					</div>

					<div style="padding-top:30px" class="panel-body" >
                        <input type="hidden" id="user_id" name="user_id" value ="<?php echo $iduser; ?>">
                        <input type="hidden" id="token" name="token" value ="<?php echo $tokenpass; ?>">
                        <div class="row">
                            <div class="form-group">
                                <label for="password" class="col-md-3 control-label">Nueva Clave</label>
                                <div class="col-md-9">
                                    <input type="password" class="form-control" id="clave1" autofocus="autofocus" name="clave1" min="6" max="10" placeholder="Escriba la clave" required <?php if (!$validarToken) { echo 'disabled';}?>>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="con_password" class="col-md-3 control-label">Confirmar Clave</label>
                                <div class="col-md-9">
                                    <input type="password" class="form-control" id="nuevaclave" min="6" max="10" name="clave2" placeholder="Vuelva a escribir la clave" required <?php if (!$validarToken) { echo 'disabled';}?>>
                                </div>
                            </div>
                            <div  class="form-group">
                                <div class="col-sm-12 controls">
                                    <button class="btn btn-success" onclick="cambiaclave();" <?php if (!$validarToken) { echo 'disabled';}?>>Modificar</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div id="rowmensaje" class="text-center"> 
                                <?php if (!$validarToken) {?>
                                    <div class="alert"  align="center">
                                        <div id="CABmsg" class="alert alert-danger">
                                            <div id="titulomsg" class="alert-heading">Cuidado !</div>
                                            <div id="msg"><p>Esta intentando hacer una accion que <b>no</b> tiene permitida !</p></div>
                                        </div>
                                    </div>
                            <?php  }?>
                            </div>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
<script>

$(document).ready(function() {
    // variables
	var pass1 = $('[name=pass1]');
	var pass2 = $('[name=pass2]');
	var confirmacion = "Las contraseñas si coinciden";
	var longitud = "La contraseña debe estar formada entre 6-10 carácteres (ambos inclusive)";
	var negacion = "No coinciden las contraseñas";
	var vacio = "La contraseña no puede estar vacía";
	//oculto por defecto el elemento span
	var span = $('<span></span>').insertAfter(pass2);
    span.hide();
    $('#btn-login').attr("disabled",true);

	//función que comprueba las dos contraseñas
	function coincidePassword(){
        var valor1 = pass1.val();
        var valor2 = pass2.val();
        //muestro el span
        span.show().removeClass();
        //condiciones dentro de la función
        if(valor1 != valor2){
            span.text(negacion).addClass('negacion');
            $('#btn-login').attr("disabled",true);
        }
        if(valor1.length==0 || valor1==""){
            span.text(vacio).addClass('negacion');
            $('#btn-login').attr("disabled",true);
        }
        if(valor1.length<6 || valor1.length>10){
            span.text(longitud).addClass('negacion');
            $('#btn-login').attr("disabled",true);
        }
        if(valor1.length!=0 && valor1==valor2){
            span.text(confirmacion).removeClass("negacion").addClass('confirmacion');
            $('#btn-login').attr("disabled",false);
        }
    }

	//ejecuto la función al soltar la tecla
	pass2.keyup(function(){
	    coincidePassword();
	});
});



function cambiaclave() {
    var user_id=$('#user_id').val();
    var token=$('#token').val();
    var nuevaclave=$('#nuevaclave').val();
    var clave1=$('#clave1').val();
    var parametros={user_id,token,nuevaclave};
    
    
        $('#rowmensaje').html('<div><img src="../../img/2.gif"><br/><br>Un momento, por favor...</div>');
        $.ajax({
            type: "POST",
            url: "../usuarios/usuarioslogica.php?accion=guardaclave",
            data: parametros,
            dataType: "json",
            success: function (response) {
                
                if (response > 0) {

                    msg=`<div class="alert"  align="center">
                            <div id="CABmsg" class="alert alert-success">
                                <div id="titulomsg" class="alert-heading"><span class="glyphicon glyphicon-check" style="color:green"> Clave modificada exitosamente ! </div>
                                <div></div>
                                <div id="msg"><div><a href="../../index.php">Iniciar Sesi&oacute;n</a></div></div>
                            </div>
                        </div>`;
                        
                } else {
                    msg=`<div class="alert"  align="center">
                            <div id="CABmsg" class="alert alert-danger">
                                <div id="titulomsg" class="alert-heading">Error</div>
                                <div></div>
                                <div id="msg"><p>No fue posible realizar esta accion.</p><p>Por favor, Comuniquese con soporte</p></div>
                            </div>
                        </div>`;
                }
                $('#rowmensaje').html(msg);
                $('#user_id').val(null);
                $('#token').val(null);
                $('#nuevaclave').val(null);
                $('#clave1').val(null);
            }
        });
    
    } 



</script>
<!-- <style>
    .confirmacion{background:#C6FFD5;border:1px solid green;}
    .negacion{background:#ffcccc;border:1px solid red}
</style> -->