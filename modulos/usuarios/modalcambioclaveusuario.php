<?php
    session_start();
    if(!$_SESSION['autentic']){
        header('Location:../php_cerrar.php');
    }else{ 
?>
    <!-- Modal Cambio de Clave Usuario-->
    <div class="modal fade" id="modelCambioClave" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <div di="msgClaveUsuario"></div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
                        <h3 align="center" class="modal-title">Cambiar clave de usuario</h3>  
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label id="IdUsuario_cambioclave"></label>
                        </div>
                        <div class="form-group">
                            <label id="NombreUsuario_cambioclave"></label>
                        </div>            
                        <div class="form-group">
                            <label for="">Nueva Contraseña</label>
                            <input type="password" class="form-control" name="NuevaClave_cambioclave" id="NuevaClave_cambioclave" placeholder="">
                                Clave minimo de 6 caracteres.
                        </div>
                    </div>
                    <div class="modal-footer" id="modal-footer_CambioClave"></div>
                </div>
            </div>
        </div>

        
        <!-- Modal cambio de clave correcto-->
        <div id="CambioClave"></div>
<?php 
}
?>