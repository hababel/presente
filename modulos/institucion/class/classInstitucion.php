<?php
class Proceso_Institucion{

    public function __construct($InstanciaDB)
    {
        $this->ConnxClass=$InstanciaDB;
    }

	function InsertarInstitucion($CodDaneInstitucion,$NombreInstitucion,$idTipoInstitucion,$idMcpio,$idVereda){	
		$sql="INSERT INTO institucion (CodDaneInstitucion,NombreInstitucion,idTipoInstitucion,idMcpio,idVereda) 
		VALUES (".$CodDaneInstitucion.",'".$NombreInstitucion."',".$idTipoInstitucion.",".$idMcpio.",".$idVereda.")";
		// echo $sql;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
	}
	
	function actualizarInstitucion($idInstitucionFM,$CodDaneInstitucionFM,$NombreInstitucionFM,$idTipoInstitucionFM,
	$idMcpioFM,$idVeredaFM){
		$sql="UPDATE institucion SET 		
		CodDaneInstitucion=".$CodDaneInstitucionFM.",
		NombreInstitucion='".$NombreInstitucionFM."',
		idTipoInstitucion=".$idTipoInstitucionFM.",
		idVereda=".$idMcpioFM." ,
		idVereda=".$idVeredaFM." 
		WHERE idInstitucion=".$idInstitucionFM;
		$this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
		return $this->resultado;		
	}

	function listaInstitucion(){
		$sql="SELECT
			institucion.*,
			tipoinstitucion.DescTipoInstitucion,
			vereda.NombreVereda,
			municipio.NombreMunicipio
			FROM institucion
			INNER JOIN tipoinstitucion ON institucion.idTipoInstitucion=tipoinstitucion.idTipoInstitucion
			INNER JOIN vereda ON vereda.idVereda=institucion.idVereda
			INNER JOIN municipio ON  municipio.idMunicipio=vereda.Municipio_idMunicipio order BY NombreInstitucion ASC";

		$this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
		return $this->resultado;
	}

	function buscarInstitucion($idInstitucion){
		$sql="SELECT * FROM institucion WHERE idInstitucion=".$idInstitucion;
		$this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
		return $this->resultado;
	}

	function buscarInstitucionxDANE($CodDaneInstitucion){
		$sql="SELECT * FROM institucion WHERE CodDaneInstitucion=".$CodDaneInstitucion;
		$this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
		return $this->resultado;
	}

	function ListaInstitucionxVereda($IdVereda){
		$sql="SELECT
			institucion.*,
			tipoinstitucion.DescTipoInstitucion
			FROM institucion
			INNER JOIN tipoinstitucion ON institucion.idTipoInstitucion=tipoinstitucion.idTipoInstitucion
			WHERE idVereda=".$IdVereda." order by NombreInstitucion ASC";
		$this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
		return $this->resultado;
	}
}
?>