
<?php 
	$IdRecurso=44;
	include_once('../principal/CABparapermisos.php');
	
	/*
        Forma de colocar permisos en cada modulo.

        Funcion: colocar_permiso($IdUsuario,$accion,$IdRecurso)

        $IdUsuario: $IdUsuariologin=$_SESSION['id'];
        $accion: 1-Consultar, 2-Editar,3-Insertar,4-Eliminar,5-Listar - > Este nuemro verifica la columna de la tabal que relaciona el recurso_perfil
		$IdRecurso: Cod del recurso - Columna: CodRecurso de la tabla recurso.
		Retorna el valor que hay en l aBD (1-SI tiene permiso,0-NO tiene permiso)    
	*/
	if($_SESSION['autentic']){
        if($Permisos_Listar==1 || $useradmon){
            $IdUser=$_SESSION['id'];
            require_once("../conn_BD.php");
            require_once("../solicitud_gastos/class/ClassSolicitudGastos.php");
            require_once("../empleados/class/classEmpleados.php");
            require_once("class/ClassaprobacionSolicGasto.php");
            require_once("../entidades/class/classEntidades.php");
            require_once("../funciones.php");
            $InstanciaDB=new Conexion();
            $InstSolicGastos=new Proceso_SolicitudGastos($InstanciaDB);   
            $InstSolicitudGasto=new Proceso_SolicitudGastos($InstanciaDB);
            $InstEmpleados=new Proceso_Empleados($InstanciaDB);
            $InstAprobarSolicGastos=new Proceso_AprobacionSolicitudGastos($InstanciaDB);
            $InstEntidades=new Proceso_Entidades($InstanciaDB);
            $ListaSGxAprobar=$InstSolicGastos->ListarSolicitudGastosxEstado(0);
            $listaEmpleados=$InstEmpleados->ListarEmpleados();
            $listaEntidades=$InstEntidades->ListarEntidades();
            $ListaaprobacionSolicGastos=$InstAprobarSolicGastos->ListarAprobarSolicitudGastos();
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- <head> -->
    <?php include_once('../headScript.php'); ?>
    
<!-- </head> -->
<body>
	<?php 
		include_once('../headWeb.php');
		include_once("../../menu/m_principal.php");
	?>
            <div id="wrapper">
                <div id="page-wrapper">
                    <div id="page-inner">
                        <div class="panel-body" align="right">
                            <?php if( $Permisos_Insertar==1 || $useradmon ){ //Permiso para Insertar o Crear ?>
                                <button type="button" class="btn btn-success btn-circle" data-toggle="modal" onclick="NuevaAprobacion();" data-target="#NuevaAprobarSolicitudGasto">
                                    <i class="fa fa-plus fa-2x"></i>
                                </button>
                            <?php } ?> 
                        </div>
                        <div class="panel panel-primary">
                        <div class="panel-heading" style="height:55px;">
                            Solicitudes de Gastos -<b>pendientes por Aprobar</b>-
                            <div style="float:right;"><button type="button" class="btn btn-primary"><span class="badge">
                                <?php echo $ListaSGxAprobar->num_rows;?></span></button></div>
                            </div>
                            <div class="panel-body">
                                <?php 
                                    if ($ListaSGxAprobar->num_rows > 0) { ?>
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                                <thead>
                                                    <th><input type="checkbox" id="AprobarSG_Todas" name="AprobarSG_Todas"></th>
                                                    <th>#</th>
                                                    <th>Fecha Solicitud</th>
                                                    <th>Municipio</th>
                                                    <th>Actividad</th>
                                                    <th>Valor Solicitado</th>
                                                    <th>Usuario</th>
                                                    <th><span class='glyphicon glyphicon-cog' title='Config'></span></th>
                                                </thead>
                                                <tbody>
                                                <form id="formid" action="#" method="post">
                                                    <?php
                                                    while($row=$ListaSGxAprobar->fetch_array()){                                                    
                                                        $datos=$row[0]."||".$row[1];  
                                                        $ListaResponsables=$InstSolicitudGasto->ListaResponsablesxIDsolicitud($row[0]);
                                                        $datosResponsables='';
                                                        while ($rowR=$ListaResponsables->fetch_array()) {
                                                            $datosResponsables=$datosResponsables."||".$rowR[2];
                                                        }
                                                ?>
                                                    <tr class="odd gradeX">
                                                        <td><input type="checkbox" id="AprobarSG_<?php echo $row[0];?>" name="AprobarSG"  value="<?php echo $row[0];?>"></td>
                                                        <td><?php echo $row[0]; ?></td>
                                                        <td><?php echo $row[1]; ?></td>
                                                        <td><?php echo $row[13]; ?> <input type="text" class="form-control" name="MunicipioSG" id="MunicipioSG<?php echo $row[0];?>" style="display:none;" value="<?php echo $row[13]; ?>"></td>
                                                        <td><?php echo $row[16]; ?> 
                                                        <td><?php echo '$'.number_format($row[9]); ?><input type="text" class="form-control" name="ValorSG" id="ValorSG<?php echo $row[0];?>" style="display:none;" value="<?php echo $row[9]; ?>"></td>
                                                        <td><?php echo $row[24]?> <input type="text" class="form-control" name="UsuarioSG" id="UsuarioSG<?php echo $row[0];?>" style="display:none;" value="<?php echo $row[24]; ?>"></td>
                                                        <td>
                                                            <?php if($Permisos_Consultar==1 || $useradmon) { //Permisos para Consultar/Ver Registro ?>
                                                                <button title="Ver" onclick="VerSolicitudGasto('<?php echo $row[0];?>','<?php echo $datosResponsables;?>')" class="btn btn-default btn-sm" data-toggle="modal" data-target="#VerSolicitudGasto">
                                                                    <span class="glyphicon glyphicon-info-sign" style="color:blue;"></span>
                                                                </button> 
                                                            <?php } ?>

                                                            <?php if( $Permisos_Insertar==1 || $useradmon ){ //Permiso para Insertar o Crear ?>        
                                                                <button title="Aprobar Gasto" onclick="AprobarSolicitudGastodesdeSolicitudes('<?php echo  $row[0];?>')" class="btn btn-default btn-sm" data-toggle="modal" data-target="#NuevaAprobarSolicitudGasto">
                                                                    <span class="glyphicon glyphicon-arrow-right" style="color:green;"></span><span style="color:green;"> $</span>
                                                                </button>
                                                            <?php } ?>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                    </form>
                                                </tbody>
                                            </table>
                                        </div>
                                            <?php if( $Permisos_Insertar==1 || $useradmon ){ //Permiso para Insertar o Crear ?>                         
                                                <div id="BotonMultiSeleccion"><button type="button" id="AprobacionMultiple" class="btn btn-primary">Aprobación Múltiple</button></div>
                                            <?php } ?>
                                        <?php } else { ?>
                                        <div class="alert alert-dismissible alert-info">
                                            <strong>Buen trabajo!</strong> <b class="alert-link"> - No hay </b> Solicitudes de Gatos pendientes por Aprobar.
                                        </div>
                                <?php } ?>
                            </div>
                        </div>               
                    </div>
                </div>
                
            </div>
    

    <!-- Modal para Editar Aprobacion-->
        <div class="modal fade" id="modaleditAprobacion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel">Editar Aprobacion</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" action="" method="post">
                        <fieldset>
                            <div id="msgAprobarizEdit"></div>
                            <div class="form-group">
                                <label>Id Aprobacion</label>
                                <input id="idAprobarizFM" name="idAprobarizFM" type="text" placeholder="formeditIdAprobariz" class="form-control" autocomplete="off" disabled>
                            </div>                           
                            <div class="form-group">
                                <label>Id Solicitud Gasto</label>
                                <input id="idSolicGastoFM" name="idSolicGastoFM" type="text" placeholder="formeditIdSolicGasto" class="form-control" autocomplete="off" disabled>
                            </div>
                            <div class="form-group">
                                <label>Fecha de Aprobacion</label>
                                <input id="FechaAprobarizFM" name="FechaAprobarizFM" type="text" placeholder="formeditFechaAprobar" class="form-control" autocomplete="off" disabled>
                            </div>
                            <div class="form-group">
                                <label>Usuario Aprobacion</label>
                                <input id="UsuarioLegFM" name="UsuarioLegFM" type="text" placeholder="formeditUsuarioLeg" class="form-control" autocomplete="off" disabled>
                            </div>
                            <div class="form-group">
                                <label>Valor Aprobar</label>
                                <input id="ValorAprobarizFM" name="ValorAprobarizFM" type="text" placeholder="formeditValorAprobariz" class="form-control" autocomplete="off" required>
                            </div>
                          
                        </fieldset>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button class="btn btn-primary" onclick="EditarAprobariz();">Grabar</button>
                </div>
                </div>
            </div>
        </div>
    <!-- /Modal para Editar Aprobacion -->

    <!-- Inicio Modal Nueva Aprobacion de solicitud de gastos --> 

        <?php include_once('modalnuevaaprobacion.php'); ?>

    <!-- Final Modal Nuevo Solicitud de gastos -->

    <?php include_once('../solicitud_gastos/VerSG.php');?>

    <!-- Inicio Modal para Aprobacion Multiple -->
    <div class="modal fade" id="ModalAprobacionMultiple" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="modal-title">Aprobacion Multiple Solicitudes de Gasto</h3>
                    </div>
                    <div class="modal-body">
                        
                        <table class="table table-striped table-inverse table-responsive">
                            <thead class="thead-inverse">
                                <tr>
                                    <th>#</th>
                                    <th>Municipio</th>
                                    <th>Usuario</th>
                                    <th class="text-right">Valor</th>
                                </tr>
                                </thead>
                                <tbody id="TbodyAprobacionMasiva"></tbody>
                        </table>

                        <div class="row form-group">
                            <div class="col-md-5"><label>Empleado que aprueba</label></div>
                            <div class="col-md-6">
                                <select class="js-example-basic-single form-control" name="responsableSGAprobacionMultiple" id="responsableSGAprobacionMultiple" style="width:250px">
                                    <option value='0'>-- Seleccione Empleado --</option>
                                    <?php
                                        mysqli_data_seek($listaEmpleados, 0);
                                        while ($rowEM=$listaEmpleados->fetch_array(MYSQLI_BOTH)) { 
                                            echo "<option value='".$rowEM[0]."'>".$rowEM[2]."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                        <label for="">Observaciones</label>
                            <textarea class="form-control" name="ObservacionSGMultiple" id="ObservacionSGMultiple" rows="3"  style="resize: none;" placeholder="Se aprueba valor total de la solicitud de gastos"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer" id="modal-footerSG">
                        
                    </div>
                </div>
            </div>
        </div>
    <!-- Fin Modal para Aprobacion Multiple -->

    <!-- modal mensaje de error -->

    <div id="MsgModalSG"></div>

    <script src="../../assets/js/jquery-1.10.2.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script src="../../assets/js/jquery.metisMenu.js"></script>
    <script src="../../assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="../../assets/js/dataTables/dataTables.bootstrap.js"></script>
    <script src="../../assets/js/jasny-bootstrap.min.js"></script>
    
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    
    <script>
       $(document).ready(function() {

                $('#dataTables-example').dataTable({
                    "order": [[ 2, "ASC" ]],
                    "language": {
                        "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Spanish.json"
                    }
                } );
                
                $('.js-example-basic-multiple').select2({
                    placeholder:"Responsables"
                });
                
                $('#responsableSG2').select2();

                $('#responsableSGAprobacionMultiple').select2({
                            dropdownParent: $("#ModalAprobacionMultiple"),
                });

                $('.NumeroDias').on("keypress keyup blur",function (event) {    
                    $(this).val($(this).val().replace(/[^\d].+/, ""));
                    if ((event.which < 48 || event.which > 57)) {
                        if (event.which == 8) {
                            
                        }else{
                            event.preventDefault();
                        }
                    }
                });

                $("#ValorGasto").on({
                    "focus": function(event) {
                        $(event.target).select();
                    },
                    "keyup": function(event) {
                        $(event.target).val(function(index, value) {
                        return value.replace(/\D/g, "")
                            .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
                        });
                    }
                });

                $(".ValorGasto").on("keypress keyup",function (event) {    
                        $(this).val($(this).val().replace(/[^0-9\.]/g,','));
                        if (($(this).val().indexOf(',') != -1) && (event.which < 48 || event.which > 57)) {
                            if (event.which == 8 || event.which == 44) {
                            }else{
                                event.preventDefault();
                            }
                        }
                });

            $('#IdSolicitudGastoSG2').change(function () {
                var IdSG =$('#IdSolicitudGastoSG2').val();
                if (IdSG != '00') {
                    $('#IdSolicitudGastoSG2').attr('disabled', false);
                    $('#CabIdSG').show();
                    $('#GuardarAprobacion').attr('disabled', false);
                    $('#SelectMultipleResponsables').show();
                    $('#ListaDetalleSGApr').show();
                    $('#ListaDetalleSGAprobardesdeSG').show();
                    AprobarSolicitudGasto2(IdSG);
                } 
                
            });

            $( '#AprobarSG_Todas' ).on( 'change', function() {
                if( $(this).is(':checked') ){
                    //  alert("si el checkbox ha sido seleccionado")
                    $(":checkbox[name=AprobarSG]").prop('checked', true);
                } else {
                    //  alert("si el checkbox ha sido deseleccionado")
                    $(":checkbox[name=AprobarSG]").prop('checked', false);
                }
            });

            $('#AprobacionMultiple').click(function(){
                var SGSeleccionadas = [];
                var SGValoresSeleccion=[]; 
                var MunicipioSG=[];
                var UsuarioSG=[];
                
                $(":checkbox[name=AprobarSG]").each(function(){
                    if (this.checked) {
                        SGSeleccionadas.push($(this).val());
                        SGValoresSeleccion.push($('#ValorSG'+$(this).val()).val());
                        MunicipioSG.push($('#MunicipioSG'+$(this).val()).val());
                        UsuarioSG.push($('#UsuarioSG'+$(this).val()).val());
                    }
                });
                if (SGSeleccionadas.length > 1) {                               
                    $("#ModalAprobacionMultiple").modal("show");
                    var texto=null;
                    var total=null;
                    for (let r = 0; r < SGSeleccionadas.length; r++) {
                        texto+=
                        `<tr>
                            <td>` +SGSeleccionadas[r]+`</td>
                            <td>` +MunicipioSG[r]+`</td>
                            <td>`+UsuarioSG[r]+`</td>
                            <td class="text-right">$ ` +formatNumber.new(SGValoresSeleccion[r])+`</td>
                        </tr>`;
                        total+=Number(SGValoresSeleccion[r]);
                    }
                    
                    texto+=`<tr><td colspan="3">Total a aprobar</td><td class="text-right"><b>$ `+ formatNumber.new(total)+`</b></td></tr>`;
                    $('#TbodyAprobacionMasiva').html(texto);
                    $('#modal-footerSG').html(
                        '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>'+
                        '<button type="button" class="btn btn-primary" onclick="AprobacionSGMultiple(['+SGSeleccionadas+'],['+SGValoresSeleccion+']);">Aprobar</button>'
                    );
                }else{
                    alert('Debe seleccionar minimo dos (2) Solicitudes para aprobacion múltiple.');
                }
                return false;
            });

        });
      
    function NuevaAprobacion(){
        var codempleado=$("#empleadologin").val();
        $('#IdSolicitudGastoSG2').val("00").change();
        $('#IdSolicitudGastoSG2').attr('disabled', false);
        $('#responsableSG2').val(0).change();
        $('#VrAproSolicGastoSG').val(null);
        $('#ObservacionesAprobacionSG').val(null);
        $('#SelectMultipleResponsables').hide().change();
        $('#ListaDetalleSGApr').hide();
        $('#CabIdSG').hide();
        $('#ListaDetalleSGAprobardesdeSG').hide();
        $('#SelectMultipleResponsables').hide();
        $('#GuardarAprobacion').attr('disabled', true);
        $('#VerCodDepartamentoSGdivApr').html('').change();
        $('#VerCodMunicipioSGdivApr').html('').change();
        $('#VerCodVeredaSGdivApr').html('').change();
        $('#VerCodProyectoSGdivApr').html('').change();
        $('#VerCodProcesoSGdivApr').html('').change();
        $('#VerCodActividadSGdivApr').html('').change();
        $('#VerFechaHoraSalidaSGdivApr').html('').change();
        $('#VerFechaHoraRegresoSGdivApr').html('').change();
        $('#VerValorAprobadoSGdivApr').html('').change();
        $('#VerDocAnticipoSGdivApr').html('').change();
        $('#responsableSG2').val(codempleado).change();
    }

    </script>
   
</body>
</html>
<?php 
        }else{
            include_once('../principal/sinpermisolistar.php');         
            }
	}else{
		header('Location:../../php_cerrar.php');
	}
?>
