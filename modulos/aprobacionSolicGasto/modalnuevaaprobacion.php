    <?php $idempleado=$_SESSION['idempleado']; ?>
    <div class="modal fade" id="NuevaAprobarSolicitudGasto" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>															
                    <h3 align="center" class="modal-title" id="myModalLabel">Nueva Aprobacion Solicitud de Gastos </h3>
                    <div id="msgAprobacionSolicitudGasto"></div>
                </div>
                <div class="panel-body">
                    <div class="col-sm-12" style="display: flex;justify-content: center;">                                   
                        <form class="form-horizontal">
                            <div class="table-responsive table-striped">
                                <div class="row form-group">
                                    <div class="col-md-4"><label>Solicitud de Gasto</label></div>
                                    <div class="col-md-8">
                                        <select class="js-example-basic-single form-control" name="IdSolicitudGastoSG2" id="IdSolicitudGastoSG2" style="width:350px">
                                            <option value="00"> -- Seleccione una solicitud de gasto -- </option>                 
                                            <?php
                                                mysqli_data_seek($ListaSGxAprobar, 0);											
                                                while ($rowMun=$ListaSGxAprobar->fetch_array(MYSQLI_BOTH)) { 
                                                    
                                                    echo "<option value='".$rowMun[0]."'>"."#".$rowMun[0]." - ".$rowMun[1]." - ".$rowMun[13]."-$".number_format($rowMun[9])."</option>";
                                                }
                                            ?> 
                                            </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-4"><label>Empleado que aprueba</label></div>
                                    <div class="col-md-8">
                                        <select class="js-example-basic-single form-control" name="responsableSG2" id="responsableSG2" style="width:300px">
                                            <option value=0> -- Seleccione Empleado --</option>
                                            <?php
                                            
                                                mysqli_data_seek($listaEmpleados, 0);
                                                while ($rowEM=$listaEmpleados->fetch_array(MYSQLI_BOTH)) { 
                                                    echo "<option value='".$rowEM[0]."'>".$rowEM[2]."</option>";
                                                }
                                            ?>
                                        </select>
                                        <input type="hidden" name="empleadologin" id="empleadologin" value="<?php echo $idempleado; ?>">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-4"><label for="Valor Aprobar">Valor  Aprobar</label></div>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="VrAproSolicGastoSG" id="VrAproSolicGastoSG" aria-describedby="Valor a Aprobar"  placeholder="Aprobar valor SOLICITADO" autocomplete="off" require style="width:300px" onchange="MASK(this,this.value,'-$##,###,##0',1)">
                                        <small id="helpId" class="form-text text-muted">Vacio = Aprueba valor Solicitado</small>
                                    </div>
                                </div>
                                
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <label for="">Observaciones Solicitud Gasto - (Maximo 100 caracteres)</label>
                                        <textarea class="form-control" name="ObservacionesAprobacionSG" id="ObservacionesAprobacionSG" rows="1" cols="50" maxlength="100" style="resize: none;width:600px;"></textarea>
                                    </div>
                                </div>
                                <div div class="row form-group">
                                    <div class="col-md-12">
                                        <button type="button" id="GuardarAprobacion" onclick="guardarAprobacionSolicitudGasto();" class="btn btn-primary form-control">Guardar Aprobación</button>           
                                    </div>
                                </div>
                            </div>
                        </form>  
                    </div>    
                    
                    <div class="form-group">
                        <!-- Inicio Encabezado SG -->
                            <div id="CabIdSG">
                                <div class="table-responsive">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading clearfix">
                                                <h3 class="panel-title">Datos Documento Solicitud de Gasto</h3>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row col-md-6 col-sm-6">
                                                    <table class="table table-hover">
                                                        <tbody>
                                                            <tr class="table-light">
                                                                <th scope="row">Departamento</th>
                                                                <td>
                                                                    <div name="VerCodDepartamentoSGdivApr" id="VerCodDepartamentoSGdivApr"></div>
                                                                </td>
                                                            </tr>
                                                            <tr class="table-light">
                                                                <th scope="row">Municipio</th>
                                                                <td>
                                                                    <div name="VerCodMunicipioSGdivApr" id="VerCodMunicipioSGdivApr"></div>
                                                                </td>
                                                            </tr>
                                                            <tr class="table-light">
                                                                <th scope="row">Vereda</th>
                                                                <td>
                                                                    <div name="VerCodVeredaSGdivApr" id="VerCodVeredaSGdivApr"></div>
                                                                </td>
                                                            </tr>
                                                            <tr class="table-light">
                                                                <th scope="row">Proyecto</th>
                                                                <td>
                                                                    <div name="VerCodProyectoSGdivApr" id="VerCodProyectoSGdivApr"></div>
                                                                </td>
                                                            </tr>
                                                            <tr class="table-light">
                                                                <th scope="row">Proceso</th>
                                                                <td>
                                                                    <div name="VerCodProcesoSGdivApr" id="VerCodProcesoSGdivApr"></div>
                                                                </td>
                                                            </tr>
                                                            
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="row col-md-6 col-sm-6">
                                                    <table class="table table-hover">
                                                        <tbody>
                                                            <tr class="table-light">
                                                                <th scope="row">Actividad</th>
                                                                <td>
                                                                    <div name="VerCodActividadSGdivApr" id="VerCodActividadSGdivApr"></div>
                                                                </td>
                                                            </tr>
                                                            <tr class="table-light">
                                                                <th scope="row">Fecha/Hora Salida</th>
                                                                <td>
                                                                    <div name="VerFechaHoraSalidaSGdivApr" id="VerFechaHoraSalidaSGdivApr"></div>
                                                                </td>
                                                            </tr>
                                                            <tr class="table-light">
                                                                <th scope="row">Fecha/Hora Regreso</th>
                                                                <td>
                                                                    <div name="VerFechaHoraRegresoSGdivApr" id="VerFechaHoraRegresoSGdivApr"></div>
                                                                </td>
                                                            </tr>
                                                            <tr class="table-light">
                                                                <th scope="row">Valor Aprobado</th>
                                                                <td>
                                                                    <div name="VerValorAprobadoSGdivApr" id="VerValorAprobadoSGdivApr"></div>
                                                                </td>
                                                            </tr>
                                                            <tr class="table-light">
                                                                <th scope="row">Anticipo</th>
                                                                <td>
                                                                    <div name="VerDocAnticipoSGdivApr" id="VerDocAnticipoSGdivApr"></div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>                 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <!-- Fin Encabezado SG -->
                        <!-- Inicio Responsables y Entidades seleciones multiples -->
                            <div class="col-sm-12" id="SelectMultipleResponsables" style="display:none;padding-bottom: 15px;">
                                <div class="col-sm-5">
                                    <div class="col-sm-12">
                                        <label>Responsables</label>
                                    </div>
                                    <div class=" col-sm-12">
                                        <label>
                                            <select class="js-example-basic-multiple form-control" name="VerresponsableSGdiv2" id="VerresponsableSGdiv2" multiple="multiple" style="width:100%">
                                                <?php
                                                        mysqli_data_seek($listaEmpleados, 0);
                                                        while ($rowEM=$listaEmpleados->fetch_array(MYSQLI_BOTH)) { 
                                                            echo "<option value='".$rowEM[0]."'>".$rowEM[2]."</option>";
                                                        }
                                                    ?>
                                            </select>
                                        </label>
                                   </div>
                                </div>
                                <div class=" col-sm-2"></div>
                                <div class=" col-sm-5">
                                    <div class="col-sm-12">
                                        <label>Entidades</label>
                                    </div>
                                    <div class="col-sm-12">
                                        <label>
                                            <select class="js-example-basic-multiple form-control" name="VerentidadesSGdiv2" id="VerentidadesSGdiv2" multiple="multiple" style="width:100%">
                                                <?php
                                                    mysqli_data_seek($listaEntidades, 0);																		
                                                    while ($rowEnt=$listaEntidades->fetch_array(MYSQLI_BOTH)) { 
                                                        echo "<option value='".$rowEnt[0]."'>".$rowEnt[2]."</option>";
                                                        }
                                                ?>
                                            </select>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        <!-- Fin Responsables y Entidades seleciones multiples -->
                    </div>
                            
                    <br>
                    <div class="row">
                        <div class="col-sm-12">
                            <div id="ListaDetalleSGApr"></div>
                            <div id="ListaDetalleSGAprobardesdeSG"></div>
                        </div>
                    </div>
                </div>      

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>	
            </div>    
        </div>
    </div>