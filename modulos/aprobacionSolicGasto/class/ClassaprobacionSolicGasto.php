<?php 

Class Proceso_AprobacionSolicitudGastos{

    function __construct($InstanciaDB){
        $this->ConnxClass=$InstanciaDB;
     }

    function ListarAprobarSolicitudGastos(){
        $sql="SELECT * FROM aprobacionsolictudgasto";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function BuscarAprobarSolicitudGastosxid($idSolictudGasto){
        $sql="SELECT * FROM aprobacionsolictudgasto where idSolicitudGasto=".$idSolictudGasto;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function InsertarAprobacionSolicitudGastos($idSolicitudGasto, $FechaAprobacion,$UsuarioAprobacion,$ValorAprobacion,$ObservacionesAprobacionSG){
        $sql="INSERT INTO aprobacionsolictudgasto (idSolicitudGasto,FechaAprobacion,UsuarioAprobacion,ValorAprobacion,ObservacionesAprobacion)
         VALUES(".$idSolicitudGasto.",'".$FechaAprobacion."','".$UsuarioAprobacion."',".$ValorAprobacion.",'".$ObservacionesAprobacionSG."')";
    
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function EditarAprobacSolicitudGastos($idAprobacionSolictudGasto,$idSolicitudGasto,
    $FechaAprobacion,$UsuarioAprobacion,$ValorAprobacion){
        $sql="UPDATE aprobacionsolictudgasto SET idSolicitudGasto=".$idSolicitudGasto.", FechaAprobacion='".$FechaAprobacion."',
        UsuarioAprobacion='".$UsuarioAprobacion."', ValorAprobacion=".$ValorAprobacion."
       WHERE idAprobacionSolictudGasto=".$idAprobacionSolictudGasto;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function ObtenerultimaSolicitudGasto(){
        $lastid=0;
        $sql="SELECT AUTO_INCREMENT FROM  INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'secretospc' AND
           TABLE_NAME   = 'solicitudgastos'";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        if ($this->resultado->num_rows > 0) {
            $this->lastid=$this->resultado->fetch_array(MYSQLI_BOTH);
        }
        return $this->lastid[0];
    }

}
?>