<?php 
    session_start();
    require_once("../conn_BD.php");
    require_once("../funciones.php");
    
    if ($_POST) {
        $accion=$_GET['accion'];
        $InstanciaDB=new Conexion();
        switch ($accion) {
            case 'InstDetalleTarifaGNR':
                require_once('../tarifas/class/classtarifagastosgnr.php');
                $InstTarifasGRN=new Proceso_TarifasGNR($InstanciaDB);
                $CabTarifaGastoGNR=$_POST['CabTarifaGastoGNR'];
                $idTipoGasto=$_POST['TipoGasto'];
                $ValoresTarifasGNR=($_POST['ValoresForm']);
                $InsNewTarifaGNR=0;
                $UpdateCombinacionTarifa=0;
                echo count($ValoresTarifasGNR);
                foreach ($ValoresTarifasGNR as $value) { 
                    $EncontrarCombinacion=$InstTarifasGRN->BuscarCombinacionTarifa($CabTarifaGastoGNR,$value['name']);
                    if ($EncontrarCombinacion->num_rows > 0 ) {
                        $Tarifa=str_replace(',','',$value['value']);
                        $UpdateCombinacionTarifa=$InstTarifasGRN->UpdateCombinacionTarifa($CabTarifaGastoGNR,$value['name'],$Tarifa);
                    } else {
                        $InsNewTarifaGNR=$InstTarifasGRN->InsDetalleTarifa($CabTarifaGastoGNR,$value['name'],$value['value']);
                    }
                }

                if ($InsNewTarifaGNR != 0 || $UpdateCombinacionTarifa != 0) {
                   echo 1;
                } else {
                    echo 0;
                }
                

            break;
            case 'InstCABTarifaGNR':
                require_once('../tarifas/class/classtarifagastosgnr.php');
                $InstTarifasGRN=new Proceso_TarifasGNR($InstanciaDB);
                $TituloTarifaGNR=primera_mayuscula(limpiar($_POST['TituloTarifaGNR']));
                $TipoGasto=$_POST['TipoGasto'];
                $fechadatetimepicker=$_POST['fechadatetimepicker'];
                $UsuarioCreacion=$_POST['UsuarioCreacion'];
                $InsCabTarifa=$InstTarifasGRN->InsCABTarifa($TituloTarifaGNR,$TipoGasto,$UsuarioCreacion,1,$fechadatetimepicker);
                echo $InsCabTarifa;
            break;
            case 'BuscarDetalleTarifaGNRxCAB':
                require_once('../tarifas/class/classtarifagastosgnr.php');
                $InstTarifasGRN=new Proceso_TarifasGNR($InstanciaDB);
                $CabTarifaGastoGNR=$_POST['idCABTarifaGNR'];
                $ListaDetalleTarifaxCAB=$InstTarifasGRN->ListaDetalleTarifaxCAB($CabTarifaGastoGNR);
                echo json_encode($ListaDetalleTarifaxCAB->fetch_all());
            break;
            
            // ********* Inicio Tarifas Veredas ************

            case 'InstCABtarifasVeredas':
                require_once('../tarifas/class/classtarifagastosgnr.php');
                $InstTarifasVereda=new Proceso_TarifasGNR($InstanciaDB);
                $TituloTarifaVereda=primera_mayuscula(limpiar($_POST['tituloTarifa']));
                $CodVeredaSG=$_POST['CodVeredaSG'];
                $fechatarifavereda=$_POST['fechatvereda'];
                $usuariocreaciontvereda=$_POST['usuariocreaciontvereda'];
                $BuscarVeredaCabTarifa=$InstTarifasVereda->BuscarVeredaenCAB($CodVeredaSG);
                if ($BuscarVeredaCabTarifa->num_rows > 0) {
                    echo 0;
                } else {
                    $InsCabTarifaVereda=$InstTarifasVereda->InsCABTarifaVereda($TituloTarifaVereda,$CodVeredaSG,$usuariocreaciontvereda,1,$fechatarifavereda);
                    echo $InsCabTarifaVereda;
                }
                
            break;
            case 'InsertarDetalleTarifaVereda';
                require_once '../tarifas/class/classtarifagastosgnr.php';
                $InstTarifasVeredas=new Proceso_TarifasGNR($InstanciaDB);
                $idCABtarifavereda=$_POST['IdCABTarifaVereda'];
                $conceptogastoTveredas=$_POST['conceptogastoTveredas'];
                $ValorTarifaxVereda=$_POST['ValorTarifaxVereda'];
                $IdVeredaTarivaVeredas=$_POST['IdVeredaTarivaVeredas'];
                $BuscarTarifaXConceptoXVereda=$InstTarifasVeredas->BuscarTarifasConcepto_Veredas($IdVeredaTarivaVeredas,$conceptogastoTveredas);
                if ($BuscarTarifaXConceptoXVereda->num_rows > 0) {
                    $row=$BuscarTarifaXConceptoXVereda->fetch_array(MYSQLI_NUM);
                   echo json_encode($row);
                } else {
                    $InsDetalleTVereda=$InstTarifasVeredas->InsDetalleVeredaTarifa($conceptogastoTveredas,$idCABtarifavereda,$ValorTarifaxVereda);
                    echo $InsDetalleTVereda;
                }
               
            break;
            case 'ListarDetalleTarifaVeredasxCAB';
                require_once '../tarifas/class/classtarifagastosgnr.php';
                $InstTarifasVeredas=new Proceso_TarifasGNR($InstanciaDB);
                $idCABTarifaVereda=$_POST['idCABTarifaVereda'];
                $ListaDetalleTVeredaXCAB=$InstTarifasVeredas->ListarDetalleTarifaVeredaxCAB($idCABTarifaVereda);
                echo json_encode($ListaDetalleTVeredaXCAB->fetch_all(MYSQLI_NUM));
            break;
            case 'EliminarDetalleTarifaVeredasxCAB':
                require_once '../tarifas/class/classtarifagastosgnr.php';
                $InstTarifasVeredas=new Proceso_TarifasGNR($InstanciaDB);
                $IdDetalleTarifaVereda=$_POST['IdDetalleTarifaVereda'];
                $EliminarDetalleTarifasVeredas=$InstTarifasVeredas->eliminarDetalleTarifaVereda($IdDetalleTarifaVereda);
                if ($EliminarDetalleTarifasVeredas > 0) {
                    echo 1;
                } else {
                    echo 0;
                }
                
            break;
            case 'TraerCABTarifaxID':
                require_once '../tarifas/class/classtarifagastosgnr.php';
                $InstTarifasVeredas=new Proceso_TarifasGNR($InstanciaDB);
                $IdTarifaVeredaCAB=$_POST['IdTarifaVeredaCAB'];
                $CabTarifaVereda=$InstTarifasVeredas->ListarCABtarifasxID($IdTarifaVeredaCAB);
                echo json_encode($CabTarifaVereda->fetch_array(MYSQLI_NUM));
            break;

            case 'updateCABtarifasVeredas':
                require_once '../tarifas/class/classtarifagastosgnr.php';
                $InstTarifasVeredas=new Proceso_TarifasGNR($InstanciaDB);
                $idCABTarifaVereda=$_POST['idCABTarifaVereda'];
                $titulotarifaveredas=$_POST['titulotarifaveredas'];
                $idVereda=$_POST['idVereda'];
                $usuariocreaciontarifavereda=$_POST['usuariocreaciontarifavereda'];
                $UpdateCABtarifasVeredas=$InstTarifasVeredas->updateCABtarifasVeredas($idCABTarifaVereda,$titulotarifaveredas,$idVereda,$usuariocreaciontarifavereda);
                if ($UpdateCABtarifasVeredas > 0) {
                    echo 1;
                } else {
                    echo 0;
                }
                
            break;
            


        }
    }

?>