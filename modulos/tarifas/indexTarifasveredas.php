<?php 
    $IdRecurso=51;
    include_once('../principal/CABparapermisos.php');
    
    /*
        Forma de colocar permisos en cada modulo.

        Funcion: colocar_permiso($IdUsuario,$accion,$IdRecurso)

        $IdUsuario: $IdUsuariologin=$_SESSION['id'];
        $accion: 1-Consultar, 2-Editar,3-Insertar,4-Eliminar,5-Listar - > Este nuemro verifica la columna de la tabal que relaciona el recurso_perfil
        $IdRecurso: Cod del recurso - Columna: CodRecurso de la tabla recurso.
        Retorna el valor que hay en l aBD (1-SI tiene permiso,0-NO tiene permiso)    
    */
    if($_SESSION['autentic']){
        if($Permisos_Listar==1 || $useradmon){
		require_once("../conn_BD.php");
		require_once("../tarifas/class/classtarifagastosgnr.php");
		require_once("../conceptos_gastos/class/classConceptoGastos.php");
		require_once("../departamentos/class/ClassDepartamentos.php");
		$InstanciaDB=new Conexion();
        $InstCABTarifasveredas=new Proceso_TarifasGNR($InstanciaDB);
        $InstConcepto=new Proceso_ConceptoGastos($InstanciaDB);
        $InstDepartamento=new Proceso_Departamento($InstanciaDB);
		$ListaDepartamento=$InstDepartamento->ListaDepartamento();
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <!-- <head> -->
        <?php include_once('../headScript.php'); ?>
    <!-- </head> -->

<body>
		<?php 
			include_once('../headWeb.php');
			include_once("../../menu/m_principal.php");
		?>
    <div id="wrapper">
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="panel-body" align="right">
                    <?php if( $Permisos_Insertar==1 || $useradmon ){ //Permiso para Insertar o Crear ?> 
                        <button type="button" class="btn btn-success btn-circle" data-toggle="modal" onclick="formnuevotarifasveredas();" data-target="#modelInsTarifaVereda">
                            <i class="fa fa-plus fa-2x"></i>
                        </button>
                    <?php } ?>
                </div>
                <div id="UsuarioCreacion" style="display:none;"><?php echo $IdUsuariologin; ?></div>
                <div class="row">
                    <div class="col-md-12">
                        <!-- Advanced Tables -->
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <b>Tarifario Gastos de Veredas</b>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Fecha</th>
                                                <th>Titulo Tarifa</th>
                                                <th>Vereda</th>
                                                <th><span class='glyphicon glyphicon-cog' title='Config'></span>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php                                         
                                            $ListaTarifasVeredas=$InstCABTarifasveredas->ListaCABTarifasVeredas();
                                            while ($rowCABt=$ListaTarifasVeredas->fetch_array(MYSQLI_NUM)) { ?>
                                              
                                                <tr class="odd gradeX">
                                                    <td><?php echo $rowCABt[0];?></td>
                                                    <td><?php echo $rowCABt[5];?></td>
                                                    <td><?php echo $rowCABt[1];?></td>
                                                    <td><?php echo $rowCABt[6];?></td>
                                                    
                                                    <td class="center">
                                                        <div class="btn-group">
                                                            <?php if($Permisos_Editar==1 || $useradmon) { //Permisos para Editar/Modificar Registro ?>
                                                                <button title="Editar Tarifa Vereda" type="button" onclick="EditarTarifaVereda(<?php echo $rowCABt[0];?>)" class="btn btn-default btn-sm" data-toggle="modal" data-target="#modelInsTarifaVereda">
                                                                    <span class="glyphicon glyphicon-pencil"></span>
                                                                </button>
                                                            <?php } ?>
                                                            <?php if($Permisos_Consultar==1 || $useradmon) { //Permisos para Consultar/Ver Registro ?>
                                                                <button title="Ver Tarifa Vereda" onclick="VerTarifaVereda(<?php echo $rowCABt[0];?>)" class="btn btn-default btn-sm" data-toggle="modal" data-target="#ModalVerTarifas">
                                                                    <span class="glyphicon glyphicon-info-sign" style="color:blue;"></span>
                                                                </button>
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?PHP } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>

        <!-- Modal Insertar Nueva Tarifa -->
        <div class="modal fade" id="modelInsTarifaVereda" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="modal-title" id="TituloCABTarifaVereda"></h3>
                    </div>
                    
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="form-group col-sm-12">
                                <label for="" class="control-label">Titulo Tarifa</label>
                                <input type="text" class="form-control form-control-sm" name="tituloTarifa" id="tituloTarifa" aria-describedby="helpId" placeholder="" style="width:300px">
                            </div>

                            <div class="form-group col-sm-4">
                                <label for="" class="control-label">Departamento</label>
                                <select class="form form-control custom-select" name="CodDepartamentoSG" id="CodDepartamentoSG" style="width:160px">
                                    <option value="0">-Seleccione Dpto-</option>
                                    <?php
                                        																		
                                        while ($rowMun=$ListaDepartamento->fetch_array(MYSQLI_NUM)) { 
                                            echo "<option value='".$rowMun[0]."'>".$rowMun[2]."</option>";
                                            }?>
                                </select>
                            </div>

                            <div class="form-group col-sm-4">
                                <label for="" class="control-label">Municipio</label>
                                <select class="form-control custom-select" name="CodMunicipioSG" id="CodMunicipioSG" style="width:160px">                                    
                                </select>
                            </div>

                            <div class="form-group col-sm-4">
                                <label for="" class="control-label">Vereda</label>
                                <select class="form-control custom-select" name="CodVeredaSG" id="CodVeredaSG" style="width:160px">                                    
                                </select>
                            </div>

                            <div id="BotonTarifasCab">
                                
                            </div>
                        </div>

                        <div class="container-fluid">
                            <div id="DetalleTarifaVereda" style="margin-top: 20px;display:none;">
                                <div style="display:none;" id="IdCABTarifaVereda"></div>
                                <div class="form-group col-sm-5">  
                                    <label >Concepto de Gasto</label>
                                    <select class="custom-select form-control" name="conceptogastoTveredas" id="conceptogastoTveredas" style="width:100%">
                                        <option selected>-Seleccione Concepto-</option>        
                                        <?php
                                            $ListaConcepto=$InstConcepto->ListarConceptoGastosparaTarifasVeredas();
                                            while ($rowConcept = $ListaConcepto->fetch_array(MYSQLI_NUM)) {
                                            echo "<option value='".$rowConcept[0]."'>".$rowConcept[2]."</option>"; 
                                            }
                                        
                                        ?>
                                    </select>      
                                </div>
                                
                                <div class="form-group col-sm-5">
                                    <label for="">Valor</label>
                                    <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" class="form-control" name="ValorTarifaxVereda" id="ValorTarifaxVereda">
                                    </div>
                                </div>

                                <div class="col-sm-2 align-bottom" style="margin-top: 24px;">
                                    <button class="btn" onclick="InsertarDetalleTarifaVereda();">
                                        <span class="glyphicon glyphicon-chevron-down"></span>
                                    </button>
                                </div>
                                <div id="ListaDetalleTarifasVeredaCAB">
                                </div>            
                            </div>
                        </div>

                    </div>
					<div class="modal-footer" id="ModalFooterTarifaGNR" style="display:none;">
                        	
                	</div>
                </div>                
            </div>
        </div>


    <!-- INICIO Modal de ver tarifas -->
        
        <div class="modal fade" id="ModalVerTarifas" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="modal-title" id="TituloModalVerTarifasVeredas"></h3>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid" id="CabVerTarifaVereda">
                            <label class="col-sm-4">Titulo:</label> <div  id="TituloTarifaVereda"></div><br>
                            <label class="col-sm-4">Departamento:</label><div  id="DepartamentoTarifaVereda"></div><br>
                            <label class="col-sm-4">Municipio:</label><div  id="MunicipioTarifaVereda"></div><br>
                            <label class="col-sm-4">Vereda:</label><div  id="VeredaTarifaVereda"></div>
                        </div>
                        <div class="container-fluid" id="ListaDetalleVerTarifasVereda"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>

    <!-- FINAL Modal de ver tarifas -->
   
    <!-- INICIO Modal mensaje de campos vacios -->
    <div class="modal fade" id="ModalCamposVacios" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div id="MensajeaMostrar">
                        <div class="alert alert-dismissible alert-warning">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <b>Atencion !!</b>: Campos no pueden estar <strong>Vacios</strong>.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

	<!-- INICIO Modal mensaje de Error de Insercion -->
	<div class="modal fade" id="ModalMensajeErrorIns" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div id="MensajeaMostrar">
                        <div class="alert alert-dismissible alert-danger">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <b>Error !!</b>: Por favor comuniquese con soporte.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- FINAL Modal mensaje de Error de Insercion -->

	<!-- INICIO Modal mensaje de Insercion Correcta-->
	<div class="modal fade" id="ModalMensajeCorrecto" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div id="MensajeaMostrar">
                        <div class="alert alert-dismissible alert-success">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <b>Eureka !!</b> Insertado con Exito.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- FINAL Modal mensaje de Insercion Correcta -->

    <!-- INICIO Modal mensaje de Comcbinacion Vereda y Concepto ya tiene valor de tarifa-->
	<div class="modal fade" id="ModalMensajeVeredaConceptoexiste" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div id="MensajeaMostrar">
                        <div class="alert alert-dismissible alert-warning">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <b>Ya Existe !!</b> Ese concepto ya tiene un valor asignado con esa vereda.
                            <br>* Asi: 
                            <div id="Id_TarifaVereda"></div>
                            <div id="NombreConceptoTVereda"></div>
                            <div id="NombreVeredaTVereda"></div>
                            <div id="ValorVeredaTVereda"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- INICIO Modal mensaje de Combinacion Vereda y Concepto ya tiene valor de tarifa -->

    <!-- INICIO Modal mensaje de CAB Vereda encontrada tarifa-->
	<div class="modal fade" id="ModalMensajeVeredaTarifaExiste" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div id="MensajeaMostrar">
                        <div class="alert alert-dismissible alert-warning">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <b>Ya Existe !!</b> La Vereda ya tiene una combinacion de tarifas.
                            <!-- <button type="button" class="btn btn-primary"><span class="glyphicon-arrow-right"></span></button> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- INICIO Modal mensaje de CAB Vereda encontrada tarifa -->


    <script src="../../assets/js/jquery-1.10.2.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script src="../../assets/js/jquery.metisMenu.js"></script>
    <script src="../../assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="../../assets/js/dataTables/dataTables.bootstrap.js"></script>
    <script src="../../assets/js/jasny-bootstrap.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script type="text/javascript" src="funcionTarifas.js"></script>

    <script>
    $(document).ready(function() {

        $('#dataTables-example').dataTable();

        $('.custom-select').select2();
        $('#DetalleTarifaVereda').show();  

        $('#CodDepartamentoSG').change(function () { 
            var CodDepartamentoSG=$('#CodDepartamentoSG').val();
            if (CodDepartamentoSG != 0 || CodDepartamentoSG != '') {
                var parametros={CodDepartamentoSG};
                $.ajax({
                    type: "POST",
                    url: "../../logica/logica.php?accion=LlenaSelectMunicipioSegunDepartamento",
                    data: parametros,
                    cache: false,
                    dataType: "json",
                    success: function (data) {
                        $('#CodMunicipioSG').html(data);
                    }
                });   
            }  
        });

        $('#CodMunicipioSG').change(function () {
            var CodMunicipioSelect=$('#CodMunicipioSG').val();
            if (CodMunicipioSelect != 0) {
                var parametros={CodMunicipioSelect};
                $.ajax({
                    type: "POST",
                    url: "../../logica/logica.php?accion=LlenaSelectVeredasSegunMunicipio",
                    data: parametros,
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        $('#CodVeredaSG').html(response).change();
                    }
                });
            }  
        });

    });

    function creartarifasveredas(){
        var tituloTarifa=$('#tituloTarifa').val();
        var CodDepartamentoSG=$('#CodDepartamentoSG').val();
        var CodMunicipioSG=$('#CodMunicipioSG').val();
        var CodVeredaSG=$('#CodVeredaSG').val();
        if (tituloTarifa == "" || CodDepartamentoSG == 0 || CodMunicipioSG == 0 || CodVeredaSG == 0) {
            var $miModal = $('#ModalCamposVacios');
            $miModal.modal('show');
            setTimeout(function() {
                $miModal.modal('toggle');
                $('.modal-backdrop').remove();
                }, 4000);
        } else {
            var fechatvereda=$('#fechadatetimepicker').text();
            var usuariocreaciontvereda=$('#UsuarioCreacion').text();

            var parametros={CodVeredaSG,tituloTarifa,usuariocreaciontvereda,fechatvereda};
            $.ajax({
                type: "POST",
                url: "logicaTarifas.php?accion=InstCABtarifasVeredas",
                data: parametros,
                // dataType: "json",
                success: function (response) {
                    if (response != 0 ) {
                        $('#DetalleTarifaVereda').show();
                        $('#IdCABTarifaVereda').html(response);
                        $('#tituloTarifa').prop( "disabled", true );
                        $('#CodDepartamentoSG').prop( "disabled", true );
                        $('#CodMunicipioSG').prop( "disabled", true );
                        $('#CodVeredaSG').prop( "disabled", true );  
                        $('#creartarifasveredas').prop( "disabled", true );
                    } else {
                        var $miModal = $('#ModalMensajeVeredaTarifaExiste');
                        $miModal.modal('show');
                        setTimeout(function() {
                            $miModal.modal('toggle');
                            $('.modal-backdrop').remove();
                            }, 4000);
                    }
                      

                }
            });  
        }
    }

    function formnuevotarifasveredas() {

        $('#TituloCABTarifaVereda').html('Insertar Tarifa Nueva para Veredas').change();
        $('#tituloTarifa').val(null).change();
        $('#CodDepartamentoSG').val(null);
        $('#DetalleTarifaVereda').hide();
        $('#BotonTarifasCab').html('<button type="button" class="btn btn-primary form-control" onclick="creartarifasveredas();" id="creartarifasveredas">Crear Tarifa Nueva</button>');

    }

    </script>
    <!-- CUSTOM SCRIPTS -->
    <script src="../../assets/js/custom.js"></script>
</body>

</html>

<?php }else{
            include_once('../principal/sinpermisolistar.php');         
        }
	}else{
		header('Location:../../php_cerrar.php');
	}
?>