<?php 
	$IdRecurso=27;
    include_once('../principal/CABparapermisos.php');
    
    /*
        Forma de colocar permisos en cada modulo.

        Funcion: colocar_permiso($IdUsuario,$accion,$IdRecurso)

        $IdUsuario: $IdUsuariologin=$_SESSION['id'];
        $accion: 1-Consultar, 2-Editar,3-Insertar,4-Eliminar,5-Listar - > Este nuemro verifica la columna de la tabal que relaciona el recurso_perfil
        $IdRecurso: Cod del recurso - Columna: CodRecurso de la tabla recurso.
        Retorna el valor que hay en l aBD (1-SI tiene permiso,0-NO tiene permiso)    
    */
    if($_SESSION['autentic']){
        if($Permisos_Listar==1 || $useradmon){
		require_once("../conn_BD.php");
		require_once("../tarifas/class/classtarifagastosgnr.php");
        require_once("../empleados/class/classEmpleados.php");
		require_once("../tipo_gastos/class/ClassTipodeGasto.php");
		require_once("../tipo_tarifas/class/ClassTipoTarifa.php");
		$InstanciaDB=new Conexion();
        $InstCABTarifasGNR=new Proceso_TarifasGNR($InstanciaDB);
        $InstEmpleados=new Proceso_Empleados($InstanciaDB);
        $InstTipoGastos=new Proceso_TipoGastos($InstanciaDB);
        $InsTipoTarifa=new Proceso_TipoTarifa($InstanciaDB);

		$ListaTipoGasto=$InstTipoGastos->listatipogastos();
		$ListaTipoTarifa=$InsTipoTarifa->Listatipotarifa();

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- <head> -->
<?php include_once('../headScript.php'); ?>
<!-- </head> -->

<body>
		<?php 
			include_once('../headWeb.php');
			include_once("../../menu/m_principal.php");
		?>
    <div id="wrapper">
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="panel-body" align="right">
                    <?php if( $Permisos_Insertar==1 || $useradmon ){ //Permiso para Insertar o Crear ?> 
                        <button type="button" class="btn btn-success btn-circle" data-toggle="modal" onclick="FormNuevo();" data-target="#modelInsTarifaGNR">
                            <i class="fa fa-plus fa-2x"></i>
                        </button>
                    <?php } ?> 
                </div>
                <div id="UsuarioCreacion" style="display:none;"><?php echo $IdUsuariologin; ?></div>
                <div class="row">
                    <div class="col-md-12">
                        <!-- Advanced Tables -->
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <b>Tarifario Tipo de Gastos</b>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Fecha</th>
                                                <th>Titulo</th>
                                                <th>Tipo de Gasto</th>
                                                <th><span class='glyphicon glyphicon-cog' title='Config'></span>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php 
                                        
                                            $ListaCabTarifa=$InstCABTarifasGNR->ListarTarifasCAB();
                                            while ($rowCABt=$ListaCabTarifa->fetch_array(MYSQLI_NUM)) { ?>
                                              
                                                <tr class="odd gradeX">
                                                    <td><?php echo $rowCABt[0];?></td>
                                                    <td><?php echo $rowCABt[5];?></td>
                                                    <td><?php echo $rowCABt[1];?></td>
                                                    <td><?php echo $rowCABt[6];?></td>
                                                    <td class="center">
                                                        <div class="btn-group">
                                                            <?php if($Permisos_Editar==1 || $useradmon) { //Permisos para Editar/Modificar Registro ?>
                                                                <button type="button" onclick="EditarTarifa(<?php echo $rowCABt[0];?>,'<?php echo $rowCABt[1];?>',<?php echo $rowCABt[2];?>)" class="btn btn-default btn-sm" data-toggle="modal" data-target="#modelInsTarifaGNR">
                                                                    <span class="glyphicon glyphicon-pencil"></span>
                                                                </button>
                                                            <?php } ?>
                                                            <?php if($Permisos_Consultar==1 || $useradmon) { //Permisos para Consultar/Ver Registro ?>
                                                                <button title="Ver" onclick="VerTarifaGNR(<?php echo $rowCABt[0];?>,'<?php echo $rowCABt[1];?>',<?php echo $rowCABt[2];?>)" class="btn btn-default btn-sm" data-toggle="modal" data-target="#modelInsTarifaGNR">
                                                                    <span class="glyphicon glyphicon-info-sign" style="color:blue;"></span>
                                                                </button>
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?PHP } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>

        <!-- Modal Insertar Nueva Tarifa -->
        <div class="modal fade" id="modelInsTarifaGNR" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="modal-title" id="TituloCABTarifaGNR"></h3>
                    </div>
                    <div id="CabTarifaGastoGNR" style="display:none;"></div>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="col-sm-12">
                                <div class="col-sm-4">
                                    <label>Titulo Tarifa</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="TituloTarifaGNR" id="TituloTarifaGNR" aria-describedby="helpId" placeholder="">
                                </div>
                                <div class="col-sm-4">
									<label for="">Tipo de Gasto</label>
								</div>
                                <div class="col-sm-8">
									<select class="custom-select form-control form-control-sm" name="TipoGasto" id="TipoGasto" style="width:300px;">
										<option value=0>-- Seleccione Tipo Gasto --</option>
										<?php 
											while ($rowTP=$ListaTipoGasto->fetch_array(MYSQLI_NUM)) {
												echo "<option value=".$rowTP[0].">".$rowTP[2]."</option>";
											}
										?>
									</select>
								</div>
                            </div>
                            <div id="BotonTarifasCab">
                                
                            </div>

							<div class="row">
								<div class="col-sm-12" id="TipoTarifas" style="display:none;">
                                    <div class="col-sm-12">
                                        <label for="">Tipos de Tarifa</label>
                                    </div>
									<?php
										echo '<form id="TarifasTT">';
										while ($rowTT=$ListaTipoTarifa->fetch_array(MYSQLI_NUM)) { 
											echo '
												<div class="col-md-4 col-sm-4 col-xs-4">
													<div>'.$rowTT[1].'</div>
													<div class="input-group">
														<span class="input-group-addon">$</span>
														<input name="'.$rowTT[0].'" id="'.$rowTT[0].'" class="form-control inputTarifas" type="text">
													</div>
												</div>
											';
										} ?>
									</form>
								</div>
							</div>
                        </div>
                    </div>
					<div class="modal-footer" id="ModalFooterTarifaGNR" style="display:none;">
						
                	</div>
                </div>
                
            </div>
        </div>
   

    <!-- INICIO Modal mensaje de campos vacios -->
    <div class="modal fade" id="ModalCamposVacios" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div id="MensajeaMostrar">
                        <div class="alert alert-dismissible alert-warning">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <b>Atencion !!</b>: Campos no pueden estar <strong>Vacios</strong>, al menos uno debe tener valor.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

	<!-- INICIO Modal mensaje de Error de Insercion -->
	<div class="modal fade" id="ModalMensajeErrorIns" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div id="MensajeaMostrar">
                            <div class="alert alert-dismissible alert-danger">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <b>Error !!</b>: Por favor comuniquese con soporte.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!-- FINAL Modal mensaje de Error de Insercion -->

	<!-- INICIO Modal mensaje de Insercion Correcta-->
	<div class="modal fade" id="ModalMensajeCorrecto" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div id="MensajeaMostrar">
                            <div class="alert alert-dismissible alert-success">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <b>Eureka !!</b> Insertado con Exito.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!-- FINAL Modal mensaje de Insercion Correcta -->


    <script src="../../assets/js/jquery-1.10.2.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script src="../../assets/js/jquery.metisMenu.js"></script>
    <script src="../../assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="../../assets/js/dataTables/dataTables.bootstrap.js"></script>
    <script src="../../assets/js/jasny-bootstrap.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script type="text/javascript" src="funcionTarifas.js"></script>

    <script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable();

        $('.custom-select').select2();

    });

    function FormNuevo() {
        $('#TituloCABTarifaGNR').html('Insertar Tarifa Nueva').change();
        $('#TipoGasto').prop('disabled', false).change();
        $('#TituloTarifaGNR').prop('disabled', false).change();
        $('#TipoGasto').val(0).change();
        $('#TipoTarifa').val(0).change();
        $('#TituloTarifaGNR').val(null).change();
        $('#DesacripcionTerifaGNR').val(null);
		$('#ValorTarifa').val(null);
        $('#TipoTarifas').hide();
        $('#ModalFooterTarifaGNR').hide();
        $('#CrearTarifaCab').prop('disabled', false).change();
        $('#BotonTarifasCab').show();
        $('#BotonTarifasCab').html(`
            <input name="CrearTarifaCab" id="CrearTarifaCab" onclick="CrearTarifaCab();" class="btn btn-primary" type="button" value="Crear Tarifa">
        `);
        $('.inputTarifas').val('').change();
        
    }
    </script>
    <!-- CUSTOM SCRIPTS -->
    <script src="../../assets/js/custom.js"></script>
</body>

</html>
<?php }else{
            include_once('../principal/sinpermisolistar.php');         
        }
	}else{
		header('Location:../../php_cerrar.php');
	}
?>