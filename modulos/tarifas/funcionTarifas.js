function InstDetalleTarifaGNR(){
    var TipoGasto=$('#TipoGasto').val();
    var ValoresForm = $('#TarifasTT').serializeArray();
    var CabTarifaGastoGNR=$('#CabTarifaGastoGNR').text();
    var InputVacios=0;
    
    for (let j = 0; j < ValoresForm.length; j++) {
        if(ValoresForm[j].value == '') {
            $('#'+ValoresForm[j].name).val(0).change();
            InputVacios++;
        }
    }

    if (TipoGasto == 0 || InputVacios == ValoresForm.length) {
      var $miModal1 = $('#ModalCamposVacios');
      $miModal1.modal('show');
      setTimeout(function() { $miModal1.modal('toggle'); }, 4000);
    } else {
        var parametros={TipoGasto,ValoresForm,CabTarifaGastoGNR};
        $.ajax({
            type: "POST",
            url: "logicaTarifas.php?accion=InstDetalleTarifaGNR",
            data: parametros,
            // dataType: "json",
            success: function (response) {
                if (response == 0) {
                    var $miModal1 = $('#ModalMensajeErrorIns');
                    $miModal1.modal('show');
                    setTimeout(function() { $miModal1.modal('toggle'); }, 4000);
                } else{
                    $("#modelInsTarifaGNR").modal('hide');
                    var $miModal1 = $('#ModalMensajeCorrecto');
                    $miModal1.modal('show');

                    setTimeout(function() {
                        $miModal1.modal('toggle');
                        $('.modal-backdrop').remove();
                      }, 4000);
                    
                      location.reload(); 
                }
            }
        });
    }
  }

function CrearTarifaCab(){
    
    var TituloTarifaGNR=$('#TituloTarifaGNR').val();
    var TipoGasto=$('#TipoGasto').val();
    var fechadatetimepicker=$('#fechadatetimepicker').text();
    var UsuarioCreacion=$('#UsuarioCreacion').text();
    if (TituloTarifaGNR=='' || TipoGasto==0 ) {
        var $miModal1 = $('#ModalCamposVacios');
        $miModal1.modal('show');
        setTimeout(function() { $miModal1.modal('toggle'); }, 4000);
    } else {
        var parametros={TituloTarifaGNR,TipoGasto,fechadatetimepicker,UsuarioCreacion};
        $.ajax({
            type: "POST",
            url: "logicaTarifas.php?accion=InstCABTarifaGNR",
            data: parametros,
            // dataType: "dataType",
            success: function (response) {
                $('#TipoGasto').prop('disabled', true).change();
                $('#TituloTarifaGNR').prop('disabled', true).change();
                $('#CrearTarifaCab').prop('disabled', true).change();
                $('#TipoTarifas').show();
                $('#ModalFooterTarifaGNR').html(`
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" onclick="InstDetalleTarifaGNR();">Guardar</button>
                `);
                $('#ModalFooterTarifaGNR').show();
                $('#CabTarifaGastoGNR').html(response);
            }
        });
    }
}

function EditarTarifa(idCABTarifaGNR,TituloCABGNR,idTipoGasto){
    $('#TituloCABTarifaGNR').html('Editar Tarifa - #' + idCABTarifaGNR).change();
    $('#CabTarifaGastoGNR').html(idCABTarifaGNR);
    $('#TipoGasto').val(idTipoGasto).change();
    $('#TituloTarifaGNR').val(TituloCABGNR).change();
    $('#TipoGasto').prop('disabled', true).change();
    $('#TituloTarifaGNR').prop('disabled', true).change();
    $('#BotonTarifasCab').hide();
    $('#TipoTarifas').show();
    var parametros={idCABTarifaGNR};
    $.ajax({
        type: "POST",
        url: "logicaTarifas.php?accion=BuscarDetalleTarifaGNRxCAB",
        data: parametros,
        dataType: "json",
        success: function (response) {

            for (let reg = 0; reg < response.length; reg++) {
                $('#'+response[reg][2]).val(formatNumber.new(response[reg][3])).change();
                $('#'+response[reg][2]).prop('disabled', false).change();
                $('#ModalFooterTarifaGNR').show();
            }
            
            $('#ModalFooterTarifaGNR').html(`
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" onclick="InstDetalleTarifaGNR();">Cambiar valores</button>
            `); 
        }
    });
}

function VerTarifaGNR(idCABTarifaGNR,TituloCABGNR,idTipoGasto){
    $('#TituloCABTarifaGNR').html('Ver Tarifa - #' + idCABTarifaGNR).change();
    $('#TipoGasto').val(idTipoGasto).change();
    $('#TituloTarifaGNR').val(TituloCABGNR).change();
    $('#TipoGasto').prop('disabled', true).change();
    $('#TituloTarifaGNR').prop('disabled', true).change();
    $('#TipoTarifas').show();
    var parametros={idCABTarifaGNR};
    $.ajax({
        type: "POST",
        url: "logicaTarifas.php?accion=BuscarDetalleTarifaGNRxCAB",
        data: parametros,
        dataType: "json",
        success: function (response) {
            for (let reg = 0; reg < response.length; reg++) {
                $('#'+response[reg][2]).val(formatNumber.new(response[reg][3])).change();
                $('#'+response[reg][2]).prop('disabled', true).change();
                $('#ModalFooterTarifaGNR').show();
            }
            $('#ModalFooterTarifaGNR').html(`
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            `);
        }
    });
}

var formatNumber = {
    separador: ",", // separador para los miles
    sepDecimal: '.', // separador para los decimales
    formatear:function (num){
    num +='';
    var splitStr = num.split('.');
    var splitLeft = splitStr[0];
    var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
    var regx = /(\d+)(\d{3})/;
    while (regx.test(splitLeft)) {
    splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
    }
    return this.simbol + splitLeft +splitRight;
    },
    new:function(num, simbol){
      this.simbol = simbol ||'';
      return this.formatear(num);
    }
   }

   //    ********* Tarifas Veredas ************

   function InsertarDetalleTarifaVereda(){
    var IdCABTarifaVereda=$('#IdCABTarifaVereda').text();
    var conceptogastoTveredas=$('#conceptogastoTveredas').val();
    var ValorTarifaxVereda=$('#ValorTarifaxVereda').val();
    var IdVeredaTarivaVeredas=$('#CodVeredaSG').val();
    var parametros={IdCABTarifaVereda,conceptogastoTveredas,ValorTarifaxVereda,IdVeredaTarivaVeredas};
    $.ajax({
        type: "POST",
        url: "logicaTarifas.php?accion=InsertarDetalleTarifaVereda",
        data: parametros,
        dataType: "json",
        success: function (response) {
            var tipo=typeof response;
           if (tipo == 'object') {
            var $miModal = $('#ModalMensajeVeredaConceptoexiste');
            $('#Id_TarifaVereda').html("Nombre tarifa:"+response[7]);
            $('#NombreConceptoTVereda').html("Concepto: "+response[4]);
            $('#NombreVeredaTVereda').html("Vereda: "+response[5]);
            $('#ValorVeredaTVereda').html("Valor: $"+formatNumber.new(response[3]));
            $miModal.modal('show');
            setTimeout(function() {
                $miModal.modal('toggle');
                $('.modal-backdrop').remove();
            },600);
           } else {
                ListarDetalleTVereda(IdCABTarifaVereda,'ListaDetalleTarifasVeredaCAB',1);
           }
        }
    });
   }


   function ListarDetalleTVereda(idCABTarifaVereda,NombreDOM,Editar){
    var parametros={idCABTarifaVereda};
    
    $.ajax({
        type: "POST",
        url: "logicaTarifas.php?accion=ListarDetalleTarifaVeredasxCAB",
        data: parametros,
        dataType: "json",
        success: function (response) {
            
            var texto=
            `
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr> 
                            <th>#</th>
                            <th>Concepto</th>
                            <th>Valor</th>`
                            if (Editar == 1) {
                                texto+=`<th><span class="glyphicon glyphicon-cog"></span></th>`;
                            }
                        texto+=`</tr>
                    </thead>
                    <tbody>`;

                        for (let x = 0; x < response.length; x++) {
                           
                        texto+=`
                        <tr>
                            <td>`+(x+1)+`</td>
                            <td>`+response[x][4]+`</td>
                            <td>$ `+formatNumber.new(response[x][3])+`</td>`
                            if (Editar == 1) {
                                texto+=`
                                    <td>
                                        <a href="#" onclick="eliminar_detalleTarifaVeredaxCAB(`+response[x][0]+`);">
                                            <span class="glyphicon glyphicon-trash" style="color:red;"></span>
                                        </a>
                                    </td>
                                `;
                            }
                            texto+=`</tr>`; 
                        }
                    texto+=`</tbody>
                </table>
            `;
            $('#'+NombreDOM).html(texto);
        }
    });
   }

    function eliminar_detalleTarifaVeredaxCAB(IdDetalleTarifaVereda){
        var IdCABTarifaVereda=$('#IdCABTarifaVereda').text();
        var parametros={IdDetalleTarifaVereda};
        $.ajax({
            type: "POST",
            url: "logicaTarifas.php?accion=EliminarDetalleTarifaVeredasxCAB",
            data: parametros,
            dataType: "json",
            success: function (response) {
                if (response > 0) {
                    ListarDetalleTVereda(IdCABTarifaVereda,'ListaDetalleTarifasVeredaCAB',1);
                } else {
                    var $miModal = $('#ModalMensajeVeredaConceptoexiste');
                    $miModal.modal('show');
                }
            }
        });
    }

   function EditarTarifaVereda(IdTarifaVeredaCAB){
        var parametros={IdTarifaVeredaCAB};
        $.ajax({
            type: "POST",
            url: "logicaTarifas.php?accion=TraerCABTarifaxID",
            data: parametros,
            dataType: "json",
            success: function (response) {
                $('#IdCABTarifaVereda').html(response[0]);
                $('#TituloCABTarifaVereda').html('Editar Tarifa para Veredas - # <strong>'+response[0]+'</strong>');
                $('#tituloTarifa').val(response[1]);
                $('#CodDepartamentoSG option[value="' + response[9] +'"]').prop("selected", 'selected').trigger("change");
                CargarMunicpioXDepartamento(response[9]);
                ListarDetalleTVereda(IdTarifaVeredaCAB,'ListaDetalleTarifasVeredaCAB',1);
                ident=response[7];
                $('#CodMunicipioSG option[value="' + ident +'"]').prop("selected", 'selected').change();
                $('#BotonTarifasCab').html('<button type="button" class="btn btn-primary form-control" onclick="updateCABtarifasVeredas('+response[0]+');" id="Editartarifasveredas">Guardar Cambios</button>');
                $('#ModalFooterTarifaGNR').show()
                $('#ModalFooterTarifaGNR').html(`
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Terminar</button>
                `);
            }
        });
        
   }

   function VerTarifaVereda(IdTarifaVeredaCAB){
        var parametros={IdTarifaVeredaCAB};
        ListarDetalleTVereda(IdTarifaVeredaCAB,'ListaDetalleVerTarifasVereda',0);
        $.ajax({
            type: "POST",
            url: "logicaTarifas.php?accion=TraerCABTarifaxID",
            data: parametros,
            dataType: "json",
            success: function (response) {
                $('#TituloModalVerTarifasVeredas').html('Tarifas Veredas -#'+response[0]);
                $('#TituloTarifaVereda').html(response[1]); 
                $('#VeredaTarifaVereda').html(response[6]);
                // $('#MunicipioTarifaVereda').html(response[8]); 
                $('#DepartamentoTarifaVereda').html(response[10]);
                var data = {
                    id: response[8],
                };
                var newOption = new Option(data.id, false, false);
                $('#MunicipioTarifaVereda').append(newOption);
            }
        });
        $('#ModalFooterTarifaGNR').show()
        $('#ModalFooterTarifaGNR').html(`
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        `);
    }

    function CargarMunicpioXDepartamento(CodMunicipioSelect){
        if (CodMunicipioSelect != 0) {
            var parametros={CodMunicipioSelect};
            $.ajax({
                type: "POST",
                url: "../../logica/logica.php?accion=LlenaSelectVeredasSegunMunicipio",
                data: parametros,
                dataType: "json",
                cache: false,
                success: function (response) {
                    $('#CodVeredaSG').html(response).change();
                }
            });
        }
    }

    function updateCABtarifasVeredas(idCABTarifaVereda){ 
        
        var titulotarifaveredas=$('#tituloTarifa').val();
        var idVereda=$('#CodVeredaSG').val();
        var usuariocreaciontarifavereda=$('#UsuarioCreacion').text();
        if (titulotarifaveredas == "" || idVereda == 0 || idVereda == '') {
            var $miModal1 = $('#ModalCamposVacios');
            $miModal1.modal('show');
            setTimeout(function() { $miModal1.modal('toggle'); }, 4000);
        } else {
            var parametros={idCABTarifaVereda,titulotarifaveredas,idVereda,usuariocreaciontarifavereda};
            $.ajax({
                type: "POST",
                url: "logicaTarifas.php?accion=updateCABtarifasVeredas",
                data: parametros,
                dataType: "json",
                success: function (response) {
                    if (response == 1) {
                        var $miModal1 = $('#ModalMensajeCorrecto');
                        $miModal1.modal('show');
                        setTimeout(function() { $miModal1.modal('toggle'); }, 4000);
                    } else {
                        var $miModal1 = $('#ModalMensajeErrorIns');
                        $miModal1.modal('show');
                        setTimeout(function() { $miModal1.modal('toggle');
                        $('.modal-backdrop').remove(); }, 4000);  
                    }
                }
            });          
        }
    }  
 
    
    