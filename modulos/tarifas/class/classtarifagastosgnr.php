<?php

Class Proceso_TarifasGNR{


    function __construct($InstanciaDB){ 
        $this->ConnxClass=$InstanciaDB;
     }

     function ListarTarifasCAB(){
        $sql="SELECT cabtarifagastognr.*,
                     tipodegasto.DescTipodeGasto
              FROM cabtarifagastognr
              INNER JOIN tipodegasto ON tipodegasto.idTipodeGasto=cabtarifagastognr.idTipoGasto";
 
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
     }

     function InsCABTarifa($TituloTarifaGastoGNR,$TipoGasto,$UsuarioTarifaGastoGNR,$EstadoTarifaGastoGNR,$FechaCreacionTarifaGastoGNR){
         $sql="INSERT INTO cabtarifagastognr
         (TituloTarifaGastoGNR,idTipoGasto,UsuarioTarifaGastoGNR,EstadoTarifaGastoGNR,FechaCreacionTarifaGastoGNR)
         VALUES ('".$TituloTarifaGastoGNR."',".$TipoGasto.",".$UsuarioTarifaGastoGNR.",".$EstadoTarifaGastoGNR.",'".$FechaCreacionTarifaGastoGNR."')";

         $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->ConnxClass->link->insert_id;
     }


    // Detalle Tarifas GNR
    function InsDetalleTarifa($CabTarifaGastoGNR,$idTipoTarifa,$ValorGastosGnr){
        $sql="INSERT INTO detalletarifagastosgnr (CabTarifaGastoGNR,idTipoTarifa,ValorGastosGnr)
        VALUES (".$CabTarifaGastoGNR.",".$idTipoTarifa.", ".$ValorGastosGnr.")";

        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function UpdateCombinacionTarifa($CabTarifaGastoGNR,$idTipoTarifa,$ValorGastosGnr){
        $sql="UPDATE detalletarifagastosgnr
              SET
                ValorGastosGnr=".$ValorGastosGnr."
            WHERE 
                CabTarifaGastoGNR=".$CabTarifaGastoGNR." AND idTipoTarifa=".$idTipoTarifa;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;   
    }

    function BuscarCombinacionTarifa($CabTarifaGastoGNR,$idTipoTarifa){
        $sql="SELECT * FROM detalletarifagastosgnr
        WHERE CabTarifaGastoGNR= ".$CabTarifaGastoGNR." AND idTipoTarifa=".$idTipoTarifa;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    } 

    function ListaDetalleTarifaxCAB($CabTarifaGastoGNR){
        $sql="SELECT *
        FROM detalletarifagastosgnr
        WHERE CabTarifaGastoGNR=".$CabTarifaGastoGNR;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
     }

     
    // ************* Tarifas Veredas ************

    function BuscarVeredaenCAB($idVereda){
        $sql="SELECT * FROM cabtarifasvereda
        WHERE cabtarifasvereda.idvereda=".$idVereda;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
     }

     function BuscarTarifasConcepto_Veredas($idVereda,$idConcepto){
        $sql="SELECT detalletarifasveredas.*,
        conceptodegasto.DesConceptodeGasto,
        vereda.NombreVereda,
        cabtarifasvereda.idtarifagastovereda,
        cabtarifasvereda.titulotarifaveredas
        FROM detalletarifasveredas
        INNER JOIN cabtarifasvereda ON cabtarifasvereda.idtarifagastovereda=detalletarifasveredas.idCABtarifavereda
        INNER JOIN conceptodegasto ON conceptodegasto.idConceptodeGasto = detalletarifasveredas.idconceptogasto
        INNER JOIN vereda ON vereda.idVereda = cabtarifasvereda.idvereda
        WHERE detalletarifasveredas.idconceptogasto=".$idConcepto." AND cabtarifasvereda.idvereda=".$idVereda;
        // echo $sql."<br>";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
     }

     function ListarCABtarifasxID($IdTarifaVeredaCAB){
        $sql="SELECT cabtarifasvereda.*,
        vereda.NombreVereda,
        municipio.idMunicipio,
        municipio.NombreMunicipio,
        departamento.idDepartamento,
        departamento.NombreDepartamento
        FROM cabtarifasvereda
        INNER JOIN vereda ON vereda.idVereda=cabtarifasvereda.idvereda
        INNER JOIN municipio ON vereda.Municipio_idMunicipio=municipio.idMunicipio
        INNER JOIN departamento ON municipio.departamento_idDepartamento=departamento.idDepartamento
        WHERE cabtarifasvereda.idtarifagastovereda=".$IdTarifaVeredaCAB;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
     }

     function ListaCABTarifasVeredas(){
        $sql="SELECT cabtarifasvereda.*,
        vereda.NombreVereda
         FROM cabtarifasvereda
         INNER JOIN vereda ON cabtarifasvereda.idvereda=vereda.idVereda";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
     }


     function InsCABTarifaVereda($titulotarifaveredas,$idvereda,$usuariocreaciontarifavereda,$estadotarifavereda,$fechatarifavereda){
        $sql="INSERT INTO cabtarifasvereda
        (titulotarifaveredas, idvereda, usuariocreaciontarifavereda, estadotarifavereda, fechatarifavereda)
        VALUES ('".$titulotarifaveredas."', ".$idvereda.", ".$usuariocreaciontarifavereda.", ".$estadotarifavereda.", '".$fechatarifavereda."')";

         $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
         return $this->ConnxClass->link->insert_id;
     }

     function InsDetalleVeredaTarifa($idconceptogasto,$idCABtarifavereda,$valor){
        $sql="INSERT INTO detalletarifasveredas
            (idconceptogasto, idCABtarifavereda, valor)
            VALUES (".$idconceptogasto.", ".$idCABtarifavereda.", ".$valor.")";
            $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
            return $this->resultado;
     }

     function ListarDetalleTarifaVeredaxCAB($idCABTarifaVereda){
         $sql="SELECT detalletarifasveredas.*,
                conceptodegasto.DesConceptodeGasto
                FROM detalletarifasveredas
                INNER JOIN conceptodegasto ON conceptodegasto.idConceptodeGasto=detalletarifasveredas.idconceptogasto
                WHERE detalletarifasveredas.idCABtarifavereda=".$idCABTarifaVereda;
                // echo $sql;
            $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
            return $this->resultado;
     }

     function eliminarDetalleTarifaVereda($idtarifagastovereda){
         $sql="DELETE
                FROM detalletarifasveredas
                WHERE detalletarifasveredas.idgastosveredas=".$idtarifagastovereda;
                // echo $sql;
            $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
            return $this->resultado;
     }

     function updateCABtarifasVeredas($idCABTarifaVereda,$titulotarifaveredas,$idVereda,$usuariocreaciontarifavereda){
         $sql="UPDATE
                    cabtarifasvereda
                SET
                    titulotarifaveredas='".$titulotarifaveredas."',
                    idvereda=".$idVereda.",
                    usuariocreaciontarifavereda=".$usuariocreaciontarifavereda."
                WHERE 
                    cabtarifasvereda.idtarifagastovereda=".$idCABTarifaVereda;
       
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
     }

}


?>