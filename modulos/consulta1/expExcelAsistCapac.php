<?php 
	 session_start();
	 if($_SESSION['autentic']){
         require_once("../conn_BD.php");
         require_once("class/ClassConsulta.php");   
         require_once("../../modulos/funciones.php");
     }       
       

        $fechainicial="";
        $fechafinal="";
        $fechainicial=$_POST['fechainicial'];
        $fechafinal=$_POST['fechafinal'];
        
		 $InstanciaDB=new Conexion();
         $InstConsulta=new Proceso_Consulta($InstanciaDB);
         $ListaAsistCapac=$InstConsulta->listaAsistenciaCapacitExp($fechainicial,$fechafinal);
         if($ListaAsistCapac->num_rows > 0 ){
            header('Content-type: application/vnd.ms-excel; charset=UTF-8');
            header('Content-Disposition: attachment;filename=asistenciacapacitaciones.xls');
            header('Pragma: no-cache');
            header('Expires: 0');

         }else{
            echo "<script>
            alert('No existen datos con los criterios de busqueda, valide rango de fechas');
            window.location= '../consulta1/indexAsistCapacit.php'
            </script>";            
         }

?>


    <h4 align="center">ASISTENCIA CAPACITACIONES</h4>
    <table width="80%" border="1" align="center">
        <tr bgcolor="#5970B2" align="center" class="encabezadoTabla">
            <td width="5%" bgcolor="#3399CC">ID</td>
            <td width="15%" bgcolor="#3399CC">Fecha</td>
            <td width="15%" bgcolor="#3399CC">Codigo formato</td>
            <td width="5%" bgcolor="#3399CC">Municipio</td>            
            <td width="15%" bgcolor="#3399CC">Descripcion taller</td>
            <td width="15%" bgcolor="#3399CC">Empleado</td>
            <td width="10%" bgcolor="#3399CC">Sitio capacitacion</td>
            <td width="15%" bgcolor="#3399CC">Cantidad asistentes</td> 
            <td width="5%" bgcolor="#3399CC">Recibo material</td>
            <td width="15%" bgcolor="#3399CC">Observaciones</td>  
        </tr>
       <?php  
          while($row=$ListaAsistCapac->fetch_array()){
                ?>
                <tr>
                    <td><?php echo utf8_decode($row[0]); ?></td>
                    <td><?php echo utf8_decode($row[1]); ?></td>
                    <td><?php echo utf8_decode($row[2]); ?></td> 
                    <td><?php echo utf8_decode($row[3]); ?></td>
                    <td><?php echo utf8_decode($row[4]); ?></td>
                    <td><?php echo utf8_decode($row[5]); ?></td> 
                    <td><?php echo utf8_decode($row[6]); ?></td> 
                    <td><?php echo utf8_decode($row[7]); ?></td> 
                    <td><?php echo utf8_decode($row[8]); ?></td>
                    <td><?php echo utf8_decode($row[9]); ?></td>                                    
                </tr>
        <?php 
          } //cerrar el while
      ?>
    </table>