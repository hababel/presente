<?php 
	 session_start();
	 if($_SESSION['autentic']){
         require_once("../conn_BD.php");
         require_once("class/ClassConsulta.php");
     
		 require_once("../../modulos/funciones.php");
		 $InstanciaDB=new Conexion();
         $InstConsulta=new Proceso_Consulta($InstanciaDB);
         $EncustaInfraestructura=$InstConsulta->listaencuestainfraest();
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- <head> -->
    <?php include_once('../headScript.php'); ?>
<!-- </head> -->        
<body>
    <div id="wrapper">
        <?php 
            include_once('../headWeb.php');
            include_once("../../menu/m_principal.php");
        ?>
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="panel-body" align="right">  
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalExpExcel">
                    <b>Exportar informe</b>
                    <i class="fa fa-plus fa-2x"></i>
                    </button>
                </div>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Encuesta infraestructura
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">                                            	                               
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>ID</i></th>
                                        <th>Fecha</th>
                                        <th>Departamento</th>
                                        <th>Municipio</th>                                       
                                        <th>Vereda</th>
                                        <th>Docente </th>
                                        <th>Cantidad Estudiantes</th>                                     
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        while($rowEI=$EncustaInfraestructura->fetch_array()){
                                        $datos=$rowEI[0]."||".$rowEI[1]."||".$rowEI[2]."||".$rowEI[3]."||".$rowEI[4]."||".$rowEI[5]."||".$rowEI[6];
                                    ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $rowEI[0]; ?></td>
                                        <td><?php echo $rowEI[1]; ?></td>
                                        <td><?php echo $rowEI[2]; ?></td> 
                                        <td><?php echo $rowEI[3]; ?></td>
                                        <td><?php echo $rowEI[4]; ?></td>
                                        <td><?php echo $rowEI[5]; ?></td> 
                                        <td><?php echo $rowEI[6]; ?></td> 
                                    </tr>                                  
                                    <?php } ?>
                                </tbody>	                                   
                            </table>							
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  

   <!-- Modal de exportar a excel -->
 <div class="modal fade" id="modalExpExcel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"> -->
            <div class="modal-dialog" role="document">          
             <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title" id="exampleModalLabel">Rango de fechas</h3>
                        <div class="col-md-10">
                            <div id="msgExpExcel"></div>
                        </div>
                    </div>
                    <form action="expExcelEncInf.php" method="post">
                    <div class="modal-body">                        
                               <label for="">Fecha inicial</label>
                                <div class="form-group">
                                    <div class='input-group date datetime' id='datetimepicker1'>
                                        <input type='text' class="form-control" id="fechainicial" name="fechainicial" value="<?php echo $fechadatetimepicker;?>" autocomplete="off">
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <label for="">Fecha final </label>
                                <div class="form-group">
                                    <div class='input-group date datetime' id='datetimepicker1'>
                                        <input type='text' class="form-control" id="fechafinal" name="fechafinal" value="<?php echo $fechadatetimepicker;?>" autocomplete="off">
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <?php
                                    $TipoCalendario='Fecha';
                                    include('../../Insertfecha.php');
                                        ?>
                                <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>                                       
                                        <button type="submit" class="btn btn-primary">Exportar</button>                                       
                                </div>    
                 </div>
                 </form>                 
                </div> 
            </div>  
        </div> 


    <script src="../../assets/js/jquery-1.10.2.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script src="../../assets/js/jquery.metisMenu.js"></script>
    <script src="../../assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="../../assets/js/dataTables/dataTables.bootstrap.js"></script>
	<script src="../../assets/js/jasny-bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });            
    </script>
         <!-- CUSTOM SCRIPTS -->
    <script src="../../assets/js/custom.js"></script>
</body>
</html>
<?php 
    }else{
         header('Location:../../php_cerrar.php');       
     }
     ?>