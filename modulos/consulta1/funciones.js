function login() {
    var email = $('#Email').val();
    var pass = $('#Clave').val();
    var params = {email,pass};
    var url = "logica/logica.php?accion=login";
    $.ajax({
      url: url,
      type: 'POST',
      cache: false,
      dataType: 'json',
      data: params,
    }).done(function(result) {
      if(result == 1){
        $("#msg").html("<div class='alert alert-dismissible alert-success'><strong>Redireccionando !!</strong></div>");
        location.href="modulos/principal/principal.php";
      } else {
        $("#msg").html("<div class='alert alert-dismissible alert-danger'><strong>ERROR:</strong> Las credenciales son incorrectas.</div>");
        location.reload(); 
      }
    });
    $("#msg").delay(3000).fadeOut(300);
  }

  