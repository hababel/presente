<?php 
	 session_start();
	 if($_SESSION['autentic']){
         require_once("../conn_BD.php");
         require_once("class/ClassConsulta.php");   
         require_once("../../modulos/funciones.php");
     }       
        

        $fechainicial="";
        $fechafinal="";
        $fechainicial=$_POST['fechainicial'];
        $fechafinal=$_POST['fechafinal'];

		 $InstanciaDB=new Conexion();
         $InstConsulta=new Proceso_Consulta($InstanciaDB);
         $ListaSolicitud=$InstConsulta->listaencuestainfraestExp($fechainicial,$fechafinal);

         if($ListaSolicitud->num_rows > 0 ){
            header('Content-type: application/vnd.ms-excel; charset=UTF-8');
            header('Content-Disposition: attachment;filename=EncInfraestructura.xls');
            header('Pragma: no-cache');
            header('Expires: 0');

         }else{
            echo "<script>
            alert('No existen datos con los criterios de busqueda, valide rango de fechas');
            window.location= '../consulta1/indexEncInfraest.php'
            </script>";            
         }
?>

    <h4 align="center">ENCUESTA DE INFRAESTRUCTURA</h4>
    <table width="80%" border="1" align="center">
        <tr bgcolor="#5970B2" align="center" class="encabezadoTabla">
            <td width="5%" bgcolor="#3399CC">ID Encuesta</td>
            <td width="15%" bgcolor="#3399CC">Fecha</td>
            <td width="15%" bgcolor="#3399CC">Departamento</td>
            <td width="5%" bgcolor="#3399CC">Municipio</td>            
            <td width="15%" bgcolor="#3399CC">Vereda</td>
            <td width="15%" bgcolor="#3399CC">C E R</td>
            <td width="10%" bgcolor="#3399CC">C E R  Legalizado</td>
            <td width="15%" bgcolor="#3399CC">Docente</td> 
            <td width="15%" bgcolor="#3399CC">Cantidad estudiantes</td> 
            <td width="5%" bgcolor="#3399CC">Modo acceso</td>
            <td width="15%" bgcolor="#3399CC">Existe huerta escolar</td>
            <td width="15%" bgcolor="#3399CC">Usos huerta escolar</td>
            <td width="10%" bgcolor="#3399CC">Existe agua potable</td>
            <td width="15%" bgcolor="#3399CC">Agua es la misma de baños</td> 
            <td width="15%" bgcolor="#3399CC">Uso del agua</td>            
            <td width="10%" bgcolor="#3399CC">Observaciones agua</td>
            <td width="15%" bgcolor="#3399CC">Fuente de abastecimiento</td>     
            <td width="15%" bgcolor="#3399CC">Material techo</td> 
            <td width="15%" bgcolor="#3399CC">Estado del techo</td> 
            <td width="5%" bgcolor="#3399CC">Material paredes</td>
            <td width="15%" bgcolor="#3399CC">Estado paredes</td>
            <td width="15%" bgcolor="#3399CC">Hay electricidad</td>
            <td width="10%" bgcolor="#3399CC">Estado electricidad</td>
            <td width="15%" bgcolor="#3399CC">Existe alcantarillado</td> 
            <td width="15%" bgcolor="#3399CC">Estado de alcantarillado</td>            
            <td width="10%" bgcolor="#3399CC">Existe pozo septico</td>
            <td width="15%" bgcolor="#3399CC">Estado de pozo septico</td>     
            <td width="15%" bgcolor="#3399CC">Elemento de dotacion</td>            
            <td width="10%" bgcolor="#3399CC">Observaciones tecnologia</td>
            <td width="15%" bgcolor="#3399CC">Otras observacciones</td>     
        </tr>
       <?php  
          while($row=$ListaSolicitud->fetch_array()){
                ?>
                <tr>
                    <td><?php echo utf8_decode($row[0]); ?></td>
                    <td><?php echo utf8_decode($row[1]); ?></td>
                    <td><?php echo utf8_decode($row[2]); ?></td> 
                    <td><?php echo utf8_decode($row[3]); ?></td>
                    <td><?php echo utf8_decode($row[4]); ?></td>
                    <td><?php echo utf8_decode($row[5]); ?></td> 
                    <td><?php echo utf8_decode($row[6]); ?></td> 
                    <td><?php echo utf8_decode($row[7]); ?></td> 
                    <td><?php echo utf8_decode($row[8]); ?></td>
                    <td><?php echo utf8_decode($row[9]); ?></td> 
                    <td><?php echo utf8_decode($row[10]); ?></td>
                    <td><?php echo utf8_decode($row[11]); ?></td>
                    <td><?php echo utf8_decode($row[12]); ?></td> 
                    <td><?php echo utf8_decode($row[13]); ?></td>
                    <td><?php echo utf8_decode($row[14]); ?></td> 
                    <td><?php echo utf8_decode($row[15]); ?></td>
                    <td><?php echo utf8_decode($row[16]); ?></td>
                    <td><?php echo utf8_decode($row[17]); ?></td>
                    <td><?php echo utf8_decode($row[18]); ?></td> 
                    <td><?php echo utf8_decode($row[19]); ?></td>
                    <td><?php echo utf8_decode($row[20]); ?></td>
                    <td><?php echo utf8_decode($row[21]); ?></td> 
                    <td><?php echo utf8_decode($row[22]); ?></td> 
                    <td><?php echo utf8_decode($row[23]); ?></td> 
                    <td><?php echo utf8_decode($row[24]); ?></td>
                    <td><?php echo utf8_decode($row[25]); ?></td> 
                    <td><?php echo utf8_decode($row[26]); ?></td>
                    <td><?php echo utf8_decode($row[27]); ?></td>
                    <td><?php echo utf8_decode($row[28]); ?></td> 
                    <td><?php echo utf8_decode($row[29]); ?></td>                   
                </tr>
        <?php 
          } //cerrar el while
      ?>
    </table>