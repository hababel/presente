<?php 
	session_start();
    if($_SESSION['autentic']){
        require_once("../conn_BD.php");
        require_once("class/ClassConsulta.php");   
        require_once("../../modulos/funciones.php");
    }          

       $fechainicial="";
       $fechafinal="";
       $fechainicial=$_POST['fechainicial'];
       $fechafinal=$_POST['fechafinal'];

        $InstanciaDB=new Conexion();
        $InstConsulta=new Proceso_Consulta($InstanciaDB);
        $ListaSolicitud=$InstConsulta->listaSolicitudesExp($fechainicial,$fechafinal);
        if($ListaSolicitud->num_rows > 0 ){
            header('Content-type: application/vnd.ms-excel; charset=UTF-8');
            header('Content-Disposition: attachment;filename=Solicitudes.xls');
            header('Pragma: no-cache');
            header('Expires: 0');

         }else{
            echo "<script>
            alert('No existen datos con los criterios de busqueda, valide rango de fechas');
            window.location= '../consulta1/indexSolicGastos.php'
            </script>";            
         }
?>

<h4 align="center">LISTA DE SOLICITUDES</h4>
    <table width="80%" border="1" align="center">
        <tr bgcolor="#5970B2" align="center" class="encabezadoTabla">
            <td width="5%" bgcolor="#3399CC">ID Solicitud</td>
            <td width="15%" bgcolor="#3399CC">Fecha</td>
            <td width="15%" bgcolor="#3399CC">Departamento</td>
            <td width="5%" bgcolor="#3399CC">Municipio</td>            
            <td width="10%" bgcolor="#3399CC">Descripcion Proyecto</td>
            <td width="15%" bgcolor="#3399CC">Descripcion Actividad</td> 
            <td width="15%" bgcolor="#3399CC">Entidad</td> 
            <td width="5%" bgcolor="#3399CC">Descripcion Proceso</td>
            <td width="15%" bgcolor="#3399CC">Responsables</td>
            <td width="15%" bgcolor="#3399CC">Desc Concepto de Gasto</td>
            <td width="15%" bgcolor="#3399CC">Plan de cuentas</td>
            <td width="15%" bgcolor="#3399CC">Desc plan de cuentas</td>
            <td width="10%" bgcolor="#3399CC">Cantidad Concepto</td>
            <td width="15%" bgcolor="#3399CC">Valor  unitario</td> 
            <td width="15%" bgcolor="#3399CC">Valor Total</td>   
            <td width="15%" bgcolor="#3399CC">Usuario</td>           
            <td width="10%" bgcolor="#3399CC">Estado Solicitud</td>
            <td width="15%" bgcolor="#3399CC">Observaciones</td>              
        </tr>
       <?php  
          while($row=$ListaSolicitud->fetch_array()){
                ?>
                <tr>
                    <td><?php echo utf8_decode($row[0]); ?></td>
                    <td><?php echo utf8_decode($row[1]); ?></td>
                    <td><?php echo utf8_decode($row[2]); ?></td> 
                    <td><?php echo utf8_decode($row[3]); ?></td>
                    <td><?php echo utf8_decode($row[4]); ?></td>
                    <td><?php echo utf8_decode($row[5]); ?></td> 
                    <td><?php echo utf8_decode($row[6]); ?></td> 
                    <td><?php echo utf8_decode($row[7]); ?></td> 
                    <td><?php echo utf8_decode($row[8]); ?></td>
                    <td><?php echo utf8_decode($row[9]); ?></td> 
                    <td><?php echo utf8_decode($row[10]); ?></td>
                    <td><?php echo utf8_decode($row[11]); ?></td>
                    <td><?php echo utf8_decode($row[12]); ?></td> 
                    <td><?php echo utf8_decode($row[13]); ?></td>
                    <td><?php echo utf8_decode($row[14]); ?></td> 
                    <td><?php echo utf8_decode($row[15]); ?></td>
                    <td><?php echo utf8_decode($row[16]); ?></td>
                    <td><?php echo utf8_decode($row[17]); ?></td>
                </tr>
        <?php 
          } //cerrar el while
      ?>
    </table>
