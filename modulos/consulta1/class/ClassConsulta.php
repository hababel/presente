<?php 

Class Proceso_Consulta{

    function __construct($InstanciaDB){
        $this->ConnxClass=$InstanciaDB;
     }

     function listaSolicitudes(){
        $sql="SELECT
        solicitudgastos.idConsecutivoSolicitudGastos AS 'id solicitud',
        solicitudgastos.FechaSolicitud,
        departamento.NombreDepartamento AS 'Departamento',
        municipio.NombreMunicipio AS 'Municipio',              
        empleado.NombreEmpleado,
        actividad.DescripcionActividad,
        solicitudgastos.ValorTotalSolicitudGastos AS 'Valor total'     
        FROM solicitudgastos 
        INNER JOIN municipio ON municipio.idMunicipio=solicitudgastos.idMunicipioSolicitudGastos     
        INNER JOIN departamento on departamento.idDepartamento=solicitudgastos.IdDepartamentoSolicitudGastos   
        INNER JOIN empleado ON empleado.idEmpleado=solicitudgastos.IdUsuarioCreacion
        INNER JOIN actividad ON actividad.idActividad=solicitudgastos.idActividadSolicitudGastos
		  ORDER BY solicitudgastos.idConsecutivoSolicitudGastos";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }
 
     function listaSolicitudesExp($fechainicial,$fechafinal){
        $sql="SELECT 
        solicitudgastos.idConsecutivoSolicitudGastos AS 'id solicitud',
        solicitudgastos.FechaSolicitud,
        departamento.NombreDepartamento AS 'Departamento',
        municipio.NombreMunicipio AS 'Municipio',       
        proyecto.DescProyecto AS 'Proyecto',
        actividad.DescripcionActividad AS 'Actividad',
        entidadvinculada.NombreEntidadVinculada AS	'Entidad',
        proceso.DescProceso AS 'Proceso',
        empleado.NombreEmpleado AS 'Responsables',
        conceptodegasto.DesConceptodeGasto AS 'Concepto',
        plandecuentas.CodPlandeCuentas,
        plandecuentas.DescPlandeCuentas,
        detallesolicitudgasto.NumdiasTrayectoDetalleSolicitud AS 'Unidades',
        detallesolicitudgasto.ValorUnitarioDetalleSolicitud AS 'Valor Unitario',
        solicitudgastos.ValorTotalSolicitudGastos AS 'Valor total',  
        empleado.NombreEmpleado,       
        CASE solicitudgastos.EstadoSolicitudGastos 
        when 0 then 'Grabada pendiente por aprobar'
        when 1 then 'Aprobada pendiente por Anticipo'
        when 2 then 'Con Anticipo Pendiente por Legaliza'
        when 3 then 'Legalizada Pendiente por cumplida'
        when 4 then 'Cumplida'
        END AS	'Estado Solicitud',
        solicitudgastos.ObservacionesSolicitudGastos
        FROM solicitudgastos 
        INNER JOIN municipio ON municipio.idMunicipio=solicitudgastos.idMunicipioSolicitudGastos
        INNER JOIN detallesolicitudgastos_entidades ON detallesolicitudgastos_entidades.IdSolicitudGasto=solicitudgastos.idConsecutivoSolicitudGastos
        INNER JOIN entidadvinculada ON	entidadvinculada.idEntidadVinculada=detallesolicitudgastos_entidades.IdEntidad
        INNER JOIN departamento on departamento.idDepartamento=solicitudgastos.IdDepartamentoSolicitudGastos   
        INNER JOIN solicitudgastos_has_empleado ON	solicitudgastos_has_empleado.solicitudgastos_idConsecutivoSolicitudGastos=solicitudgastos.idConsecutivoSolicitudGastos
        INNER JOIN empleado ON empleado.idEmpleado=solicitudgastos_has_empleado.Empleado_DocumentoEmpleado
        INNER JOIN proyecto ON proyecto.idProyecto=solicitudgastos.idProyectoSolicitudGastos
        INNER JOIN actividad ON actividad.idActividad=solicitudgastos.idActividadSolicitudGastos
        INNER JOIN proceso ON proceso.idProceso=solicitudgastos.idProcesoSolicitudGastos
        INNER JOIN detallesolicitudgasto ON detallesolicitudgasto.idConsecutivoSolicitudGasto=solicitudgastos.idConsecutivoSolicitudGastos
        INNER JOIN conceptodegasto ON conceptodegasto.idConceptodeGasto=detallesolicitudgasto.idConceptoGastoDetalleSolicitud       
        INNER JOIN plandecuentas ON plandecuentas.idPlandeCuentas=conceptodegasto.PlandeCuentas_idPlandeCuentas  
        WHERE solicitudgastos.FechaSolicitud BETWEEN '".$fechainicial."' AND '".$fechafinal."'
        ORDER BY solicitudgastos.idConsecutivoSolicitudGastos";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }
  
    function listaencuestainfraest(){
        $sql="SELECT encuestainfraestructura.idEncuestaInfraestructura AS 'Id Encuesta',
        encuestainfraestructura.FechaEncuestaInfraestructura AS 'Fecha encuesta',
        departamento.NombreDepartamento AS 'Departamento',
        municipio.NombreMunicipio AS 'Municipio',
        vereda.NombreVereda AS 'Vereda',
        encuestainfraestructura.Nombre_DocenteEncuestaInfra AS 'Docente',
        encuestainfraestructura.NumEstudiantesEncuestaInfraestructura AS 'Cant Estudiantes'
        FROM	encuestainfraestructura 
        INNER	JOIN municipio ON municipio.idMunicipio=encuestainfraestructura.idMunicipio
        INNER	JOIN vereda ON vereda.idVereda=encuestainfraestructura.idVereda
        INNER JOIN departamento ON departamento.idDepartamento=encuestainfraestructura.idDepartamento
        GROUP BY encuestainfraestructura.idEncuestaInfraestructura
        ORDER BY encuestainfraestructura.idEncuestaInfraestructura";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function listaencuestainfraestExp($fechainicial,$fechafinal){
        $sql="SELECT encuestainfraestructura.idEncuestaInfraestructura AS 'Id Encuesta',
        encuestainfraestructura.FechaEncuestaInfraestructura AS 'Fecha encuesta',
        departamento.NombreDepartamento AS 'Departamento',
        municipio.NombreMunicipio AS 'Municipio',
        vereda.NombreVereda AS 'Vereda',
        encuestainfraestructura.IER_CER AS 'CER',
        CASE encuestainfraestructura.CERLegalizadoEncuestaInfraestructura
        when 0 then 'NO'
        when 1 then 'SI'
        END AS 'CER Legalizado',
        encuestainfraestructura.Nombre_DocenteEncuestaInfra AS 'Docente',
        encuestainfraestructura.NumEstudiantesEncuestaInfraestructura AS 'Cant Estudiantes',
        encuestainfraestructura.ModoAccesoEncuestaInfraestructura AS 'Modo Acceso',
        CASE encuestainfraestructura.HuertaEscolar
        when 0 then 'NO'
        when 1 then 'SI'
        END AS 'Existe huerta escolar',
        encuestainfraestructura.UsosHuertaEscolar,
        CASE encuestainfraestructura.AguaPotable
        WHEN 0 THEN 'NO'
        WHEN 1 THEN 'SI'
        END AS 'Existe agua potable?',
        CASE encuestainfraestructura.FuenteAguaBanos 
        WHEN 0 THEN 'NO'
        WHEN 1 THEN 'SI'
        END AS 'Agua es la misma de baños?',
        encuestainfraestructura.UsoAbastecimientodeAgua AS	'Uso del agua', 
        encuestainfraestructura.ObservacionesAcueductoybanos,
        CASE 
        WHEN detalleencuestrainfra.IdPerteneceaCampoEI=1 
        THEN fuenteabastecimmiento.DescFuenteAbastecimmiento
        ELSE ''
        END AS 'Fuente de abastecimiento',
        tipomaterialencuetainfraestr.DescTipoMaterialEncuetaInfraestr AS 'Material Techo',
        CASE encuestainfraestructura.EstadoMaterialTecho
        WHEN 0 THEN 'NO TIENE'
        WHEN 1 THEN 'MALO'
        WHEN 2 THEN 'REGULAR'
        WHEN 3 THEN 'BUENO'
        END AS 'Estado del techo',
        par.DescTipoMaterialEncuetaInfraestr AS 'Material Paredes',
        CASE encuestainfraestructura.EstadoMaterialParedes
        WHEN 0 THEN 'NO TIENE'
        WHEN 1 THEN 'MALO'
        WHEN 2 THEN 'REGULAR'
        WHEN 3 THEN 'BUENO'
        END AS 'Estado de las paredes',
        CASE encuestainfraestructura.electricidadEncInf 
        WHEN 0 THEN 'NO'
        WHEN 1 THEN 'SI'
        END AS 'Hay electricidad',
        CASE encuestainfraestructura.estadoElectricidadEncInf
        WHEN 0 THEN 'NO TIENE'
        WHEN 1 THEN 'MALO'
        WHEN 2 THEN 'REGULAR'
        WHEN 3 THEN 'BUENO'
        END AS 'Estado de la electricidad',
        CASE encuestainfraestructura.AlcantarilladoEncuestaInfraestructura 
        WHEN 0 THEN 'NO'
        WHEN 1 THEN 'SI'
        END AS 'Existe alcantarillado',
        CASE encuestainfraestructura.EstadoAlcantarilladoEncuestaInfraestructura
        WHEN 0 THEN 'NO TIENE'
        WHEN 1 THEN 'MALO'
        WHEN 2 THEN 'REGULAR'
        WHEN 3 THEN 'BUENO'
        END AS 'Estado del alcantarillado',
        CASE encuestainfraestructura.PozoSeptico 
        WHEN 0 THEN 'NO'
        WHEN 1 THEN 'SI'
        END AS 'Existe Pozo septico',
        CASE encuestainfraestructura.EstadoPozoSeptico
        WHEN 0 THEN 'NO TIENE'
        WHEN 1 THEN 'MALO'
        WHEN 2 THEN 'REGULAR'
        WHEN 3 THEN 'BUENO'
        END AS 'Estado del pozo septico',
        CASE 
        WHEN detalleencuestrainfra.IdPerteneceaCampoEI=0 
        THEN elementosdotacion.DescElementosDotacion
        ELSE ''
        END AS	'Elemento dotacion',
        encuestainfraestructura.ObservacionesTecnologia,
        encuestainfraestructura.ObservacionesOtros
        FROM	encuestainfraestructura 
        INNER	JOIN municipio ON municipio.idMunicipio=encuestainfraestructura.idMunicipio
        INNER	JOIN vereda ON vereda.idVereda=encuestainfraestructura.idVereda
        INNER JOIN departamento ON departamento.idDepartamento=encuestainfraestructura.idDepartamento
        INNER	JOIN tipomaterialencuetainfraestr ON tipomaterialencuetainfraestr.idTipoMaterialEncuetaInfraestr=encuestainfraestructura.TipoMaterialTecho
        INNER	JOIN tipomaterialencuetainfraestr AS par ON par.idTipoMaterialEncuetaInfraestr=encuestainfraestructura.TipoMaterialParedes
        LEFT JOIN detalleencuestrainfra ON	detalleencuestrainfra.IdEncuestaInfra=encuestainfraestructura.idEncuestaInfraestructura
        LEFT JOIN fuenteabastecimmiento ON	fuenteabastecimmiento.idFuenteAbastecimmiento=detalleencuestrainfra.IdDatoDetalleEncuestaInfra
        LEFT JOIN elementosdotacion ON elementosdotacion.idElementosDotacion=detalleencuestrainfra.IdDatoDetalleEncuestaInfra
        WHERE encuestainfraestructura.FechaEncuestaInfraestructura BETWEEN '".$fechainicial."' AND '".$fechafinal."'
        ORDER BY encuestainfraestructura.idEncuestaInfraestructura";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }
   
    function listainfInstalacion(){
        $sql="SELECT	
        informeinstalacion.idInfInstalac,
        informeinstalacion.fecha,
        departamento.NombreDepartamento AS 'Departamento',
        municipio.NombreMunicipio AS 'Municipio',
        entidadvinculada.NombreEntidadVinculada AS 'Entidad vinculada',
        vereda.NombreVereda AS 'Vereda',    
        institucion.NombreInstitucion AS 'Institucion',
        informeinstalacion.nombre_docente AS 'Docente'
        FROM informeinstalacion
        INNER JOIN departamento ON departamento.idDepartamento=informeinstalacion.departamento
        INNER JOIN municipio ON municipio.idMunicipio=informeinstalacion.municipio
        LEFT JOIN entidadvinculada ON entidadvinculada.idEntidadVinculada=informeinstalacion.patrocinador
        INNER JOIN vereda ON vereda.idVereda=informeinstalacion.vereda      
        INNER JOIN institucion ON institucion.idInstitucion=informeinstalacion.instEducativas
        GROUP BY informeinstalacion.idInfInstalac
        ORDER BY informeinstalacion.idInfInstalac";        
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function listainfInstalacionExp($fechainicial,$fechafinal){
        $sql="SELECT 
        informeinstalacion.idInfInstalac,
        informeinstalacion.fecha,
        departamento.NombreDepartamento AS 'Departamento',
        municipio.NombreMunicipio AS 'Municipio',
        entidadvinculada.NombreEntidadVinculada AS 'Entidad vinculada',
        vereda.NombreVereda AS 'Vereda',
        tipoinstitucion.DescTipoInstitucion AS 'Tipo Institucion',
        institucion.NombreInstitucion AS 'Institucion',
        informeinstalacion.NrofamiliasProgramadas,
        informeinstalacion.NrofamiliasEntregadas,
        informeinstalacion.nombre_docente AS 'Docente',
        informeinstalacion.telefono_docente,
        informeinstalacion.correo_docente,
        informeinstalacion.observaciones,
        tipopoblacion.descTipoPoblacion AS 'Tipo de poblacion',
        CASE 
        WHEN detalleinformeinstalacion.FuenteInfoInformeInst=0
        THEN empleado.NombreEmpleado
        ELSE ''
        END AS 'Empleado',
        CASE 
        WHEN detalleinformeinstalacion.FuenteInfoInformeInst=1
        THEN archivodematerial.DescArchivodeMaterial
        ELSE ''
        END AS 'Material',  
        detalleinformeinstalacion.CantEntregadaInformeInst AS 'Cantidad Material entregada'
        FROM informeinstalacion
        INNER	JOIN departamento ON departamento.idDepartamento=informeinstalacion.departamento
        INNER JOIN municipio ON municipio.idMunicipio=informeinstalacion.municipio
        LEFT JOIN entidadvinculada ON entidadvinculada.idEntidadVinculada=informeinstalacion.patrocinador
        INNER	JOIN vereda ON	vereda.idVereda=informeinstalacion.vereda
        INNER	JOIN tipoinstitucion ON	tipoinstitucion.idTipoInstitucion=informeinstalacion.centroEducativoRural
        INNER	JOIN institucion ON institucion.idInstitucion=informeinstalacion.instEducativas
        LEFT JOIN detalleinformeinstalacion ON	detalleinformeinstalacion.idCabInformeInst=informeinstalacion.idInfInstalac
        INNER JOIN	archivodematerial ON	archivodematerial.idArchivodeMaterial=detalleinformeinstalacion.idMaterial_EmpleadoInformeInst
        INNER JOIN tipopoblacion ON tipopoblacion.idtipopoblacion=detalleinformeinstalacion.idTipoPoblacionInformeInst
        INNER JOIN	empleado ON empleado.idEmpleado=detalleinformeinstalacion.idMaterial_EmpleadoInformeInst
        WHERE informeinstalacion.fecha BETWEEN '".$fechainicial."' AND '".$fechafinal."'
        ORDER	BY informeinstalacion.idInfInstalac";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function listaAsistenciaCapacit(){
        $sql="SELECT	
        asistenciacapacitacionesinstalacion.idAsistenciaCapacitaciones AS	'ID',
        asistenciacapacitacionesinstalacion.FechaAsistenciaCapacitaciones AS 'Fecha',
        asistenciacapacitacionesinstalacion.CodFormAsistenciaCapacitaciones AS	'Codigo formato',
        municipio.NombreMunicipio AS	'Municipio',
        tipotaller.DescTipoTaller AS 'Descripcion taller',
        empleado.NombreEmpleado AS 'Empleado',
        asistenciacapacitacionesinstalacion.LugarAsistenciaCapacitaciones AS 'Sitio capacitación',
        asistenciacapacitacionesinstalacion.CantAsistentes,
        asistenciacapacitacionesinstalacion.idReciboMaterial AS 'Recibo material',
        asistenciacapacitacionesinstalacion.ObservacionesAsistenciaCapacitaciones AS 'Observaciones'
        FROM asistenciacapacitacionesinstalacion
        INNER JOIN municipio ON municipio.idMunicipio=asistenciacapacitacionesinstalacion.idMunicipio    
        INNER JOIN tipotaller ON tipotaller.idTipoTaller=asistenciacapacitacionesinstalacion.idTipoTaller
        INNER JOIN empleado ON empleado.idEmpleado=asistenciacapacitacionesinstalacion.idResponsable";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function listaAsistenciaCapacitExp($fechainicial,$fechafinal){
        $sql="SELECT	
        asistenciacapacitacionesinstalacion.idAsistenciaCapacitaciones AS	'ID',
        asistenciacapacitacionesinstalacion.FechaAsistenciaCapacitaciones AS 'Fecha',
        asistenciacapacitacionesinstalacion.CodFormAsistenciaCapacitaciones AS	'Codigo formato',
        municipio.NombreMunicipio AS	'Municipio',
        tipotaller.DescTipoTaller AS 'Descripcion taller',
        empleado.NombreEmpleado AS 'Empleado',
        asistenciacapacitacionesinstalacion.LugarAsistenciaCapacitaciones AS 'Sitio capacitación',
        asistenciacapacitacionesinstalacion.CantAsistentes,
        asistenciacapacitacionesinstalacion.idReciboMaterial AS 'Recibo material',
        asistenciacapacitacionesinstalacion.ObservacionesAsistenciaCapacitaciones AS 'Observaciones'
        FROM asistenciacapacitacionesinstalacion
        INNER JOIN municipio ON municipio.idMunicipio=asistenciacapacitacionesinstalacion.idMunicipio    
        INNER JOIN tipotaller ON tipotaller.idTipoTaller=asistenciacapacitacionesinstalacion.idTipoTaller
        INNER JOIN empleado ON empleado.idEmpleado=asistenciacapacitacionesinstalacion.idResponsable
        WHERE asistenciacapacitacionesinstalacion.FechaAsistenciaCapacitaciones BETWEEN '".$fechainicial."' AND '".$fechafinal."'
        ORDER BY asistenciacapacitacionesinstalacion.idAsistenciaCapacitaciones";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function listaRequisiciones(){
        $sql="SELECT 
        requisicionmaterial.idRequisicionMaterial AS 'I D',
        requisicionmaterial.FechaRequisicionMaterial AS 'Fecha',
        proyecto.DescProyecto AS 'Proyecto',
        programa.DescPrograma AS 'Programa',
        CASE requisicionmaterial.EstadoRequisicionMaterial
                    when 0 then 'Abierta'
                    when 1 then 'Cerrada'
                    END AS	'Estado',
        empleado.NombreEmpleado,
        requisicionmaterial.TotalCantRequisicionMaterial AS 'Cantidad'
        FROM requisicionmaterial
        INNER JOIN proyecto ON proyecto.idProyecto=requisicionmaterial.idProyectoRequisicionMaterial
        INNER JOIN programa ON programa.idPrograma=proyecto.idPrograma
        LEFT JOIN empleado ON empleado.idEmpleado=requisicionmaterial.idSolicitanteRequisicionMaterial
        ORDER BY requisicionmaterial.idRequisicionMaterial";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function listaRequisicionesExp($fechainicial,$fechafinal){
        $sql="SELECT 
        requisicionmaterial.idRequisicionMaterial AS 'I D',
        requisicionmaterial.FechaRequisicionMaterial AS 'Fecha',
        proyecto.DescProyecto AS 'Proyecto',
             CASE 
             WHEN detallerequisicionmaterial.ValidacionOrigenCAB=0
             THEN archivodematerial.DescArchivodeMaterial     
             ELSE 'No posee'    
             END AS 'Material requisicion',	     
             CASE 
             WHEN detallerequisicionmaterial.ValidacionOrigenCAB=0
             then  detallerequisicionmaterial.CantRequeridaMaterial    
             ELSE 'No posee'    
             END AS 'Cantidad',
        tipodematerial.DecTipodeMaterial AS	'Tipo material',
        centrodecostoshijo.CodCentrodeCostosHijo AS 'Cod. C. Hijo',
        centrodecostoshijo.DescCentrodeCostosHijo AS 'Centro Costo Hijo', 
        centrodecostospadre.CodCentrodeCostoPadre AS 'Cod C. Padre',
        centrodecostospadre.DescCentrodeCostosPadre AS 'Centro Costo Padre',
        programa.CodPrograma AS	'Cod Programa',
        programa.DescPrograma AS 'Programa',
             CASE requisicionmaterial.EstadoRequisicionMaterial
             when 0 then 'Abierta'
             when 1 then 'Cerrada'
             END AS	'Estado'
        FROM	requisicionmaterial
        LEFT JOIN detallerequisicionmaterial ON detallerequisicionmaterial.idRequisicionMaterial=requisicionmaterial.idRequisicionMaterial
        INNER JOIN proyecto ON proyecto.idProyecto=requisicionmaterial.idProyectoRequisicionMaterial
        INNER JOIN centrodecostoshijo ON centrodecostoshijo.idCentrodeCostosHijo=proyecto.idCentrodeCostosHijo
        LEFT JOIN centrodecostospadre ON centrodecostospadre.idCentrodeCostosPadre=centrodecostoshijo.idCentrodeCostosPadre
        INNER JOIN programa ON programa.idPrograma=proyecto.idPrograma
        LEFT JOIN empleado ON empleado.idEmpleado=requisicionmaterial.idSolicitanteRequisicionMaterial
        LEFT JOIN archivodematerial ON archivodematerial.idArchivodeMaterial=detallerequisicionmaterial.idMaterial
        LEFT JOIN tipodematerial ON tipodematerial.idTipodeMaterial=archivodematerial.idTipodeMaterial
        WHERE detallerequisicionmaterial.ValidacionOrigenCAB=0 AND 
        requisicionmaterial.FechaRequisicionMaterial BETWEEN '".$fechainicial."' AND '".$fechafinal."'
        ORDER BY requisicionmaterial.idRequisicionMaterial";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function listaRecibo(){
        $sql="SELECT 
        recibomaterial.idReciboMaterial AS 'I D',
        recibomaterial.FechaReciboMaterial AS 'Fecha',
        recibomaterial.idRequisicionMaterial,
        proyecto.DescProyecto AS 'Proyecto',
        programa.DescPrograma AS 'Programa',  
        empleado.NombreEmpleado,
        CASE recibomaterial.EstadoReciboMaterial
                    when 0 then 'Abierta'
                    when 1 then 'Cerrada'
                    END AS	'Estado'
        FROM recibomaterial
        INNER JOIN proyecto ON proyecto.idProyecto=recibomaterial.idProyectoReciboMaterial
        INNER JOIN programa ON programa.idPrograma=proyecto.idPrograma
        LEFT JOIN empleado ON empleado.idEmpleado=recibomaterial.IdEmpleadoRecibo
        ORDER BY recibomaterial.idReciboMaterial";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function listaReciboExp($fechainicial,$fechafinal){
        $sql="SELECT 
        recibomaterial.idReciboMaterial,
        recibomaterial.idRequisicionMaterial,
        recibomaterial.FechaReciboMaterial,
        proyecto.DescProyecto,
         CASE recibomaterial.EstadoReciboMaterial
          when 0 then 'Abierta'
          when 1 then 'Cerrada'
          END AS 'Estado',
           centrodecostoshijo.CodCentrodeCostosHijo,
           centrodecostoshijo.DescCentrodeCostosHijo,
           centrodecostospadre.CodCentrodeCostoPadre,
           centrodecostospadre.DescCentrodeCostosPadre,
           programa.DescPrograma,
           empleado.NombreEmpleado,
           archivodematerial.DescArchivodeMaterial,
           detallerequisicionmaterial.CantRequeridaMaterial,
           detallerequisicionmaterial.CantRecibidaMaterial,
           tiponovedadesmaterial.DescTipoNovedadesMaterial,
           detallerequisicionmaterial.CantTipoNovedad
        FROM recibomaterial
        INNER JOIN proyecto ON proyecto.idProyecto=recibomaterial.idProyectoReciboMaterial
        LEFT JOIN empleado ON empleado.idEmpleado=recibomaterial.IdEmpleadoRecibo
        LEFT JOIN detallerequisicionmaterial ON detallerequisicionmaterial.IdReciboMaterial=recibomaterial.idReciboMaterial
        INNER JOIN centrodecostoshijo ON centrodecostoshijo.idCentrodeCostosHijo=proyecto.idCentrodeCostosHijo
        LEFT JOIN centrodecostospadre ON centrodecostospadre.idCentrodeCostosPadre=centrodecostoshijo.idCentrodeCostosPadre
        LEFT JOIN programa ON programa.idPrograma=proyecto.idPrograma
        LEFT JOIN archivodematerial ON archivodematerial.idArchivodeMaterial=detallerequisicionmaterial.idMaterial
        LEFT JOIN tipodematerial ON tipodematerial.idTipodeMaterial=archivodematerial.idTipodeMaterial
        LEFT JOIN tiponovedadesmaterial ON tiponovedadesmaterial.idTipoNovedadesMaterial=detallerequisicionmaterial.idTipoNovedad  
        WHERE recibomaterial.FechaReciboMaterial BETWEEN '".$fechainicial."' AND '".$fechafinal."'
        ORDER BY recibomaterial.idReciboMaterial";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }



}


?>