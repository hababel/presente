<?php 

Class Proceso_registroEntrColeccion{

    function __construct($InstanciaDB){
        $this->ConnxClass=$InstanciaDB;
     }

    function ListarregEntregColecc(){
        $sql="SELECT registroEntregaColeccion.*,
        municipio.NombreMunicipio,
        vereda.NombreVereda,
        institucion.NombreInstitucion,
        tipopoblacion.descTipoPoblacion,
        tipodematerial.DecTipodeMaterial,
        contacto.Nombre,
        contacto.TelefonoContacto,
        contacto.emailContacto
        FROM registroEntregaColeccion
        INNER JOIN municipio ON registroentregacoleccion.idMunicipio =municipio.idMunicipio
        INNER JOIN vereda ON registroentregacoleccion.idVereda=vereda.idVereda
        INNER JOIN institucion ON registroentregacoleccion.idInstitucionEducativa=institucion.idInstitucion
        INNER JOIN tipopoblacion ON registroentregacoleccion.codTipoPoblacion=tipopoblacion.idtipopoblacion
        INNER JOIN tipodematerial ON registroentregacoleccion.codTipoMaterial=tipodematerial.idTipodeMaterial
        inner join contacto on contacto.idContacto=registroentregacoleccion.docente";
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function ListarregEntregColeccxidRegistro($idRegistrosEColeccion){
        $sql="SELECT registroEntregaColeccion.*,
        municipio.NombreMunicipio,
        vereda.NombreVereda,
        institucion.NombreInstitucion,
        tipopoblacion.descTipoPoblacion,
        tipodematerial.DecTipodeMaterial,
        contacto.Nombre,
        contacto.TelefonoContacto,
        contacto.emailContacto
        FROM registroEntregaColeccion
        INNER JOIN municipio ON registroentregacoleccion.idMunicipio =municipio.idMunicipio
        INNER JOIN vereda ON registroentregacoleccion.idVereda=vereda.idVereda
        INNER JOIN institucion ON registroentregacoleccion.idInstitucionEducativa=institucion.idInstitucion
        INNER JOIN tipopoblacion ON registroentregacoleccion.codTipoPoblacion=tipopoblacion.idtipopoblacion
        INNER JOIN tipodematerial ON registroentregacoleccion.codTipoMaterial=tipodematerial.idTipodeMaterial
        inner join contacto on contacto.idContacto=registroentregacoleccion.docente
        WHERE idRegistroEntregaColeccion=".$idRegistrosEColeccion;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function InsertarRegEntColec($codRegEntColeccion,$fecha,$idMunicipio,$idInstitucionEducativa,$idVereda,$sede,$docente,
     $internet,$internetPublico,$codTipoPoblacion,$codTipoMaterial,$cantEntregada,$imagenEntColec,$idDepartamento){
        $sql="INSERT INTO registroEntregaColeccion (codRegEntColeccion,fecha,idMunicipio,idInstitucionEducativa,idVereda,
        sede,docente,internet,internetPublico,codTipoPoblacion,codTipoMaterial,cantEntregada,imagenEntColec,idDepartamento)
         VALUES ('".$codRegEntColeccion."','".$fecha."',".$idMunicipio.",".$idInstitucionEducativa.",".$idVereda.",
         ".$sede.",'".$docente."',".$internet.",".$internetPublico.",
         ".$codTipoPoblacion.",".$codTipoMaterial.",".$cantEntregada.",'".$imagenEntColec."',".$idDepartamento.")";
        //  echo $sql;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->ConnxClass->link->insert_id;
    }

    function InsertarEmpleadoRegistroEntregaColeccion($idRegistroEntregaColeccion,$idResponsable){
        $sql="INSERT INTO registroentregacoleccion_empleados (idRegistroEColeccion,idEmpleados) values (".$idRegistroEntregaColeccion.",".$idResponsable.")";
        // echo $sql;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function ListarEmpledosxidRegistroEntregaColeccion($idRegistroEntregaColeccion){
        $sql="SELECT * FROM registroentregacoleccion_empleados where idRegistroEColeccion=".$idRegistroEntregaColeccion;
        // echo $sql;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }

    function EditarRegEntColec($idRegistroEntregaColeccion,$codRegEntColeccion,$fecha,$idMunicipio,$idInstitucionEducativa,
        $idVereda,$sede,$docente,$internet,$internetPublico,$codTipoPoblacion,$codTipoMaterial,
        $cantEntregada,$imagenEntColec){
       $sql="UPDATE registroEntregaColeccion SET codRegEntColeccion='".$codRegEntColeccion."', fecha='".$fecha."',
        idMunicipio=".$idMunicipio.",idInstitucionEducativa=".$idInstitucionEducativa.",idVereda=".$idVereda.",sede=".$sede.",
        docente='".$docente."',internet=".$internet.",internetPublico=".$internetPublico.",codTipoPoblacion=".$codTipoPoblacion.",
        codTipoMaterial=".$codTipoMaterial.",cantEntregada=".$cantEntregada.",imagenEntColec='".$imagenEntColec."'
        WHERE idRegistroEntregaColeccion=".$idRegistroEntregaColeccion;
        $this->resultado=$this->ConnxClass->link->query($sql) or trigger_error($this->con->error);
        return $this->resultado;
    }
}
?>