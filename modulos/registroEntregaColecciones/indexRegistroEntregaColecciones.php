<?php 
	session_start();
	if($_SESSION['autentic']){
		require_once("../conn_BD.php");
		require_once("../veredas/class/classVeredas.php");
		require_once("../municipios/class/ClassMunicipios.php");
		require_once("../departamentos/class/ClassDepartamentos.php");
		require_once("../institucion/class/classInstitucion.php");
		require_once("../tipo_instituciones/class/ClassTipoInstitucion.php");
		require_once("../empleados/class/classEmpleados.php");		
		require_once("class/classRegistroEntregaColeccion.php");
		require_once("../tipo_poblacion/class/ClassTipoPoblacion.php");
		require_once("../tipo_materiales/class/ClassTipoMaterial.php");
		require_once("../funciones.php");

		require_once("../contacto/class/classContacto.php");
		require_once("../filemodulos/class/classFileModulos.php");
		include_once ('../filemodulos/indexFilemodulos.php');

		$InstanciaDB=new Conexion();

		$InstVereda=new Proceso_Vereda($InstanciaDB);
		$InstMcpios=new Proceso_Municipios($InstanciaDB);
		$InstDepartamento=new Proceso_Departamento($InstanciaDB);
		$InstInstitucion=new Proceso_Institucion($InstanciaDB);
		$InstTipoInst=new Proceso_TipoInstitucion($InstanciaDB);
		$InstEmpleados=new Proceso_Empleados($InstanciaDB);		
		$InstregEntColec=new Proceso_registroEntrColeccion($InstanciaDB);
		$InstTipoPoblac=new Proceso_TipoPoblacion($InstanciaDB);
		$InstTipoMaterial=new Proceso_TipoMaterial($InstanciaDB);
		$InstContactos=New Proceso_Contacto($InstanciaDB);
		$InstFileModulos= new Proceso_FileModulos($InstanciaDB);
 
		$ListadeDocentes=$InstContactos->listaContactoxClasificacion(1);
		$ListaVeredas=$InstVereda->listaVereda();
		$ListaMcpios=$InstMcpios->ListaMunicipio();
		$ListaDepartamentos=$InstDepartamento->ListaDepartamento();
		$ListaInstitucion=$InstInstitucion->listaInstitucion();
		$ListaTipoInst=$InstTipoInst->listatipoInstitucion();
		$listaEmpleados=$InstEmpleados->ListarEmpleados();	
		$ListaTipoPoblac=$InstTipoPoblac->ListatipoPoblacion();	
		$ListaTipoMaterial=$InstTipoMaterial->listatipomaterial();
		$ListaRegEntColec=$InstregEntColec->ListarregEntregColecc();

		$rutaActual = getcwd();														
		$DirActual=basename($rutaActual);
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- <head> -->
    <?php include_once('../headScript.php'); ?>

<!-- </head> -->        
<body>
    <div id="wrapper">
		<?php 
			include_once('../headWeb.php');
			include_once("../../menu/m_principal.php");
		?>
		<div id="page-wrapper" >
			<div id="page-inner">						                
				<!-- /. ROW  -->
				<div class="panel-body" align="right">                                                                                 
					<button type="button" class="btn btn-success btn-circle" data-toggle="modal" data-target="#ModalNuevoRegEntrColec">
						<i class="fa fa-plus fa-2x"></i>
					</button>																							
				</div> 
				<div class="row">
					<div class="col-md-12">
						<!-- Advanced Tables -->
						<div class="panel panel-primary">
							<div class="panel-heading">
									Registro Entrega de Coleccion
							</div>
							<div class="panel-body">
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover" id="dataTables-example">
										<thead>
											<tr>
												<th>Id</th>
												<th>Fecha</th>
												<th>Municipio</th>
												<th>Institucion Educativa</th>
												<th>Vereda</th>
												<th>Docente</th>												
												<th><span class='glyphicon glyphicon-cog' title='Config'></span>
											</tr>
										</thead>
										<tbody>
											<?php
												while ($row=$ListaRegEntColec->fetch_array()) {
													$datosEntrega=$row[0]."||".$row[1]."||".$row[2]."||".$row[3]."||".$row[4]."||".$row[5]."||".$row[6]."||".$row[7]."||".$row[8]."||".$row[9]."||".$row[10];
												?>
											<tr class="odd gradeX">
												<td><?php echo $row[0];		?></td>
												<td><?php echo $row[2];		?></td>
												<td><?php echo $row[15];	?></td>
												<td><?php echo $row[17];	?></td>
												<td><?php echo $row[16];	?></td>			
												<td><?php echo $row[20];	?></td>       								             
												<td class="center">
													<div class="btn-group">
														<button type="button" onclick="formatEditRegEntColec('<?php echo $row[0];?>')" class="btn btn-default btn-sm" data-toggle="modal" data-target="#ModalNuevoRegEntrColec"><span class="glyphicon glyphicon-pencil"></span></button>

														<!-- Boton para subir archivos -->
														<?php 
																$ListaFilexDoc=$InstFileModulos->ListFilexOrigen_idDoc($DirActual,$row[0]);
																$CantFileDoc=$ListaFilexDoc->num_rows;
																
														?>
														
														<button type="button"
															onclick="AbrilModalUploadFile('<?php echo $DirActual; ?>','<?php echo $row[0];?>')"
															class="btn btn-default btn-sm" data-toggle="modal"
															data-target="#UploadFile" >
															<span id="ImagenFiles<?php echo $row[0];?>" <?php if($CantFileDoc>0){ echo 'style="color:DodgerBlue;"';}else{echo 'style="color:Gray;"';}?> class="glyphicon glyphicon-paperclip"></span>
														</button>



													</div>
												</td>
											</tr>
												<?php }?>   
										</tbody>									
									</table>							
								</div>                            
							</div>
						</div>
			    	</div>    
		    	</div>               
	    	</div> 
     	</div> 	

<!--  Modal Nuevo Registro Entrega Coleccion-->
	<div class="modal fade" id="ModalNuevoRegEntrColec" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">					
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>															
							<h3 align="center" class="modal-title" id="myModalLabel">Nuevo Registro Entrega Coleccion <div id="idRegEntColec" style="display:none;"></div></h3>
						</div>
						<div id="msgRegEntColec"></div>
						<div class="panel-body">	
					    	<div class="row col-sm-5">
								<div class="form-group">
									<label class="control-label"for="">Codigo Registro</label>
									<input class="form-control" name="Codigo" id="codRegEntColec" aria-describedby="" placeholder="Codigo" autocomplete="off" required>
								</div>	
								<div class="form-group">
								     <label class="control-label">Fecha</label>
										<div class='input-group date datetime' id='datetimepicker1'>
											<input type='text' class="form-control" id="fechaEnt" value="<?php echo $fechadatetimepicker;?>" autocomplete="off" disabled>
											<span class="input-group-addon">
												<span class="glyphicon glyphicon-calendar"></span>
											</span>
										</div>																
										<?php
											$TipoCalendario='Fecha';
											include('../../Insertfecha.php');
										?> 
							    </div>	


								<!-- Lista de Departamentos  -->
								<div class="form-group">
								   <label  class="control-label" for="exampleFormControlSelect1">Departamento</label>
									<select class="form-control js-example-basic-single" id="idDepartamento" name="idDepartamento" style="width:100%;">		
										<option value="0">--- Seleccione Departamento ---</option>
										<?php 
											while ($rowDpto=$ListaDepartamentos->fetch_array()) {
												echo "<option value='".$rowDpto[0]."'>".$rowDpto[2]."</option>";
											}
										?>
									</select>
								</div>

								<!-- Lista de municipios Segun Departamento -->
								<div class="form-group">
								   <label  class="control-label" for="exampleFormControlSelect1">Municipio</label>
									<select class="form-control js-example-basic-single" id="idMunicipio" name="Municipio" style="width:100%;"></select>
								</div>

								<!-- lista de Veredas segun municipio -->
								<div class="form-group">
									<label  class="control-label" for="exampleFormControlSelect1">Vereda</label>
									<select class="form-control js-example-basic-single" id="idVeredaEnt" name="Vereda" style="width:100%;"></select>
								</div> 

								<!-- Lista de Instituciones segun Vereda  -->
								<div class="form-group">
								   <label  class="control-label" for="exampleFormControlSelect1">Institucion Educativa</label>
										<select class="form-control js-example-basic-single" id="idInstitEduc" name="Institucion" style="width:100%;"></select>
								</div>  


								<div class="form-group">
									<label  class="control-label" for="exampleFormControlSelect1">Responsable</label>
									<select class="js-example-basic-multiple" id="responsable" multiple="multiple" name="Responsable" style="width:100%;">
									<?php 
										while ($rowEmp=$listaEmpleados->fetch_array()) {
											echo "<option value='".$rowEmp[0]."'>".$rowEmp[2]."</option>";
										}
									?>
									</select>
								</div>
							</div> 															                          
						<div class="row col-sm-2"></div>
						<div class="row col-sm-5">
							<div class="form-group">
									 <label  class="control-label" for="exampleFormControlSelect1">Sede</label>
									     <select class="form-control js-example-basic-single" id="sede" name="sede" style="width:100%;">
													<?php 
														while ($rowCER=$ListaTipoInst->fetch_array()) {
															echo "<option value='".$rowCER[0]."'>".$rowCER[1]."</option>";
														}
													?>
										</select>
								</div> 
							<div class="form-group">
								<div style="width:100%;">
									<label for="">Docente</label>
									<select class="form-control Select2" iname="docente" id="docente" aria-describedby="" placeholder="Docente" style="width:100%;" autocomplete="off" required>
										<option value="0">--- Seleccione Docente ---</option>
										<?php 
											while ($rowDocente=$ListadeDocentes->fetch_array()) {
												echo "<option value='".$rowDocente[0]."'>".$rowDocente[2]."</option>";
											}
										?>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label"for="">Internet en la escuela</label>
								<select class="form-control" id="internetEsc" name="Internet Escuela">
											<option value=0>No</option>
											<option value=1>Si</option>
								</select>
							</div>
							<div class="form-group">
								<label class="control-label"for="">Internet Publico</label>
								<select class="form-control" id="internetPub" name="Internet Publico">
											<option value=0>No</option>
											<option value=1>Si</option>
								</select>
							</div>
							<div class="form-group">
									 <label  class="control-label" for="exampleFormControlSelect1">Tipo Poblacion</label>
									     <select class="form-control js-example-basic-single" id="codTipoPoblacion" name="TipoPoblacion" style="width:100%;">
													<?php 
														while ($rowTipo=$ListaTipoPoblac->fetch_array()) {
															echo "<option value='".$rowTipo[0]."'>".$rowTipo[1]."</option>";
														}
													?>
										</select>
							</div> 
							<div class="form-group">
									 <label  class="control-label" for="exampleFormControlSelect1">Tipo Material</label>
									     <select class="form-control js-example-basic-single" id="codTipoMaterial" name="codTipoMaterial" style="width:100%;">
													<?php 
														while ($rowMat=$ListaTipoMaterial->fetch_array()) {
															echo "<option value='".$rowMat[0]."'>".$rowMat[1]."</option>";
														}
													?>
										</select>
							</div> 
							<div class="form-group">
									<label class="control-label" for="">Cantidad Entregada</label>
									<input class="form-control" name="Cant Entregada" id="cantEntregada" aria-describedby="" placeholder="Cantidad Entregada" autocomplete="off" required>
							</div>
							
						</div>

						<div class="row col-sm-12">
								<div class="form-group">
									<label class="control-label" for="">Observaciones</label>
									  <textarea class="form-control" name="imagen detalle" id="imagenEntColec" rows="2" placeholder="Observaciones" autocomplete="off" style="resize: none;width:100%;" required></textarea>
								</div>
							</div>		                                                         
					    </div> 
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						<button type="button" onclick="guardarregEntregaColec();" class="btn btn-primary">Guardar</button>
					</div>										 
				</div>
			</div>
	</div>
         
<!-- End Modal Nuevo Registro Entrega Coleccion-->

<!--  Modal Editar Registro Entrega Coleccion-->
	<div class="modal fade" id="ModalEditRegEntrColec" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>															
						<h3 align="center" class="modal-title" id="myModalLabel">Editar informe  instalacion</h3>
						<div id="msgEditRegEntColec"></div>
					</div>
					<div class="panel-body">
							<div class="row">  
							<div class="col-md-6">
							<div class="form-group">
									<label class="control-label"for="">Id Registro</label>
									<input class="form-control" name="Idregistro" id="idRegEntColecFM" aria-describedby="" placeholder="Codigo" autocomplete="off" disabled>
								</div>
								<div class="form-group">
									<label class="control-label"for="">Codigo Registro</label>
									<input class="form-control" name="Codigo" id="codRegEntColecFM" aria-describedby="" placeholder="Codigo" autocomplete="off" required>
								</div>	
								<div class="form-group">
								     <label class="control-label">Fecha</label>
									<div class='input-group date' id='datetimepicker1'>
										<input type='text' class="form-control" id="fechaEntFM" value="<?php echo $fechadatetimepicker;?>" autocomplete="off">
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>																
									<?php
										$TipoCalendario='Fecha';
										include('../../Insertfecha.php');
									?>
							    </div>	
								<div class="form-group">
								   <label  class="control-label" for="exampleFormControlSelect1">Municipio</label>
										<select class="form-control js-example-basic-single" id="idMunicipioFM" name="Municipio" style="width:100%;">		
													<?php 
														while ($rowMcp=$ListaMcpios->fetch_array()) {
															echo "<option value='".$rowMcp[0]."'>".$rowMcp[2]."</option>";
														}
													?>
										</select>
								</div>	
								<div class="form-group">
								   <label  class="control-label" for="exampleFormControlSelect1">Institucion Educativa</label>
										<select class="form-control js-example-basic-single" id="idInstitEducFM" name="Institucion" style="width:100%;">
													<?php 
														while ($rowInst=$ListaInstitucion->fetch_array()) {
															echo "<option value='".$rowInst[0]."'>".$rowInst[2]." -- ".$rowInst[7]."</option>";
														}
													?>
										</select>
								</div>  
								<div class="form-group">
									<label  class="control-label" for="exampleFormControlSelect1">Vereda</label>
										<select class="form-control js-example-basic-single" id="idVeredaEntFM" name="Vereda" style="width:100%;">
											<?php 
												while ($rowVda=$ListaVeredas->fetch_array()) {
													echo "<option value='".$rowVda[0]."'>".$rowVda[1]."-".$rowVda[3]."</option>";
												}
											?>
										</select>
								</div>    
								<div class="form-group">
									<label  class="control-label" for="exampleFormControlSelect1">Responsable</label>
									<select class="js-example-basic-multiple" id="responsableFM" name="Responsable" multiple="multiple" style="width:100%;">
										<?php 
											while ($rowEmp=$listaEmpleados->fetch_array()) {
												echo "<option value='".$rowEmp[0]."'>".$rowEmp[2]."</option>";
											}
										?>
									</select>
								</div>   																															
								<div class="form-group">
									 <label  class="control-label" for="exampleFormControlSelect1">Sede</label>
									     <select class="form-control js-example-basic-single" id="sedeFM" name="sede" style="width:100%;">
													<?php 
														while ($rowCER=$ListaTipoInst->fetch_array()) {
															echo "<option value='".$rowCER[0]."'>".$rowCER[1]."</option>";
														}
													?>
										</select>
								</div> 
							</div> 															                          
						<div class="row col-sm-2"></div>
						<div class="row col-sm-5">
							<div class="form-group">
								<label class="control-label"for="">Docente</label>
								<input class="form-control" name="docente" id="docenteFM" aria-describedby="" placeholder="Docente" autocomplete="off" required>
							</div>
							<div class="form-group">
								<label class="control-label"for="">Teléfono Docente</label>
								<input class="form-control" name="telDocente" id="telDocenteFM" aria-describedby="" placeholder="Tel Docente" autocomplete="off" required>
							</div>
							<div class="form-group">
								<label class="control-label"for="">Internet en la escuela</label>
								<select class="form-control" id="internetEscFM" name="Internet Escuela">
											<option value=0>No</option>
											<option value=1>Si</option>
								</select>
							</div>
							<div class="form-group">
								<label class="control-label"for="">Internet Publico</label>
								<select class="form-control" id="internetPubFM" name="Internet Publico">
											<option value=0>No</option>
											<option value=1>Si</option>
								</select>
							</div>
							<div class="form-group">
									 <label  class="control-label" for="exampleFormControlSelect1">Tipo Poblacion</label>
									     <select class="form-control js-example-basic-single" id="codTipoPoblacionFM" name="TipoPoblacion" style="width:100%;">
													<?php 
														while ($rowTipo=$ListaTipoPoblac->fetch_array()) {
															echo "<option value='".$rowTipo[0]."'>".$rowTipo[1]."</option>";
														}
													?>
										</select>
							</div> 
							<div class="form-group">
									 <label  class="control-label" for="exampleFormControlSelect1">Tipo Material</label>
									     <select class="form-control js-example-basic-single" id="codTipoMaterialFM" name="codTipoMaterial" style="width:100%;">
													<?php 
														while ($rowMat=$ListaTipoMaterial->fetch_array()) {
															echo "<option value='".$rowMat[0]."'>".$rowMat[1]."</option>";
														}
													?>
										</select>
							</div> 
							<div class="form-group">
									<label class="control-label" for="">Cantidad Entregada</label>
									<input class="form-control" name="Cant Entregada" id="cantEntregadaFM" aria-describedby="" placeholder="Cantidad Entregada" autocomplete="off" required>
							</div>
							
						</div>

						<div class="row col-sm-12">
								<div class="form-group">
									<label class="control-label" for="">Imagen detalle</label>
									  <textarea class="form-control" name="imagen detalle" id="imagenEntColecFM" rows="2" placeholder="Observaciones" autocomplete="off" style="resize: none;width:100%;" required></textarea>
								</div>
							</div>		                                                         
					    </div> 
                               
					</div> 
						<div class="modal-footer">
							<div align="right">
								<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
								<button type="submit" class="btn btn-primary" onclick="EditarEntrega();">Guardar</button>
							</div> 
						</div>										 
				</div>
			</div>
		</div>
	</div>

 <!-- End Modal Editar Registro Entrega Coleccion-->
                                        
       
  

    <script src="../../assets/js/jquery-1.10.2.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script src="../../assets/js/jquery.metisMenu.js"></script>
    <script src="../../assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="../../assets/js/dataTables/dataTables.bootstrap.js"></script>
	<script src="../../assets/js/jasny-bootstrap.min.js"></script>

	<!-- Script Select 2 -->

	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
 
         <!-- CUSTOM SCRIPTS -->
    <!-- <script src="../../assets/js/custom.js"></script> -->
	<script>
            $(document).ready(function () {
				
				$('#dataTables-example').dataTable();

				$('#idDepartamento').select2({
                	dropdownParent: $("#ModalNuevoRegEntrColec"),
				});
				
				$('#idMunicipio').select2({
                	dropdownParent: $("#ModalNuevoRegEntrColec"),
				});
				
				$('#idVeredaEnt').select2({
                	dropdownParent: $("#ModalNuevoRegEntrColec"),
				});

				$('#idInstitEduc').select2({
                	dropdownParent: $("#ModalNuevoRegEntrColec"),
				});

				$('#responsable').select2({
                	dropdownParent: $("#ModalNuevoRegEntrColec"),
				});

				$('#sede').select2({
                	dropdownParent: $("#ModalNuevoRegEntrColec"),
				});

				
				$('#idDepartamento').change(function () { 
					var CodDepartamentoSG=$('#idDepartamento').val();
					if (CodDepartamentoSG != 0) {
						var parametros={CodDepartamentoSG};
						$.ajax({
							type: "POST",
							url: "../../logica/logica.php?accion=LlenaSelectMunicipioSegunDepartamento",
							data: parametros,
							cache: false,
							dataType: "json",
							success: function (data) {
								$('#idMunicipio').html(data);
							}
						});   
                	} 
				});

				$('#idMunicipio').change(function () { 
					var CodMunicipio=$('#idMunicipio').val();
					if (CodMunicipio != 0) {
						var parametros={CodMunicipio};
						$.ajax({
							type: "POST",
							url: "../../logica/logica.php?accion=LlenaSelectVeredasegunMunicipio",
							data: parametros,
							cache: false,
							dataType: "json",
							success: function (data) {
								$('#idVeredaEnt').html(data);
							}
						});   
                	} 
				});

				$('#idVeredaEnt').change(function () { 
					var CodVereda=$('#idVeredaEnt').val();
					if (CodVereda != 0) {
						var parametros={CodVereda};
						$.ajax({
							type: "POST",
							url: "../../logica/logica.php?accion=LlenaSelectInstitucionsegunVereda",
							data: parametros,
							cache: false,
							dataType: "json",
							success: function (data) {
								$('#idInstitEduc').html(data);
							}
						});   
                	} 
				});

            });
    </script>
</body>
</html>
<?php 
	}else{
		header('Location:../../php_cerrar.php');
	}
?>