<?php 
    session_start();
    $private_id=session_id();
    if($_SESSION['autentic']){
	    require_once("../conn_BD.php");
        require_once("../funciones.php");
        $InstanciaDB=new Conexion();
	}else{
		header('Location:../../php_cerrar.php');
	}
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- <head> -->
    <?php include_once('../headScript.php'); ?>
<!-- </head> -->        
<body>
    <div id="wrapper">
		<?php 
			include_once('../headWeb.php');
			include_once("../../menu/m_principal.php");
		?>
		<div id="page-wrapper" >
			<div id="page-inner">						                
				<div class="panel-body" align="right">                                                                                 
					<button type="button" class="btn btn-success btn-circle" data-toggle="modal" data-target="#modelId">
						<i class="fa fa-plus fa-2x"></i>
					</button>																							
				</div> 
				<div class="row">
					<div class="col-md-12">
						<!-- Advanced Tables -->
						<div class="panel panel-primary">
							<div class="panel-heading">
									Titulo del Modulo
							</div>
							<div class="panel-body">
                            <div class="table-responsive">
									<table class="table table-striped table-bordered table-hover" id="dataTables-example">
										<thead>
											<tr>
												<th>Id</th>
												<th>Fecha</th>																																			
												<th><span class='glyphicon glyphicon-cog' title='Config'></span>
											</tr>
										</thead>
										<tbody>
											<tr class="odd gradeX">
												<td></td>
												<td></td>      								             
												<td class="center">
													<div class="btn-group">
														<button type="button" onclick="Editarfuncion()" class="btn btn-default btn-sm" data-toggle="modal" data-target="#modelId"><span class="glyphicon glyphicon-pencil"></span></button>
                                                        <button title="Ver" onclick="VerFuncion()" class="btn btn-default btn-sm" data-toggle="modal" data-target="#modelId"><span class="glyphicon glyphicon-info-sign" style="color:blue;"></span></button>
													</div>
												</td>
											</tr> 
										</tbody>									
									</table>							
								</div>                            
							</div>
						</div>
			    	</div>    
		    	</div>               
	    	</div> 
     	</div> 	
    

                    <div id="modelId" class="modal fade" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h3 align="center" class="modal-title" id="myModalLabel">Legalizacion de Gasto</h3>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div id="AvisoForm"></div>
                                <div class="modal-body" id="body_form">
                                    <div class="form-group">
                                        <label for="ConceptoGastoLegalizacion" class="col-sm-12 control-label" id="ResumenSG"></label>
                                    </div>
                                    <form enctype="multipart/form-data" id="ficha_InUsuarios">                                        
                                        <div class="form-group">
                                            <label for="NitBeneficiario" class="col-sm-4 control-label">Nit Beneficiario</label>
                                            <div class="col-sm-8">
                                                <input type="text" id="NitBeneficiario" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="NombreBeneficiario" class="col-sm-4 control-label">Nombre Beneficiario</label>
                                            <div class="col-sm-8">
                                                <input type="text" id="NombreBeneficiario" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="DireccionBeneficiario" class="col-sm-4 control-label">Direccion Beneficiario</label>
                                            <div class="col-sm-8">
                                                <input type="text" id="DireccionBeneficiario" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="TelefonoBeneficiario" class="col-sm-4 control-label">Telefono Beneficiario</label>
                                            <div class="col-sm-8">
                                                <input type="text" id="TelefonoBeneficiario" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="NumeroFactura" class="col-sm-4 control-label">Numero Factura</label>
                                            <div class="col-sm-8">
                                                <input type="text" id="NumeroFactura" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="ValorFactura" class="col-sm-4 control-label">Valor Factura</label>
                                            <div class="col-sm-8">
                                                <input type="text" id="ValorFactura" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group text-center" style="display: block;">
                                            <div class="col-sm-4">
                                                <div class="text-center"><b>Pago Tarjeta Crédito Corporativa</b></div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="text-center"><b>Entrega RUT</b></div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="text-center"><b>Entrega Copia Cédula</b></div>
                                            </div>
                                        </div>
                                        <div class="form-group text-center" align="center">
                                            <div class="col-sm-4">
                                                <div class="col-sm-8">
                                                    <select class="form-control" name="PagoTCD" id="PagoTCD">
                                                        <option value=0>NO</option>
                                                        <option value=1>SI</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="col-sm-8">
                                                    <select class="form-control" name="EntregaRUT" id="EntregaRUT">
                                                        <option value=0>NO</option> 
                                                        <option value=1>SI</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="col-sm-8">
                                                    <select class="form-control" name="EntregaCedula" id="EntregaCedula">
                                                        <option value=0>NO</option>
                                                        <option value=1>SI</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="Observaciones" class="col-sm-4 control-label">Observaciones</label>
                                            <div class="col-sm-8">
                                                <input type="text" id="Observaciones" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <input type="hidden" name="MAX_FILE_SIZE" value="50000" /> Enviar este fichero: <input name="Imagen_usuario" id="Imagen_usuario" type="file" />
                                            <div id="msgSubirImagen"></div>
                                        </div>
                                    </form> 
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" id="GuardarLegalizacion" name="GuardarLegalizacion" onclick="GuardarFoto();" class="btn btn-primary">
                                        <span class="fa fa-save"></span><span class="hidden-xs"> Guardar</span>
                                    </button>
                                    
                                </div>
                            </div>
                        </div>
                    </div>

    <script>
        $('#exampleModal').on('show.bs.modal', event => {
            var button = $(event.relatedTarget);
            var modal = $(this);
            // Use above variables to manipulate the DOM
            
        });
    </script>
    
    <script src="../../assets/js/jquery-1.10.2.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script src="../../assets/js/jquery.metisMenu.js"></script>
    <script src="../../assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="../../assets/js/dataTables/dataTables.bootstrap.js"></script>
	<script src="../../assets/js/jasny-bootstrap.min.js"></script>
    <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
    </script>
         <!-- CUSTOM SCRIPTS -->
    <script src="../../assets/js/custom.js"></script>
</body>
</html>s